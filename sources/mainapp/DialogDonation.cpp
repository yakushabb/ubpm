#include "DialogDonation.h"

DialogDonation::DialogDonation(QWidget *parent) : QDialog(parent)
{
	setupUi(this);

    label_amazon->setPixmap(QIcon(":/svg/qr-amazon.svg").pixmap(256, 256));
    label_paypal->setPixmap(QIcon(":/svg/qr-paypal.svg").pixmap(256, 256));
    label_sepa->setPixmap(QIcon(":/svg/qr-sepa.svg").pixmap(256, 256));

	adjustSize();

	layout()->setSizeConstraint(QLayout::SetFixedSize);
}

void DialogDonation::on_pushButton_amazon_clicked()
{
	QGuiApplication::clipboard()->setText("lazyt@mailbox.org");

	QDesktopServices::openUrl(QUrl("https://www.amazon.com/dp/B07PCMWTSG"));
}

void DialogDonation::on_pushButton_paypal_account_clicked()
{
	QDesktopServices::openUrl(QUrl("https://paypal.me/lazyt"));
}

void DialogDonation::on_pushButton_paypal_creditcard_clicked()
{
	QDesktopServices::openUrl(QUrl("https://www.paypal.com/donate/?hosted_button_id=VT55E55UYP3VA"));
}

void DialogDonation::on_pushButton_sepa_name_clicked()
{
	QGuiApplication::clipboard()->setText("Thomas Loewe");
}

void DialogDonation::on_pushButton_sepa_iban_clicked()
{
	QGuiApplication::clipboard()->setText("DE84700170008888213860");
}

void DialogDonation::on_pushButton_sepa_bic_clicked()
{
	QGuiApplication::clipboard()->setText("PAGMDEM1");
}

void DialogDonation::keyPressEvent(QKeyEvent *ke)
{
	if(ke->key() == Qt::Key_F1)
	{
		reinterpret_cast<MainWindow*>(parent())->help->showHelp("01-07");
	}

	QDialog::keyPressEvent(ke);
}
