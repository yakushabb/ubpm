APP_VERS = "1.10.0"

QT += core gui widgets network help charts sql svg xml printsupport

TARGET = ubpm

INCLUDEPATH += ../

SOURCES	+= MainWindow.cpp DialogAbout.cpp DialogAnalysis.cpp DialogDistribution.cpp DialogDonation.cpp DialogHelp.cpp DialogMigration.cpp DialogRecord.cpp DialogSettings.cpp DialogUpdate.cpp
HEADERS	+= MainWindow.h   DialogAbout.h   DialogAnalysis.h   DialogDistribution.h   DialogDonation.h   DialogHelp.h   DialogMigration.h   DialogRecord.h   DialogSettings.h   DialogUpdate.h
FORMS	+= MainWindow.ui  DialogAbout.ui  DialogAnalysis.ui  DialogDistribution.ui  DialogDonation.ui  DialogHelp.ui  DialogMigration.ui  DialogRecord.ui  DialogSettings.ui  DialogUpdate.ui

RESOURCES	 += res/ubpm.qrc res/icons.qrc
TRANSLATIONS += lng/mainapp_de.ts lng/mainapp_el.ts lng/mainapp_es.ts lng/mainapp_fa.ts lng/mainapp_fr.ts lng/mainapp_hu.ts lng/mainapp_it.ts lng/mainapp_nb_NO.ts lng/mainapp_nl.ts lng/mainapp_pl.ts lng/mainapp_pt_BR.ts lng/mainapp_ru.ts

TRANSLATIONS_BASE			= $$[QT_INSTALL_TRANSLATIONS]/qtbase_*.qm
TRANSLATIONS_CONNECTIVITY	= $$[QT_INSTALL_TRANSLATIONS]/qtconnectivity_*.qm
TRANSLATIONS_HELP			= $$[QT_INSTALL_TRANSLATIONS]/qt_help_*.qm
TRANSLATIONS_SERIALPORT		= $$[QT_INSTALL_TRANSLATIONS]/qtserialport_*.qm

VERSION = $${APP_VERS}

#equals(QT_MAJOR_VERSION, 5) | win32 {
#	system($$[QT_INSTALL_BINS]/rcc --no-compress --binary $$PWD/res/icons.qrc --output $$PWD/res/rcc/icons.rcc)
#} else {
#	system($$[QT_INSTALL_LIBEXECS]/rcc --no-compress --binary $$PWD/res/icons.qrc --output $$PWD/res/rcc/icons.rcc)
#}

### LINUX ###

unix:!macx {
	APP_DATE = "$$system(date +%d.%m.%Y)"

	BIN_PWD = $$OUT_PWD

	QMAKE_LFLAGS += -Wl,-rpath=.

	QMAKE_CLEAN += -r $$BIN_PWD/Guides $$BIN_PWD/Languages $$BIN_PWD/Themes $$BIN_PWD/Plugins

	!system(command -v linuxdeployqt-continuous-x86_64.AppImage >/dev/null) { warning("linuxdeployqt (https://github.com/probonopd/linuxdeployqt/releases) missing, cant't build appbundle!") }
	!exists($$[QT_INSTALL_PLUGINS]/sqldrivers/libqsqlcipher.so) { warning("SQLCipher (https://github.com/sjemens/qsqlcipher-qt5) missing, encryption not available!") }

### release ###

contains(DEFINES, APPBUNDLE) {
	RELEASEDIR = /tmp/ubpm.AppDir

	equals(QT_MAJOR_VERSION, 5) {
		SSL = OpenSSL
	} else {
		SSL = OpenSSLv3
	}

	appbundle.target   = appbundle
	appbundle.commands += clear && echo "Building Linux Release AppBundle for Qt $${QT_VERSION}" $$escape_expand(\n\t)
	appbundle.commands += if [ -d $$RELEASEDIR ]; then rm -r $$RELEASEDIR; fi $$escape_expand(\n\t)
	appbundle.commands += mkdir -p $$RELEASEDIR/usr/bin/Guides $$escape_expand(\n\t)
	appbundle.commands += mkdir -p $$RELEASEDIR/usr/bin/Languages $$escape_expand(\n\t)
	appbundle.commands += mkdir -p $$RELEASEDIR/usr/bin/Plugins $$escape_expand(\n\t)
	appbundle.commands += mkdir -p $$RELEASEDIR/usr/bin/Themes $$escape_expand(\n\t)
	appbundle.commands += mkdir -p $$RELEASEDIR/usr/lib $$escape_expand(\n\t)
	appbundle.commands += mkdir -p $$RELEASEDIR/usr/share/metainfo $$escape_expand(\n\t)
	appbundle.commands += cp ubpm $$RELEASEDIR/usr/bin $$escape_expand(\n\t)
	appbundle.commands += cp -P $$[QT_INSTALL_PREFIX]/../../Tools/$$SSL/binary/lib/libssl.* $$RELEASEDIR/usr/lib && cp -P $$[QT_INSTALL_PREFIX]/../../Tools/$$SSL/binary/lib/libcrypto.* $$RELEASEDIR/usr/lib $$escape_expand(\n\t)
	appbundle.commands += cp hlp/*.qch hlp/*.qhc $$RELEASEDIR/usr/bin/Guides $$escape_expand(\n\t)
	appbundle.commands += cp lng/mainapp_*.qm ../plugins/shared/plugin/lng/plugins_*.qm $$TRANSLATIONS_BASE $$TRANSLATIONS_CONNECTIVITY $$TRANSLATIONS_HELP $$TRANSLATIONS_SERIALPORT $$RELEASEDIR/usr/bin/Languages $$escape_expand(\n\t)
	appbundle.commands += cp ../plugins/*.so $$RELEASEDIR/usr/bin/Plugins $$escape_expand(\n\t)
	appbundle.commands += cp qss/*.qss $$RELEASEDIR/usr/bin/Themes $$escape_expand(\n\t)
	appbundle.commands += cp ../../release/appbundle/lin/ubpm.desktop $$RELEASEDIR $$escape_expand(\n\t)
	appbundle.commands += cp ../../release/appbundle/lin/page.codeberg.lazyt.ubpm.appdata.xml $$RELEASEDIR/usr/share/metainfo $$escape_expand(\n\t)
	appbundle.commands += cp res/ico/ubpm.png $$RELEASEDIR $$escape_expand(\n\t)
	appbundle.commands += ln -s ubpm.png $$RELEASEDIR/.DirIcon $$escape_expand(\n\t)
	appbundle.commands += export VERSION=$$VERSION && linuxdeployqt-continuous-x86_64.AppImage $$RELEASEDIR/ubpm.desktop -appimage -no-copy-copyright-files -no-translations $$escape_expand(\n\t)
	appbundle.commands += mv Universal_Blood_Pressure_Manager-*-x86_64.AppImage ubpm_qt$$QT_MAJOR_VERSION-$$VERSION-x86_64.AppImage
} else {
	appbundle.target   = appbundle
	appbundle.commands += clear && echo "APPBUNDLE must be defined to build a Linux Release AppBundle for Qt $${QT_VERSION}" $$escape_expand(\n\t)
}

	installer.target   = installer
	installer.commands += clear && echo "Building Linux Release Installer for Qt $${QT_VERSION}" $$escape_expand(\n\t)
	installer.commands += @echo ToDo...

### install ###

isEmpty(PREFIX) {
	INSTALLDIR = /opt/ubpm
} else {
	equals(PREFIX, "/app") | equals(PREFIX, "/root/parts/ubpm/install") | contains(PREFIX, "^\/(nix|gnu)\/store\/(.*)") {
		INSTALLDIR = $$PREFIX/bin
	} else {
		INSTALLDIR = $$PREFIX
	}
}
	target.path = $$INSTALLDIR

	guides.path  = $$INSTALLDIR/Guides
	guides.files = hlp/*.qch hlp/*.qhc

	languages.path  = $$INSTALLDIR/Languages
	languages.files = lng/mainapp_*.qm ../plugins/shared/plugin/lng/plugins_*.qm

	plugins.path   = $$INSTALLDIR/Plugins
	plugins.files  = ../plugins/*.so

	themes.path  = $$INSTALLDIR/Themes
	themes.files = qss/*.qss

	cmd.path  = $$INSTALLDIR
equals(PREFIX, "/app") | contains(PREFIX, "^\/(nix|gnu)\/store\/(.*)") {
	cmd.extra += install -D res/ico/ubpm.png $$INSTALLDIR/../share/icons/hicolor/256x256/apps/ubpm.png $$escape_expand(\n\t)
	cmd.extra += install -D ../../release/appbundle/lin/ubpm.desktop $$INSTALLDIR/../share/applications/ubpm.desktop $$escape_expand(\n\t)
	cmd.extra += install -D ../../release/appbundle/lin/page.codeberg.lazyt.ubpm.appdata.xml $$INSTALLDIR/../share/metainfo/page.codeberg.lazyt.ubpm.appdata.xml $$escape_expand(\n\t)
} else {
	equals(PREFIX, "/root/parts/ubpm/install") {
		cmd.extra += install -D res/ico/ubpm.png $$INSTALLDIR/../usr/share/icons/hicolor/256x256/apps/ubpm.png $$escape_expand(\n\t)
		cmd.extra += install -D ../../release/appbundle/lin/ubpm.desktop $$INSTALLDIR/../usr/share/applications/ubpm.desktop $$escape_expand(\n\t)
		cmd.extra += install -D ../../release/appbundle/lin/page.codeberg.lazyt.ubpm.appdata.xml $$INSTALLDIR/../usr/share/metainfo/page.codeberg.lazyt.ubpm.appdata.xml $$escape_expand(\n\t)
	} else {
		cmd.extra     += install -D res/ico/ubpm.png $(INSTALL_ROOT)/usr/share/icons/hicolor/256x256/apps/ubpm.png $$escape_expand(\n\t)
		cmd.extra     += install -D ../../release/appbundle/lin/ubpm.desktop $(INSTALL_ROOT)/usr/share/applications/ubpm.desktop $$escape_expand(\n\t)
		cmd.extra     += sed -i \"s|Exec=ubpm|Exec=$$INSTALLDIR/ubpm|g\" $(INSTALL_ROOT)/usr/share/applications/ubpm.desktop $$escape_expand(\n\t)
		cmd.extra     += sed -i \"s|Icon=ubpm|Icon=/usr/share/icons/hicolor/256x256/apps/ubpm.png|g\" $(INSTALL_ROOT)/usr/share/applications/ubpm.desktop $$escape_expand(\n\t)
		cmd.uninstall += rm $(INSTALL_ROOT)/usr/share/icons/hicolor/256x256/apps/ubpm.png $$escape_expand(\n\t)
		cmd.uninstall += rm $(INSTALL_ROOT)/usr/share/applications/ubpm.desktop
	}
}

	INSTALLS += target guides languages plugins themes cmd
}

### WINDOWS ###

win32 {
	APP_DATE = "$$system(date /t)"

	BIN_PWD = $$OUT_PWD

	CONFIG -= debug_and_release

	RC_ICONS = res/ico/ubpm.ico
	RC_LANG  = "0x0407"

	QMAKE_TARGET_COMPANY     = "LazyT"
	QMAKE_TARGET_DESCRIPTION = "$${APP_NAME}"
	QMAKE_TARGET_COPYRIGHT   = "Thomas L\\366we, 2020-2023"
	QMAKE_TARGET_PRODUCT     = "UBPM"

	QMAKE_CLEAN += $$BIN_PWD/Guides $$BIN_PWD/Languages $$BIN_PWD/Themes $$BIN_PWD/Plugins

	!system(7z.exe >nul 2>&1) { warning("7zip (https://www.7-zip.org/download.html) missing, cant't build appbundle!") }
	!exists($$[QT_INSTALL_PLUGINS]/sqldrivers/qsqlcipher.dll) { warning("SQLCipher (https://github.com/sjemens/qsqlcipher-qt5) missing, encryption not available!") }

### release ###

contains(DEFINES, APPBUNDLE) {
	RELEASEDIR = $$(TMP)\ubpm.7zip
	RELEASEZIP = $$(TMP)\ubpm.7z

	MAKESFX = "copy /b $$shell_path(..\..\release\appbundle\win\7zs2.sfx) + $$shell_path($$RELEASEZIP)"

	equals(QT_MAJOR_VERSION, 5) {
		SSL = OpenSSL
		DLL = $$(SYSTEMROOT)\System32\msvcr100.dll
		RUN =
	} else {
		SSL = OpenSSLv3
		DLL = $$(SYSTEMROOT)\System32\msvcp140.dll $$(SYSTEMROOT)\System32\msvcp140_1.dll $$(SYSTEMROOT)\System32\msvcp140_2.dll $$(SYSTEMROOT)\System32\vcruntime140.dll $$(SYSTEMROOT)\System32\vcruntime140_1.dll
		RUN = --no-compiler-runtime 
	}

	appdir.target   = appdir
	appdir.commands += cls && echo "Building Windows Release AppBundle for Qt $${QT_VERSION}" $$escape_expand(\n\t)
	appdir.commands += if exist $$RELEASEDIR rmdir /s /q $$RELEASEDIR $$escape_expand(\n\t)
	appdir.commands += if exist $$RELEASEZIP del $$RELEASEZIP $$escape_expand(\n\t)
	appdir.commands += mkdir $$RELEASEDIR\Guides $$escape_expand(\n\t)
	appdir.commands += mkdir $$RELEASEDIR\Languages $$escape_expand(\n\t)
	appdir.commands += mkdir $$RELEASEDIR\Plugins $$escape_expand(\n\t)
	appdir.commands += mkdir $$RELEASEDIR\Themes $$escape_expand(\n\t)
	appdir.commands += copy ubpm.exe $$RELEASEDIR $$escape_expand(\n\t)
	appdir.commands += copy $$shell_path($$[QT_INSTALL_PREFIX])\..\..\Tools\\$$SSL\Win_x64\bin\*.dll $$RELEASEDIR $$escape_expand(\n\t)
	appdir.commands += for %%i in ($$shell_path($$DLL)) do copy \"%%i\" $$RELEASEDIR $$escape_expand(\n\t)
	appdir.commands += copy hlp\*.q* $$RELEASEDIR\Guides $$escape_expand(\n\t)
	appdir.commands += copy lng\mainapp_*.qm $$RELEASEDIR\Languages && copy ..\plugins\shared\plugin\lng\plugins_*.qm $$RELEASEDIR\Languages && copy $$shell_path($$TRANSLATIONS_BASE) $$RELEASEDIR\Languages && copy $$shell_path($$TRANSLATIONS_CONNECTIVITY) $$RELEASEDIR\Languages && copy $$shell_path($$TRANSLATIONS_HELP) $$RELEASEDIR\Languages && copy $$shell_path($$TRANSLATIONS_SERIALPORT) $$RELEASEDIR\Languages $$escape_expand(\n\t)
	appdir.commands += copy ..\plugins\*.dll $$RELEASEDIR\Plugins $$escape_expand(\n\t)
	appdir.commands += copy qss\*.qss $$RELEASEDIR\Themes $$escape_expand(\n\t)
	appdir.commands += windeployqt -bluetooth -serialport --no-translations --no-system-d3d-compiler $$RUN $$RELEASEDIR $$escape_expand(\n\t)

	appbundle.target   = appbundle
	appbundle.depends  = appdir
	appbundle.commands += 7z a -mx9 $$shell_path($$RELEASEZIP) $$shell_path($$RELEASEDIR\*) $$escape_expand(\n\t)
	appbundle.commands += $$MAKESFX ubpm_qt$$QT_MAJOR_VERSION-$$VERSION-x86_64.exe
} else {
	appbundle.target   = appbundle
	appbundle.commands += clear && echo "APPBUNDLE must be defined to build a Windows Release AppBundle for Qt $${QT_VERSION}" $$escape_expand(\n\t)
}

	installer.target   = installer
	installer.commands += cls && echo "Building Windows Release Installer for Qt $${QT_VERSION}" $$escape_expand(\n\t)
	installer.commands += @echo ToDo...

	QMAKE_EXTRA_TARGETS += appdir

### install ###

	INSTALLDIR = C:\\Progra~1\\UBPM

	cmd.path      = $$INSTALLDIR
	cmd.depends   = appdir
	cmd.extra     += xcopy /e $$RELEASEDIR $$INSTALLDIR $$escape_expand(\n\t)
	cmd.extra     += copy ..\..\release\appbundle\win\UBPM.lnk C:\\Progra~3\\Micros~1\\Windows\\Startm~1\\Programs $$escape_expand(\n\t)
	cmd.uninstall += rmdir /s /q $$INSTALLDIR && mkdir $$INSTALLDIR $$escape_expand(\n\t)
	cmd.uninstall += del C:\\Progra~3\\Micros~1\\Windows\\Startm~1\\Programs\\UBPM.lnk

	INSTALLS += cmd
}

### MACOS ###

macx {
	APP_DATE = "$$system(date +%d.%m.%Y)"

	BIN_PWD = $$OUT_PWD/ubpm.app/Contents/MacOS

	ICON = res/ico/ubpm.icns

	QMAKE_INFO_PLIST = ../../release/appbundle/mac/Info.plist

	!exists($$[QT_INSTALL_PLUGINS]/sqldrivers/libqsqlcipher.dylib) { warning("SQLCipher (https://github.com/sjemens/qsqlcipher-qt5) missing, encryption not available!") }

### release ###

contains(DEFINES, APPBUNDLE) {
	RELEASEDIR = $$(TMPDIR)/ubpm

	equals(QT_MAJOR_VERSION, 5) {
		SSL = OpenSSL
		MINSYSVER = 10.13
	} else {
		SSL = OpenSSLv3
		MINSYSVER = 11.0
	}

	QMAKE_SUBSTITUTES = ../../release/appbundle/mac/Info.plist.in

	appbundle.target   = appbundle
	appbundle.commands += clear && echo "Building macOS Release AppBundle for Qt $${QT_VERSION}" $$escape_expand(\n\t)
	appbundle.commands += if [ -d $$RELEASEDIR ]; then rm -r $$RELEASEDIR; fi $$escape_expand(\n\t)
	appbundle.commands += if [ -f ubpm_qt$$QT_MAJOR_VERSION-$$VERSION-x86_64.dmg ]; then rm ubpm_qt$$QT_MAJOR_VERSION-$$VERSION-x86_64.dmg; fi $$escape_expand(\n\t)
	appbundle.commands += mkdir -p $$RELEASEDIR/ubpm.app/Contents/MacOS/Guides $$escape_expand(\n\t)
	appbundle.commands += mkdir -p $$RELEASEDIR/ubpm.app/Contents/MacOS/Languages $$escape_expand(\n\t)
	appbundle.commands += mkdir -p $$RELEASEDIR/ubpm.app/Contents/MacOS/Plugins $$escape_expand(\n\t)
	appbundle.commands += mkdir -p $$RELEASEDIR/ubpm.app/Contents/MacOS/Themes $$escape_expand(\n\t)
	appbundle.commands += cp ubpm.app/Contents/MacOS/ubpm $$RELEASEDIR/ubpm.app/Contents/MacOS $$escape_expand(\n\t)
	appbundle.commands += cp hlp/*.qch hlp/*.qhc $$RELEASEDIR/ubpm.app/Contents/MacOS/Guides $$escape_expand(\n\t)
	appbundle.commands += cp lng/mainapp_*.qm ../plugins/shared/plugin/lng/plugins_*.qm $$TRANSLATIONS_BASE $$TRANSLATIONS_CONNECTIVITY $$TRANSLATIONS_HELP $$TRANSLATIONS_SERIALPORT $$RELEASEDIR/ubpm.app/Contents/MacOS/Languages $$escape_expand(\n\t)
	appbundle.commands += cp ../plugins/*.dylib $$RELEASEDIR/ubpm.app/Contents/MacOS/Plugins $$escape_expand(\n\t)
	appbundle.commands += cp qss/*.qss $$RELEASEDIR/ubpm.app/Contents/MacOS/Themes $$escape_expand(\n\t)
	appbundle.commands += cp -R ../../release/appbundle/mac/ $$RELEASEDIR/ubpm.app/Contents $$escape_expand(\n\t)
	appbundle.commands += rm $$RELEASEDIR/ubpm.app/Contents/Info.plist.in $$escape_expand(\n\t)
	appbundle.commands += cp res/ico/ubpm.icns $$RELEASEDIR/ubpm.app/Contents/Resources $$escape_expand(\n\t)
	appbundle.commands += cd $$RELEASEDIR && ln -s /Applications $$escape_expand(\n\t)
	appbundle.commands += cd $$RELEASEDIR && $$[QT_INSTALL_BINS]/macdeployqt ubpm.app $$escape_expand(\n\t)
	appbundle.commands += cp res/ico/ubpm.icns $$RELEASEDIR/.VolumeIcon.icns $$escape_expand(\n\t)
	appbundle.commands += hdiutil create -ov -volname UBPM-Qt$$QT_MAJOR_VERSION -srcfolder $$RELEASEDIR -format UDRW $$PWD/ubpm.dmg -attach $$escape_expand(\n\t)
	appbundle.commands += SetFile -c icnC /Volumes/UBPM-Qt$$QT_MAJOR_VERSION/.VolumeIcon.icns $$escape_expand(\n\t)
	appbundle.commands += SetFile -a C /Volumes/UBPM-Qt$$QT_MAJOR_VERSION $$escape_expand(\n\t)
	appbundle.commands += hdiutil detach /Volumes/UBPM-Qt$$QT_MAJOR_VERSION $$escape_expand(\n\t)
	appbundle.commands += hdiutil convert -ov -format UDZO -imagekey zlib-level=9 -o $$PWD/ubpm_qt$$QT_MAJOR_VERSION-$$VERSION-x86_64.dmg ubpm.dmg $$escape_expand(\n\t)
	appbundle.commands += rm ubpm.dmg
} else {
	appbundle.target   = appbundle
	appbundle.commands += clear && echo "APPBUNDLE must be defined to build a macOS Release AppBundle for Qt $${QT_VERSION}" $$escape_expand(\n\t)
}

	installer.target   = installer
	installer.commands += clear && echo "Building macOS Release Installer for Qt $${QT_VERSION}" $$escape_expand(\n\t)
	installer.commands += @echo ToDo...

### install ###

	INSTALLDIR = /Applications

	target.path = $$INSTALLDIR

	guides.path  = $$INSTALLDIR/ubpm.app/Contents/MacOS/Guides
	guides.files = hlp/*.qch hlp/*.qhc

	languages.path  = $$INSTALLDIR/ubpm.app/Contents/MacOS/Languages
	languages.files = lng/mainapp_*.qm ../plugins/shared/plugin/lng/plugins_*.qm

	plugins.path  = $$INSTALLDIR/ubpm.app/Contents/MacOS/Plugins
	plugins.files = ../plugins/*.dylib

	themes.path  = $$INSTALLDIR/ubpm.app/Contents/MacOS/Themes
	themes.files = qss/*.qss

	resources.path  = $$INSTALLDIR/ubpm.app/Contents/Resources
	resources.files = ../../release/appbundle/mac/Resources/*.lproj

	INSTALLS += target guides languages plugins themes resources
}

QMAKE_EXTRA_TARGETS += appbundle installer

DEFINES += APPVERS=\"\\\"$${APP_VERS}\\\"\" APPDATE=\"\\\"$${APP_DATE}\\\"\"

!exists($$BIN_PWD/Guides) { QMAKE_POST_LINK += $(MKDIR) $$shell_path($$BIN_PWD/Guides) && $$QMAKE_COPY_FILE $$shell_path($$PWD/hlp/*.q*) $$shell_path($$BIN_PWD/Guides) $$escape_expand(\n\t) }
!exists($$BIN_PWD/Languages) { QMAKE_POST_LINK += $(MKDIR) $$shell_path($$BIN_PWD/Languages) && $$QMAKE_COPY_FILE $$shell_path($$PWD/lng/*.qm) $$shell_path($$BIN_PWD/Languages) && $$QMAKE_COPY_FILE $$shell_path($$PWD/../plugins/shared/plugin/lng/*.qm) $$shell_path($$BIN_PWD/Languages) $$escape_expand(\n\t) }
!exists($$BIN_PWD/Themes) { QMAKE_POST_LINK += $(MKDIR) $$shell_path($$BIN_PWD/Themes) && $$QMAKE_COPY_FILE $$shell_path($$PWD/qss/*.qss) $$shell_path($$BIN_PWD/Themes) $$escape_expand(\n\t) }
!exists($$BIN_PWD/Plugins) { exists($$OUT_PWD/../plugins/*.so) | exists($$OUT_PWD/../plugins/*.dll) | exists($$OUT_PWD/../plugins/*.dylib) { QMAKE_POST_LINK += $(MKDIR) $$shell_path($$BIN_PWD/Plugins) && $$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../plugins/*.*) $$shell_path($$BIN_PWD/Plugins) $$escape_expand(\n\t) }}
