#include "DialogSettings.h"

DialogSettings::DialogSettings(QWidget *parent, struct SETTINGS *psettings, const QVector <QPluginLoader*> pplugins, int *pplugin) : QDialog(parent)
{
	int index = 0;

	setupUi(this);

#ifdef UPDATE_HIDE
	tabWidget->removeTab(8);
#elif UPDATE_DISABLE
	tabWidget->setTabEnabled(8, false);
#endif

	layout()->setSizeConstraint(QLayout::SetFixedSize);

	settings = psettings;
	plugins = pplugins;
	plugin = pplugin;

	plainTextEdit_message->setFixedHeight(3 * plainTextEdit_message->fontMetrics().height() + plainTextEdit_message->document()->documentMargin() + 5);

	lineEdit_location->setText(settings->database.location);
	groupBox_encryption->setEnabled(QSqlDatabase::isDriverAvailable("QSQLCIPHER"));
	groupBox_encryption->setChecked(settings->database.encryption);
	lineEdit_password->setText(settings->database.password);
	groupBox_backup->setChecked(settings->database.backup);
	lineEdit_backup_location->setText(settings->database.backuplocation);
	comboBox_backup_mode->setCurrentIndex(settings->database.backupmode);
	spinBox_backup_copies->setValue(settings->database.backupcopies);

	toolButton_female1->setChecked(settings->user[0].gender == "Female" ? true : false);
	toolButton_female2->setChecked(settings->user[1].gender == "Female" ? true : false);
	comboBox_age1->setCurrentIndex(settings->user[0].agegroup);
	comboBox_age2->setCurrentIndex(settings->user[1].agegroup);
	lineEdit_user1->setText(settings->user[0].name);
	lineEdit_user2->setText(settings->user[1].name);
	groupBox_user1->setChecked(settings->user[0].addition);
	groupBox_user2->setChecked(settings->user[1].addition);
	dateEdit_birth1->setDisplayFormat(settings->dtfshort);
	dateEdit_birth2->setDisplayFormat(settings->dtfshort);
	dateEdit_birth1->setDate(settings->user[0].birth);
	dateEdit_birth2->setDate(settings->user[1].birth);
	dateEdit_birth1->calendarWidget()->setGridVisible(true);
	dateEdit_birth2->calendarWidget()->setGridVisible(true);
	dateEdit_birth1->calendarWidget()->setVerticalHeaderFormat(QCalendarWidget::ISOWeekNumbers);
	dateEdit_birth2->calendarWidget()->setVerticalHeaderFormat(QCalendarWidget::ISOWeekNumbers);
	spinBox_height1->setValue(settings->user[0].height);
	spinBox_height2->setValue(settings->user[1].height);
	spinBox_weight1->setValue(settings->user[0].weight);
	spinBox_weight2->setValue(settings->user[1].weight);

	foreach(QPluginLoader *plugin, plugins)
	{
		deviceInterface = qobject_cast<DeviceInterface*>(plugin->instance());
		deviceInfo = deviceInterface->getDeviceInfo();
		QString fileName = QFileInfo(plugin->fileName()).fileName();

		comboBox_plugins->addItem(QIcon(deviceInfo.icon), fileName);

		if(fileName == settings->device.plugin)
		{
			index = comboBox_plugins->count() - 1;
		}

		plugin->unload();
	}

	comboBox_plugins->setCurrentIndex(index);
	groupBox_8->setTitle(tr("Import Plugins [ %1 ]").arg(comboBox_plugins->count() - 1));

	checkBox_dynamic->setChecked(settings->chart.dynamic);
	checkBox_colored->setChecked(settings->chart.colored);
	groupBox_symbols->setChecked(settings->chart.symbols);
	toolButton_symbolcolor->setChecked(settings->chart.symbolcolor);
	horizontalSlider_symbolsize->setValue(settings->chart.symbolsize);
	groupBox_lines->setChecked(settings->chart.lines);
	horizontalSlider_linewidth->setValue(settings->chart.linewidth);
	checkBox_heartrate->setChecked(settings->chart.heartrate);
	checkBox_hrsheet->setChecked(settings->chart.hrsheet);
	groupBox_hrsheet->setEnabled(settings->chart.heartrate);
	spinBox_sys_max1->setValue(settings->chart.range[0].sys_max);
	spinBox_sys_min1->setValue(settings->chart.range[0].sys_min);
	spinBox_dia_max1->setValue(settings->chart.range[0].dia_max);
	spinBox_dia_min1->setValue(settings->chart.range[0].dia_min);
	spinBox_bpm_max1->setValue(settings->chart.range[0].bpm_max);
	spinBox_bpm_min1->setValue(settings->chart.range[0].bpm_min);
	spinBox_sys_max2->setValue(settings->chart.range[1].sys_max);
	spinBox_sys_min2->setValue(settings->chart.range[1].sys_min);
	spinBox_dia_max2->setValue(settings->chart.range[1].dia_max);
	spinBox_dia_min2->setValue(settings->chart.range[1].dia_min);
	spinBox_bpm_max2->setValue(settings->chart.range[1].bpm_max);
	spinBox_bpm_min2->setValue(settings->chart.range[1].bpm_min);

	horizontalSlider_sys1->setValue(settings->table[0].warnsys);
	horizontalSlider_dia1->setValue(settings->table[0].warndia);
	horizontalSlider_ppr1->setValue(settings->table[0].warnppr);
	horizontalSlider_bpm1->setValue(settings->table[0].warnbpm);
	horizontalSlider_sys2->setValue(settings->table[1].warnsys);
	horizontalSlider_dia2->setValue(settings->table[1].warndia);
	horizontalSlider_ppr2->setValue(settings->table[1].warnppr);
	horizontalSlider_bpm2->setValue(settings->table[1].warnbpm);

	checkBox_median->setChecked(settings->stats.median);
	checkBox_legend->setChecked(settings->stats.legend);

	lineEdit_address->setText(settings->email.address);
	lineEdit_subject->setText(settings->email.subject);
	plainTextEdit_message->setPlainText(settings->email.message);

	checkBox_import->setChecked(settings->plugin.import);
	checkBox_logging->setChecked(settings->plugin.logging);
	checkBox_discover->setChecked(settings->plugin.bluetooth.discover);
	checkBox_connect->setChecked(settings->plugin.bluetooth.connect);

	checkBox_autostart->setChecked(settings->update.autostart);
	checkBox_notification->setChecked(settings->update.notification);
}

void DialogSettings::on_comboBox_backup_mode_currentIndexChanged(int index)
{
	int copies[] = { 365, 52, 12 };

	spinBox_backup_copies->setMaximum(copies[index]);
	spinBox_backup_copies->setValue(3);
}

void DialogSettings::on_checkBox_heartrate_toggled(bool checked)
{
	groupBox_hrsheet->setEnabled(checked);
}

void DialogSettings::on_checkBox_autostart_toggled(bool checked)
{
	groupBox_notification->setEnabled(checked);
}

void DialogSettings::on_comboBox_plugins_currentIndexChanged(int index)
{
	if(index)
	{
		deviceInterface = qobject_cast<DeviceInterface*>(plugins.at(index - 1)->instance());
		deviceInfo = deviceInterface->getDeviceInfo();

		label_producer->setText(deviceInfo.producer);
		label_model->setText(deviceInfo.alias.isEmpty() ? deviceInfo.model : deviceInfo.model + " (" + deviceInfo.alias + ")");
		label_version->setText(deviceInfo.version);
		label_maintainer->setText(deviceInfo.maintainer);
	}
	else
	{
		label_producer->clear();
		label_model->clear();
		label_version->clear();
		label_maintainer->clear();
	}
}

void DialogSettings::on_toolButton_choose_clicked()
{
	QString dir = QFileDialog::getExistingDirectory(this, tr("Choose Database Location"), QFileInfo(settings->database.location).path(), QFileDialog::ShowDirsOnly);

	if(!dir.isEmpty())
	{
		lineEdit_location->setText(dir + "/ubpm.sql");
	}
}

void DialogSettings::on_toolButton_backup_choose_clicked()
{
	QString dir = QFileDialog::getExistingDirectory(this, tr("Choose Database Backup Location"), settings->database.backuplocation, QFileDialog::ShowDirsOnly);

	if(!dir.isEmpty())
	{
		lineEdit_backup_location->setText(dir);
	}
}

void DialogSettings::on_toolButton_view_pressed()
{
	lineEdit_password->setEchoMode(QLineEdit::QLineEdit::Normal);
}

void DialogSettings::on_toolButton_view_released()
{
	lineEdit_password->setEchoMode(QLineEdit::Password);
}

void DialogSettings::on_horizontalSlider_symbolsize_valueChanged(int value)
{
	label_symbolsize->setNum(value);
}

void DialogSettings::on_horizontalSlider_linewidth_valueChanged(int value)
{
	label_linewidth->setNum(value);
}

void DialogSettings::on_horizontalSlider_sys1_valueChanged(int value)
{
	label_sys1->setNum(value);
}

void DialogSettings::on_horizontalSlider_dia1_valueChanged(int value)
{
	label_dia1->setNum(value);
}

void DialogSettings::on_horizontalSlider_ppr1_valueChanged(int value)
{
	label_ppr1->setNum(value);
}

void DialogSettings::on_horizontalSlider_bpm1_valueChanged(int value)
{
	label_bpm1->setNum(value);
}

void DialogSettings::on_horizontalSlider_sys2_valueChanged(int value)
{
	label_sys2->setNum(value);
}

void DialogSettings::on_horizontalSlider_dia2_valueChanged(int value)
{
	label_dia2->setNum(value);
}

void DialogSettings::on_horizontalSlider_ppr2_valueChanged(int value)
{
	label_ppr2->setNum(value);
}

void DialogSettings::on_horizontalSlider_bpm2_valueChanged(int value)
{
	label_bpm2->setNum(value);
}

bool DialogSettings::settingsUnchanged()
{
	if((settings->database.location != lineEdit_location->text()) || (settings->database.encryption != groupBox_encryption->isChecked()) || (settings->database.password != lineEdit_password->text()) || (settings->database.backup != groupBox_backup->isChecked())  || (settings->database.backuplocation != lineEdit_backup_location->text())  || (settings->database.backupmode != comboBox_backup_mode->currentIndex())  || (settings->database.backupcopies != spinBox_backup_copies->value()))
	{
		return false;
	}

	if((settings->user[0].gender != (toolButton_male1->isChecked() ? "Male" : "Female")) || (settings->user[0].agegroup != comboBox_age1->currentIndex()) || (settings->user[0].name != lineEdit_user1->text()) || (settings->user[0].addition != groupBox_user1->isChecked()) || (settings->user[0].birth != dateEdit_birth1->date()) || (settings->user[0].height != spinBox_height1->value()) || (settings->user[0].weight != spinBox_weight1->value()))
	{
		return false;
	}

	if((settings->user[1].gender != (toolButton_male2->isChecked() ? "Male" : "Female")) || (settings->user[1].agegroup != comboBox_age2->currentIndex()) || (settings->user[1].name != lineEdit_user2->text()) || (settings->user[1].addition != groupBox_user2->isChecked()) || (settings->user[1].birth != dateEdit_birth2->date()) || (settings->user[1].height != spinBox_height2->value()) || (settings->user[1].weight != spinBox_weight2->value()))
	{
		return false;
	}

	if(settings->device.plugin != (comboBox_plugins->currentIndex() ? comboBox_plugins->currentText() : ""))
	{
		return false;
	}

	if((settings->chart.dynamic != checkBox_dynamic->isChecked()) || (settings->chart.colored != checkBox_colored->isChecked()) || (settings->chart.symbols != groupBox_symbols->isChecked()) || (settings->chart.symbolcolor != toolButton_symbolcolor->isChecked()) || (settings->chart.symbolsize != horizontalSlider_symbolsize->value()) || (settings->chart.lines != groupBox_lines->isChecked()) || (settings->chart.linewidth != horizontalSlider_linewidth->value()) || (settings->chart.heartrate != checkBox_heartrate->isChecked()) || (settings->chart.hrsheet != checkBox_hrsheet->isChecked()))
	{
		return false;
	}

	if((settings->chart.range[0].sys_max != spinBox_sys_max1->value()) || (settings->chart.range[0].sys_min != spinBox_sys_min1->value()) || (settings->chart.range[0].dia_max != spinBox_dia_max1->value()) || (settings->chart.range[0].dia_min != spinBox_dia_min1->value()) || (settings->chart.range[0].bpm_max != spinBox_bpm_max1->value()) || (settings->chart.range[0].bpm_min != spinBox_bpm_min1->value()))
	{
		return false;
	}

	if((settings->chart.range[1].sys_max != spinBox_sys_max2->value()) || (settings->chart.range[1].sys_min != spinBox_sys_min2->value()) || (settings->chart.range[1].dia_max != spinBox_dia_max2->value()) || (settings->chart.range[1].dia_min != spinBox_dia_min2->value()) || (settings->chart.range[1].bpm_max != spinBox_bpm_max2->value()) || (settings->chart.range[1].bpm_min != spinBox_bpm_min2->value()))
	{
		return false;
	}

	if((settings->table[0].warnsys != horizontalSlider_sys1->value()) || (settings->table[0].warndia != horizontalSlider_dia1->value()) || (settings->table[0].warnppr != horizontalSlider_ppr1->value()) || (settings->table[0].warnbpm != horizontalSlider_bpm1->value()) || (settings->table[1].warnsys != horizontalSlider_sys2->value()) || (settings->table[1].warndia != horizontalSlider_dia2->value()) || (settings->table[1].warnppr != horizontalSlider_ppr2->value()) || (settings->table[1].warnbpm != horizontalSlider_bpm2->value()))
	{
		return false;
	}

	if((settings->stats.median != checkBox_median->isChecked()) || (settings->stats.legend != checkBox_legend->isChecked()))
	{
		return false;
	}

	if((settings->email.address != lineEdit_address->text()) || (settings->email.subject != lineEdit_subject->text()) || (settings->email.message != plainTextEdit_message->toPlainText()))
	{
		return false;
	}

	if((settings->plugin.logging != checkBox_logging->isChecked()) || (settings->plugin.import != checkBox_import->isChecked()) || (settings->plugin.bluetooth.discover != checkBox_discover->isChecked()) || (settings->plugin.bluetooth.connect != checkBox_connect->isChecked()))
	{
		return false;
	}

	if((settings->update.autostart != checkBox_autostart->isChecked()) || (settings->update.notification != checkBox_notification->isChecked()))
	{
		return false;
	}

	return true;
}

void DialogSettings::changeDatabase()
{
	if(QFile::exists(lineEdit_location->text()))
	{
		QMessageBox msgBox(QMessageBox::Warning, APPNAME, tr("The selected database already exists.\n\nPlease choose the preferred action."));

		QAbstractButton *overwriteButton = msgBox.addButton(tr("Overwrite"), QMessageBox::ActionRole);
		QAbstractButton *mergeButton = msgBox.addButton(tr("Merge"), QMessageBox::ActionRole);
		QAbstractButton *swapButton = msgBox.addButton(tr("Swap"), QMessageBox::ActionRole);

		msgBox.setDetailedText(tr("Overwrite:\n\nOverwrites the selected database with the current database.\n\nMerge:\n\nMerges the selected database into the current database.\n\nSwap:\n\nDeletes the current database and use the selected database."));

		msgBox.exec();

		if(msgBox.clickedButton() == overwriteButton)
		{
			if(!reinterpret_cast<MainWindow*>(parent())->database[0].count() && !reinterpret_cast<MainWindow*>(parent())->database[1].count())
			{
				QMessageBox::warning(this, APPNAME, tr("The current database is empty.\n\nThe selected database will be erased on exit, if no data will be added."));
			}
		}
		else if(msgBox.clickedButton() == mergeButton)
		{
			reinterpret_cast<MainWindow*>(parent())->importFromFile(lineEdit_location->text());
		}
		else if(msgBox.clickedButton() == swapButton)
		{
			reinterpret_cast<MainWindow*>(parent())->database[0].clear();
			reinterpret_cast<MainWindow*>(parent())->database[1].clear();

			reinterpret_cast<MainWindow*>(parent())->importFromFile(lineEdit_location->text());
		}
	}
}

void DialogSettings::on_pushButton_save_clicked()
{
	int age1 = USERAGE(dateEdit_birth1->date());
	int age2 = USERAGE(dateEdit_birth2->date());
	QStringList ages1 = comboBox_age1->currentText().split(' ');
	QStringList ages2 = comboBox_age2->currentText().split(' ');
	QRegularExpression mailaddr("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b", QRegularExpression::CaseInsensitiveOption);

	if(ages1.count() < 3) ages1.append("9999");
	if(ages2.count() < 3) ages2.append("9999");

	if(groupBox_encryption->isChecked() && lineEdit_password->text().isEmpty())
	{
		QMessageBox::warning(this, APPNAME, tr("SQL encryption can't be enabled without password and will be disabled!"));

		groupBox_encryption->setChecked(false);
	}

	if(groupBox_backup->isChecked() && QFileInfo(lineEdit_location->text()).path() == lineEdit_backup_location->text())
	{
		QMessageBox::information(this, APPNAME, tr("The database backup should be located on a different hard disk, partition or directory."));
	}

	if(groupBox_user1->isChecked() && (dateEdit_birth1->date() == QDate(1900, 1, 1) || !spinBox_height1->value() || !spinBox_weight1->value()))
	{
		QMessageBox::critical(this, APPNAME, tr("Please enter valid values for additional information of user 1!"));

		return;
	}

	if(groupBox_user2->isChecked() && (dateEdit_birth2->date() == QDate(1900, 1, 1) || !spinBox_height2->value() || !spinBox_weight2->value()))
	{
		QMessageBox::critical(this, APPNAME, tr("Please enter valid values for additional information of user 2!"));

		return;
	}

	if(groupBox_user1->isChecked() && (age1 < ages1.at(0).toInt() || age1 > ages1.at(2).toInt()))
	{
		QMessageBox::critical(this, APPNAME, tr("Entered age doesn't match selected age group for user 1!"));

		return;
	}

	if(groupBox_user2->isChecked() && (age2 < ages2.at(0).toInt() || age2 > ages2.at(2).toInt()))
	{
		QMessageBox::critical(this, APPNAME, tr("Entered age doesn't match selected age group for user 2!"));

		return;
	}


	if(!groupBox_symbols->isChecked() && !groupBox_lines->isChecked())
	{
		QMessageBox::critical(this, APPNAME, tr("Please enable symbols or lines for chart!"));

		return;
	}

	if(!mailaddr.match(lineEdit_address->text()).hasMatch())
	{
		QMessageBox::critical(this, APPNAME, tr("Please enter a valid e-mail address!"));

		return;
	}

	if(lineEdit_subject->text().isEmpty())
	{
		QMessageBox::critical(this, APPNAME, tr("Please enter a e-mail subject!"));

		return;
	}

	if(!plainTextEdit_message->toPlainText().contains("$CHART") && !plainTextEdit_message->toPlainText().contains("$TABLE") && !plainTextEdit_message->toPlainText().contains("$STATS"))
	{
		QMessageBox::critical(this, APPNAME, tr("E-Mail message must contain $CHART, $TABLE and/or $STATS!"));

		return;
	}

	if(settings->database.location != lineEdit_location->text())
	{
		changeDatabase();
	}

	settings->database.location = lineEdit_location->text();
	settings->database.encryption = groupBox_encryption->isChecked();
	settings->database.password = lineEdit_password->text();
	settings->database.backup = groupBox_backup->isChecked();
	settings->database.backuplocation = lineEdit_backup_location->text();
	settings->database.backupmode = comboBox_backup_mode->currentIndex();
	settings->database.backupcopies = spinBox_backup_copies->value();

	settings->user[0].gender = toolButton_male1->isChecked() ? "Male" : "Female";
	settings->user[1].gender = toolButton_male2->isChecked() ? "Male" : "Female";
	settings->user[0].agegroup = comboBox_age1->currentIndex();
	settings->user[1].agegroup = comboBox_age2->currentIndex();
	settings->user[0].name = lineEdit_user1->text();
	settings->user[1].name = lineEdit_user2->text();
	settings->user[0].birth = dateEdit_birth1->date();
	settings->user[1].birth = dateEdit_birth2->date();
	settings->user[0].addition = groupBox_user1->isChecked();
	settings->user[1].addition = groupBox_user2->isChecked();
	settings->user[0].height = spinBox_height1->value();
	settings->user[1].height = spinBox_height2->value();
	settings->user[0].weight = spinBox_weight1->value();
	settings->user[1].weight = spinBox_weight2->value();

	settings->device.plugin = comboBox_plugins->currentIndex() ? comboBox_plugins->currentText() : "";
	*plugin = comboBox_plugins->currentIndex() - 1;

	settings->chart.dynamic = checkBox_dynamic->isChecked();
	settings->chart.colored = checkBox_colored->isChecked();
	settings->chart.symbols = groupBox_symbols->isChecked();
	settings->chart.symbolcolor = toolButton_symbolcolor->isChecked();
	settings->chart.symbolsize = horizontalSlider_symbolsize->value();
	settings->chart.lines = groupBox_lines->isChecked();
	settings->chart.linewidth = horizontalSlider_linewidth->value();
	settings->chart.heartrate = checkBox_heartrate->isChecked();
	settings->chart.hrsheet = checkBox_hrsheet->isChecked();
	settings->chart.range[0].sys_max = spinBox_sys_max1->value();
	settings->chart.range[0].sys_min = spinBox_sys_min1->value();
	settings->chart.range[0].dia_max = spinBox_dia_max1->value();
	settings->chart.range[0].dia_min = spinBox_dia_min1->value();
	settings->chart.range[0].bpm_max = spinBox_bpm_max1->value();
	settings->chart.range[0].bpm_min = spinBox_bpm_min1->value();
	settings->chart.range[1].sys_max = spinBox_sys_max2->value();
	settings->chart.range[1].sys_min = spinBox_sys_min2->value();
	settings->chart.range[1].dia_max = spinBox_dia_max2->value();
	settings->chart.range[1].dia_min = spinBox_dia_min2->value();
	settings->chart.range[1].bpm_max = spinBox_bpm_max2->value();
	settings->chart.range[1].bpm_min = spinBox_bpm_min2->value();

	settings->table[0].warnsys = horizontalSlider_sys1->value();
	settings->table[0].warndia = horizontalSlider_dia1->value();
	settings->table[0].warnppr = horizontalSlider_ppr1->value();
	settings->table[0].warnbpm = horizontalSlider_bpm1->value();
	settings->table[1].warnsys = horizontalSlider_sys2->value();
	settings->table[1].warndia = horizontalSlider_dia2->value();
	settings->table[1].warnppr = horizontalSlider_ppr2->value();
	settings->table[1].warnbpm = horizontalSlider_bpm2->value();

	settings->stats.median = checkBox_median->isChecked();
	settings->stats.legend = checkBox_legend->isChecked();

	settings->email.address = lineEdit_address->text();
	settings->email.subject = lineEdit_subject->text();
	settings->email.message = plainTextEdit_message->toPlainText();

	settings->plugin.logging = checkBox_logging->isChecked();
	settings->plugin.import = checkBox_import->isChecked();
	settings->plugin.bluetooth.discover = checkBox_discover->isChecked();
	settings->plugin.bluetooth.connect = checkBox_connect->isChecked();

	settings->update.autostart = checkBox_autostart->isChecked();
	settings->update.notification = checkBox_notification->isChecked();

	done(QDialog::Accepted);
}

void DialogSettings::on_pushButton_reset_clicked()
{
	switch(tabWidget->currentIndex())
	{
		case TAB_DATABASE:
		{
			lineEdit_location->setText(reinterpret_cast<MainWindow*>(parent())->envDatabase);

			groupBox_encryption->setChecked(false);
			lineEdit_password->clear();

			groupBox_backup->setChecked(true);
			lineEdit_backup_location->setText(QFileInfo(reinterpret_cast<MainWindow*>(parent())->envDatabase).path());
			comboBox_backup_mode->setCurrentIndex(0);

			break;
		}

		case TAB_USER:
		{
			switch(tabWidget_user->currentIndex())
			{
				case 0:
				{
					toolButton_male1->setChecked(true);
					comboBox_age1->setCurrentIndex(3);
					lineEdit_user1->setText(tr("User 1"));

					groupBox_user1->setChecked(false);
					dateEdit_birth1->setDate(QDate(1900, 1, 1));
					spinBox_height1->setValue(0);
					spinBox_weight1->setValue(0);

					break;
				}

				case 1:
				{
					toolButton_female2->setChecked(true);
					comboBox_age2->setCurrentIndex(3);
					lineEdit_user2->setText(tr("User 2"));

					groupBox_user2->setChecked(false);
					dateEdit_birth2->setDate(QDate(1900, 1, 1));
					spinBox_height2->setValue(0);
					spinBox_weight2->setValue(0);

					break;
				}
			}

			break;
		}

		case TAB_DEVICE:
		{
			comboBox_plugins->setCurrentIndex(0);

			break;
		}

		case TAB_CHART:
		{
			switch(tabWidget_chart->currentIndex())
			{
				case 0:
				{
					checkBox_dynamic->setChecked(true);
					checkBox_colored->setChecked(true);
					groupBox_symbols->setChecked(true);
					toolButton_symbolcolor->setChecked(false);
					horizontalSlider_symbolsize->setValue(24);
					groupBox_lines->setChecked(true);
					horizontalSlider_linewidth->setValue(3);

					break;
				}

				case 1:
				{
					checkBox_heartrate->setChecked(true);
					checkBox_hrsheet->setChecked(false);

					break;
				}

				case 2:
				{
					spinBox_sys_max1->setValue(SYSMAX);
					spinBox_sys_min1->setValue(SYSMIN);
					spinBox_dia_max1->setValue(DIAMAX);
					spinBox_dia_min1->setValue(DIAMIN);
					spinBox_bpm_max1->setValue(BPMMAX);
					spinBox_bpm_min1->setValue(BPMMIN);

					break;
				}

				case 3:
				{
					spinBox_sys_max2->setValue(SYSMAX);
					spinBox_sys_min2->setValue(SYSMIN);
					spinBox_dia_max2->setValue(DIAMAX);
					spinBox_dia_min2->setValue(DIAMIN);
					spinBox_bpm_max2->setValue(BPMMAX);
					spinBox_bpm_min2->setValue(BPMMIN);

					break;
				}
			}

			break;
		}

		case TAB_TABLE:
		{
			switch(tabWidget_table->currentIndex())
			{
				case 0:
				{
					horizontalSlider_sys1->setValue(WRNSYS);
					horizontalSlider_dia1->setValue(WRNDIA);
					horizontalSlider_ppr1->setValue(WRNPPR);
					horizontalSlider_bpm1->setValue(WRNBPM);

					break;
				}

				case 1:
				{
					horizontalSlider_sys2->setValue(WRNSYS);
					horizontalSlider_dia2->setValue(WRNDIA);
					horizontalSlider_ppr2->setValue(WRNPPR);
					horizontalSlider_bpm2->setValue(WRNBPM);

					break;
				}
			}

			break;
		}

		case TAB_STATS:
		{
			checkBox_median->setChecked(true);
			checkBox_legend->setChecked(true);

			break;
		}

		case TAB_EMAIL:
		{
			lineEdit_address->setText("e-m@il.net");
			lineEdit_subject->setText(tr("Blood Pressure Report"));
			plainTextEdit_message->setPlainText(tr("Dear Dr. House,\n\nplease find attached my blood pressure data for this month.\n\nBest regards,\n$USER\n$CHART$TABLE$STATS"));

			break;
		}

		case TAB_PLUGIN:
		{
			checkBox_logging->setChecked(false);
			checkBox_import->setChecked(false);
			checkBox_discover->setChecked(false);
			checkBox_connect->setChecked(false);

			break;
		}

		case TAB_UPDATE:
		{
			checkBox_autostart->setChecked(true);
			checkBox_notification->setChecked(true);

			break;
		}
	}
}

void DialogSettings::on_pushButton_close_clicked()
{
	close();
}

void DialogSettings::keyPressEvent(QKeyEvent *ke)
{
	if(ke->key() == Qt::Key_F1)
	{
		reinterpret_cast<MainWindow*>(parent())->help->showHelp(QString("01-02-%1").arg(tabWidget->currentIndex() + 1, 2, 10, QChar('0')));
	}

	QDialog::keyPressEvent(ke);
}

void DialogSettings::reject()
{
	if(settingsUnchanged() || QMessageBox::question(this, APPNAME, tr("Abort setup and discard all changes?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
		done(QDialog::Rejected);
	}
}
