#ifndef DLGRECORD_H
#define DLGRECORD_H

#include "MainWindow.h"
#include "ui_DialogRecord.h"

class DialogRecord : public QDialog, private Ui::DialogRecord
{
	Q_OBJECT

public:

	explicit DialogRecord(QWidget*, int, struct SETTINGS, struct HEALTHDATA);

private:

private slots:

	void focusChanged(QWidget*, QWidget*);

	void on_toolButton_user2_toggled(bool);

	void on_spinBox_sys_textChanged(QString);
	void on_spinBox_dia_textChanged(QString);
	void on_spinBox_bpm_textChanged(QString);

	void on_pushButton_undo_clicked();
	void on_pushButton_create_clicked();
	void on_pushButton_close_clicked();

	void keyPressEvent(QKeyEvent*);
};

#endif // DLGRECORD_H
