#ifndef DLGMIGRATION_H
#define DLGMIGRATION_H

#include "MainWindow.h"
#include "ui_DialogMigration.h"

enum VENDOR { BEURER, MEDISANA, MICROLIFE, OMRON, SANITAS, VEROVAL, CUSTOM };

class DialogMigration : public QDialog, private Ui::DialogMigration
{
	Q_OBJECT

public:

	explicit DialogMigration(QWidget*, struct SETTINGS*);

private:

	struct SETTINGS *settings;
	QStringList lines;
	int previous = 0;

	void testFailed(QString, QLineEdit*);

private slots:

	void initAfterShown();

	void saveCustom();

	void on_lineEdit_fmt_date_textChanged(const QString&);

	void on_comboBox_currentIndexChanged(int);

	void on_toolButton_open_clicked();
	void on_toolButton_copy_clicked();
	void on_pushButton_test_clicked();
	void on_pushButton_migrate_clicked();

	void keyPressEvent(QKeyEvent*);

	void reject();
};

#endif // DLGMIGRATION_H
