#ifndef DLGHELP_H
#define DLGHELP_H

#include "ui_DialogHelp.h"

class DialogHelp : public QDialog, private Ui::DialogHelp
{
	Q_OBJECT

public:

	explicit DialogHelp(QWidget*, QString, QString, struct SETTINGS*);
	~DialogHelp();

	void showHelp(QString);
	QString getSource();

private:

	bool initialized = false;
	bool fallback;

	struct SETTINGS *settings;

	QString dir, language, requested;

	QHelpEngine *helpEngine;
	QHelpContentWidget *contentWidget;

private slots:

	void setSource(QUrl);
	void setSourceFromPage(QString);
	void setSourceFromContent(QModelIndex);

	void anchorClicked(QUrl);

	void reject();
};

#include "MainWindow.h"

#endif // DLGHELP_H
