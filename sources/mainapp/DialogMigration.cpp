﻿#include "DialogMigration.h"

DialogMigration::DialogMigration(QWidget *parent, struct SETTINGS *psettings) : QDialog(parent)
{
	QRegularExpressionValidator *regexAZ	= new QRegularExpressionValidator(QRegularExpression("[a-z]{2}"));
	QRegularExpressionValidator *regex99	= new QRegularExpressionValidator(QRegularExpression("[0-9]{2}"));
	QRegularExpressionValidator *regexDelim	= new QRegularExpressionValidator(QRegularExpression(".{1}"));
	QRegularExpressionValidator *regexDate	= new QRegularExpressionValidator(QRegularExpression("[dMyhHmszapAP :/.-]{0,}"));
	QRegularExpressionValidator *regexTime	= new QRegularExpressionValidator(QRegularExpression("[hHmszapAP :]{0,}"));

	setupUi(this);

	layout()->setSizeConstraint(QLayout::SetFixedSize);

	settings = psettings;

	dateTimeEdit->setDisplayFormat(settings->dtfshort);
	lineEdit_startline->setValidator(regex99);
	lineEdit_elements->setValidator(regex99);
	lineEdit_delimiter->setValidator(regexDelim);
	lineEdit_fmt_date->setValidator(regexDate);
	lineEdit_fmt_date_lng->setValidator(regexAZ);
	lineEdit_fmt_time->setValidator(regexTime);
	lineEdit_pos_date->setValidator(regex99);
	lineEdit_pos_time->setValidator(regex99);
	lineEdit_pos_sys->setValidator(regex99);
	lineEdit_pos_dia->setValidator(regex99);
	lineEdit_pos_bpm->setValidator(regex99);
	lineEdit_pos_ihb->setValidator(regex99);
	lineEdit_pos_mov->setValidator(regex99);
	lineEdit_pos_msg->setValidator(regex99);

	lineEdit_fmt_date_lng->hide();

	on_comboBox_currentIndexChanged(BEURER);

	QTimer::singleShot(1, this, &DialogMigration::initAfterShown);
}

void DialogMigration::initAfterShown()
{
	on_toolButton_open_clicked();
}

void DialogMigration::saveCustom()
{
	settings->migration.startline = lineEdit_startline->text();
	settings->migration.delimiter = lineEdit_delimiter->text();
	settings->migration.elements = lineEdit_elements->text();
	settings->migration.fmt_date = lineEdit_fmt_date->text();
	settings->migration.fmt_date_lng = lineEdit_fmt_date_lng->text();
	settings->migration.fmt_time = lineEdit_fmt_time->text();
	settings->migration.fmt_ihb = lineEdit_fmt_ihb->text();
	settings->migration.fmt_mov = lineEdit_fmt_mov->text();
	settings->migration.fmt_msg = lineEdit_fmt_msg->text();

	settings->migration.pos_date = lineEdit_pos_date->text();
	settings->migration.pos_time = lineEdit_pos_time->text();
	settings->migration.pos_sys = lineEdit_pos_sys->text();
	settings->migration.pos_dia = lineEdit_pos_dia->text();
	settings->migration.pos_bpm = lineEdit_pos_bpm->text();
	settings->migration.pos_ihb = lineEdit_pos_ihb->text();
	settings->migration.pos_mov = lineEdit_pos_mov->text();
	settings->migration.pos_msg = lineEdit_pos_msg->text();
}

void DialogMigration::testFailed(QString message, QLineEdit *field)
{
	pushButton_migrate->setEnabled(false);

	QMessageBox::warning(this, APPNAME, message);

	if(field)
	{
		field->setFocus();
		field->selectAll();
	}
}

void DialogMigration::on_lineEdit_fmt_date_textChanged(const QString &text)
{
	if(text.contains("MMM") || text.contains("ddd"))
	{
		lineEdit_fmt_date_lng->show();
	}
	else
	{
		lineEdit_fmt_date_lng->hide();
	}
}

void DialogMigration::on_comboBox_currentIndexChanged(int index)
{
	if(previous == CUSTOM)
	{
		saveCustom();
	}

	previous = index;

	toolButton_copy->setEnabled(true);
	pushButton_migrate->setEnabled(false);

	if(index == BEURER)
	{
		lineEdit_startline->setText("15");
		lineEdit_delimiter->setText(";");
		lineEdit_elements->setText("13");
		lineEdit_fmt_date->setText("M/d/yyyy");
		lineEdit_fmt_time->setText("h:m ap");
		lineEdit_fmt_ihb->setText("Yes");
		lineEdit_fmt_mov->setText("Yes");
		lineEdit_fmt_msg->clear();

		lineEdit_pos_date->setText("0");
		lineEdit_pos_time->setText("1");
		lineEdit_pos_sys->setText("3");
		lineEdit_pos_dia->setText("4");
		lineEdit_pos_bpm->setText("5");
		lineEdit_pos_ihb->setText("8");
		lineEdit_pos_mov->setText("9");
		lineEdit_pos_msg->setText("12");
	}
	else if(index == MEDISANA)
	{
		lineEdit_startline->setText("23");
		lineEdit_delimiter->setText(";");
		lineEdit_elements->setText("7");
		lineEdit_fmt_date->setText("M/d/yyyy - h:m");
		lineEdit_fmt_time->clear();
		lineEdit_fmt_ihb->clear();
		lineEdit_fmt_mov->clear();
		lineEdit_fmt_msg->clear();

		lineEdit_pos_date->setText("0");
		lineEdit_pos_time->clear();
		lineEdit_pos_sys->setText("1");
		lineEdit_pos_dia->setText("2");
		lineEdit_pos_bpm->setText("3");
		lineEdit_pos_ihb->clear();
		lineEdit_pos_mov->clear();
		lineEdit_pos_msg->setText("6");
	}
	else if(index == MICROLIFE)
	{
		lineEdit_startline->setText("17");
		lineEdit_delimiter->setText(",");
		lineEdit_elements->setText("9");
		lineEdit_fmt_date->setText("yyyy/M/d");
		lineEdit_fmt_time->setText("h:m ap");
		lineEdit_fmt_ihb->setText("AFIB|PAD");
		lineEdit_fmt_mov->clear();
		lineEdit_fmt_msg->clear();

		lineEdit_pos_date->setText("0");
		lineEdit_pos_time->setText("1");
		lineEdit_pos_sys->setText("2");
		lineEdit_pos_dia->setText("3");
		lineEdit_pos_bpm->setText("4");
		lineEdit_pos_ihb->setText("7");
		lineEdit_pos_mov->clear();
		lineEdit_pos_msg->clear();
	}
	else if(index == OMRON)
	{
		lineEdit_startline->setText("1");
		lineEdit_delimiter->setText(",");
		lineEdit_elements->setText("13");
		lineEdit_fmt_date->setText("yyyy/M/d h:m");
		lineEdit_fmt_time->clear();
		lineEdit_fmt_ihb->setText("Detected");
		lineEdit_fmt_mov->setText("Detected");
		lineEdit_fmt_msg->clear();

		lineEdit_pos_date->setText("0");
		lineEdit_pos_time->clear();
		lineEdit_pos_sys->setText("2");
		lineEdit_pos_dia->setText("3");
		lineEdit_pos_bpm->setText("4");
		lineEdit_pos_ihb->setText("5");
		lineEdit_pos_mov->setText("7");
		lineEdit_fmt_msg->clear();
	}
	else if(index == SANITAS)
	{
		lineEdit_startline->setText("18");
		lineEdit_delimiter->setText(",");
		lineEdit_elements->setText("12");
		lineEdit_fmt_date->setText("d.M.yyyy");
		lineEdit_fmt_time->setText("h:m");
		lineEdit_fmt_ihb->setText("Yes");
		lineEdit_fmt_mov->setText("Yes");
		lineEdit_fmt_msg->clear();

		lineEdit_pos_date->setText("0");
		lineEdit_pos_time->setText("1");
		lineEdit_pos_sys->setText("3");
		lineEdit_pos_dia->setText("4");
		lineEdit_pos_bpm->setText("5");
		lineEdit_pos_ihb->setText("7");
		lineEdit_pos_mov->setText("8");
		lineEdit_pos_msg->setText("11");
	}
	else if(index == VEROVAL)
	{
		lineEdit_startline->setText("17");
		lineEdit_delimiter->setText(",");
		lineEdit_elements->setText("10");
		lineEdit_fmt_date->setText("d.M.yyyy");
		lineEdit_fmt_time->setText("h:m ap");
		lineEdit_fmt_ihb->setText("Yes");
		lineEdit_fmt_mov->clear();
		lineEdit_fmt_msg->clear();

		lineEdit_pos_date->setText("0");
		lineEdit_pos_time->setText("1");
		lineEdit_pos_sys->setText("2");
		lineEdit_pos_dia->setText("3");
		lineEdit_pos_bpm->setText("4");
		lineEdit_pos_ihb->setText("5");
		lineEdit_pos_mov->clear();
		lineEdit_pos_msg->setText("6");
	}
	else if(index == CUSTOM)
	{
		toolButton_copy->setDisabled(true);

		lineEdit_startline->setText(settings->migration.startline);
		lineEdit_delimiter->setText(settings->migration.delimiter);
		lineEdit_elements->setText(settings->migration.elements);
		lineEdit_fmt_date->setText(settings->migration.fmt_date);
		lineEdit_fmt_date_lng->setText(settings->migration.fmt_date_lng);
		lineEdit_fmt_time->setText(settings->migration.fmt_time);
		lineEdit_fmt_ihb->setText(settings->migration.fmt_ihb);
		lineEdit_fmt_mov->setText(settings->migration.fmt_mov);
		lineEdit_fmt_msg->setText(settings->migration.fmt_msg);

		lineEdit_pos_date->setText(settings->migration.pos_date);
		lineEdit_pos_time->setText(settings->migration.pos_time);
		lineEdit_pos_sys->setText(settings->migration.pos_sys);
		lineEdit_pos_dia->setText(settings->migration.pos_dia);
		lineEdit_pos_bpm->setText(settings->migration.pos_bpm);
		lineEdit_pos_ihb->setText(settings->migration.pos_ihb);
		lineEdit_pos_mov->setText(settings->migration.pos_mov);
		lineEdit_pos_msg->setText(settings->migration.pos_msg);
	}
}

void DialogMigration::on_toolButton_open_clicked()
{
	QString filename(QFileDialog::getOpenFileName(this, tr("Choose Data Source for Migration"), QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation), tr("CSV File (*.csv)")));

	if(!filename.isEmpty())
	{
		QFile file(filename);

		lineEdit_source->setText(filename);

		if(filename.contains("beurer", Qt::CaseInsensitive))
		{
			comboBox->setCurrentIndex(BEURER);
		}
		else if(filename.contains("medisana", Qt::CaseInsensitive))
		{
			comboBox->setCurrentIndex(MEDISANA);
		}
		else if(filename.contains("microlife", Qt::CaseInsensitive))
		{
			comboBox->setCurrentIndex(MICROLIFE);
		}
		else if(filename.contains("omron", Qt::CaseInsensitive))
		{
			comboBox->setCurrentIndex(OMRON);
		}
		else if(filename.contains("sanitas", Qt::CaseInsensitive))
		{
			comboBox->setCurrentIndex(SANITAS);
		}
		else if(filename.contains("veroval", Qt::CaseInsensitive))
		{
			comboBox->setCurrentIndex(VEROVAL);
		}
		else if(filename.contains("custom", Qt::CaseInsensitive))
		{
			comboBox->setCurrentIndex(CUSTOM);
		}

		if(filename.contains("2"))
		{
			toolButton_u2->setChecked(true);
		}

		if(file.open(QIODevice::ReadOnly | QIODevice::Text))
		{
			QByteArray csv = file.readAll();

			file.close();

			csv.replace('"', "");
			csv = csv.trimmed();

			lines = QString(csv).split('\n');

			lineEdit_size->setText(tr("%L1 Bytes").arg(csv.size()));
			lineEdit_lines->setText(tr("%1 Lines").arg(lines.count()));

			lineEdit_test_startline->clear();
			dateTimeEdit->setDateTime(QDateTime::fromString("01.01.2000 00:00", "dd.MM.yyyy hh:mm"));
			lineEdit_test_sys->clear();
			lineEdit_test_dia->clear();
			lineEdit_test_bpm->clear();
			lineEdit_test_ihb->clear();
			lineEdit_test_mov->clear();

			pushButton_test->setEnabled(true);
		}
		else
		{
			QMessageBox::critical(this, APPNAME, tr("Could not open \"%1\"!\n\nReason: %2").arg(filename, file.errorString()));
		}
	}
}

void DialogMigration::on_toolButton_copy_clicked()
{
	saveCustom();

	comboBox->setCurrentIndex(CUSTOM);
}

void DialogMigration::on_pushButton_test_clicked()
{
	QString line;
	QStringList values;
	QStringList positions;
	QDateTime dt;
	QLocale locale(lineEdit_fmt_date_lng->text());

	if(lineEdit_startline->text().isEmpty())
	{
		testFailed(tr("Startline can't be empty."), lineEdit_startline);

		return;
	}
	else if(lineEdit_startline->text().toInt() >= lines.count())
	{
		testFailed(tr("Startline must be smaller than the number of lines."), lineEdit_startline);

		return;
	}
	else
	{
		line = lines.at(lineEdit_startline->text().toInt());

		lineEdit_test_startline->setText(tr("Line %1 = %2").arg(lineEdit_startline->text().toInt()).arg(line));
	}

	if(lineEdit_delimiter->text().isEmpty())
	{
		testFailed(tr("Delimiter can't be empty."), lineEdit_delimiter);

		return;
	}

	if(!line.contains(lineEdit_delimiter->text()))
	{
		testFailed(tr("Startline doesn't contain \"%1\" delimiter.").arg(lineEdit_delimiter->text()), lineEdit_delimiter);

		return;
	}
	else
	{
		values = line.split(lineEdit_delimiter->text());
	}

	if(lineEdit_elements->text().isEmpty())
	{
		testFailed(tr("Elements can't be empty."), lineEdit_elements);

		return;
	}
	else if(lineEdit_elements->text().toInt() < 4)
	{
		testFailed(tr("Elements can't be smaller than 4."), lineEdit_elements);

		return;
	}
	else if(line.split(lineEdit_delimiter->text()).count() != lineEdit_elements->text().toInt())
	{
		testFailed(tr("Elements doesn't match (found %1).").arg(line.split(lineEdit_delimiter->text()).count()), lineEdit_elements);

		return;
	}

	if(lineEdit_fmt_date->text().isEmpty())
	{
		testFailed(tr("Date format can't be empty."), lineEdit_fmt_date);

		return;
	}
	else if(lineEdit_fmt_time->text().isEmpty() && lineEdit_pos_time->text().isEmpty() && !lineEdit_fmt_date->text().contains("h:m"))
	{
		testFailed(tr("Single date format must also contain a time format."), lineEdit_fmt_date);

		return;
	}

	if(lineEdit_pos_date->text().isEmpty())
	{
		testFailed(tr("Date position can't be empty."), lineEdit_pos_date);

		return;
	}
	else if(lineEdit_pos_date->text().toInt() > lineEdit_elements->text().toInt() - 1)
	{
		testFailed(tr("Date position must be smaller than elements."), lineEdit_pos_date);

		return;
	}

	if(lineEdit_fmt_time->text().isEmpty() && !lineEdit_pos_time->text().isEmpty())
	{
		testFailed(tr("Time position requires also a time parameter."), lineEdit_fmt_time);

		return;
	}
	else if(!lineEdit_fmt_time->text().isEmpty() && lineEdit_pos_time->text().isEmpty())
	{
		testFailed(tr("Time format requires also a time position."), lineEdit_pos_time);

		return;
	}
	else if(lineEdit_pos_time->text().toInt() > lineEdit_elements->text().toInt() - 1)
	{
		testFailed(tr("Time position must be smaller than elements."), lineEdit_pos_time);

		return;
	}

	if(lineEdit_pos_sys->text().isEmpty())
	{
		testFailed(tr("Systolic position can't be empty."), lineEdit_pos_sys);

		return;
	}
	else if(lineEdit_pos_sys->text().toInt() > lineEdit_elements->text().toInt() - 1)
	{
		testFailed(tr("Systolic position must be smaller than elements."), lineEdit_pos_sys);

		return;
	}

	if(lineEdit_pos_dia->text().isEmpty())
	{
		testFailed(tr("Diastolic position can't be empty."), lineEdit_pos_dia);

		return;
	}
	else if(lineEdit_pos_dia->text().toInt() > lineEdit_elements->text().toInt() - 1)
	{
		testFailed(tr("Diastolic position must be smaller than elements."), lineEdit_pos_dia);

		return;
	}

	if(lineEdit_pos_bpm->text().isEmpty())
	{
		testFailed(tr("Heartrate position can't be empty."), lineEdit_pos_bpm);

		return;
	}
	else if(lineEdit_pos_bpm->text().toInt() > lineEdit_elements->text().toInt() - 1)
	{
		testFailed(tr("Heartrate position must be smaller than elements."), lineEdit_pos_bpm);

		return;
	}

	if(lineEdit_pos_ihb->text().toInt() > lineEdit_elements->text().toInt() - 1)
	{
		testFailed(tr("Irregular position must be smaller than elements."), lineEdit_pos_ihb);

		return;
	}
	else if(lineEdit_fmt_ihb->text().isEmpty() && !lineEdit_pos_ihb->text().isEmpty())
	{
		testFailed(tr("Irregular position requires also an irregular parameter."), lineEdit_fmt_ihb);

		return;
	}

	if(lineEdit_pos_mov->text().toInt() > lineEdit_elements->text().toInt() - 1)
	{
		testFailed(tr("Movement position must be smaller than elements."), lineEdit_pos_mov);

		return;
	}
	else if(lineEdit_fmt_mov->text().isEmpty() && !lineEdit_pos_mov->text().isEmpty())
	{
		testFailed(tr("Movement position requires also a movement parameter."), lineEdit_fmt_mov);

		return;
	}

	if(lineEdit_pos_msg->text().toInt() > lineEdit_elements->text().toInt() - 1)
	{
		testFailed(tr("Comment position must be smaller than elements."), lineEdit_pos_msg);

		return;
	}

	positions.append(lineEdit_pos_date->text());
	if(!lineEdit_pos_time->text().isEmpty()) positions.append(lineEdit_pos_time->text());
	positions.append(lineEdit_pos_sys->text());
	positions.append(lineEdit_pos_dia->text());
	positions.append(lineEdit_pos_bpm->text());
	if(!lineEdit_pos_ihb->text().isEmpty()) positions.append(lineEdit_pos_ihb->text());
	if(!lineEdit_pos_mov->text().isEmpty()) positions.append(lineEdit_pos_mov->text());
	positions.append(lineEdit_pos_msg->text());

	if(positions.removeDuplicates())
	{
		testFailed(tr("Positions can't exist twice."), nullptr);

		return;
	}

	if(lineEdit_fmt_time->text().isEmpty())
	{
		dt = locale.toDateTime(values.at(lineEdit_pos_date->text().toInt()), lineEdit_fmt_date->text());
	}
	else
	{
		QDate date = locale.toDate(values.at(lineEdit_pos_date->text().toInt()), lineEdit_fmt_date->text());
		QTime time = locale.toTime(values.at(lineEdit_pos_time->text().toInt()), lineEdit_fmt_time->text());

		if(!date.isValid())
		{
			testFailed(tr("Date format is invalid."), lineEdit_fmt_date);

			return;
		}

		if(!time.isValid())
		{
			testFailed(tr("Time format is invalid."), lineEdit_fmt_time);

			return;
		}

		dt = QDateTime(date, time);
	}

	if(!dt.isValid())
	{
		testFailed(tr("Date/Time format is invalid."), lineEdit_fmt_date);

		return;
	}
	else
	{
		dateTimeEdit->setDateTime(dt);
	}

	if(comboBox->currentIndex() == MEDISANA)
	{
		lineEdit_test_sys->setText(values.at(lineEdit_pos_sys->text().toInt()).split(" ").at(0));
		lineEdit_test_dia->setText(values.at(lineEdit_pos_dia->text().toInt()).split(" ").at(0));
		lineEdit_test_bpm->setText(values.at(lineEdit_pos_bpm->text().toInt()).split(" ").at(0));
	}
	else
	{
		lineEdit_test_sys->setText(values.at(lineEdit_pos_sys->text().toInt()));
		lineEdit_test_dia->setText(values.at(lineEdit_pos_dia->text().toInt()));
		lineEdit_test_bpm->setText(values.at(lineEdit_pos_bpm->text().toInt()));
	}

	if(!lineEdit_pos_ihb->text().isEmpty())
	{
		if(lineEdit_fmt_ihb->text().contains("|"))
		{
			QStringList ihbs = lineEdit_fmt_ihb->text().split("|");

			if(ihbs.contains(values.at(lineEdit_pos_ihb->text().toInt())))
			{
				lineEdit_test_ihb->setText("1");
			}
			else
			{
				lineEdit_test_ihb->setText("0");
			}
		}
		else
		{
			lineEdit_test_ihb->setText(values.at(lineEdit_pos_ihb->text().toInt()) == lineEdit_fmt_ihb->text() ? "1" : "0");
		}
	}
	else
	{
		lineEdit_test_ihb->setText("-");
	}

	if(!lineEdit_pos_mov->text().isEmpty())
	{
		if(lineEdit_fmt_mov->text().contains("|"))
		{
			QStringList movs = lineEdit_fmt_mov->text().split("|");

			if(movs.contains(values.at(lineEdit_pos_mov->text().toInt())))
			{
				lineEdit_test_mov->setText("1");
			}
			else
			{
				lineEdit_test_mov->setText("0");
			}
		}
		else
		{
			lineEdit_test_mov->setText(values.at(lineEdit_pos_mov->text().toInt()) == lineEdit_fmt_mov->text() ? "1" : "0");
		}
	}
	else
	{
		lineEdit_test_mov->setText("-");
	}

	if(!lineEdit_pos_msg->text().isEmpty())
	{
		QString msg = values.at(lineEdit_pos_msg->text().toInt());

		lineEdit_test_msg->setText(msg.isEmpty() || msg == lineEdit_fmt_msg->text() ? "" : msg);
	}

	pushButton_migrate->setEnabled(true);
}

void DialogMigration::on_pushButton_migrate_clicked()
{
	int current = 0;
	int migrated = 0;
	int invalid = 0;
	int duplicate = 0;
	int startline = lineEdit_startline->text().toInt();
	int elements = lineEdit_elements->text().toInt();
	int pos_date = lineEdit_pos_date->text().toInt();
	int pos_time = lineEdit_pos_time->text().toInt();
	int pos_sys = lineEdit_pos_sys->text().toInt();
	int pos_dia = lineEdit_pos_dia->text().toInt();
	int pos_bpm = lineEdit_pos_bpm->text().toInt();
	int pos_ihb = lineEdit_pos_ihb->text().toInt();
	int pos_mov = lineEdit_pos_mov->text().toInt();
	int pos_msg = lineEdit_pos_msg->text().toInt();
	QString delimiter = lineEdit_delimiter->text();
	QString fmt_date = lineEdit_fmt_date->text();
	QString fmt_time = lineEdit_fmt_time->text();
	QString fmt_ihb = lineEdit_fmt_ihb->text();
	QString fmt_mov = lineEdit_fmt_mov->text();
	QLocale locale(lineEdit_fmt_date_lng->text());

	foreach(QString line, lines)
	{
		if(current >= startline)
		{
			QStringList values = line.split(delimiter);

			if(values.count() == elements)
			{
				QDateTime dt;
				int sys, dia, bpm, ihb, mov;
				QString msg;

				if(lineEdit_fmt_time->text().isEmpty())
				{
					dt = locale.toDateTime(values.at(pos_date), fmt_date);
				}
				else
				{
					QDate date = locale.toDate(values.at(pos_date), fmt_date);
					QTime time = locale.toTime(values.at(pos_time), fmt_time);

					if(!date.isValid())
					{
						invalid++;
						continue;
					}

					if(!time.isValid())
					{
						invalid++;
						continue;
					}

					dt = QDateTime(date, time);
				}

				if(comboBox->currentIndex() == MEDISANA)
				{
					sys = values.at(pos_sys).split(" ").at(0).toInt();
					dia = values.at(pos_dia).split(" ").at(0).toInt();
					bpm = values.at(pos_bpm).split(" ").at(0).toInt();
				}
				else
				{
					sys = values.at(pos_sys).toInt();
					dia = values.at(pos_dia).toInt();
					bpm = values.at(pos_bpm).toInt();
				}

				ihb = values.at(pos_ihb) == lineEdit_fmt_ihb->text() ? 1 : 0;
				mov = values.at(pos_mov) == lineEdit_fmt_mov->text() ? 1 : 0;

				msg = values.at(pos_msg);

				if(dt.isValid() && (sys > 0 && sys < 256) && (dia > 0 && dia < 256) && (bpm > 0 && bpm < 256) && (ihb >= 0 && ihb <= 1) && (mov >= 0 && mov <= 1))
				{
					HEALTHDATA record;

					record.dts = dt.toMSecsSinceEpoch();
					record.sys = sys;
					record.dia = dia;
					record.bpm = bpm;
					record.ihb = ihb;
					record.mov = mov;
					record.inv = 0;
					record.msg = msg.isEmpty() || msg == lineEdit_fmt_msg->text() ? "" : msg;

					reinterpret_cast<MainWindow*>(parent())->database[toolButton_u2->isChecked()].append(record);

					migrated++;
				}
				else
				{
					invalid++;
					continue;
				}
			}
		}

		current++;
	}

	duplicate = reinterpret_cast<MainWindow*>(parent())->validateDB();
	reinterpret_cast<MainWindow*>(parent())->updateLCD();
	reinterpret_cast<MainWindow*>(parent())->calcView();

	QString msg(tr("Successfully migrated %n record(s) for user %1.", "", migrated).arg(1 + toolButton_u2->isChecked()));

	if(invalid)
	{
		msg.append("\n\n" + tr("Skipped %n invalid record(s)!", "", invalid));
	}

	if(duplicate)
	{
		msg.append("\n\n" + tr("Skipped %n duplicate record(s)!", "", duplicate));
	}

	hide();

	QMessageBox::information(this, APPNAME, msg);

	close();
}

void DialogMigration::keyPressEvent(QKeyEvent *ke)
{
	if(ke->key() == Qt::Key_F1)
	{
		reinterpret_cast<MainWindow*>(parent())->help->showHelp("02-04");
	}

	QDialog::keyPressEvent(ke);
}

void DialogMigration::reject()
{
	if(comboBox->currentIndex() == CUSTOM)
	{
		saveCustom();
	}

	QDialog::reject();
}
