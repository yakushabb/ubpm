<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl">
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="../DialogAbout.ui" line="14"/>
        <source>About UBPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="41"/>
        <source>Version</source>
        <translation type="unfinished">Wersja</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="53"/>
        <source>Universal Blood Pressure Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="91"/>
        <source>Qt Framework</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="129"/>
        <source>Operating System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="220"/>
        <source>Environment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="231"/>
        <source>Settings</source>
        <translation type="unfinished">Ustawienia</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="266"/>
        <source>Database</source>
        <translation type="unfinished">Baza danych</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="301"/>
        <source>Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="336"/>
        <source>Guides</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="371"/>
        <source>Languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="406"/>
        <source>Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="441"/>
        <source>Themes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="469"/>
        <source>Translators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="14"/>
        <source>The program is provided as is with no warranty of any kind, including the warranty of design, merchantability and fitness for a particular purpose.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="37"/>
        <source>Could not open &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="14"/>
        <source>Data Analysis</source>
        <translation>Analiza danych</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="24"/>
        <source>Query</source>
        <translation>Zapytanie</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="43"/>
        <location filename="../DialogAnalysis.cpp" line="209"/>
        <source>Results</source>
        <translation>Wyniki</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="82"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="91"/>
        <source>Time</source>
        <translation>Czas</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="100"/>
        <source>Systolic</source>
        <translation>Skurczowe</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="109"/>
        <source>Diastolic</source>
        <translation>Rozkurczowe</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="118"/>
        <source>P.Pressure</source>
        <translation>Ciśnienie tętna</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="127"/>
        <source>Heartrate</source>
        <translation>Tętno</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="136"/>
        <source>Irregular</source>
        <translation>Nieregularny</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="145"/>
        <source>Movement</source>
        <translation>Ruch</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="154"/>
        <source>Invisible</source>
        <translation>Niewidoczny</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="163"/>
        <source>Comment</source>
        <translation>Komentarz</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="193"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="51"/>
        <source>Could not create memory database!

%1</source>
        <translation>Nie mogę utworzyć bazy danych

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>No results for this query found!</source>
        <translation>Brak wyników dla tego zapytania!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Wynik</numerusform>
            <numerusform>Wyniki</numerusform>
            <numerusform>Wyników</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Dopasowanie</numerusform>
            <numerusform>Dopasowania</numerusform>
            <numerusform>Dopasowań</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Rekord</numerusform>
            <numerusform>Rekordy</numerusform>
            <numerusform>Rekordów</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>User %1</source>
        <translation>Użytkownik %1</translation>
    </message>
</context>
<context>
    <name>DialogDistribution</name>
    <message>
        <location filename="../DialogDistribution.ui" line="20"/>
        <source>Data Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="77"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="88"/>
        <source>Print</source>
        <translation type="unfinished">Drukuj</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="99"/>
        <source>Close</source>
        <translation type="unfinished">Zamknij</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogDistribution.cpp" line="18"/>
        <source>Record(s)</source>
        <translation type="unfinished">
            <numerusform>Rekord</numerusform>
            <numerusform>Rekordy</numerusform>
            <numerusform>Rekordów</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="119"/>
        <source>Low</source>
        <translation type="unfinished">Niskie</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="121"/>
        <source>Optimal</source>
        <translation type="unfinished">Optymalny</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="123"/>
        <source>Normal</source>
        <translation type="unfinished">Normalny</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="125"/>
        <source>High Normal</source>
        <translation type="unfinished">Podwyższone normalne</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="127"/>
        <source>Hyper 1</source>
        <translation type="unfinished">Wysokie 1</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="129"/>
        <source>Hyper 2</source>
        <translation type="unfinished">Wysokie 2</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="131"/>
        <source>Hyper 3</source>
        <translation type="unfinished">Wysokie 3</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="148"/>
        <source>SYS</source>
        <translation type="unfinished">SYS</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="160"/>
        <source>DIA</source>
        <translation type="unfinished">DIA</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="171"/>
        <source>Blood Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="210"/>
        <source>Athlete</source>
        <translation type="unfinished">Sportowiec</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="212"/>
        <source>Excellent</source>
        <translation type="unfinished">Doskonały</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="214"/>
        <source>Great</source>
        <translation type="unfinished">Bardzo dobry</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="216"/>
        <source>Good</source>
        <translation type="unfinished">Dobry</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="218"/>
        <source>Average</source>
        <translation type="unfinished">Przeciętne</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="220"/>
        <source>Below Average</source>
        <translation type="unfinished">Poniżej przeciętnego</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="222"/>
        <source>Poor</source>
        <translation type="unfinished">Słaby</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="239"/>
        <source>BPM</source>
        <translation type="unfinished">BPM</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="265"/>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="346"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation type="unfinished">Wygenerowane przez UBPM dla
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="347"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation type="unfinished">Darmowe i otwarto źródłowe
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="352"/>
        <source>%1 (Age: %2, Height: %3 cm, Weight: %4 kg, BMI: %5)</source>
        <translation type="unfinished">%1 (Wiek: %2, Wzrost: %3 cm, Waga: %4 kg, BMI: %5)</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="352"/>
        <source>User %1</source>
        <translation type="unfinished">Użytkownik %1</translation>
    </message>
</context>
<context>
    <name>DialogDonation</name>
    <message>
        <location filename="../DialogDonation.ui" line="14"/>
        <source>Donation</source>
        <translation type="unfinished">Darowizna</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="67"/>
        <source>Copy E-Mail and open Amazon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="154"/>
        <source>Open PayPal (+)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="171"/>
        <source>Open PayPal (-)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="46"/>
        <source>If you have an Amazon account, you can send money by gift card.

Click the button below to open the Amazon website in your browser and

- sign in to your account
- select or enter the amount you would like to send
- select e-mail delivery
- paste the e-mail address as the recipient
- add a message
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="124"/>
        <source>If you have a PayPal account, you can send money using the &quot;Friends and Family&quot; feature.

Open your PayPal app and

- search for @LazyT (Thomas Löwe)
- enter the amount you would like to send
- add a message
- complete the donation

You can also click the first button below to do this in your browser without using the app.

If you do not have a PayPal account but do have a credit/debit card click the second button below to go to the PayPal donation page and

- select onetime or repeating donation
- select or enter the amount you would like to send
- optionally check the box to cover the fees
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="230"/>
        <source>If you are in the Euro Payments Area, you can transfer money from your bank account.

Click the buttons below to copy the name/IBAN/BIC to the clipboard and

- sign in to your bank account
- paste the name/IBAN/BIC into the transfer form
- enter the amount you would like to send
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="251"/>
        <source>Copy Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="268"/>
        <source>Copy IBAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="285"/>
        <source>Copy BIC</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Podręcznik</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="61"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>%1 Nie znaleziono podręcznika, pokażę za to wersję angielską.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="53"/>
        <source>%1 guide not found!</source>
        <translation>%1 Nie znaleziono podręcznika!</translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation>Przeniesienie danych</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation>Źródło</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation>Otwórz źródło danych</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation>Parametry</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation>Format daty</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation>Nieregularne wykrywanie</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation>Linia startowa</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation>Elementy na linię</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation>Ogranicznik</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation>Format czasu</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation>Wykrywanie ruchu</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation>Język daty</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation>Pozycja</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation>Nieregularny</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation>Skurczowe</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation>Tętno</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation>Ruch</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation>Zignoruj komentarz</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation>Czas</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation>Rozkurczowe</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation>Komentarz</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation>Wynik</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="786"/>
        <source>Migrate User 1</source>
        <translation>Transfer danych Użytkownika 1</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="815"/>
        <source>Migrate User 2</source>
        <translation>Transfer danych Użytkownika 2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="841"/>
        <source>Copy to Custom</source>
        <translation>Kopiuj jako domyślne</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="861"/>
        <source>Select predefined settings</source>
        <translation>Zaznacz domyślne ustawienia</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="895"/>
        <source>Custom</source>
        <translation>Niestandardowe</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="906"/>
        <source>Test Data Migration</source>
        <translation>Test transferu danych</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="920"/>
        <source>Start Data Migration</source>
        <translation>Rozpocznij transfer danych</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>Choose Data Source for Migration</source>
        <translation>Zakończ transfer danych</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>CSV File (*.csv)</source>
        <translation>Plik CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%L1 Bytes</source>
        <translation>%L1 Bajtów</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="305"/>
        <source>%1 Lines</source>
        <translation>%1 Linii</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="319"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Nie mogę otworzyć &quot;%1&quot;!

Powód: %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="341"/>
        <source>Startline can&apos;t be empty.</source>
        <translation>Pierwsza linia nie może być pusta.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="347"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation>Numer pierwszego rekordu musi być niższy od ilości rekordów.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="355"/>
        <source>Line %1 = %2</source>
        <translation>Wiersz %1 = %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="360"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation>Separator nie może być pusty.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="367"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation>Linia początkowa nie zawiera ogranicznika „%1”.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="378"/>
        <source>Elements can&apos;t be empty.</source>
        <translation>Wiersz nie może być pusty.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="384"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation>Elementów nie może być mniej niż 4.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="390"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation>Elementy nie pasują (znaleziono %1).</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="397"/>
        <source>Date format can&apos;t be empty.</source>
        <translation>Format daty nie może być pusty.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="403"/>
        <source>Single date format must also contain a time format.</source>
        <translation>Pojedynczy format daty musi zawierać również format czasu.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="410"/>
        <source>Date position can&apos;t be empty.</source>
        <translation>Pozycja daty nie może być pusta.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="416"/>
        <source>Date position must be smaller than elements.</source>
        <translation>Pozycja daty musi być mniejsza od elementów.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="423"/>
        <source>Time position requires also a time parameter.</source>
        <translation>Pozycja czasowa wymaga również parametru czasu.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="429"/>
        <source>Time format requires also a time position.</source>
        <translation>Format czasu wymaga również pozycji czasowej.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="435"/>
        <source>Time position must be smaller than elements.</source>
        <translation>Pozycja czasowa musi być mniejsza od elementów.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="442"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation>Wartość ciśnienia skurczowego nie może być pusta.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="448"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation>Pozycja skurczowa musi być mniejsza od elementów.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="455"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation>Pozycja rozkurczowa nie może być pusta.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="461"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation>Pozycja rozkurczowa musi być mniejsza od elementów.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="468"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation>Pozycja tętna nie może być pusta.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="474"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation>Pozycja tętna musi być mniejsza od elementów.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="481"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation>Nieregularna pozycja musi być mniejsza od elementów.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="487"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation>Nieregularna pozycja wymaga również nieregularnego parametru.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="494"/>
        <source>Movement position must be smaller than elements.</source>
        <translation>Pozycja ruchu musi być mniejsza od elementów.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="500"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation>Pozycja ruchu wymaga również parametru ruchu.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="507"/>
        <source>Comment position must be smaller than elements.</source>
        <translation>Pozycja komentarza musi być mniejsza od innych elementów.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="523"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation>Pozycje nie mogą istnieć podwójnie.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="539"/>
        <source>Date format is invalid.</source>
        <translation>Format daty jest nieprawidłowy.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="546"/>
        <source>Time format is invalid.</source>
        <translation>Format czasu jest nieprawidłowy.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="556"/>
        <source>Date/Time format is invalid.</source>
        <translation>Format daty/godziny jest nieprawidłowy.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="747"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation>
            <numerusform>Pomyślnie przeniesiono %n rekord dla użytkownika %1.</numerusform>
            <numerusform>Pomyślnie przeniesiono %n rekordy %n dla użytkownika %1.</numerusform>
            <numerusform>Pomyślnie przeniesiono %n rekordów dla użytkownika %1.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="751"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Pominięto %n nieprawidłowy rekord!</numerusform>
            <numerusform>Pominięto %n nieprawidłowe rekordy!</numerusform>
            <numerusform>Pominięto %n nieprawidłowych rekordów!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="756"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Pominięto %n zduplikowany rekord!</numerusform>
            <numerusform>Pominięto %n zduplikowane rekordy!</numerusform>
            <numerusform>Pominięto %n zduplikowanych rekordów!</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Ręczny zapis</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Zapis danych</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Dodaj rekord dla % 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Wybierz datę i godzinę</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>Wpisz SYS</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation>Wpisz DIA</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation>Wpisz BPM</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Nieregularne bicie serca</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>Ruch</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Dodaj opcjonalny komentarz</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Pokaż komunikat o pomyślnym utworzeniu / usunięciu / zmodyfikowaniu rekordu</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Utwórz</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Użytkownik 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Użytkownik 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="34"/>
        <location filename="../DialogRecord.cpp" line="187"/>
        <source>Modify</source>
        <translation>Modyfikuj</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="132"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Rekord danych nie mógł zostać usunięty!

Wpis dla tej daty i godziny nie istnieje.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="138"/>
        <source>Data record successfully deleted.</source>
        <translation>Rekord danych został pomyślnie usunięty.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="150"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Najpierw wprowadź prawidłową wartość dla &quot;SYS&quot;!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="160"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Najpierw wprowadź prawidłową wartość dla &quot;DIA&quot;!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="169"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Najpierw wprowadź prawidłową wartość dla &quot;BPM&quot;!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="191"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Rekord danych nie mógł zostać zmieniony!

Wpis dla tej daty i godziny nie istnieje.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="197"/>
        <source>Data Record successfully modified.</source>
        <translation>Rekord danych został pomyślnie zmodyfikowany.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="206"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>Rekord danych nie mógł zostać utworzony!

Wpis dla tej daty i godziny już istnieje.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="212"/>
        <source>Data Record successfully created.</source>
        <translation>Rekord danych został pomyślnie utworzony.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="161"/>
        <source>Choose Database Location</source>
        <translation>Wybierz lokalizację bazy danych</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="171"/>
        <source>Choose Database Backup Location</source>
        <translation>Wybierz lokalizację kopii zapasowej bazy danych</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="308"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation>Wybrana baza danych już istnieje.

Wybierz preferowaną akcję.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="310"/>
        <source>Overwrite</source>
        <translation>Nadpisz</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="311"/>
        <source>Merge</source>
        <translation>Połącz</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="312"/>
        <source>Swap</source>
        <translation>Zamień</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="314"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation>Nadpisz:

Nadpisuje wybraną bazę danych bieżącą bazą danych.

Połącz:

Scala wybraną bazę danych z aktualną bazą danych.

Zamień:

Usuwa bieżącą bazę danych i używa wybranej bazy danych.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="322"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation>Bieżąca baza danych jest pusta.

Wybrana baza danych zostanie skasowana przy wyjściu, jeśli nie zostaną dodane żadne dane.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="352"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>Szyfrowanie SQL nie może być włączone bez hasła i będzie wyłączone!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="359"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation>Kopia zapasowa bazy danych powinna znajdować się na innym dysku twardym, partycji lub katalogu.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="364"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Proszę wprowadzić prawidłowe wartości dla dodatkowych informacji użytkownika 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="371"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Proszę wprowadzić prawidłowe wartości dla dodatkowych informacji użytkownika 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="378"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>Wprowadzony wiek nie pasuje do wybranej grupy wiekowej dla użytkownika 1!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="385"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>Wprowadzony wiek nie pasuje do wybranej grupy wiekowej dla użytkownika 2!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="393"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation>Włącz symbole lub linie dla wykresu!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="400"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Podaj poprawny adres e-mail!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="407"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Podaj temat e-maila!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="414"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>Wiadomość e-mail musi zawierać $CHART, $TABLE i/lub $STATS!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="525"/>
        <source>User 1</source>
        <translation>Użytkownik 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="539"/>
        <source>User 2</source>
        <translation>Użytkownik 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="652"/>
        <source>Blood Pressure Report</source>
        <translation>Raport pomiaru ciśnienia</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="653"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Drogi doktorze House,

W załączniku znajdują się dane mojego ciśnienia krwi, które uzyskałem w tym miesiącu.

Pozdrawiam,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="695"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Przerwać konfigurację i odrzucić wszystkie zmiany?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Baza danych</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="52"/>
        <source>Location</source>
        <translation>Lokalizacja</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="58"/>
        <source>Current Location</source>
        <translation>Aktualna lokalizacja</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="74"/>
        <source>Change Location</source>
        <translation>Zmień lokalizację</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="100"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Szyfruj za pomocą SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="103"/>
        <source>Encryption</source>
        <translation>Szyfrowanie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="115"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="128"/>
        <source>Show Password</source>
        <translation>Pokaż hasło</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="282"/>
        <source>User</source>
        <translation>Użytkownik</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="317"/>
        <location filename="../DialogSettings.ui" line="580"/>
        <source>Male</source>
        <translation>Mężczyzna</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="346"/>
        <location filename="../DialogSettings.ui" line="609"/>
        <source>Female</source>
        <translation>Kobieta</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="442"/>
        <location filename="../DialogSettings.ui" line="714"/>
        <source>Name</source>
        <translation>Imię</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <location filename="../DialogSettings.ui" line="574"/>
        <source>Mandatory Information</source>
        <translation>Informacje obowiązkowe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="372"/>
        <location filename="../DialogSettings.ui" line="644"/>
        <source>Age Group</source>
        <translation>Grupa wiekowa</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="458"/>
        <location filename="../DialogSettings.ui" line="730"/>
        <source>Additional Information</source>
        <translation>Informacje dodatkowe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Height</source>
        <translation>Wzrost</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="547"/>
        <location filename="../DialogSettings.ui" line="819"/>
        <source>Weight</source>
        <translation>Waga</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="844"/>
        <source>Device</source>
        <translation>Urządzenie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="850"/>
        <location filename="../DialogSettings.cpp" line="70"/>
        <source>Import Plugins [ %1 ]</source>
        <translation>Importuj wtyczki [ %1 ]</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="898"/>
        <source>Open Website</source>
        <translation>Otwórz stronę internetową</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="911"/>
        <source>Maintainer</source>
        <translation>Opiekun</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="939"/>
        <source>Version</source>
        <translation>Wersja</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="946"/>
        <source>Send E-Mail</source>
        <translation>Wyślij e-mail</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="959"/>
        <source>Model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="966"/>
        <source>Producer</source>
        <translation>Producent</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="983"/>
        <source>Chart</source>
        <translation>Wykres</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1110"/>
        <source>X-Axis Range</source>
        <translation>Zakres osi X</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1116"/>
        <source>Dynamic Scaling</source>
        <translation>Skalowanie dynamiczne</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1097"/>
        <source>Healthy Ranges</source>
        <translation>Zdrowe zakresy</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1014"/>
        <source>Symbols</source>
        <translation>Symbole</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1091"/>
        <source>Colored Areas</source>
        <translation>Kolorowe obszary</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="863"/>
        <source>Please choose Device Plugin…</source>
        <translation>Wybierz wtyczkę urządzenia…</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1238"/>
        <location filename="../DialogSettings.ui" line="1513"/>
        <source>Systolic</source>
        <translation>Skurczowe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1326"/>
        <location filename="../DialogSettings.ui" line="1601"/>
        <source>Diastolic</source>
        <translation>Rozkurczowe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1414"/>
        <location filename="../DialogSettings.ui" line="1689"/>
        <source>Heartrate</source>
        <translation>Tętno</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1786"/>
        <source>Table</source>
        <translation>Tabela</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1871"/>
        <location filename="../DialogSettings.ui" line="2054"/>
        <source>Systolic Warnlevel</source>
        <translation>Skurczowy poziom ostrzeżenia</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1817"/>
        <location filename="../DialogSettings.ui" line="2114"/>
        <source>Diastolic Warnlevel</source>
        <translation>Rozkurczowy poziom ostrzeżenia</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="153"/>
        <source>Automatic Backup</source>
        <translation>Automatyczna kopia zapasowa</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="156"/>
        <source>Backup</source>
        <translation>Kopia zapasowa</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="173"/>
        <source>Current Backup Location</source>
        <translation>Aktualna lokalizacja kopii zapasowej</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="186"/>
        <source>Change Backup Location</source>
        <translation>Zmień lokalizację kopii zapasowej</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="213"/>
        <source>Backup Mode</source>
        <translation>Tryb kopii zapasowej</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="223"/>
        <source>Daily</source>
        <translation>Dziennie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="232"/>
        <source>Weekly</source>
        <translation>Tygodniowo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="241"/>
        <source>Monthly</source>
        <translation>Miesięcznie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="253"/>
        <source>Keep Copies</source>
        <translation>Zachowaj kopie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="486"/>
        <location filename="../DialogSettings.ui" line="758"/>
        <source>Birthday</source>
        <translation>Urodziny</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1023"/>
        <source>Color</source>
        <translation>Kolor</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1047"/>
        <location filename="../DialogSettings.ui" line="1075"/>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1129"/>
        <source>Lines</source>
        <translation>Linie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1138"/>
        <location filename="../DialogSettings.ui" line="1166"/>
        <source>Width</source>
        <translation>Szerokość</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1195"/>
        <source>Show Heartrate</source>
        <translation>Pokaż tętno</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1201"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Pokaż tętno w widoku wykresu</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1211"/>
        <source>Print Heartrate</source>
        <translation>Drukuj tętno</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1217"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Drukuj tętno na osobnym arkuszu</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1931"/>
        <location filename="../DialogSettings.ui" line="2168"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Poziom ostrzegania o ciśnieniu tętna</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1985"/>
        <location filename="../DialogSettings.ui" line="2222"/>
        <source>Heartrate Warnlevel</source>
        <translation>Poziom ostrzegania o tętnie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2287"/>
        <source>Statistic</source>
        <translation>Statystyki</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2293"/>
        <source>Bar Type</source>
        <translation>Rodzaj słupków</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2299"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Pokaż medianę zamiast średnich słupków</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2309"/>
        <source>Legend Type</source>
        <translation>Typ legendy</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2315"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Pokaż wartości jako legendę zamiast opisów</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2330"/>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2338"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2357"/>
        <source>Subject</source>
        <translation>Temat</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2375"/>
        <source>Message</source>
        <translation>Wiadomość</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2396"/>
        <source>Plugin</source>
        <translation>Wtyczka</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2402"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2408"/>
        <source>Log Device Communication to File</source>
        <translation>Zapisywanie dziennika komunikacji z urządzeniem do pliku</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2415"/>
        <source>Import Measurements automatically</source>
        <translation>Importuj pomiary automatycznie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2425"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2431"/>
        <source>Discover Device automatically</source>
        <translation>Wykryj urządzenie automatycznie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2438"/>
        <source>Connect Device automatically</source>
        <translation>Połącz urządzenie automatycznie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2453"/>
        <source>Update</source>
        <translation>Aktualizuj</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2459"/>
        <source>Autostart</source>
        <translation>Autostart</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Sprawdź aktualizacje podczas uruchamiania programu</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2478"/>
        <source>Notification</source>
        <translation>Powiadomienie</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2484"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Zawsze pokazuj wynik po sprawdzeniu aktualizacji online</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2500"/>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2517"/>
        <source>Reset</source>
        <translation>Resetuj</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2534"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Aktualizacja online</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Dostępna wersja</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Rozmiar pliku aktualizacji</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Zainstalowana wersja</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Szczegóły</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="181"/>
        <location filename="../DialogUpdate.ui" line="202"/>
        <source>Download</source>
        <translation>Pobierz</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="222"/>
        <source>Ignore</source>
        <translation>Ignoruj</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="183"/>
        <source>No new version found.</source>
        <translation>Nie znaleziono nowej wersji.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="42"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!!! OSTRZEŻENIE SSL - PRZECZYTAJ UWAŻNIE !!!

Problem z połączeniem sieciowym:

%1
Czy mimo to chcesz kontynuować?</numerusform>
            <numerusform>!!! OSTRZEŻENIE SSL - PRZECZYTAJ UWAŻNIE !!!

Problem z połączeniami sieciowymi:

%1
Czy mimo to chcesz kontynuować?</numerusform>
            <numerusform>!!! OSTRZEŻENIE SSL - PRZECZYTAJ UWAŻNIE !!!

Problem z połączeniami sieciowymi:

%1
Czy mimo to chcesz kontynuować?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Downloading update failed!

%1</source>
        <translation>Pobieranie aktualizacji nie powiodło się!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Checking update failed!

%1</source>
        <translation>Sprawdzanie aktualizacji nie powiodło się!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="156"/>
        <source>Unexpected response from update server!</source>
        <translation>Nieoczekiwana odpowiedź z serwera aktualizacji!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="173"/>
        <source>*%1 not found</source>
        <translation>Nie znaleziono *%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="203"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>Aktualizacja nie ma oczekiwanego rozmiaru!

%L1 : %L2

Pobierz ponownie…</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="207"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Aktualizacja zapisana do %1.

Czy chcesz uruchomić nową wersję?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="220"/>
        <source>Could not start new version!</source>
        <translation>Nie udało się uruchomić nowej wersji!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>The program was installed by Flatpak.

Please update using the Flatpak internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="253"/>
        <source>The program was installed by Snap.

Please update using the Snap internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="259"/>
        <source>The program was installed from Windows store.

Please update using the store internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="265"/>
        <source>The program was installed from distribution.

Please update using the operating systems internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="271"/>
        <source>The program was installed from source code.

Please update the sources, rebuild and reinstall manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="297"/>
        <source>Really abort download?</source>
        <translation>Czy na pewno chcesz przerwać pobieranie?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="227"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Nie udało się zapisać aktualizacji do %1!

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Uniwersalny Menadżer Ciśnienia Krwi</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation>Pokaż poprzedni okres</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation>Pokaż następny okres</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Widok wykresu</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Widok tabeli</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Czas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="159"/>
        <location filename="../MainWindow.cpp" line="4694"/>
        <source>Systolic</source>
        <translation>Ciśnienie skurczowe</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="160"/>
        <location filename="../MainWindow.cpp" line="4695"/>
        <source>Diastolic</source>
        <translation>Ciśnienie rozkurczowe</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Ciśnienie tętna</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Nieregularne</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Ruch</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Niewidoczne</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation>Komentarz</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Widok statystyk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>Kwadrans</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>Pół godziny</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Godzina</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>¼ dnia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>Pół dnia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Codziennie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>3 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Weekly</source>
        <translation>Tygodniowo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Monthly</source>
        <translation>Miesięcznie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>Quarterly</source>
        <translation>Kwartalnie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>½ Yearly</source>
        <translation>Półrocznie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="765"/>
        <source>Yearly</source>
        <translation>Roczne</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="825"/>
        <source>Last 3 Days</source>
        <translation type="unfinished">Ostatnie 28 dni {3 ?}</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="857"/>
        <source>Last 7 Days</source>
        <translation>Ostatni tydzień</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="889"/>
        <source>Last 14 Days</source>
        <translation>Ostatnie dwa tygodnie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="921"/>
        <source>Last 21 Days</source>
        <translation>Ostatnie 21 dni</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="953"/>
        <source>Last 28 Days</source>
        <translation>Ostatnie 28 dni</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="985"/>
        <source>Last 3 Months</source>
        <translation>Ostatnie 3 miesiące</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1017"/>
        <source>Last 6 Months</source>
        <translation>Ostatnie 6 miesięcy</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1049"/>
        <source>Last 9 Months</source>
        <translation>Ostatnie 9 miesięcy</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1081"/>
        <source>Last 12 Months</source>
        <translation>Ostatni rok</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1113"/>
        <source>All Records</source>
        <translation>Wszystkie Pomiary</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1160"/>
        <source>File</source>
        <translation>Plik</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1164"/>
        <source>Print</source>
        <translation>Drukuj</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1186"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1203"/>
        <source>Configuration</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1207"/>
        <source>Theme</source>
        <translation>Motyw</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1216"/>
        <source>Language</source>
        <translation>Język</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1225"/>
        <source>Style</source>
        <translation>Styl</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1234"/>
        <source>Charts</source>
        <translation>Wykresy</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1259"/>
        <source>Database</source>
        <translation>Baza danych</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1263"/>
        <source>Import</source>
        <translation>Importuj</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1275"/>
        <source>Export</source>
        <translation>Eksport</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1290"/>
        <source>Clear</source>
        <translation>Wyczyść</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Quit</source>
        <translation>Wyjdź</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1382"/>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>Quit Program</source>
        <translation>Zakończ</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1397"/>
        <source>About</source>
        <translation>Informacje</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <location filename="../MainWindow.ui" line="1403"/>
        <source>About Program</source>
        <translation>O Programie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <source>Guide</source>
        <translation>Podręcznik</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <location filename="../MainWindow.ui" line="1418"/>
        <source>Show Guide</source>
        <translation>Pokaż podręcznik</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1427"/>
        <source>Update</source>
        <translation>Aktualizacja</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1430"/>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>Check Update</source>
        <translation>Sprawdź aktualizację</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1592"/>
        <source>To PDF</source>
        <translation>Do PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <source>Export To PDF</source>
        <translation>Eksportuj do PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1607"/>
        <source>Migration</source>
        <translation>Migracja</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <source>Migrate from Vendor</source>
        <translation>Transfer danych z urządzenia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1857"/>
        <source>Translation</source>
        <translation>Tłumaczenie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1860"/>
        <location filename="../MainWindow.ui" line="1863"/>
        <source>Contribute Translation</source>
        <translation>Pomóż w tłumaczeniu</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1872"/>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1875"/>
        <location filename="../MainWindow.ui" line="1878"/>
        <source>Send E-Mail</source>
        <translation>Wyślij e-mail</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1890"/>
        <source>Icons</source>
        <translation>Ikony</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1893"/>
        <location filename="../MainWindow.ui" line="1896"/>
        <source>Change Icon Color</source>
        <translation>Zmień Kolor Ikon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1907"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1910"/>
        <location filename="../MainWindow.ui" line="1913"/>
        <source>System Colors</source>
        <translation>Kolory systemu</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1921"/>
        <source>Light</source>
        <translation>Jasny</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1924"/>
        <location filename="../MainWindow.ui" line="1927"/>
        <source>Light Colors</source>
        <translation>Jasne kolory</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1935"/>
        <source>Dark</source>
        <translation>Ciemny</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1938"/>
        <location filename="../MainWindow.ui" line="1941"/>
        <source>Dark Colors</source>
        <translation>Ciemne kolory</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1950"/>
        <source>Donation</source>
        <translation>Darowizna</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1965"/>
        <source>Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1968"/>
        <location filename="../MainWindow.ui" line="1971"/>
        <source>Show Distribution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1983"/>
        <location filename="../MainWindow.ui" line="1986"/>
        <source>Save</source>
        <translation type="unfinished">Zapisz</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1995"/>
        <source>Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1998"/>
        <location filename="../MainWindow.ui" line="2001"/>
        <source>Read Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="161"/>
        <location filename="../MainWindow.cpp" line="4696"/>
        <source>Heartrate</source>
        <translation>Tętno</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1953"/>
        <location filename="../MainWindow.ui" line="1956"/>
        <source>Make Donation</source>
        <translation>Przekaż darowiznę</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1445"/>
        <source>Bugreport</source>
        <translation>Zgłaszanie błędów</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1448"/>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>Send Bugreport</source>
        <translation>Wyślij raport o błędzie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1460"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1463"/>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>Change Settings</source>
        <translation>Zmień ustawienia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>From Device</source>
        <translation>Z urządzenia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1484"/>
        <location filename="../MainWindow.ui" line="1487"/>
        <source>Import From Device</source>
        <translation>Importuj z urządzenia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1499"/>
        <source>From File</source>
        <translation>Z pliku</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1502"/>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Import From File</source>
        <translation>Importuj z pliku</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1514"/>
        <source>From Input</source>
        <translation>Ręcznie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1517"/>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Import From Input</source>
        <translation>Wprowadź dane ręcznie</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1529"/>
        <source>To CSV</source>
        <translation>Do CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1532"/>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Export To CSV</source>
        <translation>Eksportuj do CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1547"/>
        <source>To XML</source>
        <translation>Do XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1550"/>
        <location filename="../MainWindow.ui" line="1553"/>
        <source>Export To XML</source>
        <translation>Eksportuj do XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1562"/>
        <source>To JSON</source>
        <translation>Do JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1565"/>
        <location filename="../MainWindow.ui" line="1568"/>
        <source>Export To JSON</source>
        <translation>Eksportuj do JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1577"/>
        <source>To SQL</source>
        <translation>Do SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <source>Export To SQL</source>
        <translation>Eksportuj do SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1625"/>
        <source>Print Chart</source>
        <translation>Drukuj wykres</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1628"/>
        <location filename="../MainWindow.ui" line="1631"/>
        <source>Print Chart View</source>
        <translation>Wydrukuj widok wykresu</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1640"/>
        <source>Print Table</source>
        <translation>Drukuj tabelę</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1643"/>
        <location filename="../MainWindow.ui" line="1646"/>
        <source>Print Table View</source>
        <translation>Wydrukuj widok tabeli</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1655"/>
        <source>Print Statistic</source>
        <translation>Drukuj statystyki</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1658"/>
        <location filename="../MainWindow.ui" line="1661"/>
        <source>Print Statistic View</source>
        <translation>Wydrukuj widok statystyk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1670"/>
        <source>Preview Chart</source>
        <translation>Podgląd wykresu</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1673"/>
        <location filename="../MainWindow.ui" line="1676"/>
        <source>Preview Chart View</source>
        <translation>Pokaż podgląd wykresu</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <source>Preview Table</source>
        <translation>Podgląd tabeli</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Preview Table View</source>
        <translation>Pokaż podgląd tabeli</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1700"/>
        <source>Preview Statistic</source>
        <translation>Podgląd statystyk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Preview Statistic View</source>
        <translation>Pokaż podgląd statystyk</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1715"/>
        <location filename="../MainWindow.ui" line="1718"/>
        <location filename="../MainWindow.ui" line="1721"/>
        <source>Clear All</source>
        <translation>Wyczyść wszystko</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1733"/>
        <location filename="../MainWindow.ui" line="1736"/>
        <location filename="../MainWindow.ui" line="1739"/>
        <source>Clear User 1</source>
        <translation>Wyczyść dane Użytkownika 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1748"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <location filename="../MainWindow.ui" line="1754"/>
        <source>Clear User 2</source>
        <translation>Wyczyść dane Użytkownika 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1814"/>
        <source>Analysis</source>
        <translation>Analiza</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1817"/>
        <location filename="../MainWindow.ui" line="1820"/>
        <source>Analyze Records</source>
        <translation>Analiza Danych</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1839"/>
        <location filename="../MainWindow.ui" line="1842"/>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>Time Mode</source>
        <translation>Tryb czasu</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="108"/>
        <location filename="../MainWindow.cpp" line="109"/>
        <location filename="../MainWindow.cpp" line="4664"/>
        <location filename="../MainWindow.cpp" line="4665"/>
        <source>Records For Selected User</source>
        <translation>Pomiary dla wybranego użytkownika</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="116"/>
        <location filename="../MainWindow.cpp" line="117"/>
        <location filename="../MainWindow.cpp" line="4667"/>
        <location filename="../MainWindow.cpp" line="4668"/>
        <source>Select Date &amp; Time</source>
        <translation>Wybierz datę i godzinę</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="169"/>
        <location filename="../MainWindow.cpp" line="4698"/>
        <source>Systolic - Value Range</source>
        <translation>Ciśnienie skurczowe - zakres wartości</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="170"/>
        <location filename="../MainWindow.cpp" line="4699"/>
        <source>Diastolic - Value Range</source>
        <translation>Ciśnienie rozkurczowe - zakres wartości</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="173"/>
        <location filename="../MainWindow.cpp" line="4701"/>
        <source>Systolic - Target Area</source>
        <translation>Ciśnienie skurczowe - obszar docelowy</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="174"/>
        <location filename="../MainWindow.cpp" line="4702"/>
        <source>Diastolic - Target Area</source>
        <translation>Ciśnienie rozkurczowe - obszar docelowy</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1679"/>
        <source>Athlete</source>
        <translation>Sportowiec</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1680"/>
        <source>Excellent</source>
        <translation>Doskonały</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1680"/>
        <source>Optimal</source>
        <translation>Optymalny</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1681"/>
        <source>Great</source>
        <translation>Bardzo dobry</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1682"/>
        <source>Good</source>
        <translation>Dobry</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1681"/>
        <source>Normal</source>
        <translation>Normalny</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1775"/>
        <location filename="../MainWindow.ui" line="1778"/>
        <location filename="../MainWindow.ui" line="1799"/>
        <location filename="../MainWindow.ui" line="1802"/>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="4382"/>
        <location filename="../MainWindow.cpp" line="4383"/>
        <location filename="../MainWindow.cpp" line="4384"/>
        <location filename="../MainWindow.cpp" line="4385"/>
        <location filename="../MainWindow.cpp" line="4728"/>
        <location filename="../MainWindow.cpp" line="4729"/>
        <location filename="../MainWindow.cpp" line="4730"/>
        <location filename="../MainWindow.cpp" line="4731"/>
        <source>Switch To %1</source>
        <translation>Przejdź do %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="600"/>
        <location filename="../MainWindow.cpp" line="4382"/>
        <location filename="../MainWindow.cpp" line="4383"/>
        <location filename="../MainWindow.cpp" line="4728"/>
        <location filename="../MainWindow.cpp" line="4729"/>
        <source>User 1</source>
        <translation>Użytkownik 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="610"/>
        <location filename="../MainWindow.cpp" line="4384"/>
        <location filename="../MainWindow.cpp" line="4385"/>
        <location filename="../MainWindow.cpp" line="4730"/>
        <location filename="../MainWindow.cpp" line="4731"/>
        <source>User 2</source>
        <translation>Użytkownik 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="171"/>
        <location filename="../MainWindow.cpp" line="4700"/>
        <source>Heartrate - Value Range</source>
        <translation>Tętno - zakres wartości</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="175"/>
        <location filename="../MainWindow.cpp" line="4703"/>
        <source>Heartrate - Target Area</source>
        <translation>Tętno - obszar docelowy</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="341"/>
        <source>No active device plugin, auto import aborted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="358"/>
        <source>Some plugins will not work without Bluetooth permission.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="461"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation>Nie można utworzyć katalogu kopii zapasowej.

Proszę sprawdzić ustawienia lokalizacji kopii zapasowej.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="490"/>
        <location filename="../MainWindow.cpp" line="513"/>
        <source>Could not backup database:

%1</source>
        <translation>Nie udało się wykonać kopii zapasowej bazy danych:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="503"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation>Nie udało się usunąć nieaktualnej kopii zapasowej:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="530"/>
        <source>Could not register icons.</source>
        <translation>Nie udało się zarejestrować Ikon.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="663"/>
        <source>Blood Pressure Report</source>
        <translation>Raport pomiaru ciśnienia</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1191"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1196"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1679"/>
        <source>Low</source>
        <translation>Niskie</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1682"/>
        <source>High Normal</source>
        <translation>Podwyższone normalne</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1683"/>
        <location filename="../MainWindow.cpp" line="2160"/>
        <location filename="../MainWindow.cpp" line="4707"/>
        <location filename="../MainWindow.cpp" line="4711"/>
        <location filename="../MainWindow.cpp" line="4715"/>
        <location filename="../MainWindow.h" line="164"/>
        <location filename="../MainWindow.h" line="168"/>
        <location filename="../MainWindow.h" line="172"/>
        <source>Average</source>
        <translation>Przeciętne</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1683"/>
        <source>Hyper 1</source>
        <translation>Wysokie 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1684"/>
        <source>Below Average</source>
        <translation>Poniżej przeciętnego</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1684"/>
        <source>Hyper 2</source>
        <translation>Wysokie 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1685"/>
        <source>Poor</source>
        <translation>Słaby</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1685"/>
        <source>Hyper 3</source>
        <translation>Wysokie 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1830"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>Wykrywanie urządzenia wejściowego &quot;%1&quot; błąd!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1847"/>
        <location filename="../MainWindow.cpp" line="1874"/>
        <location filename="../MainWindow.cpp" line="4674"/>
        <source>Switch Language to %1</source>
        <translation>Zmień język na %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1900"/>
        <location filename="../MainWindow.cpp" line="1915"/>
        <location filename="../MainWindow.cpp" line="4682"/>
        <source>Switch Theme to %1</source>
        <translation>Zmień motyw na %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1940"/>
        <location filename="../MainWindow.cpp" line="1960"/>
        <location filename="../MainWindow.cpp" line="4690"/>
        <source>Switch Style to %1</source>
        <translation>Zmień styl na %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2031"/>
        <location filename="../MainWindow.cpp" line="2056"/>
        <source>Edit record</source>
        <translation>Edytuj rekord</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2160"/>
        <location filename="../MainWindow.cpp" line="4708"/>
        <location filename="../MainWindow.cpp" line="4712"/>
        <location filename="../MainWindow.cpp" line="4716"/>
        <location filename="../MainWindow.h" line="165"/>
        <location filename="../MainWindow.h" line="169"/>
        <location filename="../MainWindow.h" line="173"/>
        <source>Median</source>
        <translation>Średnia</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2162"/>
        <source>Click to swap Average and Median</source>
        <translation>Kliknij aby przełączyć między przeciętną, średnią</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2257"/>
        <source>Click to swap Legend and Label</source>
        <translation>Kliknij aby przełączyć między opisem, a etykietą</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2334"/>
        <source>No records to preview for selected time range!</source>
        <translation>Brak wyników dla wybranego zakresu czasu!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2366"/>
        <source>No records to print for selected time range!</source>
        <translation>Brak wyników do wydrukowania z wybranego zakresu czasu.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2412"/>
        <location filename="../MainWindow.cpp" line="2462"/>
        <location filename="../MainWindow.cpp" line="2538"/>
        <location filename="../MainWindow.cpp" line="2669"/>
        <source>User %1</source>
        <translation>Użytkownik %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>DATE</source>
        <translation>DATA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>TIME</source>
        <translation>CZAS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>BPM</source>
        <translation>BPM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>IHB</source>
        <translation>IHB</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>COMMENT</source>
        <translation>KOMENTARZ</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>PPR</source>
        <translation>PPR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>MOV</source>
        <translation>MOV</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2734"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>Nie udało się utworzyć wiadomości e-mail, ponieważ generowanie base64 dla załącznika &quot;%1&quot; nie powiodło się!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3495"/>
        <location filename="../MainWindow.cpp" line="4290"/>
        <source>Chart</source>
        <translation>Wykres</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3495"/>
        <location filename="../MainWindow.cpp" line="4307"/>
        <source>Table</source>
        <translation>Tabela</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3495"/>
        <location filename="../MainWindow.cpp" line="4324"/>
        <source>Statistic</source>
        <translation>Statystyki</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4248"/>
        <source>No records to analyse!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4260"/>
        <source>No records to display for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4342"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>Nie można otworzyć wiadomości e-mail &quot;%1&quot;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4352"/>
        <source>Could not start e-mail client!</source>
        <translation>Nie można uruchomić klienta e-mail</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4357"/>
        <source>No records to e-mail for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4363"/>
        <source>Select Icon Color</source>
        <translation>Wybierz kolor Ikon</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4369"/>
        <source>Restart application to apply new icon color.</source>
        <translation>Uruchom ponownie aplikację aby wdrożyć nowy kolor Ikon</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="870"/>
        <source>The database can not be saved.

Please choose another location in the settings or all data will be lost.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2406"/>
        <location filename="../MainWindow.cpp" line="2456"/>
        <location filename="../MainWindow.cpp" line="2532"/>
        <location filename="../MainWindow.cpp" line="2663"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Wygenerowane przez UBPM dla
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2407"/>
        <location filename="../MainWindow.cpp" line="2457"/>
        <location filename="../MainWindow.cpp" line="2533"/>
        <location filename="../MainWindow.cpp" line="2664"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Darmowe i otwarto źródłowe
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2816"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importuj z CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2816"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>Plik CSV (*.csv);;Plik XML (*.xml);;Plik JSON (*.json);;Plik SQL (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2874"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Nie mogę otworzyć &quot;%1&quot;!

Powód: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1190"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Pomiary : %1  |  Nieregularne : %2  |  Poruszenia : %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="664"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Szanowny Dr. House,

w załączeniu przesyłam moje dane dotyczące ciśnienia krwi za ten miesiąc.

Z poważaniem,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1195"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Pomiary : 0  |  Nieregularne : 0  |  Poruszenia : 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2412"/>
        <location filename="../MainWindow.cpp" line="2462"/>
        <location filename="../MainWindow.cpp" line="2538"/>
        <location filename="../MainWindow.cpp" line="2669"/>
        <source>%1 (Age: %2, Height: %3 cm, Weight: %4 kg, BMI: %5)</source>
        <translation>%1 (Wiek: %2, Wzrost: %3 cm, Waga: %4 kg, BMI: %5)</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2854"/>
        <location filename="../MainWindow.cpp" line="4132"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>Pomyślnie zaimportowano %n rekord z %1.

     Użytkownik 1 : %2
     Użytkownik 2 : %3</numerusform>
            <numerusform>Pomyślnie zaimportowano %n rekordy z %1.

     Użytkownik 1 : %2
     Użytkownik 2 : %3</numerusform>
            <numerusform>Pomyślnie zaimportowano %n rekordów z %1.

     Użytkownik 1 : %2
     Użytkownik 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2858"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>Pomiń %n uszkodzony rekord!</numerusform>
            <numerusform>Pomiń %n uszkodzone rekordy!</numerusform>
            <numerusform>Pomiń %n uszkodzonych rekordów!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2863"/>
        <location filename="../MainWindow.cpp" line="4136"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>Pomiń %n powtórzony rekord!</numerusform>
            <numerusform>Pomiń %n powtórzone rekordy!</numerusform>
            <numerusform>Pomiń %n powtórzonych rekordów!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3244"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>To nie wygląda jak baza danych UBPM!

Może z powodu złych ustawień szyfrowania lub hasła?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3297"/>
        <location filename="../MainWindow.cpp" line="3301"/>
        <source>Export to %1</source>
        <translation>Eksportuj do %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3297"/>
        <location filename="../MainWindow.cpp" line="3301"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 Plik (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3337"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Nie mogę utworzyć &quot;%1&quot;!

Powód: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3343"/>
        <source>The database is empty, no records to export!</source>
        <translation>Baza danych jest pusta, brak rekordów do eksportowania!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3702"/>
        <source>Morning</source>
        <translation>Rano</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3702"/>
        <source>Afternoon</source>
        <translation>Popołudnie</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3758"/>
        <source>Week</source>
        <translation>Tydzień</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3813"/>
        <source>Quarter</source>
        <translation>Kwadrans</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3842"/>
        <source>Half Year</source>
        <translation>Półrocze</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3860"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4066"/>
        <source>Really delete all records for user %1?</source>
        <translation>Czy na pewno usunąć wszystkie pomiary Użytkownika %1?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4081"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Wszystkie pomiary Użytkownika %1 zostały usunięte, baza danych została zapisana do pliku &quot;ubpm.sql.bak&quot;.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4215"/>
        <source>Really delete all records?</source>
        <translation>Czy na pewno usunąć wszystkie pomiary?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4226"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Wszystkie usunięte pomiary oraz bazy danych &quot;ubpm.sql&quot; zostały przeniesione do kosza.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4546"/>
        <source>- UBPM Application
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4561"/>
        <source>- UBPM Plugins
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4705"/>
        <location filename="../MainWindow.cpp" line="4709"/>
        <location filename="../MainWindow.cpp" line="4713"/>
        <location filename="../MainWindow.h" line="162"/>
        <location filename="../MainWindow.h" line="166"/>
        <location filename="../MainWindow.h" line="170"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4706"/>
        <location filename="../MainWindow.cpp" line="4710"/>
        <location filename="../MainWindow.cpp" line="4714"/>
        <location filename="../MainWindow.h" line="163"/>
        <location filename="../MainWindow.h" line="167"/>
        <location filename="../MainWindow.h" line="171"/>
        <source>Maximum</source>
        <translation>Maksimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4779"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Nie mogę otworzyć pliku z motywem &quot;%1&quot;!

Powód: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2028"/>
        <location filename="../MainWindow.cpp" line="2045"/>
        <location filename="../MainWindow.cpp" line="5665"/>
        <source>Delete record</source>
        <translation>Usuń rekord</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5592"/>
        <source>Show Heartrate</source>
        <translation>Pokaż tętno</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5628"/>
        <location filename="../MainWindow.cpp" line="5639"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation>Nie można wyłączyć jednocześnie symboli i linii!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5667"/>
        <source>Show record</source>
        <translation>Pokaż rekord</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2029"/>
        <location filename="../MainWindow.cpp" line="2052"/>
        <location filename="../MainWindow.cpp" line="5668"/>
        <source>Hide record</source>
        <translation>Ukryj rekord</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2047"/>
        <location filename="../MainWindow.cpp" line="5679"/>
        <source>Really delete selected record?</source>
        <translation>Czy na pewno usunąć zaznaczone rekordy?</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="4658"/>
        <source>The following %n translation(s) for &quot;%1&quot; could not be loaded:

%2
Hide this warning on program startup?</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5561"/>
        <source>Scrolling has reached top/bottom, show next/previous period?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5584"/>
        <source>Dynamic Scaling</source>
        <translation>Skalowanie dynamiczne</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5585"/>
        <source>Colored Stripes</source>
        <translation>Kolorowe paski</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5587"/>
        <source>Show Symbols</source>
        <translation>Pokaż symbole</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5588"/>
        <source>Show Lines</source>
        <translation>Pokaż linie</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5590"/>
        <source>Colored Symbols</source>
        <translation>Kolorowe symbole</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5700"/>
        <source>Show Median</source>
        <translation>Pokaż średnią pomiarów</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5701"/>
        <source>Show Values</source>
        <translation>Pokaż wartości</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5756"/>
        <source>Really quit program?</source>
        <translation>Na pewno wyjść z programu?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Uniwersalny menadżer ciśnienia krwi</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation>Aplikacja jest już uruchomiona, a wielokrotne instancje nie są dozwolone.

Przełącz się na działającą instancję lub zamknij ją i spróbuj ponownie.</translation>
    </message>
</context>
</TS>
