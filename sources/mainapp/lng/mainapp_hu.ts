<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="../DialogAbout.ui" line="14"/>
        <source>About UBPM</source>
        <translation>Az UBPM névjegye</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="41"/>
        <source>Version</source>
        <translation>Verzió</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="53"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Univerzális vérnyomás nyilvántartó</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="91"/>
        <source>Qt Framework</source>
        <translation>Qt keretrendszer</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="129"/>
        <source>Operating System</source>
        <translation>Operációs rendszer</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="220"/>
        <source>Environment</source>
        <translation>Környezet</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="231"/>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="266"/>
        <source>Database</source>
        <translation>Adatbázis</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="301"/>
        <source>Cache</source>
        <translation>Gyorsítótár</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="336"/>
        <source>Guides</source>
        <translation>Leírások</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="371"/>
        <source>Languages</source>
        <translation>Nyelvek</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="406"/>
        <source>Plugins</source>
        <translation>Bővítmények</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="441"/>
        <source>Themes</source>
        <translation>Témák</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="469"/>
        <source>Translators</source>
        <translation>Fordítók</translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="14"/>
        <source>The program is provided as is with no warranty of any kind, including the warranty of design, merchantability and fitness for a particular purpose.</source>
        <translation>A programot „jelen állapotában” biztosítjuk, mindenféle garancia nélkül, ideértve a tervezés, eladhatóság és alkalmasság bármilyen célra szóló garanciát is.</translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="37"/>
        <source>Could not open &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="14"/>
        <source>Data Analysis</source>
        <translation>Adatelemzés</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="24"/>
        <source>Query</source>
        <translation>Lekérdezés</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="43"/>
        <location filename="../DialogAnalysis.cpp" line="209"/>
        <source>Results</source>
        <translation>Eredmények</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="82"/>
        <source>Date</source>
        <translation>Dátum</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="91"/>
        <source>Time</source>
        <translation>Időpont</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="100"/>
        <source>Systolic</source>
        <translation>Szisztolés</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="109"/>
        <source>Diastolic</source>
        <translation>Diasztolés</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="118"/>
        <source>P.Pressure</source>
        <translation>P.nyomás</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="127"/>
        <source>Heartrate</source>
        <translation>Pulzus</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="136"/>
        <source>Irregular</source>
        <translation>Aritmia</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="145"/>
        <source>Movement</source>
        <translation>Bemozdulás</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="154"/>
        <source>Invisible</source>
        <translation>Rejtett</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="163"/>
        <source>Comment</source>
        <translation>Megjegyzés</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="193"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="51"/>
        <source>Could not create memory database!

%1</source>
        <translation>Nem sikerült az adatbázis létrehozása!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>No results for this query found!</source>
        <translation>A lekérdezés nem talált eredményt!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 für %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Eredmény</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Találat</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Mérés</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>User %1</source>
        <translation>Páciens %1</translation>
    </message>
</context>
<context>
    <name>DialogDistribution</name>
    <message>
        <location filename="../DialogDistribution.ui" line="20"/>
        <source>Data Distribution</source>
        <translation>Adatok eloszlása</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="77"/>
        <source>Preview</source>
        <translation>Előnézet</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="88"/>
        <source>Print</source>
        <translation>Nyomtatás</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="99"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogDistribution.cpp" line="18"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Mérés</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="119"/>
        <source>Low</source>
        <translation>Alacsony</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="121"/>
        <source>Optimal</source>
        <translation>Optimális</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="123"/>
        <source>Normal</source>
        <translation>Normális</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="125"/>
        <source>High Normal</source>
        <translation>Emelkedett normális</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="127"/>
        <source>Hyper 1</source>
        <translation>Magas 1</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="129"/>
        <source>Hyper 2</source>
        <translation>Magas 2</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="131"/>
        <source>Hyper 3</source>
        <translation>Magas 3</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="148"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="160"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="171"/>
        <source>Blood Pressure</source>
        <translation>Vérnyomás</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="210"/>
        <source>Athlete</source>
        <translation>Sportoló</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="212"/>
        <source>Excellent</source>
        <translation>Kitűnő</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="214"/>
        <source>Great</source>
        <translation>Nagyon jó</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="216"/>
        <source>Good</source>
        <translation>Jó</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="218"/>
        <source>Average</source>
        <translation>Átlagos</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="220"/>
        <source>Below Average</source>
        <translation>Átlag alatti</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="222"/>
        <source>Poor</source>
        <translation>Gyenge</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="239"/>
        <source>BPM</source>
        <translation>PULZ</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="265"/>
        <source>Heart Rate</source>
        <translation>Pulzus</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="346"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Létrehozva a Windows / Linux / macOS
alatt használható UBPM programmal.</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="347"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Ingyenes és nyílt forráskódú
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="352"/>
        <source>%1 (Age: %2, Height: %3 cm, Weight: %4 kg, BMI: %5)</source>
        <translation>%1 (Életkor: %2, Magasság: %3 cm, Súly: %4 kg, BMI: %5)</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="352"/>
        <source>User %1</source>
        <translation>Páciens %1</translation>
    </message>
</context>
<context>
    <name>DialogDonation</name>
    <message>
        <location filename="../DialogDonation.ui" line="14"/>
        <source>Donation</source>
        <translation>Támogatás</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="67"/>
        <source>Copy E-Mail and open Amazon</source>
        <translation>E-mail cím másolása, Amazon megnyitása</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="154"/>
        <source>Open PayPal (+)</source>
        <translation>PayPal (+) megnyitása</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="171"/>
        <source>Open PayPal (-)</source>
        <translation>PayPal (-) megnyitása</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="46"/>
        <source>If you have an Amazon account, you can send money by gift card.

Click the button below to open the Amazon website in your browser and

- sign in to your account
- select or enter the amount you would like to send
- select e-mail delivery
- paste the e-mail address as the recipient
- add a message
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Ha van Amazon-fiókja, akkor ajándékkártyával is küldhet pénzt.

Kattintson az alábbi gombra az Amazon weboldalának megnyitásához a böngészőben, és

- jelentkezzen be a fiókjába
- válassza ki vagy adja meg a küldeni kívánt összeget
- válassza ki az e-mailes kézbesítést
- illessze be az e-mail címet címzettként
- adjon hozzá egy üzenetet
- hajtsa végre az adományozáshoz szükséges további lépéseket

A QR-kódot beolvashatja mobileszközével is.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="124"/>
        <source>If you have a PayPal account, you can send money using the &quot;Friends and Family&quot; feature.

Open your PayPal app and

- search for @LazyT (Thomas Löwe)
- enter the amount you would like to send
- add a message
- complete the donation

You can also click the first button below to do this in your browser without using the app.

If you do not have a PayPal account but do have a credit/debit card click the second button below to go to the PayPal donation page and

- select onetime or repeating donation
- select or enter the amount you would like to send
- optionally check the box to cover the fees
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Ha rendelkezik PayPal-fiókkal, akkor a „Barátok és családtagok” funkció segítségével küldhet pénzt.

Nyissa meg a PayPal alkalmazást, és

- keresse meg a @LazyT (Thomas Löwe) nevet.
- adja meg az összeget, amit küldeni szeretne
- adjon hozzá egy üzenetet
- hajtsa végre az adományozáshoz szükséges további lépéseket

Az alábbi első gombra kattintva ezt a böngészőben is megteheti, anélkül, hogy az alkalmazást használná.

Ha nincs PayPal-fiókja, de rendelkezik hitelkártyával/betéti kártyával, kattintson az alábbi második gombra, hogy a PayPal adományozási oldalára lépjen, és

- válassza ki az egyszeri vagy ismétlődő adományozást
- válassza ki vagy adja meg az összeget, amelyet küldeni szeretne
- opcionálisan jelölje be a díjak fedezésére szolgáló négyzetet
- hajtsa végre az adományozáshoz szükséges további lépéseket

A QR-kódot beolvashatja mobileszközével is.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="230"/>
        <source>If you are in the Euro Payments Area, you can transfer money from your bank account.

Click the buttons below to copy the name/IBAN/BIC to the clipboard and

- sign in to your bank account
- paste the name/IBAN/BIC into the transfer form
- enter the amount you would like to send
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Ha Ön az Európai Gazdasági Térségben (EGT) tartózkodik, akkor bankszámlájáról is utalhat pénzt.

Kattintson az alábbi gombokra a név/IBAN/BIC kód vágólapra másolásához és

- bejelentkezzen a bankszámlájára
- a név/IBAN/BIC beillesztése az átutalási űrlapba
- írja be az összeget, amit küldeni szeretne
- hajtsa végre az adományozáshoz szükséges további lépéseket

A QR-kódot beolvashatja mobileszközével is.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="251"/>
        <source>Copy Name</source>
        <translation>Név másolása</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="268"/>
        <source>Copy IBAN</source>
        <translation>IBAN másolása</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="285"/>
        <source>Copy BIC</source>
        <translation>BIC másolása</translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Leírás</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="61"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>%1 nyelvű leírás nem található, helyette az EN kerül megnyitásra.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="53"/>
        <source>%1 guide not found!</source>
        <translation>%1 leírás nem található!</translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation>Adatmigráció</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation>Forrás</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation>Adatforrás megnyitása</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation>Paraméterek</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation>Dátumformátum</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation>Aritmia felismerése</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation>Kezdő sor</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation>Soronkénti elemek száma</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation>Határoló karakter</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation>Idő formátum</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation>Mozgás észlelése</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation>Dátum nyelve</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation>Pozíciók</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation>Aritmia</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation>Dátum</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation>Szisztolés</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation>Pulzusszám</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation>Mozgás</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation>Megjegyzés figyelmen kívül hagyása</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation>Idő</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation>Diasztolés</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation>Megjegyzés</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation>Eredmény</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="786"/>
        <source>Migrate User 1</source>
        <translation>Páciens 1 adatainak átvitele</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="815"/>
        <source>Migrate User 2</source>
        <translation>Páciens 2 adatainak átvitele</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="841"/>
        <source>Copy to Custom</source>
        <translation>Egyedi másolása</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="861"/>
        <source>Select predefined settings</source>
        <translation>Előre definiált beállítások kiválasztása</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="895"/>
        <source>Custom</source>
        <translation>Egyedi</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="906"/>
        <source>Test Data Migration</source>
        <translation>Adatmigráció tesztelése</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="920"/>
        <source>Start Data Migration</source>
        <translation>Adatmigráció indítása</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>Choose Data Source for Migration</source>
        <translation>Migrálandó adatforrás kiválasztása</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>CSV File (*.csv)</source>
        <translation>CSV fájl (*.csv)</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%L1 Bytes</source>
        <translation>%L1 Byte</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="305"/>
        <source>%1 Lines</source>
        <translation>%1 sor</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="319"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>&quot;%1&quot; nem nyitható meg!

Hibaüzenet: %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="341"/>
        <source>Startline can&apos;t be empty.</source>
        <translation>A kezdő sor értéke nem maradhat üresen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="347"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation>A kezdő sor kisebb kell legyen, mint az összes sorok száma.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="355"/>
        <source>Line %1 = %2</source>
        <translation>Sor %1 = %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="360"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation>A határoló karakter nem maradhat üresen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="367"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation>A kezdő sor nem tartalmaz &quot;%1&quot; határoló karaktert.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="378"/>
        <source>Elements can&apos;t be empty.</source>
        <translation>Az elemek nem maradhatnak üresen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="384"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation>Az elemek száma nem lehet 4-nél kisebb.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="390"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation>Az elemek száma nem egyezik (%1 volt megtalálható).</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="397"/>
        <source>Date format can&apos;t be empty.</source>
        <translation>A dátum formátuma nem maradhat üresen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="403"/>
        <source>Single date format must also contain a time format.</source>
        <translation>Ha csak dátum formátum van megadva, annak tartalmaznia kell az időt is.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="410"/>
        <source>Date position can&apos;t be empty.</source>
        <translation>A dátum pozíciója nem maradhat üresen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="416"/>
        <source>Date position must be smaller than elements.</source>
        <translation>A dátum pozíciója kisebb kell legyen az elemek számánál.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="423"/>
        <source>Time position requires also a time parameter.</source>
        <translation>Idő pozíció megadásához idő paraméter is szükséges.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="429"/>
        <source>Time format requires also a time position.</source>
        <translation>Idő formátum megadásához idő pozíció is szükséges.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="435"/>
        <source>Time position must be smaller than elements.</source>
        <translation>Az idő pozíciója kisebb kell legyen az elemek számánál..</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="442"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation>A szisztolés érték pozíciója nem maradhat üresen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="448"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation>A szisztolés érték pozíciója kisebb kell legyen az elemek számánál.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="455"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation>A diasztolés érték nem maradhat üresen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="461"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation>A diasztolés érték pozíciója kisebb kell legyen az elemek számánál.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="468"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation>A pulzus pozíciója nem maradhat üresen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="474"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation>A pulzus pozíciója kisebb kell legyen az elemek számánál.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="481"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation>Az aritmia érzékelés pozíciója kisebb kell legyen az elemek számánál.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="487"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation>Aritmia pozíció megadásához aritmia paraméter is szükséges.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="494"/>
        <source>Movement position must be smaller than elements.</source>
        <translation>A mozgás érzékelésének pozíciója kisebb kell legyen az elemek számánál.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="500"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation>Mozgás érzékelés pozíció megadásához mozgás érzékelés paraméter is szükséges.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="507"/>
        <source>Comment position must be smaller than elements.</source>
        <translation>A megjegyzés pozíciója kisebb kell legyen az elemek számánál.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="523"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation>Pozícióérték nem szerepelhet kétszer.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="539"/>
        <source>Date format is invalid.</source>
        <translation>A dátum formátuma érvénytelen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="546"/>
        <source>Time format is invalid.</source>
        <translation>Az idő formátuma érvénytelen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="556"/>
        <source>Date/Time format is invalid.</source>
        <translation>A dátum/idő formátuma érvénytelen.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="747"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation>
            <numerusform>%n mérés sikeresen beolvasva %1 nevű pácienshez.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="751"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>%n érvénytelen mérés kihagyva.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="756"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>%n már meglévő mérés kihagyva.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Kézi adatrögzítés</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Mérés</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Mérés hozzáadása %1-hoz</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Dátum/Időpont kiválasztása</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>SZISZ bevitel</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation>DIA bevitel</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation>Pulzus bevitel</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Aritmia</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>Bemozdulás</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Opcionális megjegyzés hozzáadása</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Sikeres Létrehozás / Törlés / Módosítás esetén visszajelzés</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Rögzítés</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Páciens 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Páciens 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="34"/>
        <location filename="../DialogRecord.cpp" line="187"/>
        <source>Modify</source>
        <translation>Módosítás</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="132"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Adat nem törölhető!

Erre a dátum/időpontra nincsen mérési adat.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="138"/>
        <source>Data record successfully deleted.</source>
        <translation>Adat sikeresen törölve.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="150"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Először egy érvényes&quot;SYS&quot; értéket adjon meg!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="160"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Először érvényes &quot;DIA&quot; értéket adjon meg!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="169"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Először egy érvényes PULZUS értéket adjon meg!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="191"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Adat nem módosítható!

Erre a dátumra/időpontra nincsen mérési adat.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="197"/>
        <source>Data Record successfully modified.</source>
        <translation>Adat sikeresen módosítva.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="206"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>Adat nem rögzíthető!

Erre a dátumra és időpontra már létezik mérési adat.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="212"/>
        <source>Data Record successfully created.</source>
        <translation>Adat sikeresen mentve.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="161"/>
        <source>Choose Database Location</source>
        <translation>Adatbázis helyének kiválasztása</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="171"/>
        <source>Choose Database Backup Location</source>
        <translation>Adatbázis-mentés helyének kiválasztása</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="308"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation>A kijelölt adatbázis már létezik.

Kérem válassza az ajánlott megoldást.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="310"/>
        <source>Overwrite</source>
        <translation>Felülírás</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="311"/>
        <source>Merge</source>
        <translation>Beolvasztás</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="312"/>
        <source>Swap</source>
        <translation>Csere</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="314"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation>Felülírás:

A kijelölt adatbázis felülírása az aktuálissal.

Beolvasztás:

A kijelölt adatbázis méréseit beolvasztja az aktuálisba.

Csere:

Törli az aktuális adatbázist és felülírja a kijelölttel.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="322"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation>Az aktuális adatbázis még üres.

A kiválasztott adatbázis törlésre kerül kilépéskor, ha nincsen benne adat.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="352"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>SQL-titkosítás jelszó nélkül nem lehetséges és kikapcsolásra kerül!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="359"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation>Az adatbázis másolat másik háttértárolón, partíción vagy könyvtárban kellene legyen.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="364"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Kérem érvényes kiegészítő adatokat adjon meg a Páciens 1-hez!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="371"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Kérem érvényes kiegészítő adatokat adjon meg a Páciens 2-höz!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="378"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>A megadott életkor nem illeszkedik Páciens 1 korcsoportjához!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="385"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>A megadott életkor nem illeszkedik Páciens 2 korcsoportjához!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="393"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation>Kérem engedélyezze a grafikonhoz vagy szimbólumok, vagy vonalak megjelenítését!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="400"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Kérem érvényes e-mail címet adjon meg!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="407"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Kérem adja meg az e-mail tárgyát!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="414"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>Az e-mailben szerepelnie kell $CHART, $TABLE és/vagy $STATS adatoknak!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="525"/>
        <source>User 1</source>
        <translation>Páciens 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="539"/>
        <source>User 2</source>
        <translation>Páciens 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="652"/>
        <source>Blood Pressure Report</source>
        <translation>Vérnyomás jelentés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="653"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Tisztelt Doktor Úr!

Alábbiakban küldöm a havi vérnyomás adataimat.

Köszönettel,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="695"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Beállítás félbeszakítása és a módosítások elvetése?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Adatbázis</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="52"/>
        <source>Location</source>
        <translation>Helye</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="58"/>
        <source>Current Location</source>
        <translation>Jelenlegi hely</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="74"/>
        <source>Change Location</source>
        <translation>Hely módosítása</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="100"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Titkosítás SQLCipher-rel</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="103"/>
        <source>Encryption</source>
        <translation>Titkosítás</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="115"/>
        <source>Password</source>
        <translation>Jelszó</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="128"/>
        <source>Show Password</source>
        <translation>Jelszó megmutatása</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="282"/>
        <source>User</source>
        <translation>Páciens</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="317"/>
        <location filename="../DialogSettings.ui" line="580"/>
        <source>Male</source>
        <translation>Férfi</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="346"/>
        <location filename="../DialogSettings.ui" line="609"/>
        <source>Female</source>
        <translation>Nő</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="442"/>
        <location filename="../DialogSettings.ui" line="714"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <location filename="../DialogSettings.ui" line="574"/>
        <source>Mandatory Information</source>
        <translation>Alapvető információ</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="372"/>
        <location filename="../DialogSettings.ui" line="644"/>
        <source>Age Group</source>
        <translation>Korcsoport</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="458"/>
        <location filename="../DialogSettings.ui" line="730"/>
        <source>Additional Information</source>
        <translation>Egyéb adatok</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Height</source>
        <translation>Magasság</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="547"/>
        <location filename="../DialogSettings.ui" line="819"/>
        <source>Weight</source>
        <translation>Súly</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="844"/>
        <source>Device</source>
        <translation>Készülék</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="850"/>
        <location filename="../DialogSettings.cpp" line="70"/>
        <source>Import Plugins [ %1 ]</source>
        <translation>Import pluginek [ %1 ]</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="898"/>
        <source>Open Website</source>
        <translation>Weboldal megnyitása</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="911"/>
        <source>Maintainer</source>
        <translation>Készítő</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="939"/>
        <source>Version</source>
        <translation>Verzió</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="946"/>
        <source>Send E-Mail</source>
        <translation>E-Mail küldés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="959"/>
        <source>Model</source>
        <translation>Gyártmány</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="966"/>
        <source>Producer</source>
        <translation>Gyártó</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="983"/>
        <source>Chart</source>
        <translation>Grafikon</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1110"/>
        <source>X-Axis Range</source>
        <translation>X-tengely</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1116"/>
        <source>Dynamic Scaling</source>
        <translation>Dinamikus skála</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1097"/>
        <source>Healthy Ranges</source>
        <translation>Egészséges tart.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1014"/>
        <source>Symbols</source>
        <translation>Jelek mérete</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1091"/>
        <source>Colored Areas</source>
        <translation>Színezett területek</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="863"/>
        <source>Please choose Device Plugin…</source>
        <translation>Kérem válasszon ki egy készüléket…</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1238"/>
        <location filename="../DialogSettings.ui" line="1513"/>
        <source>Systolic</source>
        <translation>Szisztolés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1326"/>
        <location filename="../DialogSettings.ui" line="1601"/>
        <source>Diastolic</source>
        <translation>Diasztolés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1414"/>
        <location filename="../DialogSettings.ui" line="1689"/>
        <source>Heartrate</source>
        <translation>Szívverés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1786"/>
        <source>Table</source>
        <translation>Táblázat</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1871"/>
        <location filename="../DialogSettings.ui" line="2054"/>
        <source>Systolic Warnlevel</source>
        <translation>Szisztolés figyelmeztetés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1817"/>
        <location filename="../DialogSettings.ui" line="2114"/>
        <source>Diastolic Warnlevel</source>
        <translation>Diasztolés figyelmeztetés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="153"/>
        <source>Automatic Backup</source>
        <translation>Automatikus másolat készítés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="156"/>
        <source>Backup</source>
        <translation>Másolat készítés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="173"/>
        <source>Current Backup Location</source>
        <translation>Mentés helye</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="186"/>
        <source>Change Backup Location</source>
        <translation>Másolat helyének módosítása</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="213"/>
        <source>Backup Mode</source>
        <translation>Másolat készítés módja</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="223"/>
        <source>Daily</source>
        <translation>Naponta</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="232"/>
        <source>Weekly</source>
        <translation>Hetente</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="241"/>
        <source>Monthly</source>
        <translation>Havonta</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="253"/>
        <source>Keep Copies</source>
        <translation>Másolat megtartása</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="486"/>
        <location filename="../DialogSettings.ui" line="758"/>
        <source>Birthday</source>
        <translation>Születési dátm</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1023"/>
        <source>Color</source>
        <translation>Színek</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1047"/>
        <location filename="../DialogSettings.ui" line="1075"/>
        <source>Size</source>
        <translation>Magasság</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1129"/>
        <source>Lines</source>
        <translation>Vonalak</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1138"/>
        <location filename="../DialogSettings.ui" line="1166"/>
        <source>Width</source>
        <translation>Szélesség</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1195"/>
        <source>Show Heartrate</source>
        <translation>Szívverés kijelzése</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1201"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Szívverés kijelzése a Grafikon-nézetben</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1211"/>
        <source>Print Heartrate</source>
        <translation>Szívverés kinyomtatása</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1217"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Szívverés külön lapra nyomtatása</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1931"/>
        <location filename="../DialogSettings.ui" line="2168"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Pulzusnyomás figyelmezt.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1985"/>
        <location filename="../DialogSettings.ui" line="2222"/>
        <source>Heartrate Warnlevel</source>
        <translation>Pulzus figyelmeztetés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2287"/>
        <source>Statistic</source>
        <translation>Statisztika</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2293"/>
        <source>Bar Type</source>
        <translation>Oszlop típus</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2299"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Átlag helyett medián oszlopok megjelenítése</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2309"/>
        <source>Legend Type</source>
        <translation>Legenden Typ</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2315"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Értékek megjelenítése címkeként magyarázatok helyett</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2330"/>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2338"/>
        <source>Address</source>
        <translation>Cím</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2357"/>
        <source>Subject</source>
        <translation>Tárgy</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2375"/>
        <source>Message</source>
        <translation>Üzenet</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2396"/>
        <source>Plugin</source>
        <translation>Bővítmény</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2402"/>
        <source>General</source>
        <translation>Általános</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2408"/>
        <source>Log Device Communication to File</source>
        <translation>Készülék-kommunikáció naplózása</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2415"/>
        <source>Import Measurements automatically</source>
        <translation>Mérések automatikus beimportálása</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2425"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2431"/>
        <source>Discover Device automatically</source>
        <translation>Készülék automatikus keresése</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2438"/>
        <source>Connect Device automatically</source>
        <translation>Automatikus kapcsolódás a készülékhez</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2453"/>
        <source>Update</source>
        <translation>Frissítés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2459"/>
        <source>Autostart</source>
        <translation>Autostart</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Indításkor programfrissítés keresése</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2478"/>
        <source>Notification</source>
        <translation>Értesítés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2484"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Értesítés a frissítés-ellenőrzés eredményéről</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2500"/>
        <source>Save</source>
        <translation>Mentés</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2517"/>
        <source>Reset</source>
        <translation>Visszaállítás</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2534"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Online frissítés</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Elérhető verzió</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Frissítés mérete</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Telepített verzió</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Részletek</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="181"/>
        <location filename="../DialogUpdate.ui" line="202"/>
        <source>Download</source>
        <translation>Letöltés</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="222"/>
        <source>Ignore</source>
        <translation>Kihagyás</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="183"/>
        <source>No new version found.</source>
        <translation>Nem található újabb verzió.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="42"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!!! SSL FIGYELMEZTETÉS - GONDOSAN NÉZZE ÁT !!!

Hálózati kapcsolódási probléma:

%1
Ennek ellenére kívánja folytatni?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Downloading update failed!

%1</source>
        <translation>A frissítés letöltése sikertelen!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Checking update failed!

%1</source>
        <translation>Frissítés keresése sikertelen!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="156"/>
        <source>Unexpected response from update server!</source>
        <translation>Váratlan válasz a frissítés szerverétől!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="173"/>
        <source>*%1 not found</source>
        <translation>*%1 nem található</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="203"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>A frissítés mérete nem megfelelő!

%L1 : %L2

Ismételt letöltés…</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="207"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Frissítés elmentve: %1.

Elindítsuk az újabb verziót?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="220"/>
        <source>Could not start new version!</source>
        <translation>Az új verzió indítása nem lehetséges!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>The program was installed by Flatpak.

Please update using the Flatpak internal update function.</source>
        <translation>A program Flatpak-kal volt telepítve.

Kérem a Flatpak saját frissítő funkciójával frissítsen!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="253"/>
        <source>The program was installed by Snap.

Please update using the Snap internal update function.</source>
        <translation>A program Snap-pel volt telepítve.

Kérem, a Snap saját frissítő funkciójával frissítsen!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="259"/>
        <source>The program was installed from Windows store.

Please update using the store internal update function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="265"/>
        <source>The program was installed from distribution.

Please update using the operating systems internal update function.</source>
        <translation>A program a terjesztés által lett telepítve.

Kérjük, frissítse az alkalmazást az operációs rendszer belső frissítési funkciójával.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="271"/>
        <source>The program was installed from source code.

Please update the sources, rebuild and reinstall manually.</source>
        <translation>A program a forráskódból lett telepítve.

Kérjük, frissítse a forrásokat, építse és telepítse újra a programot kézzel.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="297"/>
        <source>Really abort download?</source>
        <translation>Valóban megszakítsuk a letöltést?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="227"/>
        <source>Could not save update to %1!

%2</source>
        <translation>A frissítés mentése %1 nem sikerült!

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Univerzális vérnyomás nyilvántartó</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation>Előző periódus kijelzése</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation>Következő periódus kijelzése</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Grafikonon ábrázolva</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Táblázatban</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Dátum</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Időpont</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="159"/>
        <location filename="../MainWindow.cpp" line="4694"/>
        <source>Systolic</source>
        <translation>Szisztolés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="160"/>
        <location filename="../MainWindow.cpp" line="4695"/>
        <source>Diastolic</source>
        <translation>Diasztolés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>P.nyomás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Aritmia</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Bemozdulás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Rejtett</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation>Megjegyzés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Statisztikai nézet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>¼-óránkénti</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>½-óránkénti</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Óránkénti</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>¼-napi</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>½-napi</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Napi</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>3 Days</source>
        <translation>3 napi</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Weekly</source>
        <translation>Heti</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Monthly</source>
        <translation>Havi</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>Quarterly</source>
        <translation>Negyedéves</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>½ Yearly</source>
        <translation>Féléves</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="765"/>
        <source>Yearly</source>
        <translation>Éves</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="825"/>
        <source>Last 3 Days</source>
        <translation>Utóbbi 3 nap</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="857"/>
        <source>Last 7 Days</source>
        <translation>Utóbbi 7 nap</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="889"/>
        <source>Last 14 Days</source>
        <translation>Utóbbi 14 nap</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="921"/>
        <source>Last 21 Days</source>
        <translation>Utóbbi 21 nap</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="953"/>
        <source>Last 28 Days</source>
        <translation>Utóbbi 28 nap</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="985"/>
        <source>Last 3 Months</source>
        <translation>Utóbbi 2 hónap</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1017"/>
        <source>Last 6 Months</source>
        <translation>Utóbbi 6 hónap</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1049"/>
        <source>Last 9 Months</source>
        <translation>Utóbbi 9 hónap</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1081"/>
        <source>Last 12 Months</source>
        <translation>Utóbbi 12 hónap</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1113"/>
        <source>All Records</source>
        <translation>Minden mérés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1160"/>
        <source>File</source>
        <translation>Fájl</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1164"/>
        <source>Print</source>
        <translation>Nyomtatás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1186"/>
        <source>Help</source>
        <translation>Segítség</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1203"/>
        <source>Configuration</source>
        <translation>Beállítás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1207"/>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1216"/>
        <source>Language</source>
        <translation>Nyelv</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1225"/>
        <source>Style</source>
        <translation>Stílus</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1234"/>
        <source>Charts</source>
        <translation>Grafikon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1259"/>
        <source>Database</source>
        <translation>Adatbázis</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1263"/>
        <source>Import</source>
        <translation>Importálás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1275"/>
        <source>Export</source>
        <translation>Exportálás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1290"/>
        <source>Clear</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Quit</source>
        <translation>Kilépés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1382"/>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>Quit Program</source>
        <translation>Programból kilépés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1397"/>
        <source>About</source>
        <translation>Névjegy</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <location filename="../MainWindow.ui" line="1403"/>
        <source>About Program</source>
        <translation>Program névjegye</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <source>Guide</source>
        <translation>Kézikönyv</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <location filename="../MainWindow.ui" line="1418"/>
        <source>Show Guide</source>
        <translation>Handbuch anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1427"/>
        <source>Update</source>
        <translation>Frissítés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1430"/>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>Check Update</source>
        <translation>Frissítés keresése</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1592"/>
        <source>To PDF</source>
        <translation>PDF-be</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <source>Export To PDF</source>
        <translation>PDF export</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1607"/>
        <source>Migration</source>
        <translation>Adatmigráció</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <source>Migrate from Vendor</source>
        <translation>Gyári programból adatmigráció</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1857"/>
        <source>Translation</source>
        <translation>Fordítás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1860"/>
        <location filename="../MainWindow.ui" line="1863"/>
        <source>Contribute Translation</source>
        <translation>Fordításhoz hozzájárulás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1872"/>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1875"/>
        <location filename="../MainWindow.ui" line="1878"/>
        <source>Send E-Mail</source>
        <translation>E-Mail küldés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1890"/>
        <source>Icons</source>
        <translation>Ikonok</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1893"/>
        <location filename="../MainWindow.ui" line="1896"/>
        <source>Change Icon Color</source>
        <translation>Ikonszín változtatás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1907"/>
        <source>System</source>
        <translation>Rendszer</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1910"/>
        <location filename="../MainWindow.ui" line="1913"/>
        <source>System Colors</source>
        <translation>Rendszerszínek</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1921"/>
        <source>Light</source>
        <translation>Világos</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1924"/>
        <location filename="../MainWindow.ui" line="1927"/>
        <source>Light Colors</source>
        <translation>Világos színek</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1935"/>
        <source>Dark</source>
        <translation>Sötét</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1938"/>
        <location filename="../MainWindow.ui" line="1941"/>
        <source>Dark Colors</source>
        <translation>Sötét színek</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1950"/>
        <source>Donation</source>
        <translation>Támogatás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1965"/>
        <source>Distribution</source>
        <translation>Adateloszlás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1968"/>
        <location filename="../MainWindow.ui" line="1971"/>
        <source>Show Distribution</source>
        <translation>Adateloszlás megjelenítése</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1983"/>
        <location filename="../MainWindow.ui" line="1986"/>
        <source>Save</source>
        <translation>Mentés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1995"/>
        <source>Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1998"/>
        <location filename="../MainWindow.ui" line="2001"/>
        <source>Read Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="161"/>
        <location filename="../MainWindow.cpp" line="4696"/>
        <source>Heartrate</source>
        <translation>Pulzus</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1953"/>
        <location filename="../MainWindow.ui" line="1956"/>
        <source>Make Donation</source>
        <translation>Támogatás küldése</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1445"/>
        <source>Bugreport</source>
        <translation>Hibajelzés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1448"/>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>Send Bugreport</source>
        <translation>Hibajelzés küldés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1460"/>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1463"/>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>Change Settings</source>
        <translation>Beállítások változtatása</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>From Device</source>
        <translation>Készülékről</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1484"/>
        <location filename="../MainWindow.ui" line="1487"/>
        <source>Import From Device</source>
        <translation>Készülékről importálás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1499"/>
        <source>From File</source>
        <translation>Fájlból</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1502"/>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Import From File</source>
        <translation>Fájlból importálás</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1514"/>
        <source>From Input</source>
        <translation>Kézi bevitel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1517"/>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Import From Input</source>
        <translation>Kézi adatbevitel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1529"/>
        <source>To CSV</source>
        <translation>CSV-be</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1532"/>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Export To CSV</source>
        <translation>CSV export</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1547"/>
        <source>To XML</source>
        <translation>XML-be</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1550"/>
        <location filename="../MainWindow.ui" line="1553"/>
        <source>Export To XML</source>
        <translation>XML export</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1562"/>
        <source>To JSON</source>
        <translation>JSON-ba</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1565"/>
        <location filename="../MainWindow.ui" line="1568"/>
        <source>Export To JSON</source>
        <translation>JSON export</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1577"/>
        <source>To SQL</source>
        <translation>SQL-be</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <source>Export To SQL</source>
        <translation>SQL export</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1625"/>
        <source>Print Chart</source>
        <translation>Grafikon nyomtatása</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1628"/>
        <location filename="../MainWindow.ui" line="1631"/>
        <source>Print Chart View</source>
        <translation>Grafikon nyomtatási előnézet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1640"/>
        <source>Print Table</source>
        <translation>Táblázat nyomtatása</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1643"/>
        <location filename="../MainWindow.ui" line="1646"/>
        <source>Print Table View</source>
        <translation>Táblázat nyomtatási előnézet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1655"/>
        <source>Print Statistic</source>
        <translation>Statisztika nyomtatása</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1658"/>
        <location filename="../MainWindow.ui" line="1661"/>
        <source>Print Statistic View</source>
        <translation>Statisztika nyomtatási előnézet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1670"/>
        <source>Preview Chart</source>
        <translation>Grafikon előnézet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1673"/>
        <location filename="../MainWindow.ui" line="1676"/>
        <source>Preview Chart View</source>
        <translation>Grafikon nyomtatási előnézet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <source>Preview Table</source>
        <translation>Táblázat nyomtatási előnézet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Preview Table View</source>
        <translation>Vorschau Tabellen-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1700"/>
        <source>Preview Statistic</source>
        <translation>Statisztika nyomtatási előnézet</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Preview Statistic View</source>
        <translation>Vorschau Statistik-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1715"/>
        <location filename="../MainWindow.ui" line="1718"/>
        <location filename="../MainWindow.ui" line="1721"/>
        <source>Clear All</source>
        <translation>Minden törlése</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1733"/>
        <location filename="../MainWindow.ui" line="1736"/>
        <location filename="../MainWindow.ui" line="1739"/>
        <source>Clear User 1</source>
        <translation>Páciens 1 törlése</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1748"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <location filename="../MainWindow.ui" line="1754"/>
        <source>Clear User 2</source>
        <translation>Páciens 2 törlése</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1814"/>
        <source>Analysis</source>
        <translation>Elemzés</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1817"/>
        <location filename="../MainWindow.ui" line="1820"/>
        <source>Analyze Records</source>
        <translation>Mérések elemzése</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1839"/>
        <location filename="../MainWindow.ui" line="1842"/>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>Time Mode</source>
        <translation>Idő mód</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="108"/>
        <location filename="../MainWindow.cpp" line="109"/>
        <location filename="../MainWindow.cpp" line="4664"/>
        <location filename="../MainWindow.cpp" line="4665"/>
        <source>Records For Selected User</source>
        <translation>A kiválasztott páciens mérési eredményei</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="116"/>
        <location filename="../MainWindow.cpp" line="117"/>
        <location filename="../MainWindow.cpp" line="4667"/>
        <location filename="../MainWindow.cpp" line="4668"/>
        <source>Select Date &amp; Time</source>
        <translation>Dátum/időpont kiválasztása</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="169"/>
        <location filename="../MainWindow.cpp" line="4698"/>
        <source>Systolic - Value Range</source>
        <translation>Szisztolés értéktartomány</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="170"/>
        <location filename="../MainWindow.cpp" line="4699"/>
        <source>Diastolic - Value Range</source>
        <translation>Diasztolés értéktartomány</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="173"/>
        <location filename="../MainWindow.cpp" line="4701"/>
        <source>Systolic - Target Area</source>
        <translation>Szisztolés cél</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="174"/>
        <location filename="../MainWindow.cpp" line="4702"/>
        <source>Diastolic - Target Area</source>
        <translation>Diasztolés cél</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1679"/>
        <source>Athlete</source>
        <translation>Sportoló</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1680"/>
        <source>Excellent</source>
        <translation>Kitűnő</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1680"/>
        <source>Optimal</source>
        <translation>Optimális</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1681"/>
        <source>Great</source>
        <translation>Nagyon jó</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1682"/>
        <source>Good</source>
        <translation>Jó</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1681"/>
        <source>Normal</source>
        <translation>Normál</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1775"/>
        <location filename="../MainWindow.ui" line="1778"/>
        <location filename="../MainWindow.ui" line="1799"/>
        <location filename="../MainWindow.ui" line="1802"/>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="4382"/>
        <location filename="../MainWindow.cpp" line="4383"/>
        <location filename="../MainWindow.cpp" line="4384"/>
        <location filename="../MainWindow.cpp" line="4385"/>
        <location filename="../MainWindow.cpp" line="4728"/>
        <location filename="../MainWindow.cpp" line="4729"/>
        <location filename="../MainWindow.cpp" line="4730"/>
        <location filename="../MainWindow.cpp" line="4731"/>
        <source>Switch To %1</source>
        <translation>%1 adatainak megjelenítése</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="600"/>
        <location filename="../MainWindow.cpp" line="4382"/>
        <location filename="../MainWindow.cpp" line="4383"/>
        <location filename="../MainWindow.cpp" line="4728"/>
        <location filename="../MainWindow.cpp" line="4729"/>
        <source>User 1</source>
        <translation>Páciens 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="610"/>
        <location filename="../MainWindow.cpp" line="4384"/>
        <location filename="../MainWindow.cpp" line="4385"/>
        <location filename="../MainWindow.cpp" line="4730"/>
        <location filename="../MainWindow.cpp" line="4731"/>
        <source>User 2</source>
        <translation>Páciens 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="171"/>
        <location filename="../MainWindow.cpp" line="4700"/>
        <source>Heartrate - Value Range</source>
        <translation>Pulzus értéktartomány</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="175"/>
        <location filename="../MainWindow.cpp" line="4703"/>
        <source>Heartrate - Target Area</source>
        <translation>Pulzus cél</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="341"/>
        <source>No active device plugin, auto import aborted.</source>
        <translation>Nincs aktív készülék csatlakoztatva, az automatikus importálás megszakadt.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="358"/>
        <source>Some plugins will not work without Bluetooth permission.</source>
        <translation>Néhány bővítmény nem fog Bluetooth engedély megadása nélkül.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="461"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation>A másolat mentési könyvtára nem hozható létre.

Kérem ellenőrizze a másolat helyének beállításait.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="490"/>
        <location filename="../MainWindow.cpp" line="513"/>
        <source>Could not backup database:

%1</source>
        <translation>Az adatbázis másolat mentése sikertelen:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="503"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation>Régi adatbázis-mentés törlése sikertelen:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="530"/>
        <source>Could not register icons.</source>
        <translation>Az ikonok nem állíthatóak be.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="663"/>
        <source>Blood Pressure Report</source>
        <translation>Vérnyomás jelentés</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1191"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  SPM : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1196"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  SPM : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1679"/>
        <source>Low</source>
        <translation>Alacsony</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1682"/>
        <source>High Normal</source>
        <translation>Emelkedett normális</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1683"/>
        <location filename="../MainWindow.cpp" line="2160"/>
        <location filename="../MainWindow.cpp" line="4707"/>
        <location filename="../MainWindow.cpp" line="4711"/>
        <location filename="../MainWindow.cpp" line="4715"/>
        <location filename="../MainWindow.h" line="164"/>
        <location filename="../MainWindow.h" line="168"/>
        <location filename="../MainWindow.h" line="172"/>
        <source>Average</source>
        <translation>Átlagos</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1683"/>
        <source>Hyper 1</source>
        <translation>Magas 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1684"/>
        <source>Below Average</source>
        <translation>Átlag alatti</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1684"/>
        <source>Hyper 2</source>
        <translation>Magas 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1685"/>
        <source>Poor</source>
        <translation>Gyenge</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1685"/>
        <source>Hyper 3</source>
        <translation>Magas 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1830"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>A &quot;%1&quot; import bővítmény scannelése sikertelen!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1847"/>
        <location filename="../MainWindow.cpp" line="1874"/>
        <location filename="../MainWindow.cpp" line="4674"/>
        <source>Switch Language to %1</source>
        <translation>%1 nyelvre váltás</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1900"/>
        <location filename="../MainWindow.cpp" line="1915"/>
        <location filename="../MainWindow.cpp" line="4682"/>
        <source>Switch Theme to %1</source>
        <translation>%1 témára váltás</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1940"/>
        <location filename="../MainWindow.cpp" line="1960"/>
        <location filename="../MainWindow.cpp" line="4690"/>
        <source>Switch Style to %1</source>
        <translation>%1 stílusra váltás</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2031"/>
        <location filename="../MainWindow.cpp" line="2056"/>
        <source>Edit record</source>
        <translation>Mérés módosítása</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2160"/>
        <location filename="../MainWindow.cpp" line="4708"/>
        <location filename="../MainWindow.cpp" line="4712"/>
        <location filename="../MainWindow.cpp" line="4716"/>
        <location filename="../MainWindow.h" line="165"/>
        <location filename="../MainWindow.h" line="169"/>
        <location filename="../MainWindow.h" line="173"/>
        <source>Median</source>
        <translation>Medián</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2162"/>
        <source>Click to swap Average and Median</source>
        <translation>Kattintásra átlag és medián közti váltás</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2257"/>
        <source>Click to swap Legend and Label</source>
        <translation>Kattintásra váltás az adat magyarázatok közt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2334"/>
        <source>No records to preview for selected time range!</source>
        <translation>Nincs megjeleníthető mérés a kijelölt időtartományban!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2366"/>
        <source>No records to print for selected time range!</source>
        <translation>Nincs nyomtatható mérés a megadott időtartományban!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2412"/>
        <location filename="../MainWindow.cpp" line="2462"/>
        <location filename="../MainWindow.cpp" line="2538"/>
        <location filename="../MainWindow.cpp" line="2669"/>
        <source>User %1</source>
        <translation>Páciens %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>DATE</source>
        <translation>DÁTUM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>TIME</source>
        <translation>IDŐPONT</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>BPM</source>
        <translation>PULZ</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>IHB</source>
        <translation>Aritm.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>COMMENT</source>
        <translation>MEGJEGYZÉS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>PPR</source>
        <translation>P.NY.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>MOV</source>
        <translation>Mozg.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2734"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>E-mail nem hozható létre, mert a &quot;%1&quot; csatolmány Base64 kódolása sikertelen!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3495"/>
        <location filename="../MainWindow.cpp" line="4290"/>
        <source>Chart</source>
        <translation>Grafikon</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3495"/>
        <location filename="../MainWindow.cpp" line="4307"/>
        <source>Table</source>
        <translation>Táblázat</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3495"/>
        <location filename="../MainWindow.cpp" line="4324"/>
        <source>Statistic</source>
        <translation>Statisztika</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4248"/>
        <source>No records to analyse!</source>
        <translation>Nincs elemzendő mérési adat!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4260"/>
        <source>No records to display for selected time range!</source>
        <translation>Nincs elemzendő mérés a kijelölt időszakban!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4342"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>E-Mail &quot;%1&quot; nem nyitható meg!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4352"/>
        <source>Could not start e-mail client!</source>
        <translation>E-Mail-küldő program nem indítható!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4357"/>
        <source>No records to e-mail for selected time range!</source>
        <translation>Nincs e-mailben elküldhető mérés a kijelölt időszakban!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4363"/>
        <source>Select Icon Color</source>
        <translation>Ikonszín választás</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4369"/>
        <source>Restart application to apply new icon color.</source>
        <translation>Más színű ikonokhoz indítsa újra az alkalmazást.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="870"/>
        <source>The database can not be saved.

Please choose another location in the settings or all data will be lost.</source>
        <translation>Az adatbázis mentése nem sikerült!

Adatvesztés ellen kérem adjon meg a beállítások alatt egy másik mentési útvonalat.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2406"/>
        <location filename="../MainWindow.cpp" line="2456"/>
        <location filename="../MainWindow.cpp" line="2532"/>
        <location filename="../MainWindow.cpp" line="2663"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Létrehozva a Windows / Linux / macOS
alatt használható UBPM-mel</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2407"/>
        <location filename="../MainWindow.cpp" line="2457"/>
        <location filename="../MainWindow.cpp" line="2533"/>
        <location filename="../MainWindow.cpp" line="2664"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Ingyenes és nyíltforrású
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2816"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importálás CSV/XML/JSON/SQL-ból</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2816"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>CSV fájl (*.csv);;XML fájl (*.xml);;JSON fájl (*.json);;SQL fájl (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2874"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>&quot;%1&quot; nem megnyitható!

Hibaüzenet: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1190"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Mérések: %1  |  Aritmia: %2  |  Bemozdulás: %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="664"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Tisztelt Doktor Úr!

Alábbiakban küldöm a havi vérnyomás adataimat.

Köszönettel,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1195"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Mérések: 0  |  Aritmia: 0  |  Bemozdulás: 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2412"/>
        <location filename="../MainWindow.cpp" line="2462"/>
        <location filename="../MainWindow.cpp" line="2538"/>
        <location filename="../MainWindow.cpp" line="2669"/>
        <source>%1 (Age: %2, Height: %3 cm, Weight: %4 kg, BMI: %5)</source>
        <translation>%1 (Életkor: %2, Magasság: %3 cm, Súly: %4 kg, BMI: %5)</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2854"/>
        <location filename="../MainWindow.cpp" line="4132"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>Sikeresen beolvasva %n mérés, összesen: %1 mérés.

     Páciens 1 : %2
     Páciens 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2858"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>%n érvénytelen mérés kihagyva.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2863"/>
        <location filename="../MainWindow.cpp" line="4136"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>%n már meglévő mérés kihagyva.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3244"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Ez nem tűnik UBPM adatbázisnak!

Esetleg hibás a titkosítási vagy jelszóbeállítás?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3297"/>
        <location filename="../MainWindow.cpp" line="3301"/>
        <source>Export to %1</source>
        <translation>%1 export</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3297"/>
        <location filename="../MainWindow.cpp" line="3301"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 fájl (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3337"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>&quot;%1&quot; létrehozása nem sikerült!

Hibaüzenet: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3343"/>
        <source>The database is empty, no records to export!</source>
        <translation>Üres adatbázisból nem lehetséges az exportálás.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3702"/>
        <source>Morning</source>
        <translation>Délelőtt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3702"/>
        <source>Afternoon</source>
        <translation>Délután</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3758"/>
        <source>Week</source>
        <translation>Hét</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3813"/>
        <source>Quarter</source>
        <translation>Negyedév</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3842"/>
        <source>Half Year</source>
        <translation>Félév</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3860"/>
        <source>Year</source>
        <translation>Év</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4066"/>
        <source>Really delete all records for user %1?</source>
        <translation>Valóban törli %1 páciens összes mérési adatát?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4081"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Páciens %1 összes mérése törölve, és a korábbi adatbázis „ubpm.sql.bak” néven elmentve.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4215"/>
        <source>Really delete all records?</source>
        <translation>Valóban törli az összes mérést?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4226"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Minden mérési adat törölve, és a korábbi &quot;ubpm.sql&quot; adatbázis a Lomtárba dobva.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4546"/>
        <source>- UBPM Application
</source>
        <translation>- UBPM Alkalmazás
</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4561"/>
        <source>- UBPM Plugins
</source>
        <translation>- UBPM Plugin-ok
</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4705"/>
        <location filename="../MainWindow.cpp" line="4709"/>
        <location filename="../MainWindow.cpp" line="4713"/>
        <location filename="../MainWindow.h" line="162"/>
        <location filename="../MainWindow.h" line="166"/>
        <location filename="../MainWindow.h" line="170"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4706"/>
        <location filename="../MainWindow.cpp" line="4710"/>
        <location filename="../MainWindow.cpp" line="4714"/>
        <location filename="../MainWindow.h" line="163"/>
        <location filename="../MainWindow.h" line="167"/>
        <location filename="../MainWindow.h" line="171"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4779"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>A &quot;%1&quot; téma nem nyitható meg!

Hibaüzenet: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2028"/>
        <location filename="../MainWindow.cpp" line="2045"/>
        <location filename="../MainWindow.cpp" line="5665"/>
        <source>Delete record</source>
        <translation>Mérés törlése</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5592"/>
        <source>Show Heartrate</source>
        <translation>Pulzus megjelenítése</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5628"/>
        <location filename="../MainWindow.cpp" line="5639"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation>A szimbólumok és a vonalak nem lehetnek egyidejűleg lekapcsolva!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5667"/>
        <source>Show record</source>
        <translation>Mérés látható</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2029"/>
        <location filename="../MainWindow.cpp" line="2052"/>
        <location filename="../MainWindow.cpp" line="5668"/>
        <source>Hide record</source>
        <translation>Mérés elrejtése</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2047"/>
        <location filename="../MainWindow.cpp" line="5679"/>
        <source>Really delete selected record?</source>
        <translation>Valóban törli a kijelölt mérést?</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="4658"/>
        <source>The following %n translation(s) for &quot;%1&quot; could not be loaded:

%2
Hide this warning on program startup?</source>
        <translation>
            <numerusform>Az alábbi %n kifejezés fordítása nem található „%1” nyelvhez:

%2
El kívánja rejteni ezt a indításkor megjelenő figyelmeztetést?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5561"/>
        <source>Scrolling has reached top/bottom, show next/previous period?</source>
        <translation>A görgetés a végére ért, nézzük a következő időszakot?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5584"/>
        <source>Dynamic Scaling</source>
        <translation>Dinamikus skála</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5585"/>
        <source>Colored Stripes</source>
        <translation>Színes sávok</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5587"/>
        <source>Show Symbols</source>
        <translation>Szimbólumok kijelzése</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5588"/>
        <source>Show Lines</source>
        <translation>Vonalak megjelenítése</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5590"/>
        <source>Colored Symbols</source>
        <translation>Színes szimbólumok</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5700"/>
        <source>Show Median</source>
        <translation>Medián kijelzése</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5701"/>
        <source>Show Values</source>
        <translation>Értékek kijelzése</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5756"/>
        <source>Really quit program?</source>
        <translation>Valóban kilép a programból?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Univerzális vérnyomás nyilvántartó</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation>Az alkalmazás már fut és egyszerre több példányban futtatás nem lehetséges.

Váltson át a már futó példányra, vagy zárja be azt és próbálja meg újra.</translation>
    </message>
</context>
</TS>
