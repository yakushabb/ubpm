<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>DialogAbout</name>
    <message>
        <location filename="../DialogAbout.ui" line="14"/>
        <source>About UBPM</source>
        <translation>Über UBPM</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="41"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="53"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universeller Blutdruck Manager</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="91"/>
        <source>Qt Framework</source>
        <translation>Qt Framework</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="129"/>
        <source>Operating System</source>
        <translation>Betriebssystem</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="220"/>
        <source>Environment</source>
        <translation>Umgebung</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="231"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="266"/>
        <source>Database</source>
        <translation>Datenbank</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="301"/>
        <source>Cache</source>
        <translation>Zwischenspeicher</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="336"/>
        <source>Guides</source>
        <translation>Handbücher</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="371"/>
        <source>Languages</source>
        <translation>Sprachen</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="406"/>
        <source>Plugins</source>
        <translation>Erweiterungen</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="441"/>
        <source>Themes</source>
        <translation>Farbschemas</translation>
    </message>
    <message>
        <location filename="../DialogAbout.ui" line="469"/>
        <source>Translators</source>
        <translation>Übersetzer</translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="14"/>
        <source>The program is provided as is with no warranty of any kind, including the warranty of design, merchantability and fitness for a particular purpose.</source>
        <translation>Das Programm wird ohne jegliche Gewährleistung bereitgestellt, einschließlich der Gewährleistung von Design, Marktgängigkeit und Eignung für einen bestimmten Zweck.</translation>
    </message>
    <message>
        <location filename="../DialogAbout.cpp" line="37"/>
        <source>Could not open &quot;%1&quot;.</source>
        <translation>Konnte &quot;%1&quot; nicht öffnen.</translation>
    </message>
</context>
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="14"/>
        <source>Data Analysis</source>
        <translation>Daten Analyse</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="24"/>
        <source>Query</source>
        <translation>Abfrage</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="43"/>
        <location filename="../DialogAnalysis.cpp" line="209"/>
        <source>Results</source>
        <translation>Ergebnisse</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="82"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="91"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="100"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="109"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="118"/>
        <source>P.Pressure</source>
        <translation>Pulsdruck</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="127"/>
        <source>Heartrate</source>
        <translation>Herzfrequenz</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="136"/>
        <source>Irregular</source>
        <translation>Unregelmäßig</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="145"/>
        <source>Movement</source>
        <translation>Bewegung</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="154"/>
        <source>Invisible</source>
        <translation>Unsichtbar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="163"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="193"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="51"/>
        <source>Could not create memory database!

%1</source>
        <translation>Konnte keine Speicherdatenbank erstellen!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>No results for this query found!</source>
        <translation>Keine Ergebnisse für diese Abfrage gefunden!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 für %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Ergebnis</numerusform>
            <numerusform>Ergebnisse</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Treffer</numerusform>
            <numerusform>Treffer</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Datensatz</numerusform>
            <numerusform>Datensätze</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="217"/>
        <source>User %1</source>
        <translation>Benutzer %1</translation>
    </message>
</context>
<context>
    <name>DialogDistribution</name>
    <message>
        <location filename="../DialogDistribution.ui" line="20"/>
        <source>Data Distribution</source>
        <translation>Daten Verteilung</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="77"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="88"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.ui" line="99"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogDistribution.cpp" line="18"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Datensatz</numerusform>
            <numerusform>Datensätze</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="119"/>
        <source>Low</source>
        <translation>Niedrig</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="121"/>
        <source>Optimal</source>
        <translation>Optimal</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="123"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="125"/>
        <source>High Normal</source>
        <translation>Hoch Normal</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="127"/>
        <source>Hyper 1</source>
        <translation>Hyper 1</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="129"/>
        <source>Hyper 2</source>
        <translation>Hyper 2</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="131"/>
        <source>Hyper 3</source>
        <translation>Hyper 3</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="148"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="160"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="171"/>
        <source>Blood Pressure</source>
        <translation>Blutdruck</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="210"/>
        <source>Athlete</source>
        <translation>Sportler</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="212"/>
        <source>Excellent</source>
        <translation>Hervorragend</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="214"/>
        <source>Great</source>
        <translation>Sehr gut</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="216"/>
        <source>Good</source>
        <translation>Gut</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="218"/>
        <source>Average</source>
        <translation>Durchschnitt</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="220"/>
        <source>Below Average</source>
        <translation>Unter Durchschnitt</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="222"/>
        <source>Poor</source>
        <translation>Schlecht</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="239"/>
        <source>BPM</source>
        <translation>SPM</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="265"/>
        <source>Heart Rate</source>
        <translation>Herzfrequenz</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="346"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Erstellt mit UBPM für
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="347"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Kostenlos und quelloffen
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="352"/>
        <source>%1 (Age: %2, Height: %3 cm, Weight: %4 kg, BMI: %5)</source>
        <translation>%1 (Alter: %2, Größe: %3 cm, Gewicht: %4 kg, BMI: %5)</translation>
    </message>
    <message>
        <location filename="../DialogDistribution.cpp" line="352"/>
        <source>User %1</source>
        <translation>Benutzer %1</translation>
    </message>
</context>
<context>
    <name>DialogDonation</name>
    <message>
        <location filename="../DialogDonation.ui" line="14"/>
        <source>Donation</source>
        <translation>Spende</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="67"/>
        <source>Copy E-Mail and open Amazon</source>
        <translation>E-Mail kopieren und Amazon öffnen</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="154"/>
        <source>Open PayPal (+)</source>
        <translation>Öffne PayPal (+)</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="171"/>
        <source>Open PayPal (-)</source>
        <translation>Öffne PayPal (-)</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="46"/>
        <source>If you have an Amazon account, you can send money by gift card.

Click the button below to open the Amazon website in your browser and

- sign in to your account
- select or enter the amount you would like to send
- select e-mail delivery
- paste the e-mail address as the recipient
- add a message
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Wenn Sie ein Amazon-Konto haben, können Sie Geld per Geschenkkarte senden.

Klicken Sie auf die Schaltfläche unten, um die Amazon-Webseite in Ihrem Browser zu öffnen und

- melden Sie sich bei Ihrem Konto an
- wählen Sie den gewünschten Betrag aus oder geben Sie ihn ein
- wählen Sie E-Mail Versand
- fügen Sie die E-Mail Adresse als Empfänger ein
- geben Sie eine Nachricht ein
- schließen Sie die Spende ab

Sie können auch den QR-Code mit Ihrem mobilen Gerät scannen.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="124"/>
        <source>If you have a PayPal account, you can send money using the &quot;Friends and Family&quot; feature.

Open your PayPal app and

- search for @LazyT (Thomas Löwe)
- enter the amount you would like to send
- add a message
- complete the donation

You can also click the first button below to do this in your browser without using the app.

If you do not have a PayPal account but do have a credit/debit card click the second button below to go to the PayPal donation page and

- select onetime or repeating donation
- select or enter the amount you would like to send
- optionally check the box to cover the fees
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Wenn Sie ein PayPal-Konto haben, können Sie mit der Funktion &quot;Freunde und Familie&quot; Geld senden.

Öffnen Sie Ihre PayPal-App und

- suchen Sie nach @LazyT (Thomas Löwe)
- geben Sie den Betrag ein den Sie senden möchten
- fügen Sie eine Nachricht hinzu
- schließen Sie die Spende ab

Sie können auch auf die erste Schaltfläche unten klicken, um dies in Ihrem Browser zu tun ohne die App zu verwenden.

Wenn Sie kein PayPal-Konto haben, aber eine Kredit-/Debitkarte besitzen, klicken Sie auf die zweite Schaltfläche unten um zur PayPal-Spendenseite zu gelangen und

- wählen Sie einmalige oder wiederkehrende Spende
- wählen Sie den gewünschten Betrag aus oder geben Sie ihn ein
- aktivieren Sie optional das Kästchen zur Deckung der Gebühren
- schließen Sie die Spende ab

Sie können auch den QR-Code mit Ihrem mobilen Gerät scannen.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="230"/>
        <source>If you are in the Euro Payments Area, you can transfer money from your bank account.

Click the buttons below to copy the name/IBAN/BIC to the clipboard and

- sign in to your bank account
- paste the name/IBAN/BIC into the transfer form
- enter the amount you would like to send
- complete the donation

You can also scan the QR code with your mobile device.</source>
        <translation>Wenn Sie sich in der Euro-Zahlungszone befinden, können Sie Geld von Ihrem Bankkonto überweisen.

Klicken Sie auf die Schaltflächen unten um Namen/IBAN/BIC in die Zwischenablage zu kopieren und

- melden Sie sich bei Ihrem Bankkonto an
- fügen Sie Namen/IBAN/BIC in das Überweisungsformular ein
- geben Sie den zu überweisenden Betrag ein
- schließen Sie die Spende ab

Sie können auch den QR-Code mit Ihrem mobilen Gerät scannen.</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="251"/>
        <source>Copy Name</source>
        <translation>Kopiere Name</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="268"/>
        <source>Copy IBAN</source>
        <translation>Kopiere IBAN</translation>
    </message>
    <message>
        <location filename="../DialogDonation.ui" line="285"/>
        <source>Copy BIC</source>
        <translation>Kopiere BIC</translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Handbuch</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="61"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>%1 Handbuch nicht gefunden, zeige stattdessen EN Handbuch an.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="53"/>
        <source>%1 guide not found!</source>
        <translation>%1 Handbuch nicht gefunden!</translation>
    </message>
</context>
<context>
    <name>DialogMigration</name>
    <message>
        <location filename="../DialogMigration.ui" line="14"/>
        <source>Data Migration</source>
        <translation>Daten-Migration</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="24"/>
        <source>Source</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="72"/>
        <source>Open Data Source</source>
        <translation>Datenquelle öffnen</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="97"/>
        <source>Parameters</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="217"/>
        <source>Date Format</source>
        <translation>Datumsformat</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="267"/>
        <source>Irregular Detection</source>
        <translation>Erkennung Unregelmäßigkeit</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="167"/>
        <source>Startline</source>
        <translation>Startzeile</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="183"/>
        <source>Elements per Line</source>
        <translation>Elemente pro Zeile</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="111"/>
        <source>Delimiter</source>
        <translation>Trennzeichen</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="303"/>
        <source>Time Format</source>
        <translation>Zeitformat</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="199"/>
        <source>Movement Detection</source>
        <translation>Erkennung Bewegung</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="239"/>
        <source>Date Language</source>
        <translation>Datumssprache</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="344"/>
        <source>Positions</source>
        <translation>Positionen</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="488"/>
        <source>Irregular</source>
        <translation>Unregelmäßigkeit</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="504"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="452"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="410"/>
        <source>Heartrate</source>
        <translation>Herzfrequenz</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="394"/>
        <source>Movement</source>
        <translation>Bewegung</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="329"/>
        <source>Ignore Comment</source>
        <translation>Kommentar ignorieren</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="368"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="436"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="550"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="567"/>
        <source>Result</source>
        <translation>Ergebnis</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="786"/>
        <source>Migrate User 1</source>
        <translation>Benutzer 1 migrieren</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="815"/>
        <source>Migrate User 2</source>
        <translation>Benutzer 2 migrieren</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="841"/>
        <source>Copy to Custom</source>
        <translation>Kopieren nach Benutzerdefiniert</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="861"/>
        <source>Select predefined settings</source>
        <translation>Vordefinierte Einstellungen auswählen</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="895"/>
        <source>Custom</source>
        <translation>Benutzerdefiniert</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="906"/>
        <source>Test Data Migration</source>
        <translation>Test der Daten-Migration</translation>
    </message>
    <message>
        <location filename="../DialogMigration.ui" line="920"/>
        <source>Start Data Migration</source>
        <translation>Start der Daten-Migration</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>Choose Data Source for Migration</source>
        <translation>Datenquelle für Migration auswählen</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="251"/>
        <source>CSV File (*.csv)</source>
        <translation>CSV Datei (*.csv)</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="304"/>
        <source>%L1 Bytes</source>
        <translation>%L1 Bytes</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="305"/>
        <source>%1 Lines</source>
        <translation>%1 Zeilen</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="319"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Konnte &quot;%1&quot; nicht öffnen!

Ursache: %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="341"/>
        <source>Startline can&apos;t be empty.</source>
        <translation>Die Startzeile darf nicht leer sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="347"/>
        <source>Startline must be smaller than the number of lines.</source>
        <translation>Die Startzeile muss kleiner sein als die Anzahl der Zeilen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="355"/>
        <source>Line %1 = %2</source>
        <translation>Zeile %1 = %2</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="360"/>
        <source>Delimiter can&apos;t be empty.</source>
        <translation>Das Trennzeichen darf nicht leer sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="367"/>
        <source>Startline doesn&apos;t contain &quot;%1&quot; delimiter.</source>
        <translation>Die Startzeile enthält kein &quot;%1&quot; Trennzeichen.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="378"/>
        <source>Elements can&apos;t be empty.</source>
        <translation>Elemente darf nicht leer sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="384"/>
        <source>Elements can&apos;t be smaller than 4.</source>
        <translation>Die Anzahl der Elemente darf nicht kleiner als 4 sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="390"/>
        <source>Elements doesn&apos;t match (found %1).</source>
        <translation>Elemente stimmen nicht überein (gefunden %1).</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="397"/>
        <source>Date format can&apos;t be empty.</source>
        <translation>Das Datumsformat darf nicht leer sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="403"/>
        <source>Single date format must also contain a time format.</source>
        <translation>Ein einzelnes Datumsformat muss auch ein Zeitformat enthalten.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="410"/>
        <source>Date position can&apos;t be empty.</source>
        <translation>Die Datumsposition darf nicht leer sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="416"/>
        <source>Date position must be smaller than elements.</source>
        <translation>Die Datumsposition muss kleiner als die Elemente sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="423"/>
        <source>Time position requires also a time parameter.</source>
        <translation>Die Zeitposition erfordert auch einen Zeitparameter.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="429"/>
        <source>Time format requires also a time position.</source>
        <translation>Das Zeitformat erfordert auch eine Zeitposition.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="435"/>
        <source>Time position must be smaller than elements.</source>
        <translation>Die Zeitposition muss kleiner als die Elemente sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="442"/>
        <source>Systolic position can&apos;t be empty.</source>
        <translation>Die systolische Position darf nicht leer sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="448"/>
        <source>Systolic position must be smaller than elements.</source>
        <translation>Die systolische Position muss kleiner als die Elemente sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="455"/>
        <source>Diastolic position can&apos;t be empty.</source>
        <translation>Die diastolische Position darf nicht leer sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="461"/>
        <source>Diastolic position must be smaller than elements.</source>
        <translation>Die diastolische Position muss kleiner als die Elemente sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="468"/>
        <source>Heartrate position can&apos;t be empty.</source>
        <translation>Die Herzfrequenzposition darf nicht leer sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="474"/>
        <source>Heartrate position must be smaller than elements.</source>
        <translation>Die Herzfrequenzposition muss kleiner als die Elemente sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="481"/>
        <source>Irregular position must be smaller than elements.</source>
        <translation>Die Unregelmäßigkeitsposition muss kleiner als die Elemente sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="487"/>
        <source>Irregular position requires also an irregular parameter.</source>
        <translation>Die Unregelmäßigkeitsposition erfordert auch einen Unregelmäßigkeitsparameter.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="494"/>
        <source>Movement position must be smaller than elements.</source>
        <translation>Die Bewegungsposition muss kleiner als die Elemente sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="500"/>
        <source>Movement position requires also a movement parameter.</source>
        <translation>Die Bewegungsposition erfordert auch einen Bewegungsparameter.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="507"/>
        <source>Comment position must be smaller than elements.</source>
        <translation>Die Kommentarposition muss kleiner als die Elemente sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="523"/>
        <source>Positions can&apos;t exist twice.</source>
        <translation>Positionen dürfen nicht doppelt vorhanden sein.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="539"/>
        <source>Date format is invalid.</source>
        <translation>Das Datumsformat ist ungültig.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="546"/>
        <source>Time format is invalid.</source>
        <translation>Das Zeitformat ist ungültig.</translation>
    </message>
    <message>
        <location filename="../DialogMigration.cpp" line="556"/>
        <source>Date/Time format is invalid.</source>
        <translation>Das Datums-/Zeitformat ist ungültig.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="747"/>
        <source>Successfully migrated %n record(s) for user %1.</source>
        <translation>
            <numerusform>Erfolgreich %n Datensatz für Benutzer %1 migriert.</numerusform>
            <numerusform>Erfolgreich %n Datensätze für Benutzer %1 migriert.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="751"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>%n ungültigen Datensatz übersprungen!</numerusform>
            <numerusform>%n ungültige Datensätze übersprungen!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogMigration.cpp" line="756"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>%n doppelten Datensatz übersprungen!</numerusform>
            <numerusform>%n doppelte Datensätze übersprungen!</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Manueller Datensatz</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Datensatz</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="211"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Datensatz für %1 hinzufügen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Datum &amp; Zeit auswählen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>SYS eingeben</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="148"/>
        <source>Enter DIA</source>
        <translation>DIA eingeben</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="167"/>
        <source>Enter BPM</source>
        <translation>SPM eingeben</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Unregelmäßiger Herzschlag</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="186"/>
        <source>Movement</source>
        <translation>Bewegung</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="239"/>
        <source>Enter Optional Comment</source>
        <translation>Optionalen Kommentar eingeben</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="269"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Meldung bei erfolgreichem Erstellen / Löschen / Ändern</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="287"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="304"/>
        <source>Create</source>
        <translation>Erstellen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="324"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Benutzer 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Benutzer 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="34"/>
        <location filename="../DialogRecord.cpp" line="187"/>
        <source>Modify</source>
        <translation>Ändern</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="132"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Der Datensatz konnte nicht gelöscht werden!

Ein Eintrag für dieses Datum und diese Uhrzeit existiert nicht.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="138"/>
        <source>Data record successfully deleted.</source>
        <translation>Datensatz erfolgreich gelöscht.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="150"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>Bitte zuerst einen gültigen Wert für &quot;SYS&quot; eingeben!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="160"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>Bitte zuerst einen gültigen Wert für &quot;DIA&quot; eingeben!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="169"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>Bitte zuerst einen gültigen Wert für &quot;SPM&quot; eingeben!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="191"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>Der Datensatz konnte nicht geändert werden!

Ein Eintrag für dieses Datum und diese Uhrzeit existiert nicht.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="197"/>
        <source>Data Record successfully modified.</source>
        <translation>Datensatz erfolgreich geändert.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="206"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>Der Datensatz konnte nicht erstellt werden!

Es existiert bereits ein Eintrag für dieses Datum und diese Uhrzeit.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="212"/>
        <source>Data Record successfully created.</source>
        <translation>Datensatz erfolgreich erstellt.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="161"/>
        <source>Choose Database Location</source>
        <translation>Datenbank-Standort wählen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="171"/>
        <source>Choose Database Backup Location</source>
        <translation>Datenbank-Sicherungsort wählen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="308"/>
        <source>The selected database already exists.

Please choose the preferred action.</source>
        <translation>Die ausgewählte Datenbank existiert bereits.

Bitte die gewünschte Aktion wählen.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="310"/>
        <source>Overwrite</source>
        <translation>Überschreiben</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="311"/>
        <source>Merge</source>
        <translation>Zusammenführen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="312"/>
        <source>Swap</source>
        <translation>Austauschen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="314"/>
        <source>Overwrite:

Overwrites the selected database with the current database.

Merge:

Merges the selected database into the current database.

Swap:

Deletes the current database and use the selected database.</source>
        <translation>Überschreiben:

Überschreibt die ausgewählte Datenbank mit der aktuellen Datenbank.

Zusammenführen:

Führt die ausgewählte Datenbank mit der aktuellen Datenbank zusammen.

Austauschen:

Löscht die aktuelle Datenbank und verwendet die ausgewählte Datenbank.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="322"/>
        <source>The current database is empty.

The selected database will be erased on exit, if no data will be added.</source>
        <translation>Die aktuelle Datenbank ist leer.

Die ausgewählte Datenbank wird beim Beenden gelöscht, wenn keine Daten hinzugefügt werden.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="352"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>Die SQL-Verschlüsselung kann ohne Passwort nicht aktiviert werden und wird deaktiviert!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="359"/>
        <source>The database backup should be located on a different hard disk, partition or directory.</source>
        <translation>Die Datenbanksicherung sollte auf einer anderen Festplatte, Partition oder Verzeichnis gespeichert werden.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="364"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Bitte gültige Werte für Zusatzinformationen von Benutzer 1 eingeben!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="371"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Bitte gültige Werte für Zusatzinformationen von Benutzer 2 eingeben!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="378"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>Eingegebenes Alter stimmt nicht mit der gewählten Altersgruppe für Benutzer 1 überein!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="385"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>Eingegebenes Alter stimmt nicht mit der gewählten Altersgruppe für Benutzer 2 überein!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="393"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation>Bitte Symbole oder Linien für das Diagramm aktivieren!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="400"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Bitte eine gültige E-Mail-Adresse eingeben!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="407"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Bitte einen E-Mail-Betreff eingeben!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="414"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>E-Mail-Nachricht muss $CHART, $TABLE und/oder $STATS enthalten!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="525"/>
        <source>User 1</source>
        <translation>Benutzer 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="539"/>
        <source>User 2</source>
        <translation>Benutzer 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="652"/>
        <source>Blood Pressure Report</source>
        <translation>Blutdruckbericht</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="653"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Sehr geehrter Herr Dr. Mustermann,

anbei finden Sie meine Blutdruckdaten für diesen Monat.

Mit freundlichen Grüßen,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="695"/>
        <source>Abort setup and discard all changes?</source>
        <translation>Einstellungen abbrechen und alle Änderungen verwerfen?</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Datenbank</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="52"/>
        <source>Location</source>
        <translation>Standort</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="58"/>
        <source>Current Location</source>
        <translation>Aktueller Standort</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="74"/>
        <source>Change Location</source>
        <translation>Standort ändern</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="100"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Mit SQLCipher verschlüsseln</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="103"/>
        <source>Encryption</source>
        <translation>Verschlüsselung</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="115"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="128"/>
        <source>Show Password</source>
        <translation>Passwort anzeigen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="282"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="317"/>
        <location filename="../DialogSettings.ui" line="580"/>
        <source>Male</source>
        <translation>Männlich</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="346"/>
        <location filename="../DialogSettings.ui" line="609"/>
        <source>Female</source>
        <translation>Weiblich</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="442"/>
        <location filename="../DialogSettings.ui" line="714"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="311"/>
        <location filename="../DialogSettings.ui" line="574"/>
        <source>Mandatory Information</source>
        <translation>Pflichtangaben</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="372"/>
        <location filename="../DialogSettings.ui" line="644"/>
        <source>Age Group</source>
        <translation>Altersgruppe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="458"/>
        <location filename="../DialogSettings.ui" line="730"/>
        <source>Additional Information</source>
        <translation>Zusatzangaben</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="518"/>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Height</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="547"/>
        <location filename="../DialogSettings.ui" line="819"/>
        <source>Weight</source>
        <translation>Gewicht</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="844"/>
        <source>Device</source>
        <translation>Gerät</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="850"/>
        <location filename="../DialogSettings.cpp" line="70"/>
        <source>Import Plugins [ %1 ]</source>
        <translation>Import Erweiterungen [ %1 ]</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="898"/>
        <source>Open Website</source>
        <translation>Webseite öffnen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="911"/>
        <source>Maintainer</source>
        <translation>Betreuer</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="939"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="946"/>
        <source>Send E-Mail</source>
        <translation>E-Mail senden</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="959"/>
        <source>Model</source>
        <translation>Modell</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="966"/>
        <source>Producer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="983"/>
        <source>Chart</source>
        <translation>Diagramm</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1110"/>
        <source>X-Axis Range</source>
        <translation>Bereich X-Achse</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1116"/>
        <source>Dynamic Scaling</source>
        <translation>Dynamische Skalierung</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1097"/>
        <source>Healthy Ranges</source>
        <translation>Gesunde Bereiche</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1014"/>
        <source>Symbols</source>
        <translation>Symbole</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1091"/>
        <source>Colored Areas</source>
        <translation>Farbbereiche</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="863"/>
        <source>Please choose Device Plugin…</source>
        <translation>Bitte Geräte Erweiterung wählen…</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1238"/>
        <location filename="../DialogSettings.ui" line="1513"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1326"/>
        <location filename="../DialogSettings.ui" line="1601"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1414"/>
        <location filename="../DialogSettings.ui" line="1689"/>
        <source>Heartrate</source>
        <translation>Herzfrequenz</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1786"/>
        <source>Table</source>
        <translation>Tabelle</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1871"/>
        <location filename="../DialogSettings.ui" line="2054"/>
        <source>Systolic Warnlevel</source>
        <translation>Systolische Warnstufe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1817"/>
        <location filename="../DialogSettings.ui" line="2114"/>
        <source>Diastolic Warnlevel</source>
        <translation>Diastolische Warnstufe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="153"/>
        <source>Automatic Backup</source>
        <translation>Automatische Sicherung</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="156"/>
        <source>Backup</source>
        <translation>Sicherung</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="173"/>
        <source>Current Backup Location</source>
        <translation>Aktueller Sicherungsort</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="186"/>
        <source>Change Backup Location</source>
        <translation>Sicherungsort ändern</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="213"/>
        <source>Backup Mode</source>
        <translation>Sicherungsmodus</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="223"/>
        <source>Daily</source>
        <translation>Täglich</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="232"/>
        <source>Weekly</source>
        <translation>Wöchentlich</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="241"/>
        <source>Monthly</source>
        <translation>Monatlich</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="253"/>
        <source>Keep Copies</source>
        <translation>Kopien behalten</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="486"/>
        <location filename="../DialogSettings.ui" line="758"/>
        <source>Birthday</source>
        <translation>Geburtstag</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1023"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1047"/>
        <location filename="../DialogSettings.ui" line="1075"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1129"/>
        <source>Lines</source>
        <translation>Linien</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1138"/>
        <location filename="../DialogSettings.ui" line="1166"/>
        <source>Width</source>
        <translation>Breite</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1195"/>
        <source>Show Heartrate</source>
        <translation>Herzfrequenz anzeigen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1201"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Herzfrequenz in der Diagramm-Ansicht anzeigen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1211"/>
        <source>Print Heartrate</source>
        <translation>Herzfrequenz drucken</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1217"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Herzfrequenz auf separatem Blatt drucken</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1931"/>
        <location filename="../DialogSettings.ui" line="2168"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Pulsdruck Warnstufe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1985"/>
        <location filename="../DialogSettings.ui" line="2222"/>
        <source>Heartrate Warnlevel</source>
        <translation>Herzfrequenz Warnstufe</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2287"/>
        <source>Statistic</source>
        <translation>Statistik</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2293"/>
        <source>Bar Type</source>
        <translation>Balken Typ</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2299"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Median anstelle Mittelwert Balken anzeigen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2309"/>
        <source>Legend Type</source>
        <translation>Legenden Typ</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2315"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Werte anstelle von Beschreibungen anzeigen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2330"/>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2338"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2357"/>
        <source>Subject</source>
        <translation>Betreff</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2375"/>
        <source>Message</source>
        <translation>Nachricht</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2396"/>
        <source>Plugin</source>
        <translation>Erweiterung</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2402"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2408"/>
        <source>Log Device Communication to File</source>
        <translation>Gerätekommunikation in Datei protokollieren</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2415"/>
        <source>Import Measurements automatically</source>
        <translation>Messwerte automatisch importieren</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2425"/>
        <source>Bluetooth</source>
        <translation>Bluetooth</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2431"/>
        <source>Discover Device automatically</source>
        <translation>Gerät automatisch entdecken</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2438"/>
        <source>Connect Device automatically</source>
        <translation>Gerät automatisch verbinden</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2453"/>
        <source>Update</source>
        <translation>Aktualisierung</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2459"/>
        <source>Autostart</source>
        <translation>Autostart</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2465"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Bei Programmstart auf Aktualisierungen prüfen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2478"/>
        <source>Notification</source>
        <translation>Benachrichtigung</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2484"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Ergebnis nach Online Prüfung immer anzeigen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2500"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2517"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2534"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Online Aktualisierung</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Verfügbare Version</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Aktualisierungsgröße</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Installierte Version</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="181"/>
        <location filename="../DialogUpdate.ui" line="202"/>
        <source>Download</source>
        <translation>Herunterladen</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="222"/>
        <source>Ignore</source>
        <translation>Ignorieren</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="183"/>
        <source>No new version found.</source>
        <translation>Keine neue Version gefunden.</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="42"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>!!! SSL WARNUNG - SORGFÄLTIG LESEN !!!

Netzwerk-Verbindungsproblem:

%1
Möchten Sie trotzdem fortfahren?</numerusform>
            <numerusform>!!! SSL WARNUNG - SORGFÄLTIG LESEN !!!

Netzwerk-Verbindungsprobleme:

%1
Möchten Sie trotzdem fortfahren?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Downloading update failed!

%1</source>
        <translation>Herunterladen der Aktualisierung fehlgeschlagen!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="73"/>
        <source>Checking update failed!

%1</source>
        <translation>Überprüfung der Aktualisierung fehlgeschlagen!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="156"/>
        <source>Unexpected response from update server!</source>
        <translation>Unerwartete Antwort vom Aktualisierungsserver!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="173"/>
        <source>*%1 not found</source>
        <translation>*%1 nicht gefunden</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="203"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>Aktualisierung hat nicht die erwartete Größe!

%L1 : %L2

Erneut herunterladen…</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="207"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Aktualisierung gespeichert nach %1.

Neue Version jetzt starten?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="220"/>
        <source>Could not start new version!</source>
        <translation>Konnte neue Version nicht starten!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="247"/>
        <source>The program was installed by Flatpak.

Please update using the Flatpak internal update function.</source>
        <translation>Das Programm wurde von Flatpak installiert.

Bitte aktualisieren Sie es mit der internen Aktualisierungsfunktion von Flatpak.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="253"/>
        <source>The program was installed by Snap.

Please update using the Snap internal update function.</source>
        <translation>Das Programm wurde von Snap installiert.

Bitte aktualisieren Sie es mit der internen Aktualisierungsfunktion von Snap.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="259"/>
        <source>The program was installed from Windows store.

Please update using the store internal update function.</source>
        <translation>Das Programm wurde aus dem Windows Store installiert.

Bitte aktualisieren Sie es mit der internen Aktualisierungsfunktion des Stores.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="265"/>
        <source>The program was installed from distribution.

Please update using the operating systems internal update function.</source>
        <translation>Das Programm wurde von der Distribution installiert.

Bitte aktualisieren Sie es mit der internen Aktualisierungsfunktion des Betriebssystems.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="271"/>
        <source>The program was installed from source code.

Please update the sources, rebuild and reinstall manually.</source>
        <translation>Das Programm wurde aus dem Quellcode installiert.

Bitte aktualisieren Sie die Quellen, erstellen das Programm neu und installieren Sie es manuell.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="297"/>
        <source>Really abort download?</source>
        <translation>Herunterladen wirklich abbrechen?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="227"/>
        <source>Could not save update to %1!

%2</source>
        <translation>Konnte Aktualisierung nicht nach %1 speichern!

%2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="14"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universeller Blutdruck Manager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="30"/>
        <source>Show Previous Period</source>
        <translation>Vorherigen Zeitraum anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="104"/>
        <source>Show Next Period</source>
        <translation>Nächsten Zeitraum anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Diagramm-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Tabellen-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="159"/>
        <location filename="../MainWindow.cpp" line="4694"/>
        <source>Systolic</source>
        <translation>Systolisch</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="160"/>
        <location filename="../MainWindow.cpp" line="4695"/>
        <source>Diastolic</source>
        <translation>Diastolisch</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Pulsdruck</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Unregelmäßig</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Bewegung</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Unsichtbar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Statistik-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>¼-Stündlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>½-Stündlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Stündlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>¼-Täglich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>½-Täglich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Täglich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>3 Days</source>
        <translation>3 Tage</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Weekly</source>
        <translation>Wöchentlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Monthly</source>
        <translation>Monatlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>Quarterly</source>
        <translation>Vierteljährlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>½ Yearly</source>
        <translation>Halbjährlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="765"/>
        <source>Yearly</source>
        <translation>Jährlich</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="825"/>
        <source>Last 3 Days</source>
        <translation>Letzte 3 Tage</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="857"/>
        <source>Last 7 Days</source>
        <translation>Letzte 7 Tage</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="889"/>
        <source>Last 14 Days</source>
        <translation>Letzte 14 Tage</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="921"/>
        <source>Last 21 Days</source>
        <translation>Letzte 21 Tage</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="953"/>
        <source>Last 28 Days</source>
        <translation>Letzte 28 Tage</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="985"/>
        <source>Last 3 Months</source>
        <translation>Letzte 3 Monate</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1017"/>
        <source>Last 6 Months</source>
        <translation>Letzte 6 Monate</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1049"/>
        <source>Last 9 Months</source>
        <translation>Letzte 9 Monate</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1081"/>
        <source>Last 12 Months</source>
        <translation>Letzte 12 Monate</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1113"/>
        <source>All Records</source>
        <translation>Alle Datensätze</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1160"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1164"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1186"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1203"/>
        <source>Configuration</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1207"/>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1216"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1225"/>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1234"/>
        <source>Charts</source>
        <translation>Diagramme</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1259"/>
        <source>Database</source>
        <translation>Datenbank</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1263"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1275"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1290"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1382"/>
        <location filename="../MainWindow.ui" line="1385"/>
        <source>Quit Program</source>
        <translation>Programm beenden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1397"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1400"/>
        <location filename="../MainWindow.ui" line="1403"/>
        <source>About Program</source>
        <translation>Über das Programm</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <source>Guide</source>
        <translation>Handbuch</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1415"/>
        <location filename="../MainWindow.ui" line="1418"/>
        <source>Show Guide</source>
        <translation>Handbuch anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1427"/>
        <source>Update</source>
        <translation>Aktualisierung</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1430"/>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>Check Update</source>
        <translation>Aktualisierung prüfen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1592"/>
        <source>To PDF</source>
        <translation>Als PDF</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <location filename="../MainWindow.ui" line="1598"/>
        <source>Export To PDF</source>
        <translation>Als PDF exportieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1607"/>
        <source>Migration</source>
        <translation>Migration</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <source>Migrate from Vendor</source>
        <translation>Vom Hersteller migrieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1857"/>
        <source>Translation</source>
        <translation>Übersetzung</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1860"/>
        <location filename="../MainWindow.ui" line="1863"/>
        <source>Contribute Translation</source>
        <translation>Übersetzung beisteuern</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1872"/>
        <source>E-Mail</source>
        <translation>E-Mail</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1875"/>
        <location filename="../MainWindow.ui" line="1878"/>
        <source>Send E-Mail</source>
        <translation>E-Mail senden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1890"/>
        <source>Icons</source>
        <translation>Symbole</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1893"/>
        <location filename="../MainWindow.ui" line="1896"/>
        <source>Change Icon Color</source>
        <translation>Symbolfarbe ändern</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1907"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1910"/>
        <location filename="../MainWindow.ui" line="1913"/>
        <source>System Colors</source>
        <translation>Systemfarben</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1921"/>
        <source>Light</source>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1924"/>
        <location filename="../MainWindow.ui" line="1927"/>
        <source>Light Colors</source>
        <translation>Helle Farben</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1935"/>
        <source>Dark</source>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1938"/>
        <location filename="../MainWindow.ui" line="1941"/>
        <source>Dark Colors</source>
        <translation>Dunkle Farben</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1950"/>
        <source>Donation</source>
        <translation>Spende</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1965"/>
        <source>Distribution</source>
        <translation>Verteilung</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1968"/>
        <location filename="../MainWindow.ui" line="1971"/>
        <source>Show Distribution</source>
        <translation>Verteilung anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1983"/>
        <location filename="../MainWindow.ui" line="1986"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1995"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1998"/>
        <location filename="../MainWindow.ui" line="2001"/>
        <source>Read Wiki</source>
        <translation>Wiki lesen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="161"/>
        <location filename="../MainWindow.cpp" line="4696"/>
        <source>Heartrate</source>
        <translation>Herzfrequenz</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1953"/>
        <location filename="../MainWindow.ui" line="1956"/>
        <source>Make Donation</source>
        <translation>Spende tätigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1445"/>
        <source>Bugreport</source>
        <translation>Fehlerbericht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1448"/>
        <location filename="../MainWindow.ui" line="1451"/>
        <source>Send Bugreport</source>
        <translation>Fehlerbericht senden</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1460"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1463"/>
        <location filename="../MainWindow.ui" line="1466"/>
        <source>Change Settings</source>
        <translation>Einstellungen ändern</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>From Device</source>
        <translation>Von Gerät</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1484"/>
        <location filename="../MainWindow.ui" line="1487"/>
        <source>Import From Device</source>
        <translation>Von Gerät importieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1499"/>
        <source>From File</source>
        <translation>Von Datei</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1502"/>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>Import From File</source>
        <translation>Von Datei importieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1514"/>
        <source>From Input</source>
        <translation>Von Eingabe</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1517"/>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Import From Input</source>
        <translation>Von Eingabe importieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1529"/>
        <source>To CSV</source>
        <translation>Als CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1532"/>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Export To CSV</source>
        <translation>Als CSV exportieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1547"/>
        <source>To XML</source>
        <translation>Als XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1550"/>
        <location filename="../MainWindow.ui" line="1553"/>
        <source>Export To XML</source>
        <translation>Als XML exportieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1562"/>
        <source>To JSON</source>
        <translation>Als JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1565"/>
        <location filename="../MainWindow.ui" line="1568"/>
        <source>Export To JSON</source>
        <translation>Als JSON exportieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1577"/>
        <source>To SQL</source>
        <translation>Als SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <location filename="../MainWindow.ui" line="1583"/>
        <source>Export To SQL</source>
        <translation>Als SQL exportieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1625"/>
        <source>Print Chart</source>
        <translation>Diagramm drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1628"/>
        <location filename="../MainWindow.ui" line="1631"/>
        <source>Print Chart View</source>
        <translation>Diagramm-Ansicht drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1640"/>
        <source>Print Table</source>
        <translation>Tabelle drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1643"/>
        <location filename="../MainWindow.ui" line="1646"/>
        <source>Print Table View</source>
        <translation>Tabellen-Ansicht drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1655"/>
        <source>Print Statistic</source>
        <translation>Statistik drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1658"/>
        <location filename="../MainWindow.ui" line="1661"/>
        <source>Print Statistic View</source>
        <translation>Statistik-Ansicht drucken</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1670"/>
        <source>Preview Chart</source>
        <translation>Vorschau Diagramm</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1673"/>
        <location filename="../MainWindow.ui" line="1676"/>
        <source>Preview Chart View</source>
        <translation>Vorschau Diagramm-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1685"/>
        <source>Preview Table</source>
        <translation>Vorschau Tabelle</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1688"/>
        <location filename="../MainWindow.ui" line="1691"/>
        <source>Preview Table View</source>
        <translation>Vorschau Tabellen-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1700"/>
        <source>Preview Statistic</source>
        <translation>Vorschau Statistik</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1703"/>
        <location filename="../MainWindow.ui" line="1706"/>
        <source>Preview Statistic View</source>
        <translation>Vorschau Statistik-Ansicht</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1715"/>
        <location filename="../MainWindow.ui" line="1718"/>
        <location filename="../MainWindow.ui" line="1721"/>
        <source>Clear All</source>
        <translation>Alles löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1733"/>
        <location filename="../MainWindow.ui" line="1736"/>
        <location filename="../MainWindow.ui" line="1739"/>
        <source>Clear User 1</source>
        <translation>Benutzer 1 löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1748"/>
        <location filename="../MainWindow.ui" line="1751"/>
        <location filename="../MainWindow.ui" line="1754"/>
        <source>Clear User 2</source>
        <translation>Benutzer 2 löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1814"/>
        <source>Analysis</source>
        <translation>Analyse</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1817"/>
        <location filename="../MainWindow.ui" line="1820"/>
        <source>Analyze Records</source>
        <translation>Datensätze analysieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1839"/>
        <location filename="../MainWindow.ui" line="1842"/>
        <location filename="../MainWindow.ui" line="1845"/>
        <source>Time Mode</source>
        <translation>Zeit Modus</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="108"/>
        <location filename="../MainWindow.cpp" line="109"/>
        <location filename="../MainWindow.cpp" line="4664"/>
        <location filename="../MainWindow.cpp" line="4665"/>
        <source>Records For Selected User</source>
        <translation>Datensätze für ausgewählten Benutzer</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="116"/>
        <location filename="../MainWindow.cpp" line="117"/>
        <location filename="../MainWindow.cpp" line="4667"/>
        <location filename="../MainWindow.cpp" line="4668"/>
        <source>Select Date &amp; Time</source>
        <translation>Datum &amp; Zeit auswählen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="169"/>
        <location filename="../MainWindow.cpp" line="4698"/>
        <source>Systolic - Value Range</source>
        <translation>Systolisch - Wertebereich</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="170"/>
        <location filename="../MainWindow.cpp" line="4699"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastolisch - Wertebereich</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="173"/>
        <location filename="../MainWindow.cpp" line="4701"/>
        <source>Systolic - Target Area</source>
        <translation>Systolisch - Zielbereich</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="174"/>
        <location filename="../MainWindow.cpp" line="4702"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastolisch - Zielbereich</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1679"/>
        <source>Athlete</source>
        <translation>Sportler</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1680"/>
        <source>Excellent</source>
        <translation>Hervorragend</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1680"/>
        <source>Optimal</source>
        <translation>Optimal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1681"/>
        <source>Great</source>
        <translation>Sehr gut</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1682"/>
        <source>Good</source>
        <translation>Gut</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1681"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1775"/>
        <location filename="../MainWindow.ui" line="1778"/>
        <location filename="../MainWindow.ui" line="1799"/>
        <location filename="../MainWindow.ui" line="1802"/>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="4382"/>
        <location filename="../MainWindow.cpp" line="4383"/>
        <location filename="../MainWindow.cpp" line="4384"/>
        <location filename="../MainWindow.cpp" line="4385"/>
        <location filename="../MainWindow.cpp" line="4728"/>
        <location filename="../MainWindow.cpp" line="4729"/>
        <location filename="../MainWindow.cpp" line="4730"/>
        <location filename="../MainWindow.cpp" line="4731"/>
        <source>Switch To %1</source>
        <translation>Zu %1 wechseln</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="102"/>
        <location filename="../MainWindow.cpp" line="103"/>
        <location filename="../MainWindow.cpp" line="600"/>
        <location filename="../MainWindow.cpp" line="4382"/>
        <location filename="../MainWindow.cpp" line="4383"/>
        <location filename="../MainWindow.cpp" line="4728"/>
        <location filename="../MainWindow.cpp" line="4729"/>
        <source>User 1</source>
        <translation>Benutzer 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="104"/>
        <location filename="../MainWindow.cpp" line="105"/>
        <location filename="../MainWindow.cpp" line="610"/>
        <location filename="../MainWindow.cpp" line="4384"/>
        <location filename="../MainWindow.cpp" line="4385"/>
        <location filename="../MainWindow.cpp" line="4730"/>
        <location filename="../MainWindow.cpp" line="4731"/>
        <source>User 2</source>
        <translation>Benutzer 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="171"/>
        <location filename="../MainWindow.cpp" line="4700"/>
        <source>Heartrate - Value Range</source>
        <translation>Herzfrequenz - Wertebereich</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="175"/>
        <location filename="../MainWindow.cpp" line="4703"/>
        <source>Heartrate - Target Area</source>
        <translation>Herzfrequenz Zielbereich</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="341"/>
        <source>No active device plugin, auto import aborted.</source>
        <translation>Keine aktive Geräte-Erweiterung, Auto-Import abgebrochen.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="358"/>
        <source>Some plugins will not work without Bluetooth permission.</source>
        <translation>Einige Plugins funktionieren nicht ohne Bluetooth Berechtigung.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="461"/>
        <source>Could not create backup directory.

Please check backup location setting.</source>
        <translation>Das Sicherungsverzeichnis konnte nicht erstellt werden.

Bitte die Einstellungen für den Sicherungsort überprüfen.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="490"/>
        <location filename="../MainWindow.cpp" line="513"/>
        <source>Could not backup database:

%1</source>
        <translation>Die Datenbank konnte nicht gesichert werden:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="503"/>
        <source>Could not delete outdated backup:

%1</source>
        <translation>Eine veraltete Sicherung konnte nicht gelöscht werden:

%1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="530"/>
        <source>Could not register icons.</source>
        <translation>Symbole konnten nicht registriert werden.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="663"/>
        <source>Blood Pressure Report</source>
        <translation>Blutdruckbericht</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1191"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  SPM : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1196"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  SPM : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1679"/>
        <source>Low</source>
        <translation>Niedrig</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1682"/>
        <source>High Normal</source>
        <translation>Hoch Normal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1683"/>
        <location filename="../MainWindow.cpp" line="2160"/>
        <location filename="../MainWindow.cpp" line="4707"/>
        <location filename="../MainWindow.cpp" line="4711"/>
        <location filename="../MainWindow.cpp" line="4715"/>
        <location filename="../MainWindow.h" line="164"/>
        <location filename="../MainWindow.h" line="168"/>
        <location filename="../MainWindow.h" line="172"/>
        <source>Average</source>
        <translation>Durchschnitt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1683"/>
        <source>Hyper 1</source>
        <translation>Hyper 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1684"/>
        <source>Below Average</source>
        <translation>Unter Durchschnitt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1684"/>
        <source>Hyper 2</source>
        <translation>Hyper 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1685"/>
        <source>Poor</source>
        <translation>Schlecht</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1685"/>
        <source>Hyper 3</source>
        <translation>Hyper 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1830"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>Scannen der Import-Erweiterung &quot;%1&quot; fehlgeschlagen!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1847"/>
        <location filename="../MainWindow.cpp" line="1874"/>
        <location filename="../MainWindow.cpp" line="4674"/>
        <source>Switch Language to %1</source>
        <translation>Sprache auf %1 umschalten</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1900"/>
        <location filename="../MainWindow.cpp" line="1915"/>
        <location filename="../MainWindow.cpp" line="4682"/>
        <source>Switch Theme to %1</source>
        <translation>Thema auf %1 umschalten</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1940"/>
        <location filename="../MainWindow.cpp" line="1960"/>
        <location filename="../MainWindow.cpp" line="4690"/>
        <source>Switch Style to %1</source>
        <translation>Stil auf %1 umschalten</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2031"/>
        <location filename="../MainWindow.cpp" line="2056"/>
        <source>Edit record</source>
        <translation>Datensatz bearbeiten</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2160"/>
        <location filename="../MainWindow.cpp" line="4708"/>
        <location filename="../MainWindow.cpp" line="4712"/>
        <location filename="../MainWindow.cpp" line="4716"/>
        <location filename="../MainWindow.h" line="165"/>
        <location filename="../MainWindow.h" line="169"/>
        <location filename="../MainWindow.h" line="173"/>
        <source>Median</source>
        <translation>Median</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2162"/>
        <source>Click to swap Average and Median</source>
        <translation>Klicken zum Wechsel zwischen Mittelwert und Median</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2257"/>
        <source>Click to swap Legend and Label</source>
        <translation>Klicken zum Wechsel zwischen Legende und Beschriftung</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2334"/>
        <source>No records to preview for selected time range!</source>
        <translation>Keine Datensätze für Vorschau im ausgewählten Zeitbereich!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2366"/>
        <source>No records to print for selected time range!</source>
        <translation>Keine Datensätze zum Drucken im ausgewählten Zeitbereich!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2412"/>
        <location filename="../MainWindow.cpp" line="2462"/>
        <location filename="../MainWindow.cpp" line="2538"/>
        <location filename="../MainWindow.cpp" line="2669"/>
        <source>User %1</source>
        <translation>Benutzer %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>DATE</source>
        <translation>DATUM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>TIME</source>
        <translation>ZEIT</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>SYS</source>
        <translation>SYS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>DIA</source>
        <translation>DIA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>BPM</source>
        <translation>SPM</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>IHB</source>
        <translation>UHS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>COMMENT</source>
        <translation>KOMMENTAR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>PPR</source>
        <translation>PDR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2574"/>
        <source>MOV</source>
        <translation>BWG</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2734"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>Konnte E-Mail nicht erstellen, da die Base64 Generierung für den Anhang &quot;%1&quot; fehlgeschlagen ist!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3495"/>
        <location filename="../MainWindow.cpp" line="4290"/>
        <source>Chart</source>
        <translation>Diagramm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3495"/>
        <location filename="../MainWindow.cpp" line="4307"/>
        <source>Table</source>
        <translation>Tabelle</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3495"/>
        <location filename="../MainWindow.cpp" line="4324"/>
        <source>Statistic</source>
        <translation>Statistik</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4248"/>
        <source>No records to analyse!</source>
        <translation>Keine Datensätze zum Analysieren!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4260"/>
        <source>No records to display for selected time range!</source>
        <translation>Keine Datensätze zum Anzeigen im ausgewählten Zeitbereich!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4342"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>E-Mail &quot;%1&quot; konnte nicht geöffnet werden!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4352"/>
        <source>Could not start e-mail client!</source>
        <translation>E-Mail-Client konnte nicht gestartet werden!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4357"/>
        <source>No records to e-mail for selected time range!</source>
        <translation>Keine Datensätze für E-Mail im ausgewählten Zeitbereich!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4363"/>
        <source>Select Icon Color</source>
        <translation>Symbolfarbe wählen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4369"/>
        <source>Restart application to apply new icon color.</source>
        <translation>Anwendung neu starten, um die neue Symbolfarbe anzuwenden.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="870"/>
        <source>The database can not be saved.

Please choose another location in the settings or all data will be lost.</source>
        <translation>Die Datenbank kann nicht gespeichert werden.

Bitte wählen Sie in den Einstellungen einen anderen Speicherort, sonst gehen alle Daten verloren.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2406"/>
        <location filename="../MainWindow.cpp" line="2456"/>
        <location filename="../MainWindow.cpp" line="2532"/>
        <location filename="../MainWindow.cpp" line="2663"/>
        <source>Created with UBPM for
Windows / Linux / macOS</source>
        <translation>Erstellt mit UBPM für
Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2407"/>
        <location filename="../MainWindow.cpp" line="2457"/>
        <location filename="../MainWindow.cpp" line="2533"/>
        <location filename="../MainWindow.cpp" line="2664"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Kostenlos und quelloffen
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2816"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Import von CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2816"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>CSV Datei (*.csv);;XML Datei (*.xml);;JSON Datei (*.json);;SQL Datei (*.sql)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2874"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>Konnte &quot;%1&quot; nicht öffnen!

Ursache: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1190"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Messwerte : %1  |  Unregelmäßig : %2  |  Bewegung : %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="664"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Sehr geehrter Herr Dr. House,

anbei finden Sie meine Blutdruckdaten für diesen Monat.

Mit freundlichen Grüßen,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1195"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Messwerte : 0  |  Unregelmäßig : 0  |  Bewegung : 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2412"/>
        <location filename="../MainWindow.cpp" line="2462"/>
        <location filename="../MainWindow.cpp" line="2538"/>
        <location filename="../MainWindow.cpp" line="2669"/>
        <source>%1 (Age: %2, Height: %3 cm, Weight: %4 kg, BMI: %5)</source>
        <translation>%1 (Alter: %2, Größe: %3 cm, Gewicht: %4 kg, BMI: %5)</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2854"/>
        <location filename="../MainWindow.cpp" line="4132"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>Erfolgreich %n Datensatz von %1 importiert.

     Benutzer 1 : %2
     Benutzer 2 : %3</numerusform>
            <numerusform>Erfolgreich %n Datensätze von %1 importiert.

     Benutzer 1 : %2
     Benutzer 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2858"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>%n ungültigen Datensatz übersprungen!</numerusform>
            <numerusform>%n ungültige Datensätze übersprungen!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2863"/>
        <location filename="../MainWindow.cpp" line="4136"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>%n doppelten Datensatz übersprungen!</numerusform>
            <numerusform>%n doppelte Datensätze übersprungen!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3244"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>Sieht nicht nach einer UBPM-Datenbank aus!

Eventuell falsche Verschlüsselungseinstellungen/Passwort?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3297"/>
        <location filename="../MainWindow.cpp" line="3301"/>
        <source>Export to %1</source>
        <translation>Export als %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3297"/>
        <location filename="../MainWindow.cpp" line="3301"/>
        <source>%1 File (*.%2)</source>
        <translation>%1 Datei (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3337"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>Konnte &quot;%1&quot; nicht erzeugen!

Ursache: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3343"/>
        <source>The database is empty, no records to export!</source>
        <translation>Die Datenbank ist leer, keine Datensätze zum exportieren!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3702"/>
        <source>Morning</source>
        <translation>Vormittag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3702"/>
        <source>Afternoon</source>
        <translation>Nachmittag</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3758"/>
        <source>Week</source>
        <translation>Woche</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3813"/>
        <source>Quarter</source>
        <translation>Quartal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3842"/>
        <source>Half Year</source>
        <translation>Halbjahr</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3860"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4066"/>
        <source>Really delete all records for user %1?</source>
        <translation>Wirklich alle Datensätze für Benutzer %1 löschen?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4081"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Alle Datensätze für Benutzer %1 gelöscht und bestehende Datenbank nach &quot;ubpm.sql.bak&quot; gesichert.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4215"/>
        <source>Really delete all records?</source>
        <translation>Wirklich alle Datensätze löschen?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4226"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Alle Datensätze gelöscht und bestehende Datenbank &quot;ubpm.sql&quot; in den Papierkorb verschoben.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4546"/>
        <source>- UBPM Application
</source>
        <translation>- UBPM Anwendung
</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4561"/>
        <source>- UBPM Plugins
</source>
        <translation>- UBPM Erweiterungen
</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4705"/>
        <location filename="../MainWindow.cpp" line="4709"/>
        <location filename="../MainWindow.cpp" line="4713"/>
        <location filename="../MainWindow.h" line="162"/>
        <location filename="../MainWindow.h" line="166"/>
        <location filename="../MainWindow.h" line="170"/>
        <source>Minimum</source>
        <translation>Minimum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4706"/>
        <location filename="../MainWindow.cpp" line="4710"/>
        <location filename="../MainWindow.cpp" line="4714"/>
        <location filename="../MainWindow.h" line="163"/>
        <location filename="../MainWindow.h" line="167"/>
        <location filename="../MainWindow.h" line="171"/>
        <source>Maximum</source>
        <translation>Maximum</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4779"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>Konnte Theme &quot;%1&quot; nicht öffnen!

Ursache: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2028"/>
        <location filename="../MainWindow.cpp" line="2045"/>
        <location filename="../MainWindow.cpp" line="5665"/>
        <source>Delete record</source>
        <translation>Datensatz löschen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5592"/>
        <source>Show Heartrate</source>
        <translation>Herzfrequenz anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5628"/>
        <location filename="../MainWindow.cpp" line="5639"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation>Symbole und Linien können nicht gleichzeitig deaktiviert werden!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5667"/>
        <source>Show record</source>
        <translation>Datensatz einblenden</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2029"/>
        <location filename="../MainWindow.cpp" line="2052"/>
        <location filename="../MainWindow.cpp" line="5668"/>
        <source>Hide record</source>
        <translation>Datensatz ausblenden</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2047"/>
        <location filename="../MainWindow.cpp" line="5679"/>
        <source>Really delete selected record?</source>
        <translation>Ausgewählten Datensatz wirklich löschen?</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="4658"/>
        <source>The following %n translation(s) for &quot;%1&quot; could not be loaded:

%2
Hide this warning on program startup?</source>
        <translation>
            <numerusform>Die folgende %n Übersetzung für &quot;%1&quot; konnte nicht geladen werden:

%2
Diese Warnung beim Programmstart ausblenden?</numerusform>
            <numerusform>Die folgenden %n Übersetzungen für &quot;%1&quot; konnten nicht geladen werden:

%2
Diese Warnung beim Programmstart ausblenden?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5561"/>
        <source>Scrolling has reached top/bottom, show next/previous period?</source>
        <translation>Scrollen ist oben/unten angekommen, den nächsten/vorherigen Zeitraum anzeigen?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5584"/>
        <source>Dynamic Scaling</source>
        <translation>Dynamische Skalierung</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5585"/>
        <source>Colored Stripes</source>
        <translation>Farbige Streifen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5587"/>
        <source>Show Symbols</source>
        <translation>Symbole anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5588"/>
        <source>Show Lines</source>
        <translation>Linien anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5590"/>
        <source>Colored Symbols</source>
        <translation>Farbige Symbole</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5700"/>
        <source>Show Median</source>
        <translation>Median anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5701"/>
        <source>Show Values</source>
        <translation>Werte anzeigen</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="5756"/>
        <source>Really quit program?</source>
        <translation>Programm wirklich beenden?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universeller Blutdruck Manager</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="13"/>
        <source>The application is already running and multiple instances are not allowed.

Switch to the running instance or close it and try again.</source>
        <translation>Die Anwendung wird bereits ausgeführt und mehrere Instanzen sind nicht zulässig.

Zur laufenden Instanz wechseln oder schließen und erneut versuchen.</translation>
    </message>
</context>
</TS>
