#ifndef DEVICEINTERFACE_H
#define DEVICEINTERFACE_H

#define DeviceInterface_iid "page.codeberg.lazyt.ubpm.deviceinterface"
#define LOGFILE QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/ubpm-import.log"

#include <QObject>
#include <QString>

struct DEVICEINFO
{
	QString producer;
	QString model;
	QString alias;
	QString maintainer;
	QString version;
	QString icon;
};

struct HEALTHDATA
{
	qint64 dts;
	int sys;
	int dia;
	int bpm;
	bool ihb;
	bool mov;
	bool inv;
	QString msg;
};

struct SETTINGS
{
	QString dtfshort;
	QString dtflong;
	QString language;
	QString style;
	QString theme;
	QString icons;
	QString imp, exp;
	QByteArray geometry;
	int mode, range, slide;
	int lastuser;
	int chartmode;
	bool translationwarning;
	bool maximize;
	bool hidebuttons;

	struct
	{
		QString location;
		bool encryption;
		QString password;
		bool backup;
		QString backuplocation;
		int backupmode;
		int backupcopies;

	}database;

	struct
	{
		QString gender;
		int agegroup;
		QString name;
		bool addition;
		QDate birth;
		int height;
		int weight;

	}user[2];

	struct
	{
		QString plugin;

	}device;

	struct
	{
		bool dynamic;
		bool colored;
		bool symbols;
		bool lines;
		bool heartrate;
		bool hrsheet;
		bool symbolcolor;
		int symbolsize;
		int linewidth;

		struct
		{
			int sys_max;
			int sys_min;
			int dia_max;
			int dia_min;
			int bpm_max;
			int bpm_min;

		}range[2];

	}chart;

	struct
	{
		int warnsys;
		int warndia;
		int warnbpm;
		int warnppr;

	}table[2];

	struct
	{
		bool median;
		bool legend;

	}stats;

	struct
	{
		QString address;
		QString subject;
		QString message;

	}email;

	struct
	{
		bool logging;
		bool import;

		struct
		{
			bool discover;
			bool connect;
			int discovertime;
			QString connectname;
			int uid1;
			int uid2;

		}bluetooth;

	}plugin;

	struct
	{
		bool autostart;
		bool notification;

	}update;

	struct
	{
		QString startline;
		QString delimiter;
		QString elements;
		QString fmt_date;
		QString fmt_date_lng;
		QString fmt_time;
		QString fmt_ihb;
		QString fmt_mov;
		QString fmt_msg;
		QString pos_date;
		QString pos_time;
		QString pos_sys;
		QString pos_dia;
		QString pos_bpm;
		QString pos_ihb;
		QString pos_mov;
		QString pos_msg;

	}migration;

	struct
	{
		QByteArray geometry;
	}help;
};

class DeviceInterface
{
public:

	virtual ~DeviceInterface() = default;

	virtual DEVICEINFO getDeviceInfo() = 0;
	virtual bool getDeviceData(QWidget*, QString, QVector<HEALTHDATA>*, QVector<HEALTHDATA>*, struct SETTINGS*) = 0;
};

Q_DECLARE_INTERFACE(DeviceInterface, DeviceInterface_iid)

#endif // DEVICEINTERFACE_H
