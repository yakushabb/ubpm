<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Geräte Import</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="84"/>
        <source>Plugin Options</source>
        <translation>Plugin Optionen</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="14"/>
        <source>Generic Bluetooth</source>
        <translation>Generisches Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="36"/>
        <source>Automatically discover devices on startup</source>
        <translation>Automatische Geräteerkennung beim Start</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="39"/>
        <source>Auto Discover</source>
        <translation>Automatische Erkennung</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="74"/>
        <source>Automatically connect to device on startup</source>
        <translation>Automatische Geräteverbindung beim Start</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="77"/>
        <source>Auto Connect</source>
        <translation>Automatische Verbindung</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="106"/>
        <source>Automatically import measurements on startup</source>
        <translation>Automatischer Messwertimport beim Start</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="109"/>
        <source>Auto Import</source>
        <translation>Automatischer Import</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="137"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="24"/>
        <source>Bluetooth Controller</source>
        <translation>Bluetooth Kontroller</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="143"/>
        <source>Select Bluetooth controller</source>
        <translation>Bluetooth-Kontroller auswählen</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="161"/>
        <source>Setup discovery timeout</source>
        <translation>Zeitüberschreitung für Suche einstellen</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="268"/>
        <source>Select Bluetooth device</source>
        <translation>Bluetooth-Gerät auswählen</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="283"/>
        <source>Enter devicename for auto connection</source>
        <translation>Gerätenamen für die automatische Verbindung eingeben</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="296"/>
        <source>Copy devicename</source>
        <translation>Kopiere Gerätenamen</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="384"/>
        <source>Current State of the Device: Unconnected</source>
        <translation>Aktueller Zustand des Gerätes: Unverbunden</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="463"/>
        <source>Manufacturer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="477"/>
        <source>Model</source>
        <translation>Modell</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="542"/>
        <source>Hardware</source>
        <translation>Hardware</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="556"/>
        <source>Software</source>
        <translation>Software</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="630"/>
        <source>Device Measurements</source>
        <translation>Geräte Messwerte</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="654"/>
        <source>ID for User 1</source>
        <translation>ID für Benutzer 1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="702"/>
        <source>ID for User 2</source>
        <translation>ID für Benutzer 2</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="790"/>
        <source>enable/disable Logfile</source>
        <translation>Logdatei aktivieren/deaktivieren</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="110"/>
        <source>Import Measurements automatically</source>
        <translation>Messwerte automatisch importieren</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="207"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="37"/>
        <source>Discover Device</source>
        <translation>Gerät entdecken</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="375"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="120"/>
        <source>Device Information</source>
        <translation>Geräte Information</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="259"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="51"/>
        <source>Bluetooth Device</source>
        <translation>Bluetooth Gerät</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="435"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="69"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="69"/>
        <source>Serial</source>
        <translation>Seriennummer</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="321"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="70"/>
        <source>Connect Device</source>
        <translation>Gerät verbinden</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="156"/>
        <source>Producer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="163"/>
        <source>Product</source>
        <translation>Produkt</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="86"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="158"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="182"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="111"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="770"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="188"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="173"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="132"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="262"/>
        <source>Import</source>
        <translation>Importieren</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="175"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="199"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="128"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="205"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="190"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="149"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="279"/>
        <source>Write Logfile</source>
        <translation>Logdatei schreiben</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="549"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="149"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="201"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="225"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="154"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="178"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="817"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="231"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="178"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="216"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="175"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="305"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="24"/>
        <source>Could not find a serial port.

Is the usb2serial driver installed and the device connected?</source>
        <translation>Konnte keine serielle Schnittstelle finden.

Ist der USB2Seriell-Treiber installiert und das Gerät angeschlossen?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="76"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="76"/>
        <source>Could not read data.

%1</source>
        <translation>Die Daten konnten nicht gelesen werden.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.</source>
        <translation>Das Gerät reagiert nicht.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="316"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="270"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="318"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="222"/>
        <source>Could not open serial port &quot;%1&quot;.

%2

Try as root or create a udev rule.

Read the wiki for details on how to do this.</source>
        <translation>Konnte serielle Schnittstelle &quot;%1&quot; nicht öffnen.

%2

Als Root versuchen oder eine udev-Regel erstellen.

Im Wiki nachlesen, wie dies getan werden kann.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="212"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="253"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="203"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="240"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="395"/>
        <source>The import was canceled.</source>
        <translation>Der Import wurde abgebrochen.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="75"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="159"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="112"/>
        <source>Could not write data.

%1</source>
        <translation>Konnte Daten nicht schreiben.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.

Press &quot;MEM&quot;, select U1/U2 with &quot;POWER&quot; and wait until the user data is shown.

Then try again…</source>
        <translation>Das Gerät antwortet nicht.

&quot;MEM&quot; drücken, U1/U2 mit &quot;POWER&quot; wählen und warten bis die Benutzerdaten angezeigt werden.

Danach erneut versuchen…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="115"/>
        <source>Was &quot;U1&quot; selected on the device?

For &quot;U2&quot; answer with No.</source>
        <translation>Wurde &quot;U1&quot; am Gerät gewählt?

Für &quot;U2&quot; mit Nein antworten.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="180"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="200"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="173"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="198"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="782"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="252"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="187"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="273"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="177"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="725"/>
        <source>Could not open the logfile %1.

%2</source>
        <translation>Die Logdatei %1 konnte nicht geöffnet werden.

%2</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="45"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="40"/>
        <source>Could not open usb device %1:%2.

Try as root or create a udev rule.

Read the wiki for details on how to do this.</source>
        <translation>Konnte USB-Gerät %1:%2 nicht öffnen.

Als Root versuchen oder eine udev-Regel erstellen.

Im Wiki nachlesen, wie dies getan werden kann.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="190"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="183"/>
        <source>The device doesn&apos;t respond.

Disconnect the USB cable and reconnect it.

Then try again…</source>
        <translation>Das Gerät antwortet nicht.

Das USB-Kabel entfernen und wieder verbinden.

Danach erneut versuchen…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="201"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="242"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="192"/>
        <source>Could not read measurement count.</source>
        <translation>Konnte Messwertanzahl nicht lesen.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="223"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="264"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="214"/>
        <source>Could not read measurement %1.</source>
        <translation>Konnte Messwert %1 nicht lesen.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="330"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="284"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="803"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="748"/>
        <source>Cancel import?</source>
        <translation>Import abbrechen?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="297"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="341"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="295"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="833"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="343"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="482"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="759"/>
        <source>Import in progress…</source>
        <translation>Import läuft…</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="280"/>
        <source>Can&apos;t set device into info mode.</source>
        <translation>Gerät kann nicht in den Info-Modus gesetzt werden.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="293"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="307"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="332"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="349"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="366"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="403"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="442"/>
        <source>Send %1 command failed.</source>
        <translation>Sende Befehl %1 fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="319"/>
        <source>Can&apos;t set device into data mode.</source>
        <translation>Gerät kann nicht in den Daten-Modus gesetzt werden.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="117"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="70"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="387"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="429"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="202"/>
        <source>Import aborted by user.</source>
        <translation>Import durch Benutzer abgebrochen.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="152"/>
        <source>Acknowledge failed.</source>
        <translation>Bestätigung fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="84"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="100"/>
        <source>Could not read data.

Is the device powered on?

%1</source>
        <translation>Konnte Daten nicht lesen.

Ist das Gerät eingeschaltet?

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="117"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="61"/>
        <source>No Bluetooth controller found.</source>
        <translation>Es wurde kein Bluetooth-Kontroller gefunden.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="191"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="125"/>
        <source>No device discovered.

Check the connection in operating system or press Bluetooth button on device and try again…</source>
        <translation>Es wurde kein Gerät gefunden.

Die Verbindung im Betriebssystem überprüfen oder die Bluetooth-Taste am Gerät drücken und erneut versuchen…</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="163"/>
        <source>Device doesn&apos;t respond, try again?</source>
        <translation>Gerät antwortet nicht, erneut versuchen?</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="180"/>
        <source>Could not connect to the device.

Check the connection in operating system or press Bluetooth button on device and try again…

%1</source>
        <translation>Es konnte keine Verbindung zum Gerät hergestellt werden.

Die Verbindung im Betriebssystem überprüfen oder die Bluetooth-Taste am Gerät drücken und erneut versuchen…

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="187"/>
        <source>Press START/STOP on device and try again…</source>
        <translation>START/STOP am Gerät drücken und erneut versuchen…</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="332"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="471"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="233"/>
        <source>Really abort import?</source>
        <translation>Import wirklich abbrechen?</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="175"/>
        <source>An error occurred during device discovery.

%1</source>
        <translation>Bei der Geräteerkennung ist ein Fehler aufgetreten.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="222"/>
        <source>The selected device is not supported.</source>
        <translation>Das gewählte Gerät wird nicht unterstützt.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Unconnected</source>
        <translation>Unverbunden</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Connecting…</source>
        <translation>Verbinden…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Connected</source>
        <translation>Verbunden</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Discovering…</source>
        <translation>Entdecken…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Discovered</source>
        <translation>Entdeckt</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Closing…</source>
        <translation>Schließen…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Advertising…</source>
        <translation>Ankündigen…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="254"/>
        <source>Current State of the Device: %1</source>
        <translation>Aktueller Zustand des Gerätes: %1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="290"/>
        <source>Unknown UID &quot;%1&quot; detected, transfer aborted.

Change UID settings and try again…</source>
        <translation>Unbekannte UID &quot;%1&quot; erkannt, Übertragung abgebrochen.

UID-Einstellungen ändern und erneut versuchen...</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="469"/>
        <source>Lost connection to device.</source>
        <translation>Verbindung zum Gerät verloren.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="505"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="444"/>
        <source>Could not access the Bluetooth service %1.</source>
        <translation>Es konnte nicht auf den Bluetooth-Dienst %1 zugegriffen werden.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="666"/>
        <source>Auto connection requires a model name.</source>
        <translation>Automatische Verbindung erfordert einen Modellnamen.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="720"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="673"/>
        <source>The selected Bluetooth controller is not available.</source>
        <translation>Der ausgewählte Bluetooth-Kontroller ist nicht verfügbar.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="30"/>
        <source>Select Bluetooth Controller</source>
        <translation>Bluetooth-Kontroller auswählen</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="60"/>
        <source>Select Bluetooth Device</source>
        <translation>Bluetooth-Gerät auswählen</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="90"/>
        <source>Discover Device automatically</source>
        <translation>Gerät automatisch entdecken</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="103"/>
        <source>Connect Device automatically</source>
        <translation>Gerät automatisch verbinden</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="110"/>
        <source>Bluetooth error.

%1</source>
        <translation>Bluetooth-Fehler.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="157"/>
        <source>The selected device is not a %1.</source>
        <translation>Das ausgewählte Gerät ist kein %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="277"/>
        <source>No answer from the device.

Did you use the Bluetooth controller with the cloned MAC address or set a pairing key?

Read the wiki for details on how to do this.</source>
        <translation>Es wurde keine Antwort vom Gerät empfangen.

Wurde der Bluetooth-Controller mit der geklonten MAC-Adresse verwendet oder ein Kopplungsschlüssel gesetzt?

Im Wiki nachlesen, wie dies zu tun ist.</translation>
    </message>
</context>
</TS>
