<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Beolvasás a készülékről</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="84"/>
        <source>Plugin Options</source>
        <translation>Plugin beállítások</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="110"/>
        <source>Import Measurements automatically</source>
        <translation>Mérések automatikus beolvasása</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="375"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="120"/>
        <source>Device Information</source>
        <translation>Készülék információ</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="14"/>
        <source>Generic Bluetooth</source>
        <translation>Általános Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="36"/>
        <source>Automatically discover devices on startup</source>
        <translation>A készülék automatikus felismerése indításkor</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="39"/>
        <source>Auto Discover</source>
        <translation>Automatikus felismerés</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="74"/>
        <source>Automatically connect to device on startup</source>
        <translation>Automatikus kapcsolódás a készülékhez indításkor</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="77"/>
        <source>Auto Connect</source>
        <translation>Automatikus kapcsolódás</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="106"/>
        <source>Automatically import measurements on startup</source>
        <translation>Mérések automatikus importálása indításkor</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="109"/>
        <source>Auto Import</source>
        <translation>Automatikus importálás</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="137"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="24"/>
        <source>Bluetooth Controller</source>
        <translation>Bluetooth vezérlő</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="143"/>
        <source>Select Bluetooth controller</source>
        <translation>Válassza ki a Bluetooth vezérlőt</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="161"/>
        <source>Setup discovery timeout</source>
        <translation>Felismerés időtúllépés beállítása</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="207"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="37"/>
        <source>Discover Device</source>
        <translation>Készülék felismerése</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="259"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="51"/>
        <source>Bluetooth Device</source>
        <translation>Bluetooth eszköz</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="268"/>
        <source>Select Bluetooth device</source>
        <translation>Válassza ki a Bluetooth eszközt</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="283"/>
        <source>Enter devicename for auto connection</source>
        <translation>Adja meg az készülék nevét az automatikus kapcsolódáshoz</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="296"/>
        <source>Copy devicename</source>
        <translation>Készüléknév másolása</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="321"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="70"/>
        <source>Connect Device</source>
        <translation>Csatlakozás a készülékhez</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="384"/>
        <source>Current State of the Device: Unconnected</source>
        <translation>A készülék jelenlegi állapota: nincs kapcsolódva</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="435"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="69"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="69"/>
        <source>Serial</source>
        <translation>Sorozatszám</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="463"/>
        <source>Manufacturer</source>
        <translation>Gyártó</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="477"/>
        <source>Model</source>
        <translation>Gyártmány</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="542"/>
        <source>Hardware</source>
        <translation>Hardver</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="549"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="149"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="556"/>
        <source>Software</source>
        <translation>Szoftver</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="630"/>
        <source>Device Measurements</source>
        <translation>Mérések száma a készüléken</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="654"/>
        <source>ID for User 1</source>
        <translation>Páciens 1 azonosítója</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="702"/>
        <source>ID for User 2</source>
        <translation>Páciens 2 azonosítója</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="790"/>
        <source>enable/disable Logfile</source>
        <translation>be- vagy kikapcsolja a naplófájl készítését</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="156"/>
        <source>Producer</source>
        <translation>Gyártó</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="163"/>
        <source>Product</source>
        <translation>Típus</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="86"/>
        <source>Description</source>
        <translation>Megnevezés</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="158"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="182"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="111"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="770"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="188"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="173"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="132"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="262"/>
        <source>Import</source>
        <translation>Beolvasás</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="175"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="199"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="128"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="205"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="190"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="149"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="279"/>
        <source>Write Logfile</source>
        <translation>Naplófájl készítése</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="201"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="225"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="154"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="178"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="817"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="231"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="178"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="216"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="175"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="305"/>
        <source>Cancel</source>
        <translation>Mégsem</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="24"/>
        <source>Could not find a serial port.

Is the usb2serial driver installed and the device connected?</source>
        <translation>Nem található soros port.

Győződjön meg róla, hogy telepítve van az usb2serial illesztőprogram, és a készülék is csatlakoztatva van.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="76"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="76"/>
        <source>Could not read data.

%1</source>
        <translation>Nem olvasható adat.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.</source>
        <translation>A készülék nem válaszol.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="316"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="270"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="318"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="222"/>
        <source>Could not open serial port &quot;%1&quot;.

%2

Try as root or create a udev rule.

Read the wiki for details on how to do this.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="212"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="253"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="203"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="240"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="395"/>
        <source>The import was canceled.</source>
        <translation>A beolvasás félbeszakítva.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="75"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="159"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="112"/>
        <source>Could not write data.

%1</source>
        <translation>Nem sikerült az adat kiírása.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.

Press &quot;MEM&quot;, select U1/U2 with &quot;POWER&quot; and wait until the user data is shown.

Then try again…</source>
        <translation>A készülék nem válaszol.

Nyomja meg rajta a „MEM” gombot, válasszon a „POWER” gombbal U1/U2 között, és várjon amíg a páciens adatai megjelennek.

Ezután próbálja ismét…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="115"/>
        <source>Was &quot;U1&quot; selected on the device?

For &quot;U2&quot; answer with No.</source>
        <translation>Ki van választva „U1” páciens a készüléken?

Az „U2” pácienshez válaszoljon „Nem”-mel.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="180"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="200"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="173"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="198"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="782"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="252"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="187"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="273"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="177"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="725"/>
        <source>Could not open the logfile %1.

%2</source>
        <translation>A %1 naplófájl megnyitása sikertelen.

%2</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="45"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="40"/>
        <source>Could not open usb device %1:%2.

Try as root or create a udev rule.

Read the wiki for details on how to do this.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="190"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="183"/>
        <source>The device doesn&apos;t respond.

Disconnect the USB cable and reconnect it.

Then try again…</source>
        <translation>A készülék nem válaszol.

Az USB kábel újracsatlakoztatása után próbálja ismét.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="201"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="242"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="192"/>
        <source>Could not read measurement count.</source>
        <translation>A mérések számának beolvasása sikertelen.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="223"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="264"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="214"/>
        <source>Could not read measurement %1.</source>
        <translation>%1. mérés beolvasása sikertelen.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="330"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="284"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="803"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="748"/>
        <source>Cancel import?</source>
        <translation>Beolvasás félbeszakítása?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="297"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="341"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="295"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="833"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="343"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="482"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="759"/>
        <source>Import in progress…</source>
        <translation>Beolvasás folyamatban…</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="280"/>
        <source>Can&apos;t set device into info mode.</source>
        <translation>A készülék infó üzemmódba állítása meghiúsult.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="293"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="307"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="332"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="349"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="366"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="403"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="442"/>
        <source>Send %1 command failed.</source>
        <translation>%1 parancs átküldése sikertelen.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="319"/>
        <source>Can&apos;t set device into data mode.</source>
        <translation>A készülék adat üzemmódba állítása meghiúsult.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="117"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="70"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="387"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="429"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="202"/>
        <source>Import aborted by user.</source>
        <translation>Beolvasás félbeszakítva felhasználó által.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="152"/>
        <source>Acknowledge failed.</source>
        <translation>Kapcsolatfelvétel sikertelen.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="84"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="100"/>
        <source>Could not read data.

Is the device powered on?

%1</source>
        <translation>Nem sikerült az adatok olvasása.

Be van kapcsolva az eszköz?

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="163"/>
        <source>Device doesn&apos;t respond, try again?</source>
        <translation>A készülék nem válaszol. Meg kívánja ismét próbálni?</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="187"/>
        <source>Press START/STOP on device and try again…</source>
        <translation>Nyomja meg a START/STOP gombot a készüléken, majd próbálja ismét.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="332"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="471"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="233"/>
        <source>Really abort import?</source>
        <translation>Valóban félbeszakítja a beolvasást?</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="117"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="61"/>
        <source>No Bluetooth controller found.</source>
        <translation>Nem található Bluetooth vezérlő.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="175"/>
        <source>An error occurred during device discovery.

%1</source>
        <translation>Hiba lépett fel a készülék felfedezése során.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="191"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="125"/>
        <source>No device discovered.

Check the connection in operating system or press Bluetooth button on device and try again…</source>
        <translation>Nincs felfedezett készülék.

Ellenőrizze a kapcsolatot az operációs rendszerben, vagy nyomja meg a Bluetooth gombot a készüléken, és próbálja meg újra…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="222"/>
        <source>The selected device is not supported.</source>
        <translation>A kiválasztott készülék nem támogatott.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="180"/>
        <source>Could not connect to the device.

Check the connection in operating system or press Bluetooth button on device and try again…

%1</source>
        <translation>Nem sikerült csatlakozni a készülékhez.

Ellenőrizze a kapcsolatot az operációs rendszerben, vagy nyomja meg a Bluetooth gombot a készüléken, és próbálja meg újra…

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Unconnected</source>
        <translation>Nincs kapcsolódva</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Connecting…</source>
        <translation>Kapcsolódás…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Connected</source>
        <translation>Kapcsolódva</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Discovering…</source>
        <translation>Felismerés…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Discovered</source>
        <translation>Felismerve</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Closing…</source>
        <translation>Lezárás…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Advertising…</source>
        <translation>Keresés…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="254"/>
        <source>Current State of the Device: %1</source>
        <translation>A készülék jelenlegi állapota: %1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="290"/>
        <source>Unknown UID &quot;%1&quot; detected, transfer aborted.

Change UID settings and try again…</source>
        <translation>Ismeretlen „%1” UID azonosító észlelve, az adatátvitel megszakadt.

Módosítsa az UID beállításokat és próbálja újra…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="469"/>
        <source>Lost connection to device.</source>
        <translation>A kapcsolat megszakadt a készülékkel.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="505"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="444"/>
        <source>Could not access the Bluetooth service %1.</source>
        <translation>Nem érhető el a Bluetooth „%1” szolgáltatás.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="666"/>
        <source>Auto connection requires a model name.</source>
        <translation>Az automatikus csatlakozáshoz a gyártmánynév megadása szükséges.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="720"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="673"/>
        <source>The selected Bluetooth controller is not available.</source>
        <translation>A kiválasztott Bluetooth vezérlő nem elérhető.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="30"/>
        <source>Select Bluetooth Controller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="60"/>
        <source>Select Bluetooth Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="90"/>
        <source>Discover Device automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="103"/>
        <source>Connect Device automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="110"/>
        <source>Bluetooth error.

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="157"/>
        <source>The selected device is not a %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="277"/>
        <source>No answer from the device.

Did you use the Bluetooth controller with the cloned MAC address or set a pairing key?

Read the wiki for details on how to do this.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
