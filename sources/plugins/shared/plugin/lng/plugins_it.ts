<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="14"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Importazione Dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="24"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="84"/>
        <source>Plugin Options</source>
        <translation>Opzioni Plugin</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="30"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="110"/>
        <source>Import Measurements automatically</source>
        <translation>Importa misurazioni automaticamente</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="375"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="40"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="120"/>
        <source>Device Information</source>
        <translation>Informazioni sul Dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="14"/>
        <source>Generic Bluetooth</source>
        <translation>Bluetooth generico</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="36"/>
        <source>Automatically discover devices on startup</source>
        <translation>Rileva automaticamente i dispositivi all&apos;avvio</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="39"/>
        <source>Auto Discover</source>
        <translation>Rilevamento automatico</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="74"/>
        <source>Automatically connect to device on startup</source>
        <translation>Connettiti automaticamente al dispositivo all&apos;avvio</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="77"/>
        <source>Auto Connect</source>
        <translation>Connessione automatica</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="106"/>
        <source>Automatically import measurements on startup</source>
        <translation>Importa automaticamente le misurazioni all&apos;avvio</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="109"/>
        <source>Auto Import</source>
        <translation>Importazione automatica</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="137"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="24"/>
        <source>Bluetooth Controller</source>
        <translation>Controller Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="143"/>
        <source>Select Bluetooth controller</source>
        <translation>Seleziona il controller Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="161"/>
        <source>Setup discovery timeout</source>
        <translation>Imposta il timeout di rilevamento</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="207"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="37"/>
        <source>Discover Device</source>
        <translation>Rileva dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="259"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="51"/>
        <source>Bluetooth Device</source>
        <translation>Dispositivo Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="268"/>
        <source>Select Bluetooth device</source>
        <translation>Seleziona il dispositivo Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="283"/>
        <source>Enter devicename for auto connection</source>
        <translation>Inserisci il nome del dispositivo per la connessione automatica</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="296"/>
        <source>Copy devicename</source>
        <translation>Copia nome dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="321"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="70"/>
        <source>Connect Device</source>
        <translation>Connetti dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="384"/>
        <source>Current State of the Device: Unconnected</source>
        <translation>Stato attuale del dispositivo: non connesso</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="435"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="69"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="69"/>
        <source>Serial</source>
        <translation>Seriale</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="463"/>
        <source>Manufacturer</source>
        <translation>Produttore</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="477"/>
        <source>Model</source>
        <translation>Modello</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="542"/>
        <source>Hardware</source>
        <translation>Hardware</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="549"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="149"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="556"/>
        <source>Software</source>
        <translation>Software</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="630"/>
        <source>Device Measurements</source>
        <translation>Misure del dispositivo</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="654"/>
        <source>ID for User 1</source>
        <translation>ID per l&apos;utente 1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="702"/>
        <source>ID for User 2</source>
        <translation>ID per l&apos;utente 2</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="790"/>
        <source>enable/disable Logfile</source>
        <translation>abilitare/disabilitare il file di registro</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="62"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="79"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="76"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="156"/>
        <source>Producer</source>
        <translation>Produttore</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="83"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="163"/>
        <source>Product</source>
        <translation>Prodotto</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="55"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="86"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="86"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="158"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="182"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="111"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="770"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="188"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="135"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="173"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="132"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="262"/>
        <source>Import</source>
        <translation>Importa</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="175"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="199"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="128"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="205"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="152"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="190"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="149"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="279"/>
        <source>Write Logfile</source>
        <translation>Scrivi file di registro</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.ui" line="201"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.ui" line="225"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.ui" line="154"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.ui" line="178"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.ui" line="817"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.ui" line="231"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.ui" line="178"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.ui" line="216"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.ui" line="175"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="305"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="24"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="24"/>
        <source>Could not find a serial port.

Is the usb2serial driver installed and the device connected?</source>
        <translation>Impossibile trovare una porta seriale.

Il driver seriale usb2 è installato e il dispositivo è collegato?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="76"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="76"/>
        <source>Could not read data.

%1</source>
        <translation>Impossibile leggere i dati.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.</source>
        <translation>Il dispositivo non risponde.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="316"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="270"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="318"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="222"/>
        <source>Could not open serial port &quot;%1&quot;.

%2

Try as root or create a udev rule.

Read the wiki for details on how to do this.</source>
        <translation>Impossibile aprire la porta seriale &quot;%1&quot;.

%2

Prova come root o crea una regola udev.

Leggi la wiki per i dettagli su come eseguire questa operazione.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="212"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="253"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="203"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="240"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="395"/>
        <source>The import was canceled.</source>
        <translation>L&apos;importazione è stata annullata.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="102"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="75"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="159"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="112"/>
        <source>Could not write data.

%1</source>
        <translation>Impossibile scrivere i dati.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="85"/>
        <source>The device doesn&apos;t respond.

Press &quot;MEM&quot;, select U1/U2 with &quot;POWER&quot; and wait until the user data is shown.

Then try again…</source>
        <translation>Il dispositivo non risponde.

Premere &quot;MEM&quot;, selezionare U1/U2 con &quot;POWER&quot; e attendere che vengano visualizzati i dati utente.

Poi riprova…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="115"/>
        <source>Was &quot;U1&quot; selected on the device?

For &quot;U2&quot; answer with No.</source>
        <translation>È stato selezionato &quot;U1&quot; sul dispositivo?

Per &quot;U2&quot; rispondi con No.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="180"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="200"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="173"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="198"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="782"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="252"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="187"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="273"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="177"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="725"/>
        <source>Could not open the logfile %1.

%2</source>
        <translation>Impossibile aprire il file di registro %1.

%2</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="38"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="45"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="40"/>
        <source>Could not open usb device %1:%2.

Try as root or create a udev rule.

Read the wiki for details on how to do this.</source>
        <translation>Impossibile aprire il dispositivo USB %1:%2.

Prova come root o crea una regola udev.

Leggi la wiki per i dettagli su come eseguire questa operazione.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="190"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="183"/>
        <source>The device doesn&apos;t respond.

Disconnect the USB cable and reconnect it.

Then try again…</source>
        <translation>Il dispositivo non risponde.

Scollegare il cavo USB e ricollegarlo.

Poi riprova…</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="201"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="242"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="192"/>
        <source>Could not read measurement count.</source>
        <translation>Impossibile leggere il conteggio delle misurazioni.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="223"/>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="264"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="214"/>
        <source>Could not read measurement %1.</source>
        <translation>Impossibile leggere la misurazione %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="286"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="330"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="284"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="803"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="748"/>
        <source>Cancel import?</source>
        <translation>Annullare l&apos;importazione?</translation>
    </message>
    <message>
        <location filename="../../../vendor/beurer/bc80/v-hid/DialogImport.cpp" line="297"/>
        <location filename="../../../vendor/beurer/bc80/v-ser/DialogImport.cpp" line="341"/>
        <location filename="../../../vendor/beurer/bm58/v-hid/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/beurer/bm58/v-ser/DialogImport.cpp" line="295"/>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="833"/>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="343"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="482"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="244"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="759"/>
        <source>Import in progress…</source>
        <translation>Importazione in corso…</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="280"/>
        <source>Can&apos;t set device into info mode.</source>
        <translation>Impossibile impostare il dispositivo in modalità informazioni.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="293"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="307"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="332"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="349"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="366"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="403"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="442"/>
        <source>Send %1 command failed.</source>
        <translation>Invio %1 comando non riuscito.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="319"/>
        <source>Can&apos;t set device into data mode.</source>
        <translation>Impossibile impostare il dispositivo in modalità dati.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="117"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="70"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="387"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="429"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="202"/>
        <source>Import aborted by user.</source>
        <translation>Importazione interrotta dall&apos;utente.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="152"/>
        <source>Acknowledge failed.</source>
        <translation>Riconoscimento fallito.</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="84"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="100"/>
        <source>Could not read data.

Is the device powered on?

%1</source>
        <translation>Impossibile leggere i dati.

Il dispositivo è acceso?

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="163"/>
        <source>Device doesn&apos;t respond, try again?</source>
        <translation>Il dispositivo non risponde, riprovare?</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="187"/>
        <source>Press START/STOP on device and try again…</source>
        <translation>Premi START/STOP sul dispositivo e riprova…</translation>
    </message>
    <message>
        <location filename="../../../vendor/hartmann/dc318/DialogImport.cpp" line="332"/>
        <location filename="../../../vendor/hartmann/gce604/DialogImport.cpp" line="236"/>
        <location filename="../../../vendor/omron/hem-7080it/DialogImport.cpp" line="471"/>
        <location filename="../../../vendor/omron/hem-7322u/DialogImport.cpp" line="233"/>
        <source>Really abort import?</source>
        <translation>Interrompere davvero l&apos;importazione?</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="117"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="61"/>
        <source>No Bluetooth controller found.</source>
        <translation>Nessun controller Bluetooth trovato.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="175"/>
        <source>An error occurred during device discovery.

%1</source>
        <translation>Si è verificato un errore durante il rilevamento del dispositivo.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="191"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="125"/>
        <source>No device discovered.

Check the connection in operating system or press Bluetooth button on device and try again…</source>
        <translation>Nessun dispositivo rilevato.

Controlla la connessione nel sistema operativo o premi il pulsante Bluetooth sul dispositivo e riprova…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="222"/>
        <source>The selected device is not supported.</source>
        <translation>Il dispositivo selezionato non è supportato.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="247"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="180"/>
        <source>Could not connect to the device.

Check the connection in operating system or press Bluetooth button on device and try again…

%1</source>
        <translation>Impossibile connettersi al dispositivo.

Controlla la connessione nel sistema operativo o premi il pulsante Bluetooth sul dispositivo e riprova...

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Unconnected</source>
        <translation>Non connesso</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Connecting…</source>
        <translation>Connessione in corso…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Connected</source>
        <translation>Connesso</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Discovering…</source>
        <translation>Rilevamento in corso…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Discovered</source>
        <translation>Rilevato</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Closing…</source>
        <translation>In chiusura…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="252"/>
        <source>Advertising…</source>
        <translation>Pubblicità…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="254"/>
        <source>Current State of the Device: %1</source>
        <translation>Stato corrente del dispositivo: %1</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="290"/>
        <source>Unknown UID &quot;%1&quot; detected, transfer aborted.

Change UID settings and try again…</source>
        <translation>Rilevato UID &quot;%1&quot; sconosciuto, trasferimento interrotto.

Modifica le impostazioni UID e riprova…</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="469"/>
        <source>Lost connection to device.</source>
        <translation>Connessione al dispositivo interrotta.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="505"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="444"/>
        <source>Could not access the Bluetooth service %1.</source>
        <translation>Impossibile accedere al servizio Bluetooth %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="666"/>
        <source>Auto connection requires a model name.</source>
        <translation>La connessione automatica richiede il nome del modello.</translation>
    </message>
    <message>
        <location filename="../../../vendor/generic/bluetooth/DialogImport.cpp" line="720"/>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="673"/>
        <source>The selected Bluetooth controller is not available.</source>
        <translation>Il controller Bluetooth selezionato non è disponibile.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="30"/>
        <source>Select Bluetooth Controller</source>
        <translation>Seleziona Controller Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="60"/>
        <source>Select Bluetooth Device</source>
        <translation>Seleziona Dispositivo Bluetooth</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="90"/>
        <source>Discover Device automatically</source>
        <translation>Rileva dispositivo automaticamente</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.ui" line="103"/>
        <source>Connect Device automatically</source>
        <translation>Connetti il dispositivo automaticamente</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="110"/>
        <source>Bluetooth error.

%1</source>
        <translation>Errore Bluetooth.

%1</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="157"/>
        <source>The selected device is not a %1.</source>
        <translation>Il dispositivo selezionato non è un %1.</translation>
    </message>
    <message>
        <location filename="../../../vendor/omron/hem-7361t/DialogImport.cpp" line="277"/>
        <source>No answer from the device.

Did you use the Bluetooth controller with the cloned MAC address or set a pairing key?

Read the wiki for details on how to do this.</source>
        <translation>Nessuna risposta dal dispositivo.

Hai utilizzato il controller Bluetooth con l&apos;indirizzo MAC clonato o hai impostato una chiave di accoppiamento?

Leggi la wiki per i dettagli su come eseguire questa operazione.</translation>
    </message>
</context>
</TS>
