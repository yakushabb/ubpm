#include "DialogImport.h"

DialogImport::DialogImport(QWidget *parent, QString theme, QVector <struct HEALTHDATA> *user1, QVector <struct HEALTHDATA> *user2, struct SETTINGS *psettings) : QDialog(parent)
{
	u1 = user1;
	u2 = user2;
	settings = psettings;

	setStyleSheet(theme);

	setupUi(this);

	resize(0, 0);
	layout()->setSizeConstraint(QLayout::SetFixedSize);

	setWindowTitle(MODEL);

	checkBox_auto_import->setChecked(settings->plugin.import);

	toolButton->setChecked(settings->plugin.logging);

	if(!spi.availablePorts().count())
	{
		QMessageBox::critical(nullptr, MODEL, tr("Could not find a serial port.\n\nIs the usb2serial driver installed and the device connected?"));

		failed = true;

		return;
	}

	foreach(QSerialPortInfo pi, spi.availablePorts())
	{
		comboBox->addItem(pi.systemLocation(), QStringList() << pi.manufacturer() << pi.description() << "0x" + QString("%1").arg(pi.vendorIdentifier(), 4, 16, QChar('0')).toUpper() + " 0x" + QString("%1").arg(pi.productIdentifier(), 4, 16, QChar('0')).toUpper());

		if(pi.vendorIdentifier() == VID && pi.productIdentifier() == PID)
		{
			comboBox->setCurrentIndex(comboBox->count() - 1);
		}
	}

	on_comboBox_activated(-1);

	log.setFileName(LOGFILE);

	if(settings->plugin.import)
	{
		QTimer::singleShot(100, this, &DialogImport::on_pushButton_import_clicked);
	}
}

bool DialogImport::sendCMD(QByteArray cmd)
{
	sp.write(cmd);

	logRawData(true, cmd.length(), cmd);

	if(sp.waitForBytesWritten(TIMEOUT))
	{
		if(cmd == cmd_init)
		{
			int records = -1;
			int size = 0;

			finished = false;

			while(sp.waitForReadyRead(TIMEOUT))
			{
				if(abort)
				{
					QMessageBox::warning(this, MODEL, tr("Import aborted by user."));

					done(QDialog::Rejected);

					return false;
				}

				payload += sp.readAll();

				if(records == -1 && payload.length() >= 3)
				{
					records = quint8(payload[2]);
					size = 3 + records*14 + 1;
				}

				if(size)
				{
					progressBar->setFormat(QString("%1/%2 [%p%]").arg((payload.length() - 3) / 14).arg(records));
					progressBar->setValue((payload.length() * 100) / size);
				}

				QApplication::processEvents();
			}

			finished = true;

			logRawData(false, payload.length(), payload);

			if(!payload.length())
			{
				QMessageBox::critical(nullptr, MODEL, tr("Could not read data.\n\nIs the device powered on?\n\n%1").arg(sp.errorString()));

				return false;
			}
		}
		else
		{
			QThread::msleep(TIMEOUT/100);
		}
	}
	else
	{
		QMessageBox::critical(nullptr, MODEL, tr("Could not write data.\n\n%1").arg(sp.errorString()));

		return false;
	}

	return true;
}

void DialogImport::decryptPayload()
{
	HEALTHDATA record;

	for(int i = 3; i < payload.length() - 4; i += 14)
	{
		record.dts = QDateTime(QDate(quint8(payload[i + 10]) << 8 | quint8(payload[i + 11]), quint8(payload[i + 8]), quint8(payload[i + 9])), QTime(quint8(payload[i + 12]), quint8(payload[i + 13]), 0, 0)).toMSecsSinceEpoch();
		record.sys = quint8(payload[i + 2]) << 8 | quint8(payload[i + 3]);
		record.dia = quint8(payload[i + 4]) << 8 | quint8(payload[i + 5]);
		record.bpm = quint8(payload[i + 7]);
		record.ihb = quint8(payload[i + 6]) & 0x01;
		record.mov = false;
		record.inv = false;
		record.msg = "";

		quint8(payload[i]) == 1 ? u1->append(record) : u2->append(record);
	}
}

void DialogImport::logRawData(bool direction, int bytes, QByteArray data)
{
	if(log.isOpen())
	{
		log.write(QString("%1 %2 : ").arg(direction ? "->" : "<-").arg(bytes, 2, 10, QChar('0')).toUtf8());

		if(bytes)
		{
			log.write(data.toHex(' ').toUpper());
		}
		else
		{
			log.write(QString("Error!").toUtf8());
		}

		log.write("\n");
	}
}

void DialogImport::on_comboBox_activated(int /*index*/)
{
	label_producer->setText(comboBox->currentData().toStringList().at(0));
	label_description->setText(comboBox->currentData().toStringList().at(1));
	label_ids->setText(comboBox->currentData().toStringList().at(2));
}

void DialogImport::on_checkBox_auto_import_toggled(bool state)
{
	settings->plugin.import = state;
}

void DialogImport::on_toolButton_toggled(bool state)
{
	settings->plugin.logging = state;
}

void DialogImport::on_pushButton_import_clicked()
{
	if(toolButton->isChecked())
	{
		if(!log.isOpen())
		{
			if(log.open(QIODevice::WriteOnly))
			{
				log.write(QString("Hartmann Veroval %1\n\n").arg(MODEL).toUtf8());
			}
			else
			{
				QMessageBox::critical(this, MODEL, tr("Could not open the logfile %1.\n\n%2").arg(log.fileName(), log.errorString()));
			}
		}
	}

	sp.setPortName(comboBox->currentText());

	if(sp.open(QIODevice::ReadWrite))
	{
		pushButton_import->setDisabled(true);
		pushButton_cancel->setEnabled(true);

		sp.clear();

		sp.setBaudRate(19200);
		sp.setDataBits(QSerialPort::Data8);
		sp.setParity(QSerialPort::NoParity);
		sp.setStopBits(QSerialPort::OneStop);
		sp.setFlowControl(QSerialPort::NoFlowControl);

		if(!sendCMD(cmd_init))
		{
			sp.close();

			done(QDialog::Rejected);

			return;
		}

		sendCMD(cmd_exit);

		sp.close();
	}
	else
	{
		QMessageBox::critical(this, MODEL, tr("Could not open serial port \"%1\".\n\n%2\n\nTry as root or create a udev rule.\n\nRead the wiki for details on how to do this.").arg(comboBox->currentText(), sp.errorString()));

		done(QDialog::Rejected);

		return;
	}

	decryptPayload();

	done(QDialog::Accepted);
}

void DialogImport::on_pushButton_cancel_clicked()
{
	if(QMessageBox::question(this, MODEL, tr("Really abort import?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
		finished = true;
		abort = true;
	}
}

void DialogImport::reject()
{
	if(!finished)
	{
		QMessageBox::warning(this, MODEL, tr("Import in progress…"));

		return;
	}

	log.close();

	QDialog::reject();
}
