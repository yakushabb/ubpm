#include "DialogImport.h"

DialogImport::DialogImport(QWidget *parent, QString theme, QVector <struct HEALTHDATA> *user1, QVector <struct HEALTHDATA> *user2, struct SETTINGS *psettings) : QDialog(parent)
{
	u1 = user1;
	u2 = user2;
	settings = psettings;

	setStyleSheet(theme);

	setupUi(this);

	resize(0, 0);
	layout()->setSizeConstraint(QLayout::SetFixedSize);

	setWindowTitle(MODEL);

	checkBox_auto_import->setChecked(settings->plugin.import);

	toolButton->setChecked(settings->plugin.logging);

	if(!spi.availablePorts().count())
	{
		QMessageBox::critical(nullptr, MODEL, tr("Could not find a serial port.\n\nIs the usb2serial driver installed and the device connected?"));

		failed = true;

		return;
	}

	foreach(QSerialPortInfo pi, spi.availablePorts())
	{
		comboBox->addItem(pi.systemLocation(), QStringList() << pi.manufacturer() << pi.description() << "0x" + QString("%1").arg(pi.vendorIdentifier(), 4, 16, QChar('0')).toUpper() + " 0x" + QString("%1").arg(pi.productIdentifier(), 4, 16, QChar('0')).toUpper());

		if(pi.vendorIdentifier() == VID && pi.productIdentifier() == PID)
		{
			comboBox->setCurrentIndex(comboBox->count() - 1);
		}
	}

	on_comboBox_activated(-1);

	log.setFileName(LOGFILE);

	if(settings->plugin.import)
	{
		QTimer::singleShot(100, this, &DialogImport::on_pushButton_import_clicked);
	}
}

bool DialogImport::sendCMD(QByteArray cmd)
{
	sp.write(cmd);

	logRawData(true, cmd.length(), cmd);

	if(sp.waitForBytesWritten(TIMEOUT))
	{
		QByteArray acknowledge;

		QThread::msleep(1000);

		if(sp.waitForReadyRead(TIMEOUT))
		{
			acknowledge = sp.read(1);

			logRawData(false, acknowledge.length(), acknowledge);

			sp.write(cmd_data);

			logRawData(true, cmd_data.length(), cmd_data);

			if(!sp.waitForBytesWritten(TIMEOUT))
			{
				QMessageBox::critical(nullptr, MODEL, tr("Could not write data.\n\n%1").arg(sp.errorString()));

				return false;
			}

			QThread::msleep(500);
		}
		else
		{
			QMessageBox::critical(nullptr, MODEL, tr("Could not read data.\n\nIs the device powered on?\n\n%1").arg(sp.errorString()));

			return false;
		}

		if(quint8(acknowledge[0]) == 6)
		{
			if(cmd == cmd_mrn1 || cmd == cmd_mrn2)
			{
				QByteArray answer;

				while(sp.waitForReadyRead(TIMEOUT))
				{
					answer += sp.readAll();
				}

				logRawData(false, answer.length(), answer);

				(cmd == cmd_mrn1 ? cnt1 : cnt2) = QString(answer).mid(5, 3).toInt();
				cmd == cmd_mrn1 ? progressBar_u1->setFormat(QString("0/%1 [%p%]").arg(cnt1)) : progressBar_u2->setFormat(QString("0/%1 [%p%]").arg(cnt2));

				QApplication::processEvents();
			}
			else
			{
				QByteArray payload;

				finished = false;

				while(sp.waitForReadyRead(TIMEOUT))
				{
					if(abort)
					{
						QMessageBox::warning(this, MODEL, tr("Import aborted by user."));

						done(QDialog::Rejected);

						return false;
					}

					payload += sp.readAll();

					cmd == cmd_mdr1 ? progressBar_u1->setFormat(QString("%1/%2 [%p%]").arg((payload.length() - 6) / 25).arg(cnt1)) : progressBar_u2->setFormat(QString("%1/%2 [%p%]").arg((payload.length() - 6) / 25).arg(cnt2));
					cmd == cmd_mdr1 ? progressBar_u1->setValue(((payload.length() - 6) * 100) / (cnt1 * 25)) : progressBar_u2->setValue(((payload.length() - 6) * 100) / (cnt2 * 25));

					QApplication::processEvents();
				}

				logRawData(false, payload.length(), payload);

				if(cmd == cmd_mdr1)
				{
					payloads[0].append(payload.remove(0, 5));
					payloads[0].chop(1);
				}
				else
				{
					payloads[1].append(payload.remove(0, 5));
					payloads[1].chop(1);
				}

				finished = true;
			}
		}
		else
		{
			logRawData(false, 1, acknowledge);

			QMessageBox::critical(nullptr, MODEL, tr("Acknowledge failed."));

			return false;
		}
	}
	else
	{
		QMessageBox::critical(nullptr, MODEL, tr("Could not write data.\n\n%1").arg(sp.errorString()));

		return false;
	}

	return true;
}

void DialogImport::decryptPayload()
{
	HEALTHDATA record;

	for(int i = 0; i < cnt1*25; i += 25)
	{
		record.dts = QDateTime::fromString(payloads[0].mid(i + 0, 10), "yyMMddhhmm").addYears(100).toMSecsSinceEpoch();
		record.sys = QString(payloads[0].mid(i + 11, 3)).toInt();
		record.dia = QString(payloads[0].mid(i + 14, 3)).toInt();
		record.bpm = QString(payloads[0].mid(i + 17, 3)).toInt();
		record.ihb = QString(payloads[0].mid(i + 10, 1)).toInt();
		record.mov = QString(payloads[0].mid(i + 20, 1)).toInt();
		record.inv = false;
		record.msg = "";

		u1->append(record);
	}

	for(int i = 0; i < cnt2*25; i += 25)
	{
		record.dts = QDateTime::fromString(payloads[1].mid(i + 0, 10), "yyMMddhhmm").addYears(100).toMSecsSinceEpoch();
		record.sys = QString(payloads[1].mid(i + 11, 3)).toInt();
		record.dia = QString(payloads[1].mid(i + 14, 3)).toInt();
		record.bpm = QString(payloads[1].mid(i + 17, 3)).toInt();
		record.ihb = QString(payloads[1].mid(i + 10, 1)).toInt();
		record.mov = QString(payloads[1].mid(i + 20, 1)).toInt();
		record.inv = false;
		record.msg = "";

		u2->append(record);
	}
}

void DialogImport::logRawData(bool direction, int bytes, QByteArray data)
{
	if(log.isOpen())
	{
		log.write(QString("%1 %2 : ").arg(direction ? "->" : "<-").arg(bytes, 2, 10, QChar('0')).toUtf8());

		if(bytes)
		{
			log.write(data.toHex(' ').toUpper());

			log.write("\t[");
			log.write(data.simplified());
			log.write("]");
		}
		else
		{
			log.write(QString("Error!").toUtf8());
		}

		log.write("\n");
	}
}

void DialogImport::on_comboBox_activated(int /*index*/)
{
	label_producer->setText(comboBox->currentData().toStringList().at(0));
	label_description->setText(comboBox->currentData().toStringList().at(1));
	label_ids->setText(comboBox->currentData().toStringList().at(2));
}

void DialogImport::on_checkBox_auto_import_toggled(bool state)
{
	settings->plugin.import = state;
}

void DialogImport::on_toolButton_toggled(bool state)
{
	settings->plugin.logging = state;
}

void DialogImport::on_pushButton_import_clicked()
{
	if(toolButton->isChecked())
	{
		if(!log.isOpen())
		{
			if(log.open(QIODevice::WriteOnly))
			{
				log.write(QString("Hartmann Veroval %1\n\n").arg(MODEL).toUtf8());
			}
			else
			{
				QMessageBox::critical(this, MODEL, tr("Could not open the logfile %1.\n\n%2").arg(log.fileName(), log.errorString()));
			}
		}
	}

	sp.setPortName(comboBox->currentText());

	if(sp.open(QIODevice::ReadWrite))
	{
		pushButton_import->setDisabled(true);
		pushButton_cancel->setEnabled(true);

		sp.clear();

		sp.setBaudRate(9600);
		sp.setDataBits(QSerialPort::Data8);
		sp.setParity(QSerialPort::NoParity);
		sp.setStopBits(QSerialPort::OneStop);
		sp.setFlowControl(QSerialPort::NoFlowControl);

		if(!sendCMD(cmd_mrn1))
		{
			sp.close();

			done(QDialog::Rejected);

			return;
		}

		if(!sendCMD(cmd_mrn2))
		{
			sp.close();

			done(QDialog::Rejected);

			return;
		}

		if(cnt1)
		{
			if(!sendCMD(cmd_mdr1))
			{
				sp.close();

				done(QDialog::Rejected);

				return;
			}
		}

		if(cnt2)
		{
			if(!sendCMD(cmd_mdr2))
			{
				sp.close();

				done(QDialog::Rejected);

				return;
			}
		}

		sp.close();
	}
	else
	{
		QMessageBox::critical(this, MODEL, tr("Could not open serial port \"%1\".\n\n%2\n\nTry as root or create a udev rule.\n\nRead the wiki for details on how to do this.").arg(comboBox->currentText(), sp.errorString()));

		done(QDialog::Rejected);

		return;
	}

	decryptPayload();

	done(QDialog::Accepted);
}

void DialogImport::on_pushButton_cancel_clicked()
{
	if(QMessageBox::question(this, MODEL, tr("Really abort import?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
		finished = true;
		abort = true;
	}
}

void DialogImport::reject()
{
	if(!finished)
	{
		QMessageBox::warning(this, MODEL, tr("Import in progress…"));

		return;
	}

	log.close();

	QDialog::reject();
}
