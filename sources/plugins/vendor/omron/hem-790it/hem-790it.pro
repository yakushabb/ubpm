TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets
DEFINES		+= HEM790IT
INCLUDEPATH	+= ../../../../ ../../../shared/hidapi
SOURCES		= DialogImport.cpp ../../../shared/plugin/plugin.cpp
HEADERS		= DialogImport.h   ../../../shared/plugin/plugin.h
FORMS		= DialogImport.ui
RESOURCES	= res/hem-790it.qrc
TARGET		= ../../../omron-hem790it

unix:!macx {
SOURCES		+= ../../../shared/hidapi/hidlin.c
LIBS		+= -ludev
}

win32 {
SOURCES		+= ../../../shared/hidapi/hidwin.c
LIBS		+= -lsetupapi
CONFIG		-= debug_and_release
}

macx {
SOURCES		+= ../../../shared/hidapi/hidmac.c
}

system($$QMAKE_COPY_FILE $$shell_path($$PWD/../hem-7080it/DialogImport.* .))
system($$QMAKE_COPY_FILE $$shell_path($$PWD/../../../../mainapp/res/svg/plugin/*.svg res/svg))
QMAKE_CLEAN += $$OUT_PWD/res/svg/*.svg
QMAKE_CLEAN += $$OUT_PWD/DialogImport.*
