#include "DialogImport.h"

DialogImport::DialogImport(QWidget *parent, QString theme, QVector <struct HEALTHDATA> *user1, QVector <struct HEALTHDATA> *user2, struct SETTINGS *psettings) : QDialog(parent)
{
	u1 = user1;
	u2 = user2;
	settings = psettings;

	int controllers;

	setStyleSheet(theme);

	setupUi(this);

	resize(0, 0);
	layout()->setSizeConstraint(QLayout::SetFixedSize);

	setWindowTitle(MODEL);

	progressBar_1->setFormat(QString("0/%1 [%p%]").arg(devicedata.memory));
	progressBar_2->setFormat(QString("0/%1 [%p%]").arg(devicedata.memory));

	if(devicedata.user == 1)
	{
		label_8->hide();
		progressBar_2->hide();
		resize(0, 0);
	}

	checkBox_auto_discover->setChecked(settings->plugin.bluetooth.discover);
	checkBox_auto_connect->setChecked(settings->plugin.bluetooth.connect);
	checkBox_auto_import->setChecked(settings->plugin.import);

	toolButton->setChecked(settings->plugin.logging);

	log.setFileName(LOGFILE);

	if(!(controllers = searchBtController()))
	{
		failed = true;
	}
	else if(controllers == 1 && settings->plugin.bluetooth.discover)
	{
		QTimer::singleShot(100, this, &DialogImport::on_pushButton_discover_clicked);
	}
}

int DialogImport::searchBtController()
{
	QList <QBluetoothHostInfo> bhil = QBluetoothLocalDevice::allDevices();

	if(bhil.count())
	{
		foreach(QBluetoothHostInfo bhi, bhil)
		{
			comboBox_controller->addItem(QString("%1 | %2").arg(bhi.address().toString(), bhi.name()), bhi.address().toString());
		}
	}
	else
	{
		QMessageBox::warning(nullptr, MODEL, tr("No Bluetooth controller found."));
	}

	return bhil.count();
}

// ###  QBluetoothDeviceDiscoveryAgent ###

void DialogImport::bddaDeviceDiscovered(QBluetoothDeviceInfo info)
{
	QStringList items;
	QString address;

#ifdef Q_OS_MACOS
	address = info.deviceUuid().toString();
#else
	address = info.address().toString();
#endif

	for(int i = 0; i < comboBox_device->count(); i++)
	{
		items.append(comboBox_device->itemData(i).toString());
	}

	if(!info.name().isEmpty() && !items.contains(address))
	{
		comboBox_device->addItem(info.name(), address);

		bdi.append(info);

		if(info.name() == "X4 Smart" || info.name() == "X7 Smart" || info.name() == "M4 Intelli IT" || info.name() == "M7 Intelli IT" || info.name() == "M400 Intelli IT" || info.name() == "M500 Intelli IT" || info.name() == "EVOLV" || info.name() == "RS7 Intelli IT" || info.name().startsWith("BLESmart_"))
		{
			comboBox_device->setCurrentIndex(comboBox_device->count() - 1);

			if(settings->plugin.bluetooth.connect)
			{
				bdda->stop();

				on_pushButton_connect_clicked();
			}
		}
	}
}

void DialogImport::bddaError(QBluetoothDeviceDiscoveryAgent::Error /*error*/)
{
	comboBox_controller->setEnabled(true);
	pushButton_discover->setEnabled(true);

	QMessageBox::warning(this, MODEL, tr("Bluetooth error.\n\n%1").arg(bdda->errorString()));
}

void DialogImport::bddaFinished()
{
	comboBox_controller->setEnabled(true);
	pushButton_discover->setEnabled(true);

	if(comboBox_device->count())
	{
		comboBox_device->setEnabled(true);
		pushButton_connect->setEnabled(true);
	}
	else
	{
		QMessageBox::warning(this, MODEL, tr("No device discovered.\n\nCheck the connection in operating system or press Bluetooth button on device and try again…"));
	}
}

// ### QLowEnergyController ###

void DialogImport::lecConnected()
{
	lec->discoverServices();
}

void DialogImport::lecDisconnected()
{
	comboBox_controller->setEnabled(true);
	pushButton_discover->setEnabled(true);

	comboBox_device->setEnabled(true);
	pushButton_connect->setEnabled(true);

	pushButton_import->setDisabled(true);
}

void DialogImport::lecDiscoveryFinished()
{
	if(!lec->services().contains(QBluetoothUuid(UUID_300)))
	{
		comboBox_controller->setEnabled(true);
		pushButton_discover->setEnabled(true);

		comboBox_device->setEnabled(true);
		pushButton_connect->setEnabled(true);

		QMessageBox::warning(this, MODEL, tr("The selected device is not a %1.").arg(MODEL));

		return;
	}

	readBTInfo();

	pushButton_import->setEnabled(true);

	if(settings->plugin.import)
	{
		on_pushButton_import_clicked();
	}
}

void DialogImport::lecError(QLowEnergyController::Error /*error*/)
{
	comboBox_controller->setEnabled(true);
	pushButton_discover->setEnabled(true);

	comboBox_device->setEnabled(true);
	pushButton_connect->setEnabled(true);

	QMessageBox::warning(this, MODEL, tr("Could not connect to the device.\n\nCheck the connection in operating system or press Bluetooth button on device and try again…\n\n%1").arg(lec->errorString()));
}

// ### QLowEnergyService ###

void DialogImport::lesCharacteristicChanged(QLowEnergyCharacteristic characteristic, QByteArray value)
{
#if QT_VERSION < 0x060000
	if(les_data->state() != QLowEnergyService::DiscoveringServices)
#else
	if(les_data->state() != QLowEnergyService::RemoteServiceDiscovering)
#endif
	{
		static QByteArray rawdata;
		static int bytes = 0;

		if(characteristic.uuid() == UUID_311)
		{
			bt_finished = true;

			return;
		}
		else if(characteristic.uuid() == UUID_361)
		{
			bytes = quint8(value[0]);

			rawdata.clear();
			rawdata.append(value);
		}
		else
		{
			rawdata.append(value);
		}

		if(rawdata.size() >= bytes)
		{
			logRawData(false, characteristic, rawdata);

			if(quint8(rawdata[1]) == 0x81 && bytes > 8 && quint8(rawdata[6]) != 0xFF && quint8(rawdata[7]) != 0xFF && quint8(rawdata[8]) != 0xFF && quint8(rawdata[9]) != 0xFF)
			{
				payloads[payload].append(rawdata.mid(6, 8));
			}

			bt_finished = true;
		}
	}
}

void DialogImport::lesCharacteristicRead(QLowEnergyCharacteristic characteristic, QByteArray value)
{
#if QT_VERSION < 0x060000
	if(characteristic.uuid() == QBluetoothUuid::ManufacturerNameString)
#else
	if(characteristic.uuid() == QBluetoothUuid::CharacteristicType::ManufacturerNameString)
#endif
	{
		label_producer->setText(value);
	}
#if QT_VERSION < 0x060000
	else if(characteristic.uuid() == QBluetoothUuid::ModelNumberString)
#else
	else if(characteristic.uuid() == QBluetoothUuid::CharacteristicType::ModelNumberString)
#endif
	{
		label_product->setText(value);
	}
#if QT_VERSION < 0x060000
	else if(characteristic.uuid() == QBluetoothUuid::FirmwareRevisionString)
#else
	else if(characteristic.uuid() == QBluetoothUuid::CharacteristicType::FirmwareRevisionString)
#endif
	{
		label_firmware->setText(value);
	}
}

void DialogImport::lesCharacteristicWritten(QLowEnergyCharacteristic characteristic, QByteArray value)
{
	logRawData(true, characteristic, value);
}

bool DialogImport::waitBTFinished()
{
	QElapsedTimer timeout;

	bt_finished = false;

	timeout.start();

	while(!bt_finished)
	{
		QApplication::processEvents();

		QThread::usleep(250);

		if(timeout.hasExpired(5000))
		{
			QMessageBox::warning(this, MODEL, tr("No answer from the device.\n\nDid you use the Bluetooth controller with the cloned MAC address or set a pairing key?\n\nRead the wiki for details on how to do this."));

			return false;
		}
	}

	return true;
}

void DialogImport::readBTInfo()
{
#if QT_VERSION < 0x060000
	if((les_info = lec->createServiceObject(QBluetoothUuid::DeviceInformation)))
#else
	if((les_info = lec->createServiceObject(QBluetoothUuid::ServiceClassUuid::DeviceInformation)))
#endif
	{
		les_info->discoverDetails();

#if QT_VERSION < 0x060000
		while(les_info->state() != QLowEnergyService::ServiceDiscovered)
#else
		while(les_info->state() != QLowEnergyService::RemoteServiceDiscovered)
#endif
		{
			QApplication::processEvents();
		}

		connect(les_info, &QLowEnergyService::characteristicRead, this, &DialogImport::lesCharacteristicRead);

#if QT_VERSION < 0x060000
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::ModelNumberString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::ManufacturerNameString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::FirmwareRevisionString));
#else
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::CharacteristicType::ModelNumberString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::CharacteristicType::ManufacturerNameString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::CharacteristicType::FirmwareRevisionString));
#endif
	}
	else
	{
		label_producer->setText("?");
		label_product->setText("?");
		label_firmware->setText("?");
	}
}

bool DialogImport::readBTData()
{
	if((les_data = lec->createServiceObject(QBluetoothUuid(UUID_300))))
	{
		int addr = devicedata.addr1;
		QByteArray cmd_data = QByteArray::fromHex("0801000000000000");

		cmd_data[3] = addr >> 8;
		cmd_data[4] = addr & 0xFF;
		cmd_data[5] = devicedata.step;
		cmd_data[7] = buildCRC(cmd_data);

		QLowEnergyCharacteristic lechar311, lechar321, lechar361, lechar371/*, lechar381, lechar391*/;	// 381 + 391 only required for >16 bytes notifications
		QLowEnergyDescriptor ledesc311, ledesc321, ledesc361, ledesc371/*, ledesc381, ledesc391*/;

		les_data->discoverDetails();

#if QT_VERSION < 0x060000
		while(les_data->state() != QLowEnergyService::ServiceDiscovered)
#else
		while(les_data->state() != QLowEnergyService::RemoteServiceDiscovered)
#endif
		{
			QApplication::processEvents();
		}

		connect(les_data, &QLowEnergyService::characteristicChanged, this, &DialogImport::lesCharacteristicChanged);
		connect(les_data, &QLowEnergyService::characteristicWritten, this, &DialogImport::lesCharacteristicWritten);

		lechar311 = les_data->characteristic(QBluetoothUuid(UUID_311));
		lechar321 = les_data->characteristic(QBluetoothUuid(UUID_321));
		lechar361 = les_data->characteristic(QBluetoothUuid(UUID_361));
		lechar371 = les_data->characteristic(QBluetoothUuid(UUID_371));
/*		lechar381 = les_data->characteristic(QBluetoothUuid(UUID_381));
		lechar391 = les_data->characteristic(QBluetoothUuid(UUID_391));*/

#if QT_VERSION < 0x060000
		ledesc311 = lechar311.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
//		ledesc321 = lechar321.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
		ledesc361 = lechar361.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
		ledesc371 = lechar371.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
/*		ledesc381 = lechar381.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
		ledesc391 = lechar391.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);*/
#else
		ledesc311 = lechar311.descriptor(QBluetoothUuid::DescriptorType::ClientCharacteristicConfiguration);
//		ledesc321 = lechar321.descriptor(QBluetoothUuid::DescriptorType::ClientCharacteristicConfiguration);
		ledesc361 = lechar361.descriptor(QBluetoothUuid::DescriptorType::ClientCharacteristicConfiguration);
		ledesc371 = lechar371.descriptor(QBluetoothUuid::DescriptorType::ClientCharacteristicConfiguration);
/*		ledesc381 = lechar381.descriptor(QBluetoothUuid::DescriptorType::ClientCharacteristicConfiguration);
		ledesc391 = lechar391.descriptor(QBluetoothUuid::DescriptorType::ClientCharacteristicConfiguration);*/
#endif

		les_data->writeDescriptor(ledesc311, QByteArray::fromHex("0100"));
		les_data->writeDescriptor(ledesc361, QByteArray::fromHex("0100"));
		les_data->writeDescriptor(ledesc371, QByteArray::fromHex("0100"));
/*		les_data->writeDescriptor(ledesc381, QByteArray::fromHex("0100"));
		les_data->writeDescriptor(ledesc391, QByteArray::fromHex("0100"));*/

		les_data->writeCharacteristic(lechar311, QByteArray::fromHex("0100000000000000000000000000000000"));
		waitBTFinished();

		les_data->writeCharacteristic(lechar321, cmd_init);
		if(!waitBTFinished()) return false;

		for(int i = 0; i < devicedata.memory; i++)
		{
			if(abort)
			{
				les_data->writeCharacteristic(lechar321, cmd_fail);

				QMessageBox::warning(this, MODEL, tr("The import was canceled."));

				done(QDialog::Rejected);

				return false;
			}

			bt_finished = false;

			les_data->writeCharacteristic(lechar321, cmd_data);

			if(!waitBTFinished())
			{
				return false;
			}

			if(!payload)
			{
				progressBar_1->setFormat(QString("%1/%2 [%p%]").arg(i+1).arg(devicedata.memory));
				progressBar_1->setValue((i+1)*100 / devicedata.memory);
			}
			else
			{
				progressBar_2->setFormat(QString("%1/%2 [%p%]").arg(i+1).arg(devicedata.memory));
				progressBar_2->setValue((i+1)*100 / devicedata.memory);
			}

			if(devicedata.user == 2)
			{
				if(!payload && i == devicedata.memory - 1)
				{
					i = -1;

					payload = 1;
				}
			}

			addr += devicedata.step;

			cmd_data[3] = addr >> 8;
			cmd_data[4] = addr & 0xFF;
			cmd_data[7] = buildCRC(cmd_data);
		}

		les_data->writeCharacteristic(lechar321, cmd_done);
		if(!waitBTFinished()) return false;
	}
	else
	{
		QMessageBox::warning(this, MODEL, tr("Could not access the Bluetooth service %1.").arg(UUID_300.toString()));

		return false;
	}

	return true;
}

int DialogImport::buildCRC(QByteArray data)
{
	int crc = 0;
	int len = quint8(data[0]);

	while(--len)
	{
		crc ^= quint8(data[len - 1]);
	}

	return crc;
}

int DialogImport::bits2Value(QBitArray bits, int byte, int from, int len)
{
	if(byte >= bits.size() / 8)
	{
		return -1;
	}

	int value = 0;
	int shift = 0;

	for(int bit = byte*8 + from; bit < byte*8 + from + len; bit++)
	{
		value |= bits.at(bit) << shift++;
	}

	return value;
}

void DialogImport::decryptPayload()
{
	QByteArray endian, decode;
	QBitArray bits;
	HEALTHDATA record;
	int user1 = payloads[0].size() / 8;
	int user2 = payloads[1].size() / 8;

	if(log.isOpen())
	{
		log.write(QString("\nUser 1: %1 Records, Payload = ").arg(user1, 3, 10, QChar('0')).toUtf8());
		log.write(payloads[0].toHex());

		if(devicedata.user == 2)
		{
			log.write(QString("\nUser 2: %2 Records, Payload = ").arg(user2, 3, 10, QChar('0')).toUtf8());
			log.write(payloads[1].toHex());
		}

		log.write("\n\nNR    DATE      TIME   SYS DIA BPM I M\n--------------------------------------");
	}

	if(devicedata.bigendian)
	{
		for(int i = 0; i < payloads[0].size(); i += 2)
		{
			endian.append(payloads[0].at(i + 1));
			endian.append(payloads[0].at(i));
		}

		decode = endian;
	}
	else
	{
		decode = payloads[0];
	}

	bits.resize(decode.size() * 8);

	for(int byte = 0; byte < decode.size(); byte++)
	{
		for(int bit = 0; bit < 8; bit++)
		{
			bits.setBit(byte*8 + bit, (decode[byte] >> bit) & 1);
		}
	}

	for(int i = 0; i < decode.size(); i += 8)
	{
		record.dts = QDateTime(QDate(2000 + bits2Value(bits, i + devicedata.year.byte, devicedata.year.bit, devicedata.year.len), bits2Value(bits, i + devicedata.month.byte, devicedata.month.bit, devicedata.month.len), bits2Value(bits, i + devicedata.day.byte, devicedata.day.bit, devicedata.day.len)), QTime(bits2Value(bits, i + devicedata.hour.byte, devicedata.hour.bit, devicedata.hour.len), bits2Value(bits, i + devicedata.minute.byte, devicedata.minute.bit, devicedata.minute.len), bits2Value(bits, i + devicedata.second.byte, devicedata.second.bit, devicedata.second.len))).toMSecsSinceEpoch();
		record.sys = 25 + bits2Value(bits, i + devicedata.sys.byte, devicedata.sys.bit, devicedata.sys.len);
		record.dia = bits2Value(bits, i + devicedata.dia.byte, devicedata.dia.bit, devicedata.dia.len);
		record.bpm = bits2Value(bits, i + devicedata.bpm.byte, devicedata.bpm.bit, devicedata.bpm.len);
		record.ihb = bits2Value(bits, i + devicedata.ihb.byte, devicedata.ihb.bit, devicedata.ihb.len);
		record.mov = bits2Value(bits, i + devicedata.mov.byte, devicedata.mov.bit, devicedata.mov.len);
		record.inv = false;
		record.msg = "";

		if(log.isOpen())
		{
			log.write(QString("\n%1 %2 %3 %4 %5 %6 %7").arg(i/8, 2, 10, QChar('0')).arg(QDateTime::fromMSecsSinceEpoch(record.dts).toString("dd.MM.yyyy hh:mm:ss")).arg(record.sys, 3, 10, QChar('0')).arg(record.dia, 3, 10, QChar('0')).arg(record.bpm, 3, 10, QChar('0')).arg(record.ihb).arg(record.mov).toUtf8());
		}

		u1->append(record);
	}

	if(devicedata.user == 2)
	{
		endian.clear();
		decode.clear();
		bits.clear();

		if(devicedata.bigendian)
		{
			for(int i = 0; i < payloads[1].size(); i += 2)
			{
				endian.append(payloads[1].at(i + 1));
				endian.append(payloads[1].at(i));
			}

			decode = endian;
		}
		else
		{
			decode = payloads[1];
		}

		bits.resize(decode.size() * 8);

		for(int byte = 0; byte < decode.size(); byte++)
		{
			for(int bit = 0; bit < 8; bit++)
			{
				bits.setBit(byte*8 + bit, (decode[byte] >> bit) & 1);
			}
		}

		for(int i = 0; i < decode.size(); i += 8)
		{
			record.dts = QDateTime(QDate(2000 + bits2Value(bits, i + devicedata.year.byte, devicedata.year.bit, devicedata.year.len), bits2Value(bits, i + devicedata.month.byte, devicedata.month.bit, devicedata.month.len), bits2Value(bits, i + devicedata.day.byte, devicedata.day.bit, devicedata.day.len)), QTime(bits2Value(bits, i + devicedata.hour.byte, devicedata.hour.bit, devicedata.hour.len), bits2Value(bits, i + devicedata.minute.byte, devicedata.minute.bit, devicedata.minute.len), bits2Value(bits, i + devicedata.second.byte, devicedata.second.bit, devicedata.second.len))).toMSecsSinceEpoch();
			record.sys = 25 + bits2Value(bits, i + devicedata.sys.byte, devicedata.sys.bit, devicedata.sys.len);
			record.dia = bits2Value(bits, i + devicedata.dia.byte, devicedata.dia.bit, devicedata.dia.len);
			record.bpm = bits2Value(bits, i + devicedata.bpm.byte, devicedata.bpm.bit, devicedata.bpm.len);
			record.ihb = bits2Value(bits, i + devicedata.ihb.byte, devicedata.ihb.bit, devicedata.ihb.len);
			record.mov = bits2Value(bits, i + devicedata.mov.byte, devicedata.mov.bit, devicedata.mov.len);
			record.inv = false;
			record.msg = "";

			if(log.isOpen())
			{
				log.write(QString("\n%1 %2 %3 %4 %5 %6 %7").arg(i/8, 2, 10, QChar('0')).arg(QDateTime::fromMSecsSinceEpoch(record.dts).toString("dd.MM.yyyy hh:mm:ss")).arg(record.sys, 3, 10, QChar('0')).arg(record.dia, 3, 10, QChar('0')).arg(record.bpm, 3, 10, QChar('0')).arg(record.ihb).arg(record.mov).toUtf8());
			}

			u2->append(record);
		}
	}
}

void DialogImport::logRawData(bool direction, QLowEnergyCharacteristic characteristics, QByteArray data)
{
	if(log.isOpen())
	{
		if(characteristics.uuid() != UUID_311)
		{
			log.write(QString("%1 %2 : %3 ").arg(direction ? "->" : "<-").arg(quint8(data[0]), 2, 10, QChar('0')).arg(characteristics.uuid().toString()).toUtf8());
			log.write(data.toHex(), quint8(data[0]) * 2);
			log.write("\n");
		}
	}
}

void DialogImport::on_comboBox_controller_currentIndexChanged(int /*index*/)
{
	comboBox_device->clear();
	bdi.clear();

	pushButton_connect->setDisabled(true);
}

void DialogImport::on_checkBox_auto_discover_toggled(bool state)
{
	settings->plugin.bluetooth.discover = state;
}

void DialogImport::on_checkBox_auto_connect_toggled(bool state)
{
	settings->plugin.bluetooth.connect = state;
}

void DialogImport::on_checkBox_auto_import_toggled(bool state)
{
	settings->plugin.import = state;
}

void DialogImport::on_toolButton_toggled(bool state)
{
	settings->plugin.logging = state;
}

void DialogImport::on_pushButton_discover_clicked()
{
	bdda = new QBluetoothDeviceDiscoveryAgent(QBluetoothAddress(comboBox_controller->currentData().toString()));
	bdda->setLowEnergyDiscoveryTimeout(5*1000);

	connect(bdda, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered, this, &DialogImport::bddaDeviceDiscovered);
#if QT_VERSION < 0x060000
	connect(bdda, QOverload<QBluetoothDeviceDiscoveryAgent::Error>::of(&QBluetoothDeviceDiscoveryAgent::error), this, &DialogImport::bddaError);
#else
	connect(bdda, QOverload<QBluetoothDeviceDiscoveryAgent::Error>::of(&QBluetoothDeviceDiscoveryAgent::errorOccurred), this, &DialogImport::bddaError);
#endif
	connect(bdda, &QBluetoothDeviceDiscoveryAgent::finished, this, &DialogImport::bddaFinished);

	comboBox_device->clear();
	bdi.clear();

	comboBox_controller->setDisabled(true);
	pushButton_discover->setDisabled(true);

	comboBox_device->setDisabled(true);
	pushButton_connect->setDisabled(true);

	bdda->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);	// only paired devices in Windows!
}

void DialogImport::on_pushButton_connect_clicked()
{
	bld = new QBluetoothLocalDevice(QBluetoothAddress(comboBox_controller->currentData().toString()));

	if(!bld->isValid())
	{
		QMessageBox::warning(this, MODEL, tr("The selected Bluetooth controller is not available."));

		return;
	}

#ifdef Q_OS_MACOS
	lec = QLowEnergyController::createCentral(bdi.at(comboBox_device->currentIndex()));
#else
#if QT_VERSION < 0x060000
	lec = QLowEnergyController::createCentral(QBluetoothAddress(bdi.at(comboBox_device->currentIndex()).address()), bld->address());
#else
	lec = QLowEnergyController::createCentral(bdi.at(comboBox_device->currentIndex()), bld->address());
#endif
#endif

	connect(lec, &QLowEnergyController::connected, this, &DialogImport::lecConnected);
	connect(lec, &QLowEnergyController::disconnected, this, &DialogImport::lecDisconnected);
	connect(lec, &QLowEnergyController::discoveryFinished, this, &DialogImport::lecDiscoveryFinished);
#if QT_VERSION < 0x060000
	connect(lec, QOverload<QLowEnergyController::Error>::of(&QLowEnergyController::error), this, &DialogImport::lecError);
#else
	connect(lec, QOverload<QLowEnergyController::Error>::of(&QLowEnergyController::errorOccurred), this, &DialogImport::lecError);
#endif

	comboBox_controller->setDisabled(true);
	pushButton_discover->setDisabled(true);

	comboBox_device->setDisabled(true);
	pushButton_connect->setDisabled(true);

	label_producer->clear();
	label_product->clear();
	label_firmware->clear();

	lec->connectToDevice();
}

void DialogImport::on_pushButton_import_clicked()
{
	pushButton_import->setDisabled(true);
	pushButton_cancel->setEnabled(true);

	if(toolButton->isChecked())
	{
		if(!log.isOpen())
		{
			if(log.open(QIODevice::WriteOnly))
			{
				log.write(QString("%1\n\n   Producer : %2\n   Product  : %3\n   Firmware : %4\n\n").arg(MODEL, label_producer->text(), label_product->text(), label_firmware->text()).toUtf8());
			}
			else
			{
				QMessageBox::critical(this, MODEL, tr("Could not open the logfile %1.\n\n%2").arg(log.fileName(), log.errorString()));
			}
		}
	}

	finished = false;

	if(!readBTData())
	{
		done(QDialog::Rejected);

		return;
	}

	finished = true;

	decryptPayload();

	done(QDialog::Accepted);
}

void DialogImport::on_pushButton_cancel_clicked()
{
	if(QMessageBox::question(this, MODEL, tr("Cancel import?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
		finished = true;
		abort = true;
	}
}

void DialogImport::reject()
{
	if(!finished)
	{
		QMessageBox::warning(this, MODEL, tr("Import in progress…"));

		return;
	}

	log.close();

	QDialog::reject();
}
