#ifndef DLGIMPORT_H
#define DLGIMPORT_H

#define MAINTAINER	QString("<a href='mailto:lazyt@mailbox.org?subject=UBPM Plugin %1'>Thomas Löwe</a>%2").arg(MODEL, HELPER)
#define VERSION		QString("1.4.0 [ BLUETOOTH ]")
#define PRODUCER	"<a href='https://omronhealthcare.com/blood-pressure'>OMRON Corporation</a>"
#define ICON		":/plugin/svg/bluetooth.svg"

#ifdef HEM6232T

	#define MODEL	"HEM-6232T"
	#define ALIAS	"RS7 Intelli IT"
	#define HELPER	", Thomas Wiedemann"

	struct
	{
		int user		= 2;
		int memory		= 100;
		int addr1		= 0x02E8;
		int addr2		= 0x0860;
		int step		= 14;
		bool bigendian	= 1;

		struct { int byte = 3; int bit =  0; int len = 6; } year;
		struct { int byte = 4; int bit = 10; int len = 4; } month;
		struct { int byte = 4; int bit =  5; int len = 5; } day;
		struct { int byte = 4; int bit =  0; int len = 5; } hour;
		struct { int byte = 6; int bit =  6; int len = 6; } minute;
		struct { int byte = 6; int bit =  0; int len = 6; } second;
		struct { int byte = 0; int bit =  0; int len = 8; } sys;
		struct { int byte = 1; int bit =  0; int len = 8; } dia;
		struct { int byte = 2; int bit =  0; int len = 8; } bpm;
		struct { int byte = 4; int bit = 14; int len = 1; } ihb;
		struct { int byte = 4; int bit = 15; int len = 1; } mov;
	}devicedata;

#elif HEM7150T

	#define MODEL	"HEM-7150T"
	#define ALIAS	"BP7250"
	#define HELPER	", Bill Abbas"

	struct {
		int user		= 1;
		int memory		= 60;
		int addr1		= 0x0098;
		int addr2		= 0x0098;
		int step		= 16;
		bool bigendian	= 0;

		struct { int byte = 3; int bit =  0; int len = 6; } year;
		struct { int byte = 4; int bit = 10; int len = 4; } month;
		struct { int byte = 4; int bit =  5; int len = 5; } day;
		struct { int byte = 4; int bit =  0; int len = 5; } hour;
		struct { int byte = 6; int bit =  6; int len = 6; } minute;
		struct { int byte = 6; int bit =  0; int len = 6; } second;
		struct { int byte = 0; int bit =  0; int len = 8; } sys;
		struct { int byte = 1; int bit =  0; int len = 8; } dia;
		struct { int byte = 2; int bit =  0; int len = 8; } bpm;
		struct { int byte = 4; int bit = 14; int len = 1; } ihb;
		struct { int byte = 4; int bit = 15; int len = 1; } mov;
	}devicedata;

#elif HEM7155T

	#define MODEL	"HEM-7155T"
	#define ALIAS	"M4/M400 Intelli IT, X4 Smart"
	#define HELPER	", Annett Heyder"

	struct
	{
		int user		= 2;
		int memory		= 60;
		int addr1		= 0x0098;
		int addr2		= 0x0458;
		int step		= 16;
		bool bigendian	= 0;

		struct { int byte = 3; int bit =  0; int len = 6; } year;
		struct { int byte = 4; int bit = 10; int len = 4; } month;
		struct { int byte = 4; int bit =  5; int len = 5; } day;
		struct { int byte = 4; int bit =  0; int len = 5; } hour;
		struct { int byte = 6; int bit =  6; int len = 6; } minute;
		struct { int byte = 6; int bit =  0; int len = 6; } second;
		struct { int byte = 0; int bit =  0; int len = 8; } sys;
		struct { int byte = 1; int bit =  0; int len = 8; } dia;
		struct { int byte = 2; int bit =  0; int len = 8; } bpm;
		struct { int byte = 4; int bit = 14; int len = 1; } ihb;
		struct { int byte = 4; int bit = 15; int len = 1; } mov;
	}devicedata;

#elif HEM7322T

	#define MODEL	"HEM-7322T"
	#define ALIAS	"M7/M700 Intelli IT"
	#define HELPER	""

	struct
	{
		int user		= 2;
		int memory		= 100;
		int addr1		= 0x02AC;
		int addr2		= 0x0824;
		int step		= 14;
		bool bigendian	= 1;

		struct { int byte = 3; int bit = 0; int len = 6; } year;
		struct { int byte = 5; int bit = 2; int len = 4; } month;
		struct { int byte = 4; int bit = 5; int len = 5; } day;
		struct { int byte = 4; int bit = 0; int len = 5; } hour;
		struct { int byte = 6; int bit = 6; int len = 6; } minute;
		struct { int byte = 6; int bit = 0; int len = 6; } second;
		struct { int byte = 0; int bit = 0; int len = 8; } sys;
		struct { int byte = 1; int bit = 0; int len = 8; } dia;
		struct { int byte = 2; int bit = 0; int len = 8; } bpm;
		struct { int byte = 5; int bit = 6; int len = 1; } ihb;
		struct { int byte = 5; int bit = 7; int len = 1; } mov;
	}devicedata;

#elif HEM7342T

	#define MODEL	"HEM-7342T"
	#define ALIAS	"BP7450"
	#define HELPER	", \"deviantintegral\""

	struct
	{
		int user		= 2;
		int memory		= 100;
		int addr1		= 0x0098;
		int addr2		= 0x06D8;
		int step		= 16;
		bool bigendian	= 0;

		struct { int byte = 3; int bit =  0; int len = 6; } year;
		struct { int byte = 4; int bit = 10; int len = 4; } month;
		struct { int byte = 4; int bit =  5; int len = 5; } day;
		struct { int byte = 4; int bit =  0; int len = 5; } hour;
		struct { int byte = 6; int bit =  6; int len = 6; } minute;
		struct { int byte = 6; int bit =  0; int len = 6; } second;
		struct { int byte = 0; int bit =  0; int len = 8; } sys;
		struct { int byte = 1; int bit =  0; int len = 8; } dia;
		struct { int byte = 2; int bit =  0; int len = 8; } bpm;
		struct { int byte = 4; int bit = 14; int len = 1; } ihb;
		struct { int byte = 4; int bit = 15; int len = 1; } mov;
	}devicedata;

#elif HEM7361T

	#define MODEL	"HEM-7361T"
	#define ALIAS	"M7/M500 Intelli IT, X7 Smart"
	#define HELPER	", \"Elwood\""

	struct
	{
		int user		= 2;
		int memory		= 100;
		int addr1		= 0x0098;
		int addr2		= 0x06D8;
		int step		= 16;
		bool bigendian	= 0;

		struct { int byte = 3; int bit =  0; int len = 6; } year;
		struct { int byte = 4; int bit = 10; int len = 4; } month;
		struct { int byte = 4; int bit =  5; int len = 5; } day;
		struct { int byte = 4; int bit =  0; int len = 5; } hour;
		struct { int byte = 6; int bit =  6; int len = 6; } minute;
		struct { int byte = 6; int bit =  0; int len = 6; } second;
		struct { int byte = 0; int bit =  0; int len = 8; } sys;
		struct { int byte = 1; int bit =  0; int len = 8; } dia;
		struct { int byte = 2; int bit =  0; int len = 8; } bpm;
		struct { int byte = 4; int bit = 14; int len = 1; } ihb;
		struct { int byte = 4; int bit = 15; int len = 1; } mov;
	}devicedata;

#elif HEM7530T

	#define MODEL	"HEM-7530T"
	#define ALIAS	"Complete"
	#define HELPER	""

	struct
	{
		int user		= 1;
		int memory		= 90;
		int addr1		= 0x02E8;
		int addr2		= 0x02E8;
		int step		= 14;
		bool bigendian	= 1;

		struct { int byte = 3; int bit =  0; int len = 6; } year;
		struct { int byte = 4; int bit = 10; int len = 4; } month;
		struct { int byte = 4; int bit =  5; int len = 5; } day;
		struct { int byte = 4; int bit =  0; int len = 5; } hour;
		struct { int byte = 6; int bit =  6; int len = 6; } minute;
		struct { int byte = 6; int bit =  0; int len = 6; } second;
		struct { int byte = 0; int bit =  0; int len = 8; } sys;
		struct { int byte = 1; int bit =  0; int len = 8; } dia;
		struct { int byte = 2; int bit =  0; int len = 8; } bpm;
		struct { int byte = 4; int bit = 14; int len = 1; } ihb;
		struct { int byte = 4; int bit = 15; int len = 1; } mov;
	}devicedata;

#elif HEM7600T

	#define MODEL	"HEM-7600T"
	#define ALIAS	"Evolv"
	#define HELPER	", Alex Morris"

	struct
	{
		int user		= 1;
		int memory		= 100;
		int addr1		= 0x02AC;
		int addr2		= 0x02AC;
		int step		= 14;
		bool bigendian	= 1;

		struct { int byte = 3; int bit = 0; int len = 6; } year;
		struct { int byte = 5; int bit = 2; int len = 4; } month;
		struct { int byte = 4; int bit = 5; int len = 5; } day;
		struct { int byte = 4; int bit = 0; int len = 5; } hour;
		struct { int byte = 6; int bit = 6; int len = 6; } minute;
		struct { int byte = 6; int bit = 0; int len = 6; } second;
		struct { int byte = 0; int bit = 0; int len = 8; } sys;
		struct { int byte = 1; int bit = 0; int len = 8; } dia;
		struct { int byte = 2; int bit = 0; int len = 8; } bpm;
		struct { int byte = 5; int bit = 6; int len = 1; } ihb;
		struct { int byte = 5; int bit = 7; int len = 1; } mov;
	}devicedata;

#endif

#define UUID_300 QBluetoothUuid(QString("ecbe3980-c9a2-11e1-b1bd-0002a5d5c51b"))
#define UUID_311 QBluetoothUuid(QString("b305b680-aee7-11e1-a730-0002a5d5c51b"))
#define UUID_321 QBluetoothUuid(QString("db5b55e0-aee7-11e1-965e-0002a5d5c51b"))
#define UUID_361 QBluetoothUuid(QString("49123040-aee8-11e1-a74d-0002a5d5c51b"))
#define UUID_371 QBluetoothUuid(QString("4d0bf320-aee8-11e1-a0d9-0002a5d5c51b"))
#define UUID_381 QBluetoothUuid(QString("5128ce60-aee8-11e1-b84b-0002a5d5c51b"))
#define UUID_391 QBluetoothUuid(QString("560f1420-aee8-11e1-8184-0002a5d5c51b"))

#define cmd_init QByteArray::fromHex("0800000000100018")
#define cmd_done QByteArray::fromHex("080f000000000007")
#define cmd_fail QByteArray::fromHex("080f0f0f0f000008")

#include "ui_DialogImport.h"

#include <QBluetoothLocalDevice>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothServiceDiscoveryAgent>
#include <QLowEnergyController>
#include <QBitArray>
#include <QDateTime>
#include <QFile>
#include <QMessageBox>
#include <QStandardPaths>
#include <QTimer>
#include <QThread>
#include <QTranslator>

#include "deviceinterface.h"

class DialogImport : public QDialog, private Ui::DialogImport
{
	Q_OBJECT

public:

	explicit DialogImport(QWidget*, QString, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*, struct SETTINGS*);

	bool failed = false;

private:

	QBluetoothLocalDevice *bld;
	QBluetoothDeviceDiscoveryAgent *bdda;
	QLowEnergyController *lec;
	QLowEnergyService *les_info, *les_data;
	QList<QBluetoothDeviceInfo> bdi;

	QByteArray payloads[2];
	int payload = 0;

	QFile log;

	QVector <struct HEALTHDATA> *u1, *u2;

	struct SETTINGS *settings;

	bool abort = false;
	bool finished = true;
	bool bt_finished;

	int searchBtController();
	bool waitBTFinished();
	void readBTInfo();
	bool readBTData();
	int buildCRC(QByteArray);
	int bits2Value(QBitArray, int, int, int);
	void decryptPayload();
	void logRawData(bool, QLowEnergyCharacteristic, QByteArray);

private slots:

	void on_comboBox_controller_currentIndexChanged(int);

	void on_checkBox_auto_discover_toggled(bool);
	void on_checkBox_auto_connect_toggled(bool);
	void on_checkBox_auto_import_toggled(bool);

	void on_toolButton_toggled(bool);

	void bddaDeviceDiscovered(QBluetoothDeviceInfo);
	void bddaError(QBluetoothDeviceDiscoveryAgent::Error);
	void bddaFinished();

	void lecConnected();
	void lecDisconnected();
	void lecDiscoveryFinished();
	void lecError(QLowEnergyController::Error);

	void lesCharacteristicChanged(QLowEnergyCharacteristic, QByteArray);
	void lesCharacteristicRead(QLowEnergyCharacteristic, QByteArray);
	void lesCharacteristicWritten(QLowEnergyCharacteristic, QByteArray);

	void on_pushButton_discover_clicked();
	void on_pushButton_connect_clicked();
	void on_pushButton_import_clicked();
	void on_pushButton_cancel_clicked();

	void reject();
};

#endif // DLGIMPORT_H
