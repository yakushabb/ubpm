#ifndef DLGIMPORT_H
#define DLGIMPORT_H

#define VID 0x0590
#define PID 0x0028

#define DEVICE_INFO_MODE 0x1111
#define DEVICE_DATA_MODE 0x74BC

#define RETRIES	10
#define DELAY	300

#define MAINTAINER	QString("<a href='mailto:lazyt@mailbox.org?subject=UBPM Plugin %1'>Thomas Löwe</a>%2").arg(MODEL, HELPER)
#define VERSION		QString("1.4.0 [ HIDAPI %1 ]").arg(HID_API_VERSION_STR)
#define PRODUCER	"<a href='https://omronhealthcare.com/blood-pressure'>OMRON Corporation</a>"
#ifdef HEM730XIT
	#define MODEL	"HEM-730XIT"
	#define ALIAS	"MIT Elite / Plus"
	#define HELPER	", Helio Machado"
#elif HEM790IT
	#define MODEL	"HEM-790IT"
	#define ALIAS	"790IT"
	#define HELPER	", \"Sghosh151\""
#else
	#define MODEL	"HEM-7080IT"
	#define ALIAS	"M10 IT, M9 Premium"
	#define HELPER	", Sven Schirrmacher"
#endif
#define ICON		":/plugin/svg/usb-hid.svg"

#include "ui_DialogImport.h"

#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QStandardPaths>
#include <QTimer>
#include <QTranslator>
#include <QThread>

#include "deviceinterface.h"

#include "hidapi.h"

class DialogImport : public QDialog, private Ui::DialogImport
{
	Q_OBJECT

public:

	explicit DialogImport(QWidget*, QString, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*, struct SETTINGS*);

	bool failed = false;

private:

	quint8 cmd_clr[8]  = { 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	quint8 cmd_ver[6]  = { 0x05, 0x56, 0x45, 0x52, 0x30, 0x30 };
	quint8 cmd_gdc[10] = { 0x07, 0x47, 0x44, 0x43, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00 };	// 5 = user 0|1|2, 9 = crc
	quint8 cmd_gme[10] = { 0x07, 0x47, 0x4D, 0x45, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00 };	// 5 = user 0|1|2, 7 = index 0...83, 9 = crc
	quint8 cmd_end[6]  = { 0x05, 0x45, 0x4E, 0x44, 0xFF, 0xFF };

	hid_device *hid;
	quint8 rawdata[24];
	QByteArray payload;
	QFile log;

	QVector <struct HEALTHDATA> *u1, *u2;

	struct SETTINGS *settings;

	bool abort = false;
	bool finished = true;
	int retry;
	int rc;
	int cnt1, cnt2;

	int setMode(quint16);
	bool sendVER();
	bool sendCLR();
	bool sendGDC(int);
	bool sendGME(int, int);
	bool sendEND();
	bool checkRC();
	int retryCMD();
	int buildCRC(quint8*);
	void decryptPayload();
	void logRawData(bool, int, quint8*);

private slots:

	void on_checkBox_auto_import_toggled(bool);

	void on_toolButton_toggled(bool);

	void on_pushButton_import_clicked();
	void on_pushButton_cancel_clicked();

	void reject();
};

#endif // DLGIMPORT_H
