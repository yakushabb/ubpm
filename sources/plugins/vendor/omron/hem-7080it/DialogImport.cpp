#include "DialogImport.h"

DialogImport::DialogImport(QWidget *parent, QString theme, QVector <struct HEALTHDATA> *user1, QVector <struct HEALTHDATA> *user2, struct SETTINGS *psettings) : QDialog(parent)
{
	u1 = user1;
	u2 = user2;
	settings = psettings;

	setStyleSheet(theme);

	setupUi(this);

	resize(0, 0);
	layout()->setSizeConstraint(QLayout::SetFixedSize);

	setWindowTitle(MODEL);

#ifdef HEM730XIT
	label_A->hide();
	label_B->hide();
	progressBar_B->hide();
	resize(0, 0);
#endif

	checkBox_auto_import->setChecked(settings->plugin.import);

	toolButton->setChecked(settings->plugin.logging);

	hid_init();

	if((hid = hid_open(VID, PID, nullptr)))
	{
		wchar_t manufacturer[255], product[255];

		hid_get_manufacturer_string(hid, manufacturer, 255);
		hid_get_product_string(hid, product, 255);

		label_producer->setText(QString::fromWCharArray(manufacturer));
		label_product->setText(QString::fromWCharArray(product));
	}
	else
	{
		hid_exit();

		QMessageBox::critical(nullptr, MODEL, tr("Could not open usb device %1:%2.\n\nTry as root or create a udev rule.\n\nRead the wiki for details on how to do this.").arg(VID, 4, 16, QChar('0')).arg(PID, 4, 16, QChar('0')));

		failed = true;

		return;
	}

	log.setFileName(LOGFILE);

	if(settings->plugin.import)
	{
		QTimer::singleShot(100, this, &DialogImport::on_pushButton_import_clicked);
	}
}

int DialogImport::setMode(quint16 mode)
{
	quint8 feature_report[3] = { 0x00, quint8((mode & 0xFF00) >> 8), quint8((mode & 0x00FF)) };

	return hid_send_feature_report(hid, feature_report, 3);
}

bool DialogImport::sendCLR()
{
	rc = hid_write(hid, cmd_clr, 8);
	logRawData(true, rc, cmd_clr);
	rc = hid_write(hid, cmd_clr, 8);
	logRawData(true, rc, cmd_clr);

	rc = hid_read_timeout(hid, rawdata, 8, 1000);
	logRawData(false, rc, rawdata);

	return checkRC();
}

bool DialogImport::sendVER()
{
	rc = hid_write(hid, cmd_ver, 6);
	logRawData(true, rc, cmd_ver);

	rc = hid_read_timeout(hid, rawdata, 8, 1000);
	rc += hid_read_timeout(hid, rawdata + 8, 8, 1000);
	rc += hid_read_timeout(hid, rawdata + 16, 8, 1000);
	logRawData(false, rc, rawdata);

	return checkRC();
}
bool DialogImport::sendGDC(int user)
{
	cmd_gdc[5] = user;
	cmd_gdc[9] = buildCRC(cmd_gdc);

	rc = hid_write(hid, cmd_gdc, 8);
	rc += hid_write(hid, cmd_gdc + 8, 2);
	logRawData(true, rc, cmd_gdc);

	rc = hid_read_timeout(hid, rawdata, 8, 1000);
	rc += hid_read_timeout(hid, rawdata + 8, 8, 1000);
	logRawData(false, rc, rawdata);

	user == 1 ? cnt2 = rawdata[7] : cnt1 = rawdata[7];

	return checkRC();
}

bool DialogImport::sendGME(int user, int index)
{
	cmd_gme[5] = user;
	cmd_gme[7] = index;
	cmd_gme[9] = buildCRC(cmd_gme);

	rc = hid_write(hid, cmd_gme, 8);
	rc += hid_write(hid, cmd_gme + 8, 2);
	logRawData(true, rc, cmd_gme);

	rc = hid_read_timeout(hid, rawdata, 8, 1000);
	rc += hid_read_timeout(hid, rawdata + 8, 8, 1000);
	rc += hid_read_timeout(hid, rawdata + 16, 8, 1000);
	logRawData(false, rc, rawdata);

	return checkRC();
}

bool DialogImport::sendEND()
{
	rc = hid_write(hid, cmd_end, 6);
	logRawData(true, rc, cmd_end);

	rc = hid_read_timeout(hid, rawdata, 8, 1000);
	logRawData(false, rc, rawdata);

	return checkRC();
}

int DialogImport::buildCRC(quint8* data)
{
	int crc = 0;
	int len = data[0] - 3;

	while(len--)
	{
		crc ^= data[7 - len];
	}

	return crc;
}

bool DialogImport::checkRC()
{
	return (rawdata[1] == 'O' && rawdata[2] == 'K') ? true : false;
}

int DialogImport::retryCMD()
{
	retry--;

	if(!retry)
	{
		if(QMessageBox::question(this, MODEL, tr("Device doesn't respond, try again?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
		{
			retry = RETRIES;
		}
	}

	return retry;
}

void DialogImport::decryptPayload()
{
	HEALTHDATA record;

	for(int i = 0; i < 14*cnt1; i += 14)
	{
		record.dts = QDateTime(QDate(2000 + quint8(payload[i]), quint8(payload[i + 1]), quint8(payload[i + 2])), QTime(quint8(payload[i + 3]), quint8(payload[i + 4]), quint8(payload[i + 5]))).toMSecsSinceEpoch();
		record.sys = quint8(payload[i + 8]);
		record.dia = quint8(payload[i + 9]);
		record.bpm = quint8(payload[i + 10]);
		record.ihb = quint8(payload[i + 12]) & 0x01;
		record.mov = quint8(payload[i + 12]) & 0x02;
		record.inv = false;
		record.msg = "";

		u1->append(record);
	}
#ifndef HEM730XIT
	int ofs = cnt1 * 14;

	for(int i = 0; i < 14*cnt2; i += 14)
	{
		record.dts = QDateTime(QDate(2000 + quint8(payload[ofs + i]), quint8(payload[ofs + i + 1]), quint8(payload[ofs + i + 2])), QTime(quint8(payload[ofs + i + 3]), quint8(payload[ofs + i + 4]), quint8(payload[ofs + i + 5]))).toMSecsSinceEpoch();
		record.sys = quint8(payload[ofs + i + 8]);
		record.dia = quint8(payload[ofs + i + 9]);
		record.bpm = quint8(payload[ofs + i + 10]);
		record.ihb = quint8(payload[ofs + i + 12]) & 0x01;
		record.mov = quint8(payload[ofs + i + 12]) & 0x02;
		record.inv = false;
		record.msg = "";

		u2->append(record);
	}
#endif
}

void DialogImport::logRawData(bool direction, int bytes, quint8* data)
{
	if(log.isOpen())
	{
		QByteArray ba(reinterpret_cast<char*>(data), bytes);

		log.write(QString("%1 %2 : ").arg(direction ? "->" : "<-").arg(bytes, 2, 10, QChar('0')).toUtf8());

		if(bytes)
		{
			int len;

			log.write(ba.toHex(' ').toUpper());

			if(bytes > 16)
			{
				len = 3 + ba[0] + ba[8] + ba[16];
			}
			else if(bytes > 8)
			{
				len = 2 + ba[0] + ba[8];
			}
			else
			{
				len = 1 + ba[0];
			}

			log.write("\t[");
			if(len > 1) log.write(ba.left(len).simplified());
			log.write("]");
		}
		else
		{
			log.write(QString("Error!").toUtf8());
		}

		log.write("\n");
	}
}

void DialogImport::on_checkBox_auto_import_toggled(bool state)
{
	settings->plugin.import = state;
}

void DialogImport::on_toolButton_toggled(bool state)
{
	settings->plugin.logging = state;
}

void DialogImport::on_pushButton_import_clicked()
{
	pushButton_import->setDisabled(true);
	pushButton_cancel->setEnabled(true);

	if(toolButton->isChecked())
	{
		if(!log.isOpen())
		{
			if(log.open(QIODevice::WriteOnly))
			{
				log.write(QString("%1\n\n   Producer : %2\n   Product  : %3\n\n").arg(MODEL, label_producer->text(), label_product->text()).toUtf8());
			}
			else
			{
				QMessageBox::critical(this, MODEL, tr("Could not open the logfile %1.\n\n%2").arg(log.fileName(), log.errorString()));
			}
		}
	}

	if(setMode(DEVICE_INFO_MODE) == -1)
	{
		QMessageBox::warning(this, MODEL, tr("Can't set device into info mode."));

		done(QDialog::Rejected);

		return;
	}

	retry = RETRIES;

	while(!sendCLR())
	{
		if(!retryCMD())
		{
			QMessageBox::warning(this, MODEL, tr("Send %1 command failed.").arg("CLR"));

			done(QDialog::Rejected);

			return;
		}
	}

	retry = RETRIES;

	while(!sendVER())
	{
		if(!retryCMD())
		{
			QMessageBox::warning(this, MODEL, tr("Send %1 command failed.").arg("VER"));

			done(QDialog::Rejected);

			return;
		}
	}

	label_serial->setText(QString("%1%2").arg(QByteArray(reinterpret_cast<char*>(&rawdata[4]), 4), QByteArray(reinterpret_cast<char*>(&rawdata[9]), 7)));

	if(setMode(DEVICE_DATA_MODE) == -1)
	{
		QMessageBox::warning(this, MODEL, tr("Can't set device into data mode."));

		done(QDialog::Rejected);

		return;
	}

	retry = RETRIES;

	while(!sendCLR())
	{
		if(!retryCMD())
		{
			QMessageBox::warning(this, MODEL, tr("Send %1 command failed.").arg("CLR"));

			done(QDialog::Rejected);

			return;
		}
	}

	retry = RETRIES;
#ifndef HEM730XIT
	while(!sendGDC(0))
#else
	while(!sendGDC(2))
#endif
	{
		if(!retryCMD())
		{
			QMessageBox::warning(this, MODEL, tr("Send %1 command failed.").arg("GDC A"));

			done(QDialog::Rejected);

			return;
		}
	}

	progressBar_A->setFormat(QString("0/%1 [%p%]").arg(cnt1));
	progressBar_A->setMaximum(cnt1 - 1);
#ifndef HEM730XIT
	retry = RETRIES;

	while(!sendGDC(1))
	{
		if(!retryCMD())
		{
			QMessageBox::warning(this, MODEL, tr("Send %1 command failed.").arg("GDC B"));

			done(QDialog::Rejected);

			return;
		}
	}

	progressBar_B->setFormat(QString("0/%1 [%p%]").arg(cnt2));
	progressBar_B->setMaximum(cnt2 - 1);
#endif
	finished = false;

	for(int i = 0; i < cnt1; i++)
	{
		QGuiApplication::processEvents();

		if(abort)
		{
			sendEND();

			QMessageBox::warning(this, MODEL, tr("Import aborted by user."));

			done(QDialog::Rejected);

			return;
		}

		retry = RETRIES;
#ifndef HEM730XIT
		while(!sendGME(0, i))
#else
		while(!sendGME(2, i))
#endif
		{
			if(!retryCMD())
			{
				QMessageBox::warning(this, MODEL, tr("Send %1 command failed.").arg("GME A"));

				done(QDialog::Rejected);

				return;
			}
		}

		payload.append(reinterpret_cast<char*>(&rawdata[4]), 4);
		payload.append(reinterpret_cast<char*>(&rawdata[9]), 7);
		payload.append(reinterpret_cast<char*>(&rawdata[17]), 3);

		progressBar_A->setFormat(QString("%1/%2 [%p%]").arg(i + 1).arg(cnt1));
		progressBar_A->setValue(i);

		QThread::msleep(DELAY);
	}
#ifndef HEM730XIT
	for(int i = 0; i < cnt2; i++)
	{
		QGuiApplication::processEvents();

		if(abort)
		{
			sendEND();

			QMessageBox::warning(this, MODEL, tr("Import aborted by user."));

			done(QDialog::Rejected);

			return;
		}

		retry = RETRIES;

		while(!sendGME(1, i))
		{
			if(!retryCMD())
			{
				QMessageBox::warning(this, MODEL, tr("Send %1 command failed.").arg("GME B"));

				done(QDialog::Rejected);

				return;
			}
		}

		payload.append(reinterpret_cast<char*>(&rawdata[4]), 4);
		payload.append(reinterpret_cast<char*>(&rawdata[9]), 7);
		payload.append(reinterpret_cast<char*>(&rawdata[17]), 3);

		progressBar_B->setFormat(QString("%1/%2 [%p%]").arg(i + 1).arg(cnt2));
		progressBar_B->setValue(i);

		QThread::msleep(DELAY);
	}
#endif
	sendEND();

	finished = true;

	decryptPayload();

	done(QDialog::Accepted);
}

void DialogImport::on_pushButton_cancel_clicked()
{
	if(QMessageBox::question(this, MODEL, tr("Really abort import?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
		finished = true;
		abort = true;
	}
}

void DialogImport::reject()
{
	if(!finished)
	{
		QMessageBox::warning(this, MODEL, tr("Import in progress…"));

		return;
	}

	hid_close(hid);
	hid_exit();

	log.close();

	QDialog::reject();
}
