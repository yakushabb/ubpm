#include "DialogImport.h"

DialogImport::DialogImport(QWidget *parent, QString theme, QVector <struct HEALTHDATA> *user1, QVector <struct HEALTHDATA> *user2, struct SETTINGS *psettings) : QDialog(parent)
{
	u1 = user1;
	u2 = user2;
	settings = psettings;

	setStyleSheet(theme);

	setupUi(this);

	resize(0, 0);
	layout()->setSizeConstraint(QLayout::SetFixedSize);

	setWindowTitle(MODEL);

	checkBox_auto_import->setChecked(settings->plugin.import);

	toolButton->setChecked(settings->plugin.logging);

	if(!spi.availablePorts().count())
	{
		QMessageBox::critical(nullptr, MODEL, tr("Could not find a serial port.\n\nIs the usb2serial driver installed and the device connected?"));

		failed = true;

		return;
	}

	foreach(QSerialPortInfo pi, spi.availablePorts())
	{
		comboBox->addItem(pi.systemLocation(), QStringList() << pi.manufacturer() << pi.description() << "0x" + QString("%1").arg(pi.vendorIdentifier(), 4, 16, QChar('0')).toUpper() + " 0x" + QString("%1").arg(pi.productIdentifier(), 4, 16, QChar('0')).toUpper());

		if(pi.vendorIdentifier() == VID && pi.productIdentifier() == PID)
		{
			comboBox->setCurrentIndex(comboBox->count() - 1);
		}
	}

	on_comboBox_activated(-1);

	log.setFileName(LOGFILE);

	if(settings->plugin.import)
	{
		QTimer::singleShot(100, this, &DialogImport::on_pushButton_import_clicked);
	}
}

bool DialogImport::sendCMD(QByteArray cmd)
{
	QByteArray answer;

	sp.write(cmd);

	logRawData(true, cmd.length(), cmd);

	if(sp.waitForBytesWritten(TIMEOUT))
	{
		if(cmd == cmd_exit)
		{
			QThread::msleep(TIMEOUT/100);
		}
		else
		{
			while(sp.waitForReadyRead(TIMEOUT))
			{
				answer += sp.readAll();
			}

			logRawData(false, answer.length(), answer);

			if(!answer.length())
			{
				QMessageBox::critical(nullptr, MODEL, tr("Could not read data.\n\n%1").arg(sp.errorString()));

				return false;
			}

			if(cmd == cmd_init)
			{
				if(quint8(answer[0]) != 0x55)
				{
					QMessageBox::warning(this, MODEL, tr("The device doesn't respond."));

					return false;
				}
			}
			else if(cmd == cmd_gcnt)
			{
				measurements[user] = quint8(answer[0]);
			}
			else if(cmd == cmd_gmes)
			{
				payloads[user].append(answer.right(10));	// macOS returns 11 instead 10 byte?
			}
		}
	}
	else
	{
		QMessageBox::critical(nullptr, MODEL, tr("Could not write data.\n\n%1").arg(sp.errorString()));

		return false;
	}

	return true;
}

void DialogImport::decryptPayload()
{
	HEALTHDATA record;

	if(log.isOpen())
	{
		log.write(QString("\nUser 1: %2 Records, Payload = ").arg(measurements[0]).toUtf8());
		log.write(payloads[0].toHex());

		log.write(QString("\nUser 2: %2 Records, Payload = ").arg(measurements[1]).toUtf8());
		log.write(payloads[1].toHex());
	}

	for(int i = 0; i < measurements[0] * 10; i += 10)
	{
		record.dts = QDateTime(QDate(2000 + (quint8(payloads[0][i + 8])), quint8(payloads[0][i + 4]), quint8(payloads[0][i + 5])), QTime(quint8(payloads[0][i + 6]), quint8(payloads[0][i + 7]), 0, 0)).toMSecsSinceEpoch();
		record.sys = quint8(payloads[0][i + 1] + 25);
		record.dia = quint8(payloads[0][i + 2] + 25);
		record.bpm = quint8(payloads[0][i + 3]);
		record.ihb = quint8(payloads[0][i + 9]);
		record.mov = false;
		record.inv = false;
		record.msg = "";

		u1->append(record);
	}

	for(int i = 0; i < measurements[1] * 10; i += 10)
	{
		record.dts = QDateTime(QDate(2000 + (quint8(payloads[1][i + 8])), quint8(payloads[1][i + 4]), quint8(payloads[1][i + 5])), QTime(quint8(payloads[1][i + 6]), quint8(payloads[1][i + 7]), 0, 0)).toMSecsSinceEpoch();
		record.sys = quint8(payloads[1][i + 1] + 25);
		record.dia = quint8(payloads[1][i + 2] + 25);
		record.bpm = quint8(payloads[1][i + 3]);
		record.ihb = quint8(payloads[1][i + 9]);
		record.mov = false;
		record.inv = false;
		record.msg = "";

		u2->append(record);
	}
}

void DialogImport::logRawData(bool direction, int bytes, QByteArray data)
{
	if(log.isOpen())
	{
		log.write(QString("%1 %2 : ").arg(direction ? "->" : "<-").arg(bytes, 2, 10, QChar('0')).toUtf8());

		if(bytes)
		{
			log.write(data.toHex(' ').toUpper());
		}
		else
		{
			log.write(QString("Error!").toUtf8());
		}

		log.write("\n");
	}
}

void DialogImport::on_comboBox_activated(int /*index*/)
{
	label_producer->setText(comboBox->currentData().toStringList().at(0));
	label_description->setText(comboBox->currentData().toStringList().at(1));
	label_ids->setText(comboBox->currentData().toStringList().at(2));
}

void DialogImport::on_checkBox_auto_import_toggled(bool state)
{
	settings->plugin.import = state;
}

void DialogImport::on_toolButton_toggled(bool state)
{
	settings->plugin.logging = state;
}

void DialogImport::on_pushButton_import_clicked()
{
	if(toolButton->isChecked())
	{
		if(!log.isOpen())
		{
			if(log.open(QIODevice::WriteOnly))
			{
				log.write(QString("Beurer %1\n\n").arg(MODEL).toUtf8());
			}
			else
			{
				QMessageBox::critical(this, MODEL, tr("Could not open the logfile %1.\n\n%2").arg(log.fileName(), log.errorString()));
			}
		}
	}

	sp.setPortName(comboBox->currentText());

	if(sp.open(QIODevice::ReadWrite))
	{
		pushButton_import->setDisabled(true);
		pushButton_cancel->setEnabled(true);

		sp.clear();

		sp.setBaudRate(4800);
		sp.setDataBits(QSerialPort::Data8);
		sp.setParity(QSerialPort::NoParity);
		sp.setStopBits(QSerialPort::OneStop);
		sp.setFlowControl(QSerialPort::NoFlowControl);

		if(!sendCMD(cmd_init))
		{
			sp.close();

			done(QDialog::Rejected);

			return;
		}

		user = 0;

		if(!sendCMD(cmd_gcnt))
		{
			sp.close();

			done(QDialog::Rejected);

			return;
		}

		for(int i = 1; i <= measurements[user]; i++)
		{
			if(abort)
			{
				QMessageBox::warning(this, MODEL, tr("The import was canceled."));

				done(QDialog::Rejected);

				return;
			}

			cmd_gmes[2] = i;

			if(!sendCMD(cmd_gmes))
			{
				sp.close();

				done(QDialog::Rejected);

				return;
			}

			progressBar_u1->setFormat(QString("%1/%2 [%p%]").arg(i).arg(measurements[user]));
			progressBar_u1->setValue(i*100 / measurements[user]);

			QApplication::processEvents();
		}

		user = 1;

		cmd_gcnt[1] = user;
		cmd_gmes[1] = user;

		if(!sendCMD(cmd_gcnt))
		{
			sp.close();

			done(QDialog::Rejected);

			return;
		}

		for(int i = 1; i <= measurements[user]; i++)
		{
			if(abort)
			{
				QMessageBox::warning(this, MODEL, tr("The import was canceled."));

				done(QDialog::Rejected);

				return;
			}

			cmd_gmes[2] = i;

			if(!sendCMD(cmd_gmes))
			{
				sp.close();

				done(QDialog::Rejected);

				return;
			}

			progressBar_u2->setFormat(QString("%1/%2 [%p%]").arg(i).arg(measurements[user]));
			progressBar_u2->setValue(i*100 / measurements[user]);

			QApplication::processEvents();
		}

		sendCMD(cmd_exit);

		sp.close();
	}
	else
	{
		QMessageBox::critical(this, MODEL, tr("Could not open serial port \"%1\".\n\n%2\n\nTry as root or create a udev rule.\n\nRead the wiki for details on how to do this.").arg(comboBox->currentText(), sp.errorString()));

		done(QDialog::Rejected);

		return;
	}

	decryptPayload();

	done(QDialog::Accepted);
}

void DialogImport::on_pushButton_cancel_clicked()
{
	if(QMessageBox::question(this, MODEL, tr("Cancel import?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
		finished = true;
		abort = true;
	}
}

void DialogImport::reject()
{
	if(!finished)
	{
		QMessageBox::warning(this, MODEL, tr("Import in progress…"));

		return;
	}

	log.close();

	QDialog::reject();
}
