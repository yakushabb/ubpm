TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets serialport
DEFINES		+= BM55
INCLUDEPATH	+= ../../../../../
SOURCES		= DialogImport.cpp ../../../../shared/plugin/plugin.cpp
HEADERS		= DialogImport.h   ../../../../shared/plugin/plugin.h
FORMS		= DialogImport.ui
RESOURCES	= ../res/bm55.qrc
TARGET		= ../../../../beurer-bm55-s

unix:!macx {
QMAKE_RPATHDIR	+= $ORIGIN/../../lib
}

win32 {
CONFIG		-= debug_and_release
}

macx {
}

system($$QMAKE_COPY_FILE $$shell_path($$PWD/../../bm58/v-ser/DialogImport.* .))
system($$QMAKE_COPY_FILE $$shell_path($$PWD/../../../../../mainapp/res/svg/plugin/*.svg ../res/svg))
QMAKE_CLEAN += $$OUT_PWD/DialogImport.*
QMAKE_CLEAN += $$OUT_PWD/../res/svg/*.svg
