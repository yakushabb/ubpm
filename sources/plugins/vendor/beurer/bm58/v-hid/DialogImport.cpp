#include "DialogImport.h"

DialogImport::DialogImport(QWidget *parent, QString theme, QVector <struct HEALTHDATA> *user1, QVector <struct HEALTHDATA> *user2, struct SETTINGS *psettings) : QDialog(parent)
{
	u1 = user1;
	u2 = user2;
	settings = psettings;

	setStyleSheet(theme);

	setupUi(this);

	resize(0, 0);
	layout()->setSizeConstraint(QLayout::SetFixedSize);

	setWindowTitle(MODEL);

	checkBox_auto_import->setChecked(settings->plugin.import);

	toolButton->setChecked(settings->plugin.logging);

	hid_init();

	if((hid = hid_open(VID, PID, nullptr)))
	{
		wchar_t manufacturer[255], product[255];

		hid_get_manufacturer_string(hid, manufacturer, 255);
		hid_get_product_string(hid, product, 255);

		label_producer->setText(QString::fromWCharArray(manufacturer));
		label_description->setText(QString::fromWCharArray(product));
	}
	else
	{
		hid_exit();

		QMessageBox::critical(nullptr, MODEL, tr("Could not open usb device %1:%2.\n\nTry as root or create a udev rule.\n\nRead the wiki for details on how to do this.").arg(VID, 4, 16, QChar('0')).arg(PID, 4, 16, QChar('0')));

		failed = true;

		return;
	}

	log.setFileName(LOGFILE);

	if(settings->plugin.import)
	{
		QTimer::singleShot(100, this, &DialogImport::on_pushButton_import_clicked);
	}
}

bool DialogImport::sendINI()
{
	rc = hid_write(hid, cmd_init, 9);
	logRawData(true, rc, cmd_init);

	rc = hid_read_timeout(hid, rawdata, 8, TIMEOUT);
	logRawData(false, rc, rawdata);

	return (rawdata[0] == 0x55 ? true : false);
}

bool DialogImport::sendGDC()
{
	rc = hid_write(hid, cmd_gcnt, 9);
	logRawData(true, rc, cmd_gcnt);

	rc = hid_read_timeout(hid, rawdata, 8, TIMEOUT);
	logRawData(false, rc, rawdata);

	measurements = rawdata[0];

	return (rc != -1 ? true : false);
}

bool DialogImport::sendGME()
{
	rc = hid_write(hid, cmd_gmes, 9);
	logRawData(true, rc, cmd_gmes);

	rc = hid_read_timeout(hid, rawdata, 8, TIMEOUT);
	logRawData(false, rc, rawdata);

	payload.append(reinterpret_cast<char*>(rawdata), 8);

	return (rc != -1 ? true : false);
}

bool DialogImport::sendEND()
{
	rc = hid_write(hid, cmd_exit, 9);
	logRawData(true, rc, cmd_exit);

	return (rc != -1 ? true : false);
}

void DialogImport::decryptPayload()
{
	HEALTHDATA record;

	for(int i = 0; i < measurements*8; i += 8)
	{
		record.dts = QDateTime(QDate(2000 + (quint8(payload[i + 7]) & 0x7F), quint8(payload[i + 3]) & 0x0F, quint8(payload[i + 4]) & 0x1F), QTime(quint8(payload[i + 5]), quint8(payload[i + 6]), 0, 0)).toMSecsSinceEpoch();
		record.sys = quint8(payload[i] + 25);
		record.dia = quint8(payload[i + 1] + 25);
		record.bpm = quint8(payload[i + 2]);
#if defined BC58 || defined BM65
		record.ihb = false;
		record.mov = false;
#elif BM55
		record.ihb = quint8(payload[i + 7]) & 0x80;
		record.mov = quint8(payload[i + 3]) >> 7;
#else
		record.ihb = quint8(payload[i + 4]) & 0x80;
		record.mov = false;
#endif
		record.inv = false;
		record.msg = "";

		quint8(payload[i + 4])>>7 ? u2->append(record) : u1->append(record);
	}

	if(log.isOpen())
	{
		log.write(QString("\nUser 1 : %1 Records\n").arg(u1->count()).toUtf8());
		log.write(QString("\nUser 2 : %1 Records\n").arg(u2->count()).toUtf8());
		log.write("Payload: " + payload.toHex());
	}
}

void DialogImport::logRawData(bool direction, int bytes, quint8 *data)
{
	if(log.isOpen())
	{
		log.write(QString("%1 %2 : ").arg(direction ? "->" : "<-").arg(bytes, 2, 10, QChar('0')).toUtf8());

		if(bytes)
		{
			log.write(QByteArray(reinterpret_cast<char*>(data), 8).toHex(' ').toUpper());
		}
		else
		{
			log.write(QString("Error!").toUtf8());
		}

		log.write("\n");
	}
}

void DialogImport::on_checkBox_auto_import_toggled(bool state)
{
	settings->plugin.import = state;
}

void DialogImport::on_toolButton_toggled(bool state)
{
	settings->plugin.logging = state;
}

void DialogImport::on_pushButton_import_clicked()
{
	if(toolButton->isChecked())
	{
		if(!log.isOpen())
		{
			if(log.open(QIODevice::WriteOnly))
			{
				log.write(QString("Beurer %1\n\n").arg(MODEL).toUtf8());
			}
			else
			{
				QMessageBox::critical(this, MODEL, tr("Could not open the logfile %1.\n\n%2").arg(log.fileName(), log.errorString()));
			}
		}
	}

	pushButton_import->setDisabled(true);
	pushButton_cancel->setEnabled(true);

	if(!sendINI())
	{
		QMessageBox::warning(this, MODEL, tr("The device doesn't respond.\n\nDisconnect the USB cable and reconnect it.\n\nThen try again…"));

		done(QDialog::Rejected);

		return;
	}

	if(!sendGDC())
	{
		QMessageBox::critical(this, MODEL, tr("Could not read measurement count."));

		done(QDialog::Rejected);

		return;
	}

	for(int i = 1; i <= measurements; i++)
	{
		if(abort)
		{
			QMessageBox::warning(this, MODEL, tr("The import was canceled."));

			done(QDialog::Rejected);

			return;
		}

		cmd_gmes[2] = i;

		if(!sendGME())
		{
			QMessageBox::critical(this, MODEL, tr("Could not read measurement %1.").arg(i));

			done(QDialog::Rejected);

			return;
		}

		progressBar->setFormat(QString("%1/%2 [%p%]").arg(i).arg(measurements));
		progressBar->setValue(i*100 / measurements);

		QApplication::processEvents();
	}

	sendEND();

	decryptPayload();

	done(QDialog::Accepted);
}

void DialogImport::on_pushButton_cancel_clicked()
{
	if(QMessageBox::question(this, MODEL, tr("Cancel import?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
		finished = true;
		abort = true;
	}
}

void DialogImport::reject()
{
	if(!finished)
	{
		QMessageBox::warning(this, MODEL, tr("Import in progress…"));

		return;
	}

	hid_close(hid);
	hid_exit();

	log.close();

	QDialog::reject();
}
