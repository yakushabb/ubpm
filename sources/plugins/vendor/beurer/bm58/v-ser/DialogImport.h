#ifndef DLGIMPORT_H
#define DLGIMPORT_H

#define VID 0x067B
#define PID 0x2303

#define MAINTAINER	QString("<a href='mailto:lazyt@mailbox.org?subject=UBPM Plugin %1'>Thomas Löwe</a>%2").arg(MODEL, HELPER)
#define VERSION		QString("1.4.0 [ USB2SERIAL ]")
#define PRODUCER	"<a href='https://www.beurer.com/uk/c/medical/blood-pressure-monitor'>Beurer</a>"
#ifdef BC58
	#define MODEL	"BC58S"
	#define ALIAS	"BC 58"
	#define HELPER	""
#elif defined BM55
	#define MODEL	"BM55S"
	#define ALIAS	"BM 55"
	#define HELPER	""
#elif defined BM65
	#define MODEL	"BM65S"
	#define ALIAS	"BM 65"
	#define HELPER	""
#else
	#define MODEL	"BM58S"
	#define ALIAS	"BM 58"
	#define HELPER	", \"Christel\""
#endif
#define ICON		":/plugin/svg/usb-srl.svg"

#define TIMEOUT 1000

#include "ui_DialogImport.h"

#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QStandardPaths>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QThread>
#include <QTimer>
#include <QTranslator>

#include "deviceinterface.h"

class DialogImport : public QDialog, private Ui::DialogImport
{
	Q_OBJECT

public:

	explicit DialogImport(QWidget*, QString, QVector <struct HEALTHDATA>*, QVector <struct HEALTHDATA>*, struct SETTINGS*);

	bool failed = false;

private:

	QByteArray cmd_init = QByteArray::fromHex("AA");
	QByteArray cmd_gcnt = QByteArray::fromHex("A2");
	QByteArray cmd_gmes = QByteArray::fromHex("A300");
	QByteArray cmd_exit = QByteArray::fromHex("F7");;

	QSerialPortInfo spi;
	QSerialPort sp;

	QByteArray payload;
	QFile log;

	QVector <struct HEALTHDATA> *u1, *u2;

	struct SETTINGS *settings;

	bool abort = false;
	bool finished = true;

	int measurements = 0;

	bool sendCMD(QByteArray);
	void decryptPayload();
	void logRawData(bool, int, QByteArray);

private slots:

	void on_comboBox_activated(int);

	void on_checkBox_auto_import_toggled(bool);

	void on_toolButton_toggled(bool);

	void on_pushButton_import_clicked();
	void on_pushButton_cancel_clicked();

	void reject();
};

#endif // DLGIMPORT_H
