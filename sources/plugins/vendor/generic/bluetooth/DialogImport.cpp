#include "DialogImport.h"

DialogImport::DialogImport(QWidget *parent, QString theme, QVector <struct HEALTHDATA> *user1, QVector <struct HEALTHDATA> *user2, struct SETTINGS *psettings) : QDialog(parent)
{
	u1 = user1;
	u2 = user2;
	settings = psettings;

	int controllers;

	setStyleSheet(theme);

	setupUi(this);

	resize(0, 0);
	layout()->setSizeConstraint(QLayout::SetFixedSize);

	label_discover->setMinimumWidth(label_discover->fontMetrics().horizontalAdvance("000"));
	label_connectstate->setMinimumHeight(label_discover->fontMetrics().height() + 10);

	lineEdit_uid1->setValidator(new QRegularExpressionValidator(QRegularExpression("[01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-4]")));
	lineEdit_uid2->setValidator(new QRegularExpressionValidator(QRegularExpression("[01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-4]")));

	horizontalSlider_discover->setValue(settings->plugin.bluetooth.discovertime);
	lineEdit_device->setText(settings->plugin.bluetooth.connectname);
	lineEdit_uid1->setText(QString::number(settings->plugin.bluetooth.uid1));
	lineEdit_uid2->setText(QString::number(settings->plugin.bluetooth.uid2));

	toolButton_auto_discover->setChecked(settings->plugin.bluetooth.discover);
	toolButton_auto_connect->setChecked(settings->plugin.bluetooth.connect);
	toolButton_auto_import->setChecked(settings->plugin.import);

	lcdNumber_u1->display("000");
	lcdNumber_u2->display("000");

	toolButton_log->setChecked(settings->plugin.logging);

	log.setFileName(LOGFILE);

	connect(timerDiscover, &QTimer::timeout, this, &DialogImport::timeoutDiscover);
	connect(timerConnect, &QTimer::timeout, this, &DialogImport::timeoutConnect);
	connect(timerMeasurement, &QTimer::timeout, this, &DialogImport::timeoutMeasurement);

	if(!(controllers = searchBtController()))
	{
		failed = true;
	}
	else if(controllers == 1 && settings->plugin.bluetooth.discover)
	{
		QTimer::singleShot(100, this, &DialogImport::on_pushButton_discover_clicked);
	}
}

void DialogImport::timeoutDiscover()
{
	progressBar_discover->setValue(progressBar_discover->value() + 1);

	if(progressBar_discover->value() == progressBar_discover->maximum())
	{
		timerDiscover->stop();
	}
}

void DialogImport::timeoutConnect()
{
	progressBar_connect->setValue(progressBar_connect->value() + 1);

	if(progressBar_connect->value() == progressBar_connect->maximum())
	{
		timerConnect->stop();

		lec->disconnectFromDevice();
	}
}

void DialogImport::timeoutMeasurement()
{
	progressBar_measurements->setValue(progressBar_measurements->value() + 1);

	if(progressBar_measurements->value() == progressBar_measurements->maximum())
	{
		timerMeasurement->stop();

		lec->disconnectFromDevice();
	}
}

void DialogImport::enableControllerElements(bool state)
{
	comboBox_controller->setEnabled(state);
	horizontalSlider_discover->setEnabled(state);
	label_discover->setEnabled(state);
	pushButton_discover->setEnabled(state);
}

void DialogImport::enableDeviceElements(bool state)
{
	comboBox_device->setEnabled(state);
	lineEdit_device->setEnabled(state);
	toolButton_copy->setEnabled(state);
	pushButton_connect->setEnabled(state);
}

int DialogImport::searchBtController()
{
	QList <QBluetoothHostInfo> bhil = QBluetoothLocalDevice::allDevices();

	if(bhil.count())
	{
		foreach(QBluetoothHostInfo bhi, bhil)
		{
			comboBox_controller->addItem(QString("%1 | %2").arg(bhi.address().toString(), bhi.name()), bhi.address().toString());
		}
	}
	else
	{
		QMessageBox::warning(nullptr, MODEL, tr("No Bluetooth controller found."));
	}

	return bhil.count();
}

// ###  QBluetoothDeviceDiscoveryAgent ###

void DialogImport::bddaCanceled()
{
	timerDiscover->stop();

	enableControllerElements(true);
	enableDeviceElements(true);

	if(settings->plugin.bluetooth.connect)
	{
		on_pushButton_connect_clicked();
	}
}

void DialogImport::bddaDeviceDiscovered(QBluetoothDeviceInfo info)
{
	QStringList items;
	QString address;

#ifdef Q_OS_MACOS
	address = info.deviceUuid().toString();
#else
	address = info.address().toString();
#endif

	for(int i = 0; i < comboBox_device->count(); i++)
	{
		items.append(comboBox_device->itemData(i).toString());
	}

	if(info.serviceUuids().contains(QBluetoothUuid::ServiceClassUuid::BloodPressure) && !items.contains(address))
	{
		comboBox_device->addItem(info.name(), address);

		bdi.append(info);

		if(info.name() == lineEdit_device->text())
		{
			comboBox_device->setCurrentIndex(comboBox_device->count() - 1);

			bdda->stop();
		}
	}
}

void DialogImport::bddaError(QBluetoothDeviceDiscoveryAgent::Error error)
{
	Q_UNUSED(error)

	enableControllerElements(true);

	QMessageBox::warning(this, MODEL, tr("An error occurred during device discovery.\n\n%1").arg(bdda->errorString()));
}

void DialogImport::bddaFinished()
{
	timerDiscover->stop();
	progressBar_discover->setValue(progressBar_discover->maximum());

	enableControllerElements(true);

	if(comboBox_device->count())
	{
		enableDeviceElements(true);
	}
	else
	{
		QMessageBox::warning(this, MODEL, tr("No device discovered.\n\nCheck the connection in operating system or press Bluetooth button on device and try again…"));
	}
}

// ### QLowEnergyController ###

void DialogImport::lecConnected()
{
	timerConnect->stop();
	progressBar_connect->setValue(progressBar_connect->maximum());

	lec->discoverServices();
}

void DialogImport::lecDisconnected()
{
	enableControllerElements(true);
	enableDeviceElements(true);

	pushButton_import->setDisabled(true);

	finished = true;
}

void DialogImport::lecDiscoveryFinished()
{
	if(!lec->services().contains(QBluetoothUuid::ServiceClassUuid::BloodPressure))
	{
		enableControllerElements(true);
		enableDeviceElements(true);

		QMessageBox::warning(this, MODEL, tr("The selected device is not supported."));

		return;
	}

	readBTInfo();

	pushButton_import->setEnabled(true);

	if(settings->plugin.import)
	{
		on_pushButton_import_clicked();
	}
}

void DialogImport::lecErrorOccurred(QLowEnergyController::Error error)
{
	Q_UNUSED(error)

	timerConnect->stop();
	progressBar_connect->setValue(progressBar_connect->maximum());

	enableControllerElements(true);
	enableDeviceElements(true);

	QMessageBox::warning(this, MODEL, tr("Could not connect to the device.\n\nCheck the connection in operating system or press Bluetooth button on device and try again…\n\n%1").arg(lec->errorString()));
}

void DialogImport::lecstateChanged(QLowEnergyController::ControllerState state)
{
	QStringList states = { tr("Unconnected"), tr("Connecting…"), tr("Connected"), tr("Discovering…"), tr("Discovered"), tr("Closing…"), tr("Advertising…") };

	label_connectstate->setText(tr("Current State of the Device: %1").arg(states.at(state)));
}

// ### QLowEnergyService ###

void DialogImport::lesCharacteristicChanged(QLowEnergyCharacteristic characteristic, QByteArray value)
{
	timeout.restart();

	timerMeasurement->stop();
	progressBar_measurements->setValue(progressBar_measurements->maximum());

#if QT_VERSION < 0x060000
	if(les_data->state() != QLowEnergyService::DiscoveringServices)
#else
	if(les_data->state() != QLowEnergyService::RemoteServiceDiscovering)
#endif
	{
		if(characteristic.uuid() == QBluetoothUuid::CharacteristicType::BloodPressureMeasurement)
		{
			if(quint8(value[16]) == lineEdit_uid1->text().toUInt())
			{
				payloads[0].append(value);

				lcdNumber_u1->display(QString("%1").arg(payloads[0].size() / 19, 3, 10, QChar('0')));
			}
			else if(quint8(value[16]) == lineEdit_uid2->text().toUInt())
			{
				payloads[1].append(value);

				lcdNumber_u2->display(QString("%1").arg(payloads[1].size() / 19, 3, 10, QChar('0')));
			}
			else //if(quint8(value[16]) == 0xFF)
			{
				lec->disconnectFromDevice();

				QMessageBox::warning(this, MODEL, tr("Unknown UID \"%1\" detected, transfer aborted.\n\nChange UID settings and try again…").arg(quint8(value[16])));
			}

			logRawData(false, characteristic, value);
		}
	}
}

void DialogImport::lesCharacteristicRead(QLowEnergyCharacteristic characteristic, QByteArray value)
{
	timeout.restart();

	progressBar_information->setValue(progressBar_information->value() + 1);

#if QT_VERSION < 0x060000
	if(characteristic.uuid() == QBluetoothUuid::ManufacturerNameString)
#else
	if(characteristic.uuid() == QBluetoothUuid::CharacteristicType::ManufacturerNameString)
#endif
	{
		label_manufacturer->setText(value);
	}
#if QT_VERSION < 0x060000
	else if(characteristic.uuid() == QBluetoothUuid::ModelNumberString)
#else
	else if(characteristic.uuid() == QBluetoothUuid::CharacteristicType::ModelNumberString)
#endif
	{
		label_model->setText(value);
	}
#if QT_VERSION < 0x060000
	else if(characteristic.uuid() == QBluetoothUuid::SerialNumberString)
#else
	else if(characteristic.uuid() == QBluetoothUuid::CharacteristicType::SerialNumberString)
#endif
	{
		label_serial->setText(value);
	}
#if QT_VERSION < 0x060000
	else if(characteristic.uuid() == QBluetoothUuid::HardwareRevisionString)
#else
	else if(characteristic.uuid() == QBluetoothUuid::CharacteristicType::HardwareRevisionString)
#endif
	{
		label_hardware->setText(value);
	}
#if QT_VERSION < 0x060000
	else if(characteristic.uuid() == QBluetoothUuid::FirmwareRevisionString)
#else
	else if(characteristic.uuid() == QBluetoothUuid::CharacteristicType::FirmwareRevisionString)
#endif
	{
		label_firmware->setText(value);
	}
#if QT_VERSION < 0x060000
	else if(characteristic.uuid() == QBluetoothUuid::SoftwareRevisionString)
#else
	else if(characteristic.uuid() == QBluetoothUuid::CharacteristicType::SoftwareRevisionString)
#endif
	{
		label_software->setText(value);
	}
}

/*void DialogImport::lesCharacteristicWritten(QLowEnergyCharacteristic characteristic, QByteArray value)
{
	logRawData(true, characteristic, value);
}*/

void DialogImport::lesDescriptorWritten(const QLowEnergyDescriptor &descriptor, const QByteArray &value)
{
	Q_UNUSED(descriptor)
	Q_UNUSED(value)
}

void DialogImport::lesErrorOccurred(QLowEnergyService::ServiceError error)
{
	Q_UNUSED(error)
}

void DialogImport::lesStateChanged(QLowEnergyService::ServiceState state)
{
	Q_UNUSED(state)
}

bool DialogImport::waitBT(qint64 time)
{
	timeout.start();

	while(!timeout.hasExpired(time))
	{
		QApplication::processEvents();

		if(lec->state() == QLowEnergyController::UnconnectedState)
		{
			return false;
		}
	}

	return true;
}

void DialogImport::readBTInfo()
{
#if QT_VERSION < 0x060000
	if((les_info = lec->createServiceObject(QBluetoothUuid::DeviceInformation)))
#else
	if((les_info = lec->createServiceObject(QBluetoothUuid::ServiceClassUuid::DeviceInformation)))
#endif
	{
		progressBar_information->setValue(0);

		les_info->discoverDetails();

#if QT_VERSION < 0x060000
		while(les_info->state() != QLowEnergyService::ServiceDiscovered)
#else
		while(les_info->state() != QLowEnergyService::RemoteServiceDiscovered)
#endif
		{
			QApplication::processEvents();
		}

		connect(les_info, &QLowEnergyService::characteristicRead, this, &DialogImport::lesCharacteristicRead);

#if QT_VERSION < 0x060000
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::ManufacturerNameString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::ModelNumberString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::SerialNumberString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::HardwareRevisionString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::FirmwareRevisionString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::SoftwareRevisionString));
#else
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::CharacteristicType::ManufacturerNameString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::CharacteristicType::ModelNumberString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::CharacteristicType::SerialNumberString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::CharacteristicType::HardwareRevisionString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::CharacteristicType::FirmwareRevisionString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::CharacteristicType::SoftwareRevisionString));
#endif
		waitBT(500);

		if(label_manufacturer->text().isEmpty())	label_manufacturer->setText("?");
		if(label_model->text().isEmpty())			label_model->setText("?");
		if(label_firmware->text().isEmpty())		label_firmware->setText("?");
		if(label_hardware->text().isEmpty())		label_hardware->setText("?");
		if(label_software->text().isEmpty())		label_software->setText("?");
		if(label_serial->text().isEmpty())			label_serial->setText("?");
	}
	else
	{
		label_manufacturer->setText("?");
		label_model->setText("?");
		label_firmware->setText("?");
		label_hardware->setText("?");
		label_software->setText("?");
		label_serial->setText("?");
	}
}

bool DialogImport::readBTData()
{
	if((les_data = lec->createServiceObject(QBluetoothUuid::ServiceClassUuid::BloodPressure)))
	{
		QLowEnergyCharacteristic lechar;
		QLowEnergyDescriptor ledesc;

		les_data->discoverDetails();

#if QT_VERSION < 0x060000
		while(les_data->state() != QLowEnergyService::ServiceDiscovered)
#else
		while(les_data->state() != QLowEnergyService::RemoteServiceDiscovered)
#endif
		{
			if(les_data->state() == QLowEnergyService::InvalidService)
			{
				enableDeviceElements(true);

				QMessageBox::warning(this, MODEL, tr("Lost connection to device."));

				return false;
			}

			QApplication::processEvents();
		}

		connect(les_data, &QLowEnergyService::characteristicChanged, this, &DialogImport::lesCharacteristicChanged);
//		connect(les_data, &QLowEnergyService::characteristicWritten, this, &DialogImport::lesCharacteristicWritten);
		connect(les_data, &QLowEnergyService::descriptorWritten, this, &DialogImport::lesDescriptorWritten);
#if QT_VERSION < 0x060000
		connect(les_data, QOverload<QLowEnergyService::ServiceError>::of(&QLowEnergyService::error), this, &DialogImport::lesErrorOccurred);
#else
		connect(les_data, QOverload<QLowEnergyService::ServiceError>::of(&QLowEnergyService::errorOccurred), this, &DialogImport::lesErrorOccurred);
#endif
		connect(les_data, &QLowEnergyService::stateChanged, this, &DialogImport::lesStateChanged);

		lechar = les_data->characteristic(QBluetoothUuid::CharacteristicType::BloodPressureMeasurement);

#if QT_VERSION < 0x060000
		ledesc = lechar.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
#else
		ledesc = lechar.descriptor(QBluetoothUuid::DescriptorType::ClientCharacteristicConfiguration);
#endif

		logRawData(true, lechar, QByteArray::fromHex("0200"));

		les_data->writeDescriptor(ledesc, QByteArray::fromHex("0200"));

		timerMeasurement->start(1000);

		waitBT(15000);
	}
	else
	{
		QMessageBox::warning(this, MODEL, tr("Could not access the Bluetooth service %1.").arg(QBluetoothUuid(QBluetoothUuid::ServiceClassUuid::BloodPressure).toString()));

		return false;
	}

	return true;
}

int DialogImport::bits2Value(QBitArray bits, int byte, int from, int len)
{
	if(byte >= bits.size() / 8)
	{
		return -1;
	}

	int value = 0;
	int shift = 0;

	for(int bit = byte*8 + from; bit < byte*8 + from + len; bit++)
	{
		value |= bits.at(bit) << shift++;
	}

	return value;
}

void DialogImport::decryptPayload()
{
	QByteArray decode;
	QBitArray bits;
	HEALTHDATA record;
	int user1 = payloads[0].size() / 19;
	int user2 = payloads[1].size() / 19;

	if(log.isOpen())
	{
		log.write(QString("\nUser 1: %1 Record(s), Payload = ").arg(user1, 3, 10, QChar('0')).toUtf8());
		log.write(payloads[0].toHex());

		log.write(QString("\nUser 2: %2 Record(s), Payload = ").arg(user2, 3, 10, QChar('0')).toUtf8());
		log.write(payloads[1].toHex());

		log.write("\n\nNR    DATE      TIME   SYS DIA BPM I M\n--------------------------------------");
	}

	decode = payloads[0];

	bits.resize(decode.size() * 8);

	for(int byte = 0; byte < decode.size(); byte++)
	{
		for(int bit = 0; bit < 8; bit++)
		{
			bits.setBit(byte*8 + bit, (decode[byte] >> bit) & 1);
		}
	}

	for(int i = 0; i < decode.size(); i += 19)
	{
		record.dts = QDateTime(QDate(bits2Value(bits, i + devicedata.year.byte, devicedata.year.bit, devicedata.year.len), bits2Value(bits, i + devicedata.month.byte, devicedata.month.bit, devicedata.month.len), bits2Value(bits, i + devicedata.day.byte, devicedata.day.bit, devicedata.day.len)), QTime(bits2Value(bits, i + devicedata.hour.byte, devicedata.hour.bit, devicedata.hour.len), bits2Value(bits, i + devicedata.minute.byte, devicedata.minute.bit, devicedata.minute.len), bits2Value(bits, i + devicedata.second.byte, devicedata.second.bit, devicedata.second.len))).toMSecsSinceEpoch();
		record.sys = bits2Value(bits, i + devicedata.sys.byte, devicedata.sys.bit, devicedata.sys.len);
		record.dia = bits2Value(bits, i + devicedata.dia.byte, devicedata.dia.bit, devicedata.dia.len);
		record.bpm = bits2Value(bits, i + devicedata.bpm.byte, devicedata.bpm.bit, devicedata.bpm.len);
		record.ihb = bits2Value(bits, i + devicedata.ihb.byte, devicedata.ihb.bit, devicedata.ihb.len);
		record.mov = bits2Value(bits, i + devicedata.mov.byte, devicedata.mov.bit, devicedata.mov.len);
		record.inv = false;
		record.msg = "";

		if(log.isOpen())
		{
			log.write(QString("\n%1 %2 %3 %4 %5 %6 %7").arg(i/8, 2, 10, QChar('0')).arg(QDateTime::fromMSecsSinceEpoch(record.dts).toString("dd.MM.yyyy hh:mm:ss")).arg(record.sys, 3, 10, QChar('0')).arg(record.dia, 3, 10, QChar('0')).arg(record.bpm, 3, 10, QChar('0')).arg(record.ihb).arg(record.mov).toUtf8());
		}

		u1->append(record);
	}

	if(log.isOpen())
	{
		log.write("\n");
	}

	decode = payloads[1];

	bits.resize(decode.size() * 8);

	for(int byte = 0; byte < decode.size(); byte++)
	{
		for(int bit = 0; bit < 8; bit++)
		{
			bits.setBit(byte*8 + bit, (decode[byte] >> bit) & 1);
		}
	}

	for(int i = 0; i < decode.size(); i += 19)
	{
		record.dts = QDateTime(QDate(bits2Value(bits, i + devicedata.year.byte, devicedata.year.bit, devicedata.year.len), bits2Value(bits, i + devicedata.month.byte, devicedata.month.bit, devicedata.month.len), bits2Value(bits, i + devicedata.day.byte, devicedata.day.bit, devicedata.day.len)), QTime(bits2Value(bits, i + devicedata.hour.byte, devicedata.hour.bit, devicedata.hour.len), bits2Value(bits, i + devicedata.minute.byte, devicedata.minute.bit, devicedata.minute.len), bits2Value(bits, i + devicedata.second.byte, devicedata.second.bit, devicedata.second.len))).toMSecsSinceEpoch();
		record.sys = bits2Value(bits, i + devicedata.sys.byte, devicedata.sys.bit, devicedata.sys.len);
		record.dia = bits2Value(bits, i + devicedata.dia.byte, devicedata.dia.bit, devicedata.dia.len);
		record.bpm = bits2Value(bits, i + devicedata.bpm.byte, devicedata.bpm.bit, devicedata.bpm.len);
		record.ihb = bits2Value(bits, i + devicedata.ihb.byte, devicedata.ihb.bit, devicedata.ihb.len);
		record.mov = bits2Value(bits, i + devicedata.mov.byte, devicedata.mov.bit, devicedata.mov.len);
		record.inv = false;
		record.msg = "";

		if(log.isOpen())
		{
			log.write(QString("\n%1 %2 %3 %4 %5 %6 %7").arg(i/8, 2, 10, QChar('0')).arg(QDateTime::fromMSecsSinceEpoch(record.dts).toString("dd.MM.yyyy hh:mm:ss")).arg(record.sys, 3, 10, QChar('0')).arg(record.dia, 3, 10, QChar('0')).arg(record.bpm, 3, 10, QChar('0')).arg(record.ihb).arg(record.mov).toUtf8());
		}

		u2->append(record);
	}
}

void DialogImport::logRawData(bool direction, QLowEnergyCharacteristic characteristics, QByteArray data)
{
	if(log.isOpen())
	{
		log.write(QString("%1 %2 : %3 ").arg(direction ? "->" : "<-").arg(data.size(), 2, 10, QChar('0')).arg(characteristics.uuid().toString()).toUtf8());
		log.write(data.toHex());
		log.write("\n");
	}
}

void DialogImport::on_comboBox_controller_currentIndexChanged(int index)
{
	Q_UNUSED(index)

	comboBox_device->clear();
	bdi.clear();

	pushButton_connect->setDisabled(true);
}

void DialogImport::on_lineEdit_device_textChanged(const QString &text)
{
	if(text.isEmpty())
	{
		toolButton_auto_connect->setChecked(false);
	}

	settings->plugin.bluetooth.connectname = text;
}

void DialogImport::on_horizontalSlider_discover_valueChanged(int value)
{
	progressBar_discover->setMaximum(value);

	label_discover->setText(QString(" %1 ").arg(value));

	settings->plugin.bluetooth.discovertime = value;
}

void DialogImport::on_toolButton_auto_discover_toggled(bool state)
{
	settings->plugin.bluetooth.discover = state;
}

void DialogImport::on_toolButton_auto_connect_toggled(bool state)
{
	if(state == true && lineEdit_device->text().isEmpty())
	{
		QMessageBox::warning(this, MODEL, tr("Auto connection requires a model name."));

		toolButton_auto_connect->setChecked(false);

		lineEdit_device->setFocus();

		return;
	}

	settings->plugin.bluetooth.connect = state;
}

void DialogImport::on_toolButton_auto_import_toggled(bool state)
{
	settings->plugin.import = state;
}

void DialogImport::on_toolButton_log_toggled(bool state)
{
	settings->plugin.logging = state;
}

void DialogImport::on_pushButton_discover_clicked()
{
	bdda = new QBluetoothDeviceDiscoveryAgent(QBluetoothAddress(comboBox_controller->currentData().toString()));
	bdda->setLowEnergyDiscoveryTimeout(horizontalSlider_discover->value() * 1000);

	connect(bdda, &QBluetoothDeviceDiscoveryAgent::canceled, this, &DialogImport::bddaCanceled);
	connect(bdda, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered, this, &DialogImport::bddaDeviceDiscovered);
#if QT_VERSION < 0x060000
	connect(bdda, QOverload<QBluetoothDeviceDiscoveryAgent::Error>::of(&QBluetoothDeviceDiscoveryAgent::error), this, &DialogImport::bddaError);
#else
	connect(bdda, QOverload<QBluetoothDeviceDiscoveryAgent::Error>::of(&QBluetoothDeviceDiscoveryAgent::errorOccurred), this, &DialogImport::bddaError);
#endif
	connect(bdda, &QBluetoothDeviceDiscoveryAgent::finished, this, &DialogImport::bddaFinished);

	comboBox_device->clear();
	bdi.clear();

	enableControllerElements(false);
	enableDeviceElements(false);

	progressBar_discover->setValue(0);
	timerDiscover->start(1000);

	bdda->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);	// only paired devices in Windows!
}

void DialogImport::on_pushButton_connect_clicked()
{
	bld = new QBluetoothLocalDevice(QBluetoothAddress(comboBox_controller->currentData().toString()));

	if(!bld->isValid())
	{
		QMessageBox::warning(this, MODEL, tr("The selected Bluetooth controller is not available."));

		return;
	}

#ifdef Q_OS_MACOS
	lec = QLowEnergyController::createCentral(bdi.at(comboBox_device->currentIndex()));
#else
#if QT_VERSION < 0x060000
	lec = QLowEnergyController::createCentral(QBluetoothAddress(bdi.at(comboBox_device->currentIndex()).address()), bld->address());
#else
	lec = QLowEnergyController::createCentral(bdi.at(comboBox_device->currentIndex()), bld->address());
#endif
#endif

	connect(lec, &QLowEnergyController::connected, this, &DialogImport::lecConnected);
	connect(lec, &QLowEnergyController::disconnected, this, &DialogImport::lecDisconnected);
	connect(lec, &QLowEnergyController::discoveryFinished, this, &DialogImport::lecDiscoveryFinished);
#if QT_VERSION < 0x060000
	connect(lec, QOverload<QLowEnergyController::Error>::of(&QLowEnergyController::error), this, &DialogImport::lecErrorOccurred);
#else
	connect(lec, QOverload<QLowEnergyController::Error>::of(&QLowEnergyController::errorOccurred), this, &DialogImport::lecErrorOccurred);
#endif
	connect(lec, &QLowEnergyController::stateChanged, this, &DialogImport::lecstateChanged);

	enableControllerElements(false);
	enableDeviceElements(false);

	label_manufacturer->clear();
	label_model->clear();
	label_firmware->clear();
	label_hardware->clear();
	label_software->clear();
	label_serial->clear();
	progressBar_information->setValue(0);

	progressBar_connect->setValue(0);
	timerConnect->start(1000);

	lec->connectToDevice();
}

void DialogImport::on_toolButton_copy_clicked()
{
	lineEdit_device->setText(comboBox_device->currentText());
}

void DialogImport::on_pushButton_import_clicked()
{
	pushButton_import->setDisabled(true);
	pushButton_cancel->setEnabled(true);

	if(toolButton_log->isChecked())
	{
		if(!log.isOpen())
		{
			if(log.open(QIODevice::WriteOnly))
			{
				log.write(QString("Device \"%1\"\n\n   Manufacturer : %2\n   Model        : %3\n   Firmware     : %4\n   Hardware     : %5\n   Software     : %6\n   Serial       : %7\n\n").arg(comboBox_device->currentText(), label_manufacturer->text(), label_model->text(), label_firmware->text(), label_hardware->text(), label_software->text(), label_serial->text()).toUtf8());
			}
			else
			{
				QMessageBox::critical(this, MODEL, tr("Could not open the logfile %1.\n\n%2").arg(log.fileName(), log.errorString()));
			}
		}
	}

	finished = false;

	if(!readBTData())
	{
		return;
	}

	finished = true;

	decryptPayload();

	done(QDialog::Accepted);
}

void DialogImport::on_pushButton_cancel_clicked()
{
	if(QMessageBox::question(this, MODEL, tr("Cancel import?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
		finished = true;
		abort = true;
	}
}

void DialogImport::resizeEvent(QResizeEvent *event)
{
	Q_UNUSED(event)

	QRect parentRect(parentWidget()->mapToGlobal(QPoint(0, 0)), parentWidget()->size());
	int maxwidth1 = groupBox_controller->width();
	int maxwidth2 = groupBox_info1->width();

	if(maxwidth1 < groupBox_device->width()) maxwidth1 = groupBox_device->width();
	if(maxwidth2 < groupBox_info2->width()) maxwidth2 = groupBox_info2->width();

	groupBox_controller->setMinimumWidth(maxwidth1);
	groupBox_device->setMinimumWidth(maxwidth1);
	groupBox_info1->setMinimumWidth(maxwidth2);
	groupBox_info2->setMinimumWidth(maxwidth2);

	move(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), parentRect).topLeft());
}

void DialogImport::reject()
{
	if(!finished)
	{
		QMessageBox::warning(this, MODEL, tr("Import in progress…"));

		return;
	}

	log.close();

	settings->plugin.bluetooth.uid1 = lineEdit_uid1->text().toInt();
	settings->plugin.bluetooth.uid2 = lineEdit_uid2->text().toInt();

	QDialog::reject();
}
