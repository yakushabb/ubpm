TEMPLATE = subdirs
SUBDIRS  = vendor/beurer vendor/generic vendor/hartmann vendor/omron

TRANSLATIONS = shared/plugin/lng/plugins_de.ts shared/plugin/lng/plugins_es.ts shared/plugin/lng/plugins_fr.ts shared/plugin/lng/plugins_hu.ts shared/plugin/lng/plugins_it.ts shared/plugin/lng/plugins_nb_NO.ts shared/plugin/lng/plugins_nl.ts shared/plugin/lng/plugins_pl.ts

appbundle.target = appbundle
installer.target = installer

QMAKE_EXTRA_TARGETS += appbundle installer
