<img src="https://codeberg.org/LazyT/ubpm/raw/branch/master/sources/mainapp/res/ico/ubpm.png" align="left" width="64" height="64">

# UBPM - Universal Blood Pressure Manager

[![](https://codeberg.org/LazyT/ubpm/raw/branch/master/website/badges/framework.svg)](https://qt.io "Qt Info")
[![](https://codeberg.org/LazyT/ubpm/raw/branch/master/website/badges/platform.svg)](https://codeberg.org/lazyt/ubpm/releases "Download Release")
[![](https://codeberg.org/LazyT/ubpm/raw/branch/master/website/badges/license.svg)](https://codeberg.org/lazyt/ubpm/src/branch/master/LICENSE "Show License")
[![](https://codeberg.org/LazyT/ubpm/raw/branch/master/website/badges/translation.svg)](https://hosted.weblate.org/engage/ubpm "Contribute Translation")
[![](https://codeberg.org/LazyT/ubpm/raw/branch/master/website/badges/paypal.svg)](https://www.paypal.com/donate/?hosted_button_id=VT55E55UYP3VA "Support Development")

## Description

Tired of the original software supplied by the manufacturer of your blood pressure monitor because it's only available for Windows and requires an internet connection for uploading your private health data into the cloud? Then try UBPM for free and use it on Windows, Linux and macOS!

The current version supports the following features:

* import data from manual input, file (CSV, JSON, XML, SQL) or directly from supported blood pressure monitors
* export data to CSV, JSON, XML, SQL or PDF format
* migrate data from vendor software
* view, print and mail data as chart, table or statistics
* analyze data via SQL queries
* plugin interface for blood pressure monitors with a computer interface (USB, Bluetooth)
* online updater
* context-sensitive help via F1 key
* style GUI via CSS files
* multi-language (EN, DE, ES, FR, HU, IT, NL, NO, PL, PT, RU)
* cross-platform (same look & feel on Windows, Linux, macOS)

## Devices

<details><summary>Click to show supported blood pressure monitors</summary>
<br>

Manufacturer | Model | Description | Memory | Interface | Comment
:---:|:---:|:---:|:---:|:---:|:---:
[BEURER](https://www.beurer.com/uk/c/medical/blood-pressure-monitor)         | BC58       | BC 58                        | 2 x 60  | USB (HID/SERIAL) | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#beurer-h-and-s)<br>Thanks to Andreas Hoppe (HID) for testing, SERIAL untested
[BEURER](https://www.beurer.com/uk/c/medical/blood-pressure-monitor)         | BC80       | BC 80                        | 2 x 60  | USB (HID/SERIAL) | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#beurer-h-and-s)<br>Thanks to Paul Müller (SERIAL) for testing, HID untested
[BEURER](https://www.beurer.com/uk/c/medical/blood-pressure-monitor)         | BM55       | BM 55                        | 2 x 60  | USB (HID/SERIAL) | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#beurer-h-and-s)<br>Thanks to Werner Panocha (HID) for testing, SERIAL untested
[BEURER](https://www.beurer.com/uk/c/medical/blood-pressure-monitor)         | BM58       | BM 58                        | 2 x 60  | USB (HID/SERIAL) | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#beurer-h-and-s)<br>Thanks to "Christel" (SERIAL) for sponsoring the device and Karsten Festag (HID) for testing
[BEURER](https://www.beurer.com/uk/c/medical/blood-pressure-monitor)         | BM65       | BM 65                        | 1 x 30  | USB (HID/SERIAL) | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#beurer-h-and-s)<br>Untested, please report
[HARTMANN](https://www.veroval.info/en/products/bloodpressure)               | BPM25      | Veroval Wrist                | 2 x 100 | USB (SERIAL)     | Thanks to Carsten Presser for testing
[HARTMANN](https://www.veroval.info/en/products/bloodpressure)               | DC318      | Veroval Duo Control          | 2 x 100 | USB (SERIAL)     | Thanks to Andreas Hencke for giving access via usbip over internet
[HARTMANN](https://www.veroval.info/en/products/bloodpressure)               | GCE604     | Veroval Upper Arm            | 2 x 100 | USB (SERIAL)     | Thanks to Annett Heyder for testing
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-7600T  | Evolv                        | 1 x 100 | BLUETOOTH        | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#omron-message-no-answer-from-device)<br>Thanks to Alex Morris for testing
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-7530T  | Complete                     | 1 x 90  | BLUETOOTH        | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#omron-message-no-answer-from-device)<br>Untested, please report
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-7361T  | M7/M500 Intelli IT, X7 Smart | 2 x 100 | BLUETOOTH        | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#omron-message-no-answer-from-device)<br>Thanks to "Elwood" for sponsoring the device
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-7342T  | BP7450                       | 2 x 100 | BLUETOOTH        | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#omron-message-no-answer-from-device)<br>Thanks to "deviantintegral" for testing
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-7322T  | M7/M700 Intelli IT           | 2 x 100 | BLUETOOTH        | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#omron-message-no-answer-from-device)<br>Untested, please report
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-7155T  | M4/M400 Intelli IT, X4 Smart | 2 x 60  | BLUETOOTH        | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#omron-message-no-answer-from-device)<br>Thanks to Annett Heyder for testing
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-7150T  | BP7250                       | 1 x 60  | BLUETOOTH        | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#omron-message-no-answer-from-device)<br>Thanks to Bill Abbas for testing
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-6232T  | RS7 Intelli IT               | 2 x 100 | BLUETOOTH        | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#omron-message-no-answer-from-device)<br>Thanks to Thomas Wiedemann for testing
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-7322U  | M6 Comfort IT, M500 IT       | 2 x 100 | USB (HID)        | [Where it all began](https://github.com/LazyT/obpm)
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-7131U  | M3 IT, M400 IT               | 2 x 60  | USB (HID)        | Thanks to Simon Peter for testing
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-7080IT | M10 IT, M9 Premium           | 2 x 84  | USB (HID)        | Thanks to Sven Schirrmacher for giving access via usbip over internet
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-790IT  | 790IT                        | 2 x 84  | USB (HID)        | Thanks to "Sghosh151" for testing
[OMRON](https://omronhealthcare.com/blood-pressure)                          | HEM-730xIT | MIT Elite (Plus)             | 1 x 90  | USB (HID)        | Thanks to Helio Machado for giving access via usbip over internet
[ALL](https://www.bluetooth.com/specifications/specs/blood-pressure-service) | GENERIC    | GATT Standard 0x1810/0x02A35 |         | BLUETOOTH        | [Important, read me first!](https://codeberg.org/LazyT/ubpm/wiki/Plugins#generic-bluetooth-plugin)
</details>

Your device is not yet supported? Check the following options:

* contact me or open an issue if you have an Omron Bluetooth device, I'll send you a test version
* read the "Device Plugins" section of the guide and take a look at the plugin sources ([usb-hid](https://codeberg.org/LazyT/ubpm/src/branch/master/sources/plugins/vendor/omron/hem-7322u), [usb-serial](https://codeberg.org/LazyT/ubpm/src/branch/master/sources/plugins/vendor/hartmann/gce604), [bluetooth](https://codeberg.org/LazyT/ubpm/src/branch/master/sources/plugins/vendor/omron/hem-7361t)) to start coding your own plugin if you are a developer
* allow remote access to your USB device over the internet via usbip so I can access it remotely
* sponsor a device or donate money for the purchase (see help menu entry "About" for e-mail or "Donation" for payment methods)

## Screenshots

<details><summary>Click to show images of the app</summary>
<br>

Main window with some records…

![Chart View](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/chartview.png "Chart View")
![Table View](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/tableview.png "Table View")
![Stats View](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/statsview.png "Stats View")

Print or mail records…

![Chart Print](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/chartprint.png "Chart Print")
![Table Print](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/tableprint.png "Table Print")
![Stats Print](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/statsprint.png "Stats Print")

Create or modify records…

![Input](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/input.png "Input")

Import from supported device…

![Import](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/import.gif "Import")

Migrate from vendor software…

![Migration](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/migration.png "Migration")

Analyze records via SQL queries…

![Analysis](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/analysis.png "Analysis")

Show data distribution…

![Distribution](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/distribution.png "Distribution")

Configure your preferred settings…

![Settings](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/settings.png "Settings")

Press F1 to get help for the current context…

![Guide](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/guide.png "Guide")
</details>

## Download

Download the latest version for your operating system. All 3 files (EXE, DMG, AppImage) contain everything to run UBPM on the target platform without installing anything.

* [Windows (EXE)](https://codeberg.org/LazyT/ubpm/releases/latest)

This is a [7zip](https://www.7-zip.org) self extracting archive. It will be automatically extracted to "%temp%\7zxxxxxxxx" and after that the "ubpm.exe" is started. You can copy this directory or extract the file if you want the content.

* [Linux (AppImage)](https://codeberg.org/LazyT/ubpm/releases/latest)

This is a [AppImage](https://appimage.org) package. Don't forget to "chmod +x *.AppImage" after download and execute. You can use the parameter "--appimage-extract" if you want the content.

* [macOS (DMG)](https://codeberg.org/LazyT/ubpm/releases/latest)

This is an Apple disc image. You can mount and run or copy the application.

### Verify Download

To verify the download was not tampered, a corresponding "*.sig" file is also available. It contains the hash and the signature of the release.

Two external commands are needed to check the integrity and authenticity: "sha256sum" and "minisign". If these are missing on your system please install first:

**sha256sum**

* Windows: download [busybox](https://frippery.org/files/busybox/busybox.exe) and save as "sha256sum.exe"
* Linux: install via package manager, e.g. ```apt install coreutils```
* macOS: install via package manager like [Homebrew](https://brew.sh), e.g. ```brew install coreutils```

**minisign**

* Windows: download and unzip [minisign-win.zip](https://github.com/jedisct1/minisign/releases/latest)
* Linux: install via package manager, use [PPA](https://launchpad.net/~dysfunctionalprogramming/+archive/ubuntu/minisign) or compile from [source](https://github.com/jedisct1/minisign/archive/refs/heads/master.zip)
* macOS: download and unzip [minisign-osx.zip](https://github.com/jedisct1/minisign/releases/latest) or install via package manager, e.g. ```brew install minisign```

After downloading the release and signature files open a terminal and copy & paste the commands found in the "*.sig" comment sections.

Example for Linux version 1.0.4:

```echo 344e9fe0882d0fdb3057d094cfa6d986e7cbda7d1b23d5f2b734365b5549fce8  ubpm-1.0.4.AppImage | sha256sum -c```

verified -> "ubpm-1.0.4.AppImage: **OK**"\
failed -> "ubpm-1.0.4.AppImage: **FAILED**"

```minisign -V -P RWRjSAJt4zCGy2SYhAWoLpygWZv5640wfhLqi/NLnaznnu18+txes5YA -m ubpm-1.0.4.AppImage -x ubpm-1.0.4.AppImage.sig```

verified -> "Signature and comment signature **verified**"\
failed -> "(Comment) Signature verification **failed**"

If you get a failed result, don't use the downloaded file! Maybe it was only corrupted during download, but it's also possible that it was intentionally manipulated to harm you.

The public signing key for UBPM is always **RWRjSAJt4zCGy2SYhAWoLpygWZv5640wfhLqi/NLnaznnu18+txes5YA** for all releases.

## Install from AppStore

- Linux (developer costs: €0)

You can install UBPM for Linux also from [Flathub](https://flathub.org/apps/page.codeberg.lazyt.ubpm) or [Snapcraft](https://snapcraft.io/ubpm).

- Windows (developer costs: €17, once)

You can install UBPM for Windows also from [Windows Store](https://apps.microsoft.com/detail/9n52v9m890sg).

- macOS (developer costs: €99, yearly)

This variant cannot be offered due to the excessive fees for a developer account.

## Build from Source

Download the latest [Qt Online Installer](https://qt.io/download-open-source) and install for

Qt5
- "Desktop gcc 64-bit" or "macOS" or "MinGW x.x.x 64-bit"
- "Qt Charts"
- "OpenSSL 64-bit binaries" (Windows only)

Qt6
- "Desktop gcc 64-bit" or "macOS" or "MSVC 2019 64-Bit" (Bluetooth doesn't work with MinGW)
- from "Additional Libraries" the "Qt Charts", "Qt Connectivity" and "Qt Serial Port"
- "OpenSSL 64-bit binaries" (Windows only)
- [Visual Studio](https://aka.ms/vs/17/release/vs_buildtools.exe) and install "Desktop development with C++" (Windows only)

To be uniform on all operating systems you should search and copy "mingw32-make.exe" in the Qt install path (Qt5) or "nmake.exe" in the Visual Studio install path (Qt6) to "make.exe" on Windows (or replace the command "make" with "mingw32-make" / "nmake" below).

For SQL database encryption [qsqlciper](https://github.com/sjemens/qsqlcipher-qt5) is also required.

To build the appbundle for Windows you need [7zip](https://www.7-zip.org/download.html) too.

- Checkout the source code via git (or download and extract the [zip](https://codeberg.org/LazyT/ubpm/archive/master.zip))

		git clone https://codeberg.org/lazyt/ubpm

- Change into the UBPM sources directory and generate the Makefile (on Windows run "Qt 5.15.x (MinGW x.x.x 64-bit)" or "Qt 6.6.0 (MSVC 2019 64-bit)" from startmenu instead of using normal command prompt)

		cd ubpm/sources && qmake

- Compile the source code

		make

- Install UBPM to system (as root/administrator)

		make install

- Optional create a release appbundle (AppImage, dmg, exe)

		make appbundle

### Build options

To define optional parameters, you can use

		qmake DEFINES+="option1 option2"

Valid options are

Option | Description
:---:|:---:
APPBUNDLE      | [AppImage, DMG or EXE](https://codeberg.org/LazyT/ubpm/releases)
FLATPAK        | [Flathub](https://flathub.org/de/apps/page.codeberg.lazyt.ubpm)
SNAP           | [Snapcraft](https://snapcraft.io/ubpm)
MSIX           | [Windows Store](https://apps.microsoft.com/detail/9n52v9m890sg)
DISTRIBUTION   | Maintaining UBPM for a Linux distribution
UPDATE_HIDE    | Hide the update menu entry and disable online update checks (e.g. for internal distribution package management)
UPDATE_DISABLE | Disable the update menu entry and online update checks (e.g. for internal distribution package management)

## Add new Localization

If your preferred language is missing, please contribute a translation. There are 2 ways of doing this:

### Online

The easiest way is to translate everything online via [Weblate](https://hosted.weblate.org/engage/ubpm), but you need to register a free account first.

Open the [project page](https://hosted.weblate.org/projects/ubpm) and select the component you would like to start with, e.g. "Application". Click on "Start new translation" and set your language (or select an already existing but unfinished language to complete), translate all strings and repeat with the next component, e.g. "Guide" and "Plugins".

It can be helpful to open the "\*.ts" file in "linguist", because you can see the context of the string in a real-time view of the user interface.

You can download all components via "Files -> Download ZIP" and test it like described under the Local section below, **but skip:**

* application/plugins: all steps and only run ```lrelease *.ts``` instead to create the "*.qm" files
* guide: the step copying "en.json" and use your downloaded "xx.json" instead

### Local

The "xx" in the following text stands for your 2 digit [ISO-639-1](https://www.loc.gov/standards/iso639-2/php/code_list.php) language code, for example "en" for English.

The commands "lrelease", "lupdate" and "linguist" are part of the [Qt Framework](https://www.qt.io/download-qt-installer). Standalone binaries are also available for [Windows](https://github.com/thurask/Qt-Linguist/releases). For Linux/macOS use the package manager of your distribution and search for something like "qttools5-dev-tools".

A complete localization consists of 3 parts:

**1. Application**

To translate the application go to "sources/mainapp" and

* find and add "TRANSLATIONS ... lng/mainapp_xx.ts" in "mainapp.pro"
* run ```lupdate -no-obsolete mainapp.pro``` to create the "mainapp_xx.ts" with English strings
* run ```linguist lng/mainapp_xx.ts``` and translate all strings
* click on "File\Save" and "File\Release"
* new file "mainapp_xx.qm" should be created and contains the application localization
* to test your new application translation, copy this file to your "UBPM/Languages" directory

**2. Plugins**

To translate the plugins go to "sources/plugins/vendor" and

* find and add "TRANSLATIONS ... ../shared/plugin/lng/plugins_xx.ts" in "plugins.pro"
* run ```lupdate -no-obsolete plugin.pro``` to create the "plugins_xx.ts" with English strings
* run ```linguist ../shared/plugin/lng/plugins_xx.ts``` and translate all strings
* click on "File\Save" and "File\Release"
* new file "../shared/plugin/lng/plugins_xx.qm" should be created and contains the plugins localization
* to test your new plugins translation, copy this file to your "UBPM/Languages" directory

**3. Guide**

To translate the guide, go to "sources/mainapp/hlp" and

* copy "help/en.qh\*" to "help/xx.qh\*"
* change ```<file>../en.qch</file>``` in "xx.qhcp" to ```<file>../xx.qch</file>```
* change ```<virtualFolder>en</virtualFolder>``` in "xx.qhp" to ```<virtualFolder>xx</virtualFolder>``` and translate all section titles
* copy "html/lang/en.json" to "html/lang/xx.json" and translate all strings
* copy "html/img/en" to "html/img/xx" and replace all images with your localized screenshots
* find and add "for LNG ... xx; do" in "gen-help.sh"
* run this script to build the "xx" guide (requires static-i18n installed)
* new files "xx.qch" and "xx.qhc" should be created and contains the localized guide
* to test your new guide translation, copy these files to your "UBPM/Guides" directory

Now start UBPM, switch to your new language and test everything…

## Privacy Policy

UBPM is an open source application that is provided free of charge and intended for use as is.

It does not require a user account and does not collect, transmit or share any personal data. All user-defined data (name, birthday, gender, height, weight, blood pressure, heart rate, ...) is used only internally and stored locally.

Some features (checking for updates, opening links, sending emails) require an Internet connection to third-party servers, which may log technical information about the connection and store cookies.

Hardware access (Bluetooth, HID, Serial) is used only on request to import data directly from supported blood pressure monitors.

The software does not contain advertising, trackers or telemetry.

There is no business model and the only income is voluntary donations.

## Credits

UBPM is based on

* [Qt](https://www.qt.io)
* [Weblate](https://weblate.org)
* [HIDAPI](https://github.com/libusb/hidapi)
* [Beurer](https://connect.beurer.com/developer), [Omron](https://github.com/openyou/libomron/blob/master/doc/omron_protocol_notes.asciidoc), Veroval [1](https://github.com/cpresser/veroval/blob/master/veroval.py) [2](https://github.com/ignisf/vdc_export/blob/master/vdc_export.rb)
* [Icon](https://www.iconarchive.com/show/medical-icons-by-dapino.html), [Symbols](https://pictogrammers.com/library/mdi), [Flags](https://github.com/lipis/flag-icons)

Thanks for this great software! :+1:
